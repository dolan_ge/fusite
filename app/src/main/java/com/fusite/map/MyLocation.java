package com.fusite.map;

import java.io.Serializable;

/**
 * Created by Jelly on 2016/3/30.
 */
public class MyLocation implements Serializable{
    private String adCode; //城市代码
    private String address; //详细地址
    private String latitude;
    private String longitude;
    private String city; //市
    private String province;//省
    private String district;//区

    public MyLocation(){}

    public MyLocation(String adCode, String latitude, String address, String longitude,String city,String province,String district) {
        this.adCode = adCode;
        this.latitude = latitude;
        this.address = address;
        this.longitude = longitude;
        this.city = city;
        this.province = province;
        this.district = district;
    }


    public String getAdCode() {
        return adCode;
    }

    public void setAdCode(String adCode) {
        this.adCode = adCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}

package com.fusite.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fusite.utils.DialogUtil;
import com.fusite.utils.BitmapUtil;
import com.fusite.utils.ErrorParamUtil;
import com.fusite.utils.PopupWindowUtil;
import com.fusite.utils.ResourceUtil;
import com.fusite.utils.SPCacheUtil;
import com.fusite.utils.SearchUtil;
import com.fusite.utils.ToastUtil;
import com.fusite.utils.WindowUtil;

/**
 * 抽象出工具
 * Created by Jelly on 2016/3/29.
 */
public abstract class UtilActivity extends SimpleActivity{

    /**
     * Dialog操作工具
     */
    public DialogUtil dialogUtil = DialogUtil.getInstance();
    /**
     * 圆形图像操作工具
     */
    public BitmapUtil bitmapUtil = BitmapUtil.getInstance();
    /**
     * 错误代码工具
     */
    public ErrorParamUtil errorParamUtil = ErrorParamUtil.getInstance();
    /**
     * Toast操作工具
     */
    public ToastUtil toastUtil = ToastUtil.getInstance();
    /**
     * PopupWindow操作工具
     */
    public PopupWindowUtil popupWindowUtil = PopupWindowUtil.getInstance();
    /**
     * Window操作工具
     */
    public WindowUtil windowUtil = WindowUtil.getInstance();
    /**
     * SharePreference缓存操作工具
     */
    public SPCacheUtil spCacheUtil = SPCacheUtil.getInstance();
    /**
     * 搜索工具
     */
    public SearchUtil searchUtil = SearchUtil.getInstance();

    public ResourceUtil resourceUtil = ResourceUtil.getInstance();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


}

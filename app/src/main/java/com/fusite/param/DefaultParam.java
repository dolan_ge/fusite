package com.fusite.param;

/**
 * 默认的参数
 * Created by Jelly on 2016/2/26.
 */
public class DefaultParam {
    /**
     * 默认的地图缩放比例
     */
    public static final float ZOOM = 13.5f;
    /**
     * 获取验证码的倒计时
     */
    public static final int COUNTDOWN = 60;
    /**
     * 支付密码位数
     */
    public static final int PAYPASSWOEDSUM = 6;
}

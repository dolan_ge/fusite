package com.fusite.param;

/**
 *
 * Created by Jelly on 2016/5/10.
 */
public class BeanParam {

    public static String Success = "Y";
    public static String Failure = "N";

    public static final class Login{
        public static String isVerified = "1"; //已经认证
        public static String noVerified = "0"; //未认证
    }

    public static final class ServiceStations{
        public static String IsMoving = "1"; //服务点
    }

    public static final class PileStations {
        public static final String OPEN = "1";//开放
        public static final String CLOSE = "0";//封闭
    }

    public static final class RentStations{
        public static final String OPEN = "1";
        public static final String CLOSE = "0";
    }

    public static final class Bill{
        public static final String AliReCharge = "1";//支付宝充值
        public static final String WReCharge = "2";//微信充值
        public static final String BalancePayCharge = "3";//余额支付充电订单
        public static final String AliPayCharge = "4";//支付宝支付充电订单
        public static final String WPayCharge = "5";//微信支付充电订单
        public static final String BalancePayAppoint = "6";//余额支付充电预约费用
        public static final String AliPayAppoint = "7";//支付宝支付充电预约费用
        public static final String WPayAppoint = "8";//微信支付充电预约费用
        public static final String BalancePayRentCar = "9";//余额支付租车费用
        public static final String AliPayRentCar = "10";//支付宝支付租车费用
        public static final String WPayRentCar = "11";//微信支付租车费用
        public static final String BalancePayRentBill = "12";//余额支付租车结算费用
        public static final String AliPayRentBill = "13";//支付宝支付租车结算费用
        public static final String WPayRentBill = "14";//微信支付租车结算费用
        public static final String RentBillBackBalance = "15";//租车结算退还余额
        public static final String CancelRentOrder = "16";//取消租车订单
        public static final String CardAliRecharge = "17";//充电卡支付宝充值
        public static final String CardWxRecharge = "18";//微信充值
        public static final String CardPay = "19";//充电卡消费
    }


    public static final class RentOrderInfo{
        public static final String BalancePay = "1";
        public static final String AliPay = "2";
        public static final String WPay = "3";
    }

    public static final class CollectList{
        public static final String EVC = "EVC";//充电点
        public static final String EVR = "EVR";//租车点
    }

    public static final class Dynamic{
        public static final String like = "0"; //待点赞
        public static final String onlike = "1";//已点赞
        public static final String commit = "0";//评论
        public static final String reply = "1";//回复
    }

    public static final class Verified{
        public static final String State0 = "0";//原始状态
        public static final String State1 = "1";//认证中
        public static final String State2 = "8";//认证通过
        public static final String State3 = "16";//认证取消
        public static final String State4 = "32";//认证失败
    }

    public static final class Invoice{
        public static final String EVC = "EVC"; //充电点
        public static final String EVR = "EVR"; //租车点
        public static final String EVM = "EVM"; //服务点
    }

    public static final class InvoiceHistory{
        public static final String Status0 = "0";//已申请
        public static final String Status1 = "1";//已审核
        public static final String Status2 = "2";//以邮寄
        public static final String Status3 = "3";//已取消
    }

    public static final class EVCOrderInfo{
        public static final String BalancePay = "1"; //余额支付
        public static final String AliPay = "2"; //支付宝支付
        public static final String WXPay = "3"; //微信支付
    }

    /**
     * 充电卡
     */
    public static final class PileCard{
        public static final String Normal = "8"; //正常
        public static final String Freeze = "16"; //冻结
        public static final String Logout = "32"; //注销
    }

    public static final class UpdateInfo{
        public static final String isCertification = "1";
        public static final String noCertification = "0";
        public static final String HasMessage = "1";
    }

    public static final class ServiceOrder{
        public static final String State1 = "1"; //预约中
        public static final String State2 = "2";//已确认
        public static final String State4 = "4";//服务中
        public static final String State8 = "8";//已完成
        public static final String BalancePay = "1"; //余额支付
        public static final String AliPay = "2"; //支付宝支付
        public static final String WXPay = "3"; //微信支付
    }


}

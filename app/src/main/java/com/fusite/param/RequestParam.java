package com.fusite.param;

/**
 * Created by Jelly on 2016/8/25.
 */
public class RequestParam {
    /**
     * 服务点列表
     */
    public static final class ServiceStations{
        public static String InterfaceName = "evm.stations.get";
        public static String Position = "Position";
        public static String PositionValue = "000000";
    }

    /**
     * 电桩列表
     */
    public static final class PileStations{
        public static String InterfaceName = "evc.stations.getall";
        public static String NearbyInterfaceName = "evc.stations.get";
        public static String Position = "Position";
        public static String PositionValue = "000000";
    }

    /**
     * 租车列表
     */
    public static class RentStations{
        public static String InterfaceName = "evr.stations.get";
        public static String Position = "Position";
        public static String PositionValue = "000000";
    }

    /**
     * 验证码
     */
    public static final class Code{
        public static String InterfaceName = "crm.validation";
        public static String Mobile = "Mobile";
        public static String CodeType = "CodeType";
        public static final String LOGINCODE = "1";
        public static final String REGCODE = "0";
        public static final String FORGETCODE = "3";
    }

    /**
     * 微信预下单
     */
    public static final class PrePay{
        public static String InterfaceName = "prePayOrder";
        public static String Token = "token";
        public static String PAY_TYPE = "payType"; // 0: 微信
        public static String ORDER_NO = "orderNo";
        public static String TYPE = "type";
        public static String BODY = "body";
        public static String DETAIL = "detail";
        public static String TOTAL_FEE = "total_fee";
    }

    /**
     * 服务列表
     */
    public static final class ServiceTypes{
        public static String InterfaceName = "evm.servicetypes.get";
        public static String Token = "Token";
    }

    /**
     * 筛选适合的服务点
     */
    public static final class filterServiceStation{
        public static String InterfaceName = "evm.station.getbypos";
        public static String ServiceId = "ServiceId";
        public static String Longitude = "Longitude";
        public static String Latitude = "Latitude";
    }

    /**
     * 登录
     */
    public static final class Login{
        public static String InterfaceName = "crm.loginnew";
        public static String Type = "Type";
        public static String PasswordLogin = "0";
        public static String CodeLogin = "1";
        public static String Mobile = "Mobile";
        public static String Password = "Password";
        public static String Code = "Code";
    }

    /**
     * 注册
     */
    public static final class RegUser{
        public static String InterfaceName = "crm.register";
        public static String Mobile = "Mobile";
        public static String Password = "Password";
        public static String Code = "Code";
    }

    /**
     * 校验验证码
     */
    public static final class CheckCode{
        public static String InterfaceName = "crm.chkcode";
        public static String Mobile = "Mobile";
        public static String Code = "Code";
        public static String Type = "Type";
        public static String Login = "3";
        public static String Pay = "2";
    }

    /**
     * 重设登录密码
     */
    public static final class ResetLoginPwd{
        public static String InterfaceName = "crm.pwd.reset";
        public static String Mobile = "Mobile";
        public static String Password = "Password";
        public static String Code = "Code";
    }

    /**
     * 注销
     */
    public static final class Logout{
        public static String InterfaceName = "crm.logout";
        public static String Token = "Token";
    }

    /**
     * 更改用户信息
     */
    public static final class UpdateUserInfo{
        public static String InterfaceName = "crm.customer.update";
        public static String Token = "Token";
        public static String CustName = "CustName";
        public static String Sex = "Sex";
        public static String Addr = "Addr";
        public static String Usericon = "Usericon";
        public static String Brand = "Brand";
        public static String Series = "Series";
    }

    /**
     * 获得余额
     */
    public static final class GetBalance{
        public static String InterfaceName = "crm.balanceamt.get";
        public static String Token = "Token";
    }

    /**
     * 设置服务类型
     */
    public static final class SetServiceOrder{
        public static String InterfaceName = "evm.order.set";
        public static String Token = "Token";
        public static String Tel = "Tel";
        public static String Model = "Model";
        public static String OrgId = "OrgId";
        public static String ServiceId = "ServiceId";
        public static String Area = "Area";
        public static String Adress = "Adress";
        public static String Notes = "Notes";
        public static String Picture = "Picture";
    }

    /**
     * 获得服务订单
     */
    public static final class GetServiceOrder{
        public static String InterfaceName = "evm.orders.get";
        public static String Token = "Token";
        public static String OrderType = "OrderType";
        public static String PLAN = "PLAN";
        public static String UNFINISHED = "UNFINISHED";
        public static String FINISHED = "FINISHED";
    }

    /**
     * 充电卡
     */
    public static final class GetChargeItemCard{
        public static String InterfaceName = "crm.accounts.get";
        public static String Token = "Token";
    }

    /**
     * 账单
     */
    public static final class GetBill{
        public static String InterfaceName = "crm.balancelog.get";
        public static String Token = "Token";
    }

    /**
     * 获得车辆状态
     */
    public static final class GetCarStatus{
        public static String InterfaceName = "evr.vehiclestate.get";
        public static String Token = "Token";
        public static String VehicleId = "VehicleId";
    }

    /**
     * 获取租车订单
     */
    public static final class GetRentOrders{
        public static String InterfaceName = "evr.orders.get";
        public static String Token = "Token";
        public static String OrderType = "OrderType";
        public static final String PendingOrder = "0";
        public static final String FinishOrder = "1";
    }

    /**
     * 取消服务预约
     */
    public static final class CancelServiceOrder{
        public static String InterfaceName = "evm.order.cancel";
        public static String Token = "Token";
        public static String OrderNo = "OrderNo";
    }

    /**
     * 获取电桩点图片
     */
    public static final class GetPileStationImage{
        public static String InterfaceName = "evc.orgattach.get";
        public static String OrgId = "OrgId";
    }

    /**
     * 获取租车点图片
     */
    public static final class GetRentStationImage{
        public static String InterfaceName = "evr.orgattach.get";
        public static String OrgId = "OrgId";
    }

    /**
     * 获取评论
     */
    public static final class GetCommit{
        public static String InterfaceName = "ev.comments.get";
        public static String OrgId = "OrgId";
    }

    /**
     * 发送评论
     */
    public static final class SendCommit{
        public static String InterfaceName = "ev.comment.set";
        public static String Token = "Token";
        public static String OrgClasses = "OrgClasses";
        public static String OrderNo = "OrderNo";
        public static String Comment = "Comment";
        public static String Score = "Score";
    }

    /**
     * 设置收藏
     */
    public static final class SetCollect{
        public static String InterfaceName = "ev.myfavourite.set";
        public static String Token = "Token";
        public static String OrgId = "OrgId";
    }

    /**
     * 获得收藏列表
     */
    public static final class GetCollect{
        public static String InterfaceName = "ev.myfavourites.get";
        public static String Token = "Token";
    }

    /**
     * 取消收藏
     */
    public static final class CancelCollect{
        public static String InterfaceName = "ev.myfavourite.cncl";
    }

    /**
     * 更新密码
     */
    public static final class UpdatePassword{
        public static String InterfaceName = "crm.pwd.upbyoldpwd";
        public static String Token = "Token";
        public static String Type = "Type";
        public static String Password = "Password";
        public static String Paypassword = "Paypassword";
        public static String OldPassword = "OldPassword";
        public static String NewPassword = "NewPassword";
    }

    /**
     * 获取动态
     */
    public static final class GetDynamic{
        public static String InterfaceName = "crm.forums.get";
        public static String Token = "Token";
        public static String ForumNo = "ForumNo";
    }

    /**
     * 发送动态
     */
    public static final class SendDynamic{
        public static String InterfaceName = "crm.forum.set";
        public static String Token = "Token";
        public static String Content = "Content";
        public static String Url = "URL";
    }

    /**
     * 点赞
     */
    public static final class Like{
        public static String InterfaceName = "crm.forum.like";
        public static String Token = "Token";
        public static String Type = "Type";
        public static String Like = "Like";
        public static String Cancel = "Cancel";
        public static String ForumNo = "ForumNo";
    }

    /**
     * 发送评论
     */
    public static final class DynamicCommit{
        public static String InterfaceName = "crm.forum.reply";
        public static String Token = "Token";
        public static String Content = "Content";
        public static String ForumNo = "ForumNo";
        public static String ToId = "ToId";
    }

    /**
     * 获取动态详情
     */
    public static final class OneDynamic{
        public static String InterfaceName = "crm.forum.get";
        public static String Token = "Token";
        public static String ForumNo = "ForumNo";
    }

    /**
     * 获得审核详情
     */
    public static final class GetVerified{
        public static String InterfaceName = "crm.cert.get";
        public static String Token = "Token";
    }

    /**
     * 更新用户详情
     */
    public static final class UpdateVerifiedInfo{
        public static String InterfaceName = "crm.cert.set";
        public static String Token = "Token";
        public static String DrivingLicence = "DrivingLicence";//驾照
        public static String IdentityPositive = "IdentityPositive"; //身份证正面
        public static String IdentityNegative = "IdentityNegative"; //身份证反面
        public static String Name = "Name";
        public static String IdentityNum = "IdentityNum";
    }

    /**
     * 获取可开票的列表
     */
    public static final class GetInvoicing{
        public static String InterfaceName = "ev.invoiceorder.get";
        public static String Token = "Token";
    }

    /**
     * 开票
     */
    public static final class InvoiceSet{
        public static String InterfaceName = "ev.invoice.set";
        public static String Token = "Token";
        public static String OrderNo = "OrderNo";
        public static String BillTo = "BillTo";
        public static String Recipient = "Recipient";
        public static String Tel = "Tel";
        public static String Area = "Area";
        public static String Address = "Address";
        public static String OrderType = "OrderType"; //发票类型（充电订单，租车订单）
        public static String InvoiceType = "InvoiceType"; //发票类型（普通发票，专用发票）
        public static String CommentInvoice = "0"; //普通发票
        public static String SpecialInvoice = "1"; //专用发票
        public static String Identifaction = "Identifaction";
        public static String BankName = "BankName";
        public static String AccountNo = "AccountNo";
        public static String RegisterAddr = "RegisterAddr";
        public static String CompanyTel = "CompanyTel";
        public static String BusinessLicense = "BusinessLicense";
        public static String OrgCode = "OrgCode";
        public static String TaxRegister = "TaxRegister";
    }

    /**
     * 开票历史
     */
    public static final class InvoiceHistory{
        public static String InterfaceName = "ev.invoice.get";
        public static String Token = "Token";
    }

    /**
     * 获得消息
     */
    public static final class GetNews{
        public static String InterfaceName = "crm.message.get";
        public static String Token = "Token";
    }

    /**
     * 获得版本更新信息
     */
    public static final class GetVersionUpdateInfo{
        public static String InterfaceName = "crm.androidapk.get";
        public static String Token = "Token";
    }

    /**
     * 发送建议
     */
    public static final class SendSuggest{
        public static String InterfaceName = "crm.suggest.set";
        public static String Token = "Token";
        public static String Suggestion = "Suggestion";
    }

    /**
     * 附近租车点
     */
    public static final class NearbyRentStation {
        public static String InterfaceName = "evr.stationset.get";
        public static String Position = "Position";
    }

    public static final class CardRecharge{
        public static String Token = "Token";
        public static String InterfaceName = "crm.accounts.rechg";
        public static String AccountNo = "AccountNo";
        public static String Type = "Type";
        public static String AliPay = "1";
        public static String WxPay = "2";
        public static String Amount = "Amount";
        public static String PayOrderNo = "PayOrderNo";
    }

    public static final class DeleteChargeCard{
        public static String Token = "Token";
        public static String InterfaceName = "crm.accounts.delete";
        public static String AccountId = "AccountId";
        public static String AccountPwd = "AccountPwd";
    }

    public static final class CardModPassword{
        public static String Token = "Token";
        public static String InterfaceName = "crm.account.pwdupd";
        public static String AccountNo = "AccountNo";
        public static String AccountPwd = "AccountPwd";
        public static String NewAccountPwd = "NewAccountPwd";
    }

    public static final class FreezeCard{
        public static String Token = "Token";
        public static String InterfaceName = "crm.account.freeze";
        public static String AccountNo = "AccountNo";
        public static String AccountPwd = "AccountPwd";
    }

    public static final class UpdateInfo{
        public static String Token = "Token";
        public static String InterfaceName = "crm.customer.refresh";
    }

    public static final class ServicePay{
        public static String Token = "Token";
        public static String InterfaceName = "evm.order.pay";
        public static String OrderNo = "OrderNo";
        public static String Type = "Type";
        public static String BalancePay = "1"; //余额支付
        public static String AliPay = "2"; //支付宝支付
        public static String WxPay = "3"; //微信支付
        public static String PayAmt = "PayAmt"; //支付金额
        public static String PayPassword = "PayPassword";
        public static String PayOrderNo = "PayOrderNo"; //第三方的支付订单
    }

    public static final class ServiceOrderDelete{
        public static String Token = "Token";
        public static String InterfaceName = "evm.order.delete";
        public static String OrderNo = "OrderNo";
    }

    public static final class ServiceOrderInfo{
        public static String Token = "Token";
        public static String InterfaceName = "evm.order.get";
        public static String OrderNo = "OrderNo";
    }

    public static final class Brands{
        public static String Token = "Token";
        public static String InterfaceName = "crm.brands.get";
    }

    public static final class ActivityInfo{
        public static String Token = "Token";
        public static String InterfaceName = "crm.activity.get";
    }

    public static final class WithdrawInfo {
        public static String Token = "Token";
        public static String InterfaceName = "crm.account.withdraw";
        public static String AccountNo = "AccountNo";
        public static String Type = "Type"; // 1、余额到充点卡，2、充电卡到余额
        public static String Amount = "Amount";
    }
}

package com.fusite.constant;

/**
 * Created by caolz on 2018/4/28.
 */

public class TypeDefined {
    /**
     *  直流电桩
     */
    public static final String FACILITY_TYPE_DC = "0";

    /**
     *  交流电桩
     */
    public static final String FACILITY_TYPE_AC = "1";
}

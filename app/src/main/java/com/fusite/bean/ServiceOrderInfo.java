package com.fusite.bean;

/**
 * 服务订单详情
 * Created by Jelly on 2016/5/16.
 */
public class ServiceOrderInfo {

    /**
     * interfaceName : evm_order_get
     * returnStatus : 100
     * isSuccess : Y
     * data : {"OrderNo":"O-20160516-01025","OrderStatus":"8","CustId":"86201604050000000728","PlanTime":"2016-05-16 11:15:46","OrgId":"JKY0001","OrgName":"科园维修店","Latitude":"22.534116","Longitude":"113.943546","ServiceAmt":"30.00","ServiceId":"S0000001","ServiceName":"汽车服务","Model":"23","Tel":"12334455","CreateDate":"2016-05-16 11:15:46","Deposit":"0.00","ServicePayType":"0","UpdateDate":"2016-07-26 15:45:11"}
     */
    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrderNo : O-20160516-01025
     * OrderStatus : 8
     * CustId : 86201604050000000728
     * PlanTime : 2016-05-16 11:15:46
     * OrgId : JKY0001
     * OrgName : 科园维修店
     * Latitude : 22.534116
     * Longitude : 113.943546
     * ServiceAmt : 30.00
     * ServiceId : S0000001
     * ServiceName : 汽车服务
     * Model : 23
     * Tel : 12334455
     * CreateDate : 2016-05-16 11:15:46
     * Deposit : 0.00
     * ServicePayType : 0
     * UpdateDate : 2016-07-26 15:45:11
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String OrderNo;
        private String OrderStatus;
        private String CustId;
        private String PlanTime;
        private String OrgId;
        private String OrgName;
        private String Latitude;
        private String Longitude;
        private String ServiceAmt;
        private String ServiceId;
        private String ServiceName;
        private String Model;
        private String Tel;
        private String CreateDate;
        private String Deposit;
        private String ServicePayType;
        private String UpdateDate;
        private String Addr;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getPlanTime() {
            return PlanTime;
        }

        public void setPlanTime(String PlanTime) {
            this.PlanTime = PlanTime;
        }

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getServiceAmt() {
            return ServiceAmt;
        }

        public void setServiceAmt(String ServiceAmt) {
            this.ServiceAmt = ServiceAmt;
        }

        public String getServiceId() {
            return ServiceId;
        }

        public void setServiceId(String ServiceId) {
            this.ServiceId = ServiceId;
        }

        public String getServiceName() {
            return ServiceName;
        }

        public void setServiceName(String ServiceName) {
            this.ServiceName = ServiceName;
        }

        public String getModel() {
            return Model;
        }

        public void setModel(String Model) {
            this.Model = Model;
        }

        public String getTel() {
            return Tel;
        }

        public void setTel(String Tel) {
            this.Tel = Tel;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }

        public String getDeposit() {
            return Deposit;
        }

        public void setDeposit(String Deposit) {
            this.Deposit = Deposit;
        }

        public String getServicePayType() {
            return ServicePayType;
        }

        public void setServicePayType(String ServicePayType) {
            this.ServicePayType = ServicePayType;
        }

        public String getUpdateDate() {
            return UpdateDate;
        }

        public void setUpdateDate(String UpdateDate) {
            this.UpdateDate = UpdateDate;
        }

        public String getAddr() {
            return Addr;
        }

        public void setAddr(String addr) {
            Addr = addr;
        }
    }
}

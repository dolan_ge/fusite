package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/5/16.
 */
public class Commit {

    /**
     * interfaceName : ev_comments_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"OrgId":"C00000001","CustId":"86201604050000000728","CreateDate":"2016-06-01 11:38:14","OrderNo":"O-20160524-01258","Comment":"充电服务很到位，环境干净优雅，很赞，66666","Score":"9.20","CustName":"毛寨主","Usericon":"http://www.genlex.com.cn/images/1464085261019.png"},{"OrgId":"C00000001","CustId":"86201604050000000728","CreateDate":"2016-06-02 11:38:14","OrderNo":"O-20160524-01258","Comment":"还可以吧，总体来说","Score":"8.70","CustName":"毛寨主","Usericon":"http://www.genlex.com.cn/images/1464085261019.png"},{"OrgId":"C00000001","CustId":"86201604050000000728","CreateDate":"2016-06-01 11:48:14","OrderNo":"O-20160524-01258","Comment":"生意太火爆，等太久了","Score":"8.50","CustName":"毛寨主","Usericon":"http://www.genlex.com.cn/images/1464085261019.png"},{"OrgId":"C00000001","CustId":"86201604050000000728","CreateDate":"2016-06-01 15:28:14","OrderNo":"O-20160524-01258","Comment":"充电服务很到位，环境干净优雅，很赞，66666","Score":"9.20","CustName":"毛寨主","Usericon":"http://www.genlex.com.cn/images/1464085261019.png"},{"OrgId":"C00000001","CustId":"86201604050000000728","CreateDate":"2016-06-01 15:08:14","OrderNo":"O-20160524-01258","Comment":"充电服务呢，这个我是要赞的，很贴心，很棒，我很喜欢，下次继续来这里充电，赞","CustName":"毛寨主","Usericon":"http://www.genlex.com.cn/images/1464085261019.png"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrgId : C00000001
     * CustId : 86201604050000000728
     * CreateDate : 2016-06-01 11:38:14
     * OrderNo : O-20160524-01258
     * Comment : 充电服务很到位，环境干净优雅，很赞，66666
     * Score : 9.20
     * CustName : 毛寨主
     * Usericon : http://www.genlex.com.cn/images/1464085261019.png
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        private String OrgId;
        private String CustId;
        private String CreateDate;
        private String OrderNo;
        private String Comment;
        private String Score;
        private String CustName;
        private String Usericon;

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getComment() {
            return Comment;
        }

        public void setComment(String Comment) {
            this.Comment = Comment;
        }

        public String getScore() {
            return Score;
        }

        public void setScore(String Score) {
            this.Score = Score;
        }

        public String getCustName() {
            return CustName;
        }

        public void setCustName(String CustName) {
            this.CustName = CustName;
        }

        public String getUsericon() {
            return Usericon;
        }

        public void setUsericon(String Usericon) {
            this.Usericon = Usericon;
        }
    }
}

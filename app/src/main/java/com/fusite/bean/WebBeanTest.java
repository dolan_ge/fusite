package com.fusite.bean;

/**
 * Created by Jelly on 2016/4/28.
 */
public class WebBeanTest {

    /**
     * interfaceName : crm_login
     * returnStatus : 100
     * isSuccess : Y
     * data : {"filename":"86201603290000000656"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * filename : 86201603290000000656
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String filename;

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }
    }
}

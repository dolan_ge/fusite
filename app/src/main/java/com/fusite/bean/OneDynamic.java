package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/6/15.
 */
public class OneDynamic {

    /**
     * interfaceName : crm_forum_get
     * returnStatus : 100
     * isSuccess : Y
     * data : {"ForumNo":"O-20160614-01448","CustId":"86201604060000000772","CustName":"马化腾","Usericon":"http://www.genlex.com.cn/images/1465891408076.png","Content":"刚刚闪退了","CreateDate":"2016-06-14 17:11:44","Liked":"0","Likes":"2","Replies":"4","ReplyContents":[{"Item":"1","Type":"0","From_Id":"86201604050000000728","From_Name":"毛寨主","To_Id":"","To_Name":"","Content":"haha，蔡工和翁工好帅","CreateDate":"2016-06-14 18:57:00"},{"Item":"2","Type":"1","From_Id":"86201604070000000784","From_Name":"Al菜菜","To_Id":"","To_Name":"","Content":"阿大声道","CreateDate":"2016-06-15 13:34:34"},{"Item":"3","Type":"0","From_Id":"86201604070000000784","From_Name":"Al菜菜","To_Id":"","To_Name":"","Content":"上路莫慌，打野爸爸来了","CreateDate":"2016-06-15 13:37:26"},{"Item":"4","Type":"0","From_Id":"86201604070000000784","From_Name":"Al菜菜","To_Id":"","To_Name":"","Content":"sb，你把中路送超神了！！！","CreateDate":"2016-06-15 13:46:35"}],"Pictures":[{"url":"http://www.genlex.com.cn/images/1465895486211.png"}]}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * ForumNo : O-20160614-01448
     * CustId : 86201604060000000772
     * CustName : 马化腾
     * Usericon : http://www.genlex.com.cn/images/1465891408076.png
     * Content : 刚刚闪退了
     * CreateDate : 2016-06-14 17:11:44
     * Liked : 0
     * Likes : 2
     * Replies : 4
     * ReplyContents : [{"Item":"1","Type":"0","From_Id":"86201604050000000728","From_Name":"毛寨主","To_Id":"","To_Name":"","Content":"haha，蔡工和翁工好帅","CreateDate":"2016-06-14 18:57:00"},{"Item":"2","Type":"1","From_Id":"86201604070000000784","From_Name":"Al菜菜","To_Id":"","To_Name":"","Content":"阿大声道","CreateDate":"2016-06-15 13:34:34"},{"Item":"3","Type":"0","From_Id":"86201604070000000784","From_Name":"Al菜菜","To_Id":"","To_Name":"","Content":"上路莫慌，打野爸爸来了","CreateDate":"2016-06-15 13:37:26"},{"Item":"4","Type":"0","From_Id":"86201604070000000784","From_Name":"Al菜菜","To_Id":"","To_Name":"","Content":"sb，你把中路送超神了！！！","CreateDate":"2016-06-15 13:46:35"}]
     * Pictures : [{"url":"http://www.genlex.com.cn/images/1465895486211.png"}]
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String ForumNo;
        private String CustId;
        private String CustName;
        private String Usericon;
        private String Content;
        private String CreateDate;
        private String Liked;
        private String Likes;
        private String Replies;
        /**
         * Item : 1
         * Type : 0
         * From_Id : 86201604050000000728
         * From_Name : 毛寨主
         * To_Id :
         * To_Name :
         * Content : haha，蔡工和翁工好帅
         * CreateDate : 2016-06-14 18:57:00
         */

        private List<ReplyContentsEntity> ReplyContents;
        /**
         * url : http://www.genlex.com.cn/images/1465895486211.png
         */

        private List<PicturesEntity> Pictures;

        public String getForumNo() {
            return ForumNo;
        }

        public void setForumNo(String ForumNo) {
            this.ForumNo = ForumNo;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getCustName() {
            return CustName;
        }

        public void setCustName(String CustName) {
            this.CustName = CustName;
        }

        public String getUsericon() {
            return Usericon;
        }

        public void setUsericon(String Usericon) {
            this.Usericon = Usericon;
        }

        public String getContent() {
            return Content;
        }

        public void setContent(String Content) {
            this.Content = Content;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }

        public String getLiked() {
            return Liked;
        }

        public void setLiked(String Liked) {
            this.Liked = Liked;
        }

        public String getLikes() {
            return Likes;
        }

        public void setLikes(String Likes) {
            this.Likes = Likes;
        }

        public String getReplies() {
            return Replies;
        }

        public void setReplies(String Replies) {
            this.Replies = Replies;
        }

        public List<ReplyContentsEntity> getReplyContents() {
            return ReplyContents;
        }

        public void setReplyContents(List<ReplyContentsEntity> ReplyContents) {
            this.ReplyContents = ReplyContents;
        }

        public List<PicturesEntity> getPictures() {
            return Pictures;
        }

        public void setPictures(List<PicturesEntity> Pictures) {
            this.Pictures = Pictures;
        }

        public static class ReplyContentsEntity {
            private String Item;
            private String Type;
            private String From_Id;
            private String From_Name;
            private String To_Id;
            private String To_Name;
            private String Content;
            private String CreateDate;

            public String getItem() {
                return Item;
            }

            public void setItem(String Item) {
                this.Item = Item;
            }

            public String getType() {
                return Type;
            }

            public void setType(String Type) {
                this.Type = Type;
            }

            public String getFrom_Id() {
                return From_Id;
            }

            public void setFrom_Id(String From_Id) {
                this.From_Id = From_Id;
            }

            public String getFrom_Name() {
                return From_Name;
            }

            public void setFrom_Name(String From_Name) {
                this.From_Name = From_Name;
            }

            public String getTo_Id() {
                return To_Id;
            }

            public void setTo_Id(String To_Id) {
                this.To_Id = To_Id;
            }

            public String getTo_Name() {
                return To_Name;
            }

            public void setTo_Name(String To_Name) {
                this.To_Name = To_Name;
            }

            public String getContent() {
                return Content;
            }

            public void setContent(String Content) {
                this.Content = Content;
            }

            public String getCreateDate() {
                return CreateDate;
            }

            public void setCreateDate(String CreateDate) {
                this.CreateDate = CreateDate;
            }
        }

        public static class PicturesEntity {
            private String url;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }
    }
}

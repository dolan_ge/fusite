package com.fusite.bean;

import java.util.List;

/**
 * 租车服务点
 * Created by Jelly on 2016/4/15.
 */
public class RentStations {

    /**
     * interfaceName : evr_stations_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"OrgId":"KJ0000001","OrgName":"科技园租车","City":"深圳","District":"南山区","CityCode":"440300","Longitude":"113.943264","Latitude":"22.533556","OrgStatus":"1","Addr":"深圳市南山区科技园中山大学"},{"OrgId":"SD0000001","OrgName":"深圳大学租车","City":"深圳","District":"南山区","CityCode":"440300","Longitude":"113.936463","Latitude":"22.540172","OrgStatus":"1","Addr":"深圳市深圳大学北门"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrgId : KJ0000001
     * OrgName : 科技园租车
     * City : 深圳
     * District : 南山区
     * CityCode : 440300
     * Longitude : 113.943264
     * Latitude : 22.533556
     * OrgStatus : 1
     * Addr : 深圳市南山区科技园中山大学
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity extends Search{
        private String OrgId;
        private String OrgName;
        private String City;
        private String District;
        private String CityCode;
        private String Longitude;
        private String Latitude;
        private String OrgStatus;
        private String Addr;

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String City) {
            this.City = City;
        }

        public String getDistrict() {
            return District;
        }

        public void setDistrict(String District) {
            this.District = District;
        }

        public String getCityCode() {
            return CityCode;
        }

        public void setCityCode(String CityCode) {
            this.CityCode = CityCode;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getOrgStatus() {
            return OrgStatus;
        }

        public void setOrgStatus(String OrgStatus) {
            this.OrgStatus = OrgStatus;
        }

        public String getAddr() {
            return Addr;
        }

        public void setAddr(String Addr) {
            this.Addr = Addr;
        }
    }
}

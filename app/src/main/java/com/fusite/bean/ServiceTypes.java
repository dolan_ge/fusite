package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/5/10.
 */
public class ServiceTypes {

    /**
     * interfaceName : evm_servicetypes_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"ServiceId":"S0000001","ServiceName":"汽车维修"},{"ServiceId":"S0000002","ServiceName":"汽车维护"},{"ServiceId":"S0000003","ServiceName":"汽车安装"},{"ServiceId":"S0000004","ServiceName":"救援服务"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * ServiceId : S0000001
     * ServiceName : 汽车维修
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        private String ServiceId;
        private String ServiceName;

        public String getServiceId() {
            return ServiceId;
        }

        public void setServiceId(String ServiceId) {
            this.ServiceId = ServiceId;
        }

        public String getServiceName() {
            return ServiceName;
        }

        public void setServiceName(String ServiceName) {
            this.ServiceName = ServiceName;
        }
    }
}

package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/26.
 */
public class Suggest {

    /**
     * interfaceName : crm_suggest_set
     * returnStatus : 100
     * isSuccess : Y
     * data : {"SuggestId":"6","CustId":"86201604070000000784"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * SuggestId : 6
     * CustId : 86201604070000000784
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String SuggestId;
        private String CustId;

        public String getSuggestId() {
            return SuggestId;
        }

        public void setSuggestId(String SuggestId) {
            this.SuggestId = SuggestId;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }
    }
}

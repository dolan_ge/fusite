package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/25.
 */
public class InvoiceSet {

    /**
     * interfaceName : ev_invoice_set
     * returnStatus : 100
     * isSuccess : Y
     * data : {"DocNo":"INV-20160625-037","InvoiceStatus":"1","InvcAmt":"8.35"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * DocNo : INV-20160625-037
     * InvoiceStatus : 1
     * InvcAmt : 8.35
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String DocNo;
        private String InvoiceStatus;
        private String InvcAmt;

        public String getDocNo() {
            return DocNo;
        }

        public void setDocNo(String DocNo) {
            this.DocNo = DocNo;
        }

        public String getInvoiceStatus() {
            return InvoiceStatus;
        }

        public void setInvoiceStatus(String InvoiceStatus) {
            this.InvoiceStatus = InvoiceStatus;
        }

        public String getInvcAmt() {
            return InvcAmt;
        }

        public void setInvcAmt(String InvcAmt) {
            this.InvcAmt = InvcAmt;
        }
    }
}

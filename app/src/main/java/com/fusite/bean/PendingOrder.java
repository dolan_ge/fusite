package com.fusite.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jelly on 2016/3/10.
 */
public class PendingOrder implements Parcelable {

    /**
     * returnStatus : 100
     * isSuccess : Y
     * OrderNo : O-20160422-00721
     * OrderStatus : 4
     * OrderType : EVCR
     * CompanyID : C01
     * CustID : 86201604070000000784
     * OrgID : C00000001
     * OrgName : 深大站
     * FacilityID : 0755300000000091
     * FacilityName : 91号桩
     * Longitude : 113.938000
     * Latitude : 22.539223
     * ChargingAmt : 24.50
     * ServiceAmt : 0.00
     * ChargingQty : 30.00
     * ChargingTime : 15
     * PlanCost : 4.50
     * PlanCostPayed : 1
     * PlanPayType : 1
     * PlanBeginDateTime : 2016-04-22 10:59:31
     * PlanEndDateTime : 2016-04-22 11:44:31
     * RealBeginDateTime : 2016-04-22 11:01:03
     * RealEndDateTime : 2016-04-22 11:01:04
     * Tel : 0755-82891087
     */

    private EvcOrdersGetEntity evc_orders_get;

    public EvcOrdersGetEntity getEvc_orders_get() {
        return evc_orders_get;
    }

    public void setEvc_orders_get(EvcOrdersGetEntity evc_orders_get) {
        this.evc_orders_get = evc_orders_get;
    }

    public static class EvcOrdersGetEntity implements Parcelable {
        private String returnStatus;
        private String isSuccess;
        private String OrderNo;
        private String OrderStatus;
        private String OrderType;
        private String CompanyID;
        private String CustID;
        private String OrgID;
        private String OrgName;
        private String FacilityID;
        private String GunID;
        private String FacilityName;
        private String Longitude;
        private String Latitude;
        private String ChargingAmt;
        private String ServiceAmt;
        private String ChargingQty;
        private String ChargingTime;
        private String PlanCost;
        private String PlanCostPayed;
        private String PlanPayType;
        private String PlanBeginDateTime;
        private String PlanEndDateTime;
        private String RealBeginDateTime;
        private String RealEndDateTime;
        private String Tel;
        private String OverTimeFee;
        private String BillUploaded;

        public String getOverTimeFee() {
            return OverTimeFee;
        }

        public void setOverTimeFee(String overTimeFee) {
            OverTimeFee = overTimeFee;
        }

        public String getBillUploaded() {
            return BillUploaded;
        }

        public void setBillUploaded(String billUploaded) {
            BillUploaded = billUploaded;
        }

        public String getReturnStatus() {
            return returnStatus;
        }

        public void setReturnStatus(String returnStatus) {
            this.returnStatus = returnStatus;
        }

        public String getIsSuccess() {
            return isSuccess;
        }

        public void setIsSuccess(String isSuccess) {
            this.isSuccess = isSuccess;
        }

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getOrderType() {
            return OrderType;
        }

        public void setOrderType(String OrderType) {
            this.OrderType = OrderType;
        }

        public String getCompanyID() {
            return CompanyID;
        }

        public void setCompanyID(String CompanyID) {
            this.CompanyID = CompanyID;
        }

        public String getCustID() {
            return CustID;
        }

        public void setCustID(String CustID) {
            this.CustID = CustID;
        }

        public String getOrgID() {
            return OrgID;
        }

        public void setOrgID(String OrgID) {
            this.OrgID = OrgID;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getFacilityID() {
            return FacilityID;
        }

        public void setFacilityID(String FacilityID) {
            this.FacilityID = FacilityID;
        }

        public String getGunID() {
            return GunID;
        }

        public void setGunID(String gunID) {
            this.GunID = GunID;
        }

        public String getFacilityName() {
            return FacilityName;
        }

        public void setFacilityName(String FacilityName) {
            this.FacilityName = FacilityName;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getChargingAmt() {
            return ChargingAmt;
        }

        public void setChargingAmt(String ChargingAmt) {
            this.ChargingAmt = ChargingAmt;
        }

        public String getServiceAmt() {
            return ServiceAmt;
        }

        public void setServiceAmt(String ServiceAmt) {
            this.ServiceAmt = ServiceAmt;
        }

        public String getChargingQty() {
            return ChargingQty;
        }

        public void setChargingQty(String ChargingQty) {
            this.ChargingQty = ChargingQty;
        }

        public String getChargingTime() {
            return ChargingTime;
        }

        public void setChargingTime(String ChargingTime) {
            this.ChargingTime = ChargingTime;
        }

        public String getPlanCost() {
            return PlanCost;
        }

        public void setPlanCost(String PlanCost) {
            this.PlanCost = PlanCost;
        }

        public String getPlanCostPayed() {
            return PlanCostPayed;
        }

        public void setPlanCostPayed(String PlanCostPayed) {
            this.PlanCostPayed = PlanCostPayed;
        }

        public String getPlanPayType() {
            return PlanPayType;
        }

        public void setPlanPayType(String PlanPayType) {
            this.PlanPayType = PlanPayType;
        }

        public String getPlanBeginDateTime() {
            return PlanBeginDateTime;
        }

        public void setPlanBeginDateTime(String PlanBeginDateTime) {
            this.PlanBeginDateTime = PlanBeginDateTime;
        }

        public String getPlanEndDateTime() {
            return PlanEndDateTime;
        }

        public void setPlanEndDateTime(String PlanEndDateTime) {
            this.PlanEndDateTime = PlanEndDateTime;
        }

        public String getRealBeginDateTime() {
            return RealBeginDateTime;
        }

        public void setRealBeginDateTime(String RealBeginDateTime) {
            this.RealBeginDateTime = RealBeginDateTime;
        }

        public String getRealEndDateTime() {
            return RealEndDateTime;
        }

        public void setRealEndDateTime(String RealEndDateTime) {
            this.RealEndDateTime = RealEndDateTime;
        }

        public String getTel() {
            return Tel;
        }

        public void setTel(String Tel) {
            this.Tel = Tel;
        }

        public EvcOrdersGetEntity() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.returnStatus);
            dest.writeString(this.isSuccess);
            dest.writeString(this.OrderNo);
            dest.writeString(this.OrderStatus);
            dest.writeString(this.OrderType);
            dest.writeString(this.CompanyID);
            dest.writeString(this.CustID);
            dest.writeString(this.OrgID);
            dest.writeString(this.OrgName);
            dest.writeString(this.FacilityID);
            dest.writeString(this.GunID);
            dest.writeString(this.FacilityName);
            dest.writeString(this.Longitude);
            dest.writeString(this.Latitude);
            dest.writeString(this.ChargingAmt);
            dest.writeString(this.ServiceAmt);
            dest.writeString(this.ChargingQty);
            dest.writeString(this.ChargingTime);
            dest.writeString(this.PlanCost);
            dest.writeString(this.PlanCostPayed);
            dest.writeString(this.PlanPayType);
            dest.writeString(this.PlanBeginDateTime);
            dest.writeString(this.PlanEndDateTime);
            dest.writeString(this.RealBeginDateTime);
            dest.writeString(this.RealEndDateTime);
            dest.writeString(this.Tel);
            dest.writeString(this.OverTimeFee);
            dest.writeString(this.BillUploaded);
        }

        protected EvcOrdersGetEntity(Parcel in) {
            this.returnStatus = in.readString();
            this.isSuccess = in.readString();
            this.OrderNo = in.readString();
            this.OrderStatus = in.readString();
            this.OrderType = in.readString();
            this.CompanyID = in.readString();
            this.CustID = in.readString();
            this.OrgID = in.readString();
            this.OrgName = in.readString();
            this.FacilityID = in.readString();
            this.GunID = in.readString();
            this.FacilityName = in.readString();
            this.Longitude = in.readString();
            this.Latitude = in.readString();
            this.ChargingAmt = in.readString();
            this.ServiceAmt = in.readString();
            this.ChargingQty = in.readString();
            this.ChargingTime = in.readString();
            this.PlanCost = in.readString();
            this.PlanCostPayed = in.readString();
            this.PlanPayType = in.readString();
            this.PlanBeginDateTime = in.readString();
            this.PlanEndDateTime = in.readString();
            this.RealBeginDateTime = in.readString();
            this.RealEndDateTime = in.readString();
            this.Tel = in.readString();
            this.OverTimeFee = in.readString();
            this.BillUploaded = in.readString();
        }

        public static final Creator<EvcOrdersGetEntity> CREATOR = new Creator<EvcOrdersGetEntity>() {
            @Override
            public EvcOrdersGetEntity createFromParcel(Parcel source) {
                return new EvcOrdersGetEntity(source);
            }

            @Override
            public EvcOrdersGetEntity[] newArray(int size) {
                return new EvcOrdersGetEntity[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.evc_orders_get, flags);
    }

    public PendingOrder() {
    }

    protected PendingOrder(Parcel in) {
        this.evc_orders_get = in.readParcelable(EvcOrdersGetEntity.class.getClassLoader());
    }

    public static final Parcelable.Creator<PendingOrder> CREATOR = new Parcelable.Creator<PendingOrder>() {
        @Override
        public PendingOrder createFromParcel(Parcel source) {
            return new PendingOrder(source);
        }

        @Override
        public PendingOrder[] newArray(int size) {
            return new PendingOrder[size];
        }
    };
}

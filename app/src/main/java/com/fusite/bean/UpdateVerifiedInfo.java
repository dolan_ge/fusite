package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/16.
 */
public class UpdateVerifiedInfo {

    /**
     * interfaceName : crm_cert_set
     * returnStatus : 100
     * isSuccess : Y
     * data : {"CustId":"86201604070000000784","AuditStatus":"1"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * CustId : 86201604070000000784
     * AuditStatus : 1
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String CustId;
        private String AuditStatus;

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getAuditStatus() {
            return AuditStatus;
        }

        public void setAuditStatus(String AuditStatus) {
            this.AuditStatus = AuditStatus;
        }
    }
}

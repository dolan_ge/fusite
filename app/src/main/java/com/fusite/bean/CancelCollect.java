package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/6.
 */
public class CancelCollect {
    /**
     * interfaceName : ev_myfavourite_cncl
     * returnStatus : 100
     * isSuccess : Y
     * data : {"OrgId":"C00000001","CustId":"86201604070000000784"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrgId : C00000001
     * CustId : 86201604070000000784
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String OrgId;
        private String CustId;

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }
    }
}

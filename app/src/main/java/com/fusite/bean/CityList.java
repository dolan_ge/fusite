package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/4/19.
 */
public class CityList {

    /**
     * interfaceName : evr_citylist_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"CityCode":"440100","Area":"广州"},{"CityCode":"440300","Area":"深圳"},{"CityCode":"120000","Area":"天津"},{"CityCode":"310000","Area":"上海"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * CityCode : 440100
     * Area : 广州
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        private String CityCode;
        private String Area;

        public String getCityCode() {
            return CityCode;
        }

        public void setCityCode(String CityCode) {
            this.CityCode = CityCode;
        }

        public String getArea() {
            return Area;
        }

        public void setArea(String Area) {
            this.Area = Area;
        }

        @Override
        public String toString() {
            return "DataEntity{" +
                    "CityCode='" + CityCode + '\'' +
                    ", Area='" + Area + '\'' +
                    '}';
        }
    }
}

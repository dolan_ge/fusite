package com.fusite.bean;

/**
 * Created by Jelly on 2016/7/22.
 */
public class FreezeCard {

    /**
     * interfaceName : crm_account_freeze
     * returnStatus : 100
     * isSuccess : Y
     * data : {"CustID":"86201604050000000728","AccountNo":"84807555001000000991","AccountStatus":"16"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * CustID : 86201604050000000728
     * AccountNo : 84807555001000000991
     * AccountStatus : 16
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String CustID;
        private String AccountNo;
        private String AccountStatus;

        public String getCustID() {
            return CustID;
        }

        public void setCustID(String CustID) {
            this.CustID = CustID;
        }

        public String getAccountNo() {
            return AccountNo;
        }

        public void setAccountNo(String AccountNo) {
            this.AccountNo = AccountNo;
        }

        public String getAccountStatus() {
            return AccountStatus;
        }

        public void setAccountStatus(String AccountStatus) {
            this.AccountStatus = AccountStatus;
        }
    }
}

package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/7.
 */
public class UpdatePassword {

    /**
     * interfaceName : crm_pwd_upbyoldpwd
     * returnStatus : 100
     * isSuccess : Y
     * data : {"CustId":"86201604070000000784"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * CustId : 86201604070000000784
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String CustId;

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }
    }
}

package com.fusite.bean;

/**
 * 充电图片
 * Created by Jelly on 2016/5/30.
 */
public class StationImage {

    /**
     * interfaceName : evc_orgattach_get
     * returnStatus : 100
     * isSuccess : Y
     * data : {"OrgID":"C00000001","FileUrl1":"http://www.genlex.com.cn/image/img/2016/05/30/1464590141532/204656-6_o.jpg","FileUrl2":"http://www.genlex.com.cn/image/img/2016/05/30/1464590151126/12282342_12282342_1341853767671_mthumb.jpg","FileUrl3":"http://www.genlex.com.cn/image/img/2016/05/30/1464590159338/16294637.jpg","FileUrl5":"http://www.genlex.com.cn/image/img/2016/05/30/1464590187268/12282342_12282342_1341853767671_mthumb.jpg","FileUrl6":"http://www.genlex.com.cn/image/img/2016/05/30/1464590168735/scen4.jpg"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrgID : C00000001
     * FileUrl1 : http://www.genlex.com.cn/image/img/2016/05/30/1464590141532/204656-6_o.jpg
     * FileUrl2 : http://www.genlex.com.cn/image/img/2016/05/30/1464590151126/12282342_12282342_1341853767671_mthumb.jpg
     * FileUrl3 : http://www.genlex.com.cn/image/img/2016/05/30/1464590159338/16294637.jpg
     * FileUrl5 : http://www.genlex.com.cn/image/img/2016/05/30/1464590187268/12282342_12282342_1341853767671_mthumb.jpg
     * FileUrl6 : http://www.genlex.com.cn/image/img/2016/05/30/1464590168735/scen4.jpg
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String OrgID;
        private String FileUrl1;
        private String FileUrl2;
        private String FileUrl3;
        private String FileUrl4;
        private String FileUrl5;
        private String FileUrl6;
        private String FileUrl7;
        private String FileUrl8;
        private String FileUrl9;
        private String FileUrl10;

        public String getOrgID() {
            return OrgID;
        }

        public void setOrgID(String OrgID) {
            this.OrgID = OrgID;
        }

        public String getFileUrl1() {
            return FileUrl1;
        }

        public void setFileUrl1(String FileUrl1) {
            this.FileUrl1 = FileUrl1;
        }

        public String getFileUrl2() {
            return FileUrl2;
        }

        public void setFileUrl2(String FileUrl2) {
            this.FileUrl2 = FileUrl2;
        }

        public String getFileUrl3() {
            return FileUrl3;
        }

        public void setFileUrl3(String FileUrl3) {
            this.FileUrl3 = FileUrl3;
        }

        public String getFileUrl4() {
            return FileUrl4;
        }

        public void setFileUrl4(String fileUrl4) {
            FileUrl4 = fileUrl4;
        }

        public String getFileUrl5() {
            return FileUrl5;
        }

        public void setFileUrl5(String FileUrl5) {
            this.FileUrl5 = FileUrl5;
        }

        public String getFileUrl6() {
            return FileUrl6;
        }

        public void setFileUrl6(String FileUrl6) {
            this.FileUrl6 = FileUrl6;
        }

        public String getFileUrl7() {
            return FileUrl7;
        }

        public void setFileUrl7(String fileUrl7) {
            FileUrl7 = fileUrl7;
        }

        public String getFileUrl8() {
            return FileUrl8;
        }

        public void setFileUrl8(String fileUrl8) {
            FileUrl8 = fileUrl8;
        }

        public String getFileUrl9() {
            return FileUrl9;
        }

        public void setFileUrl9(String fileUrl9) {
            FileUrl9 = fileUrl9;
        }

        public String getFileUrl10() {
            return FileUrl10;
        }

        public void setFileUrl10(String fileUrl10) {
            FileUrl10 = fileUrl10;
        }
    }
}

package com.fusite.bean;

import java.util.List;

/**
 * 服务点集合
 * Created by Jelly on 2016/5/5.
 */
public class ServiceStations {
    /**
     * interfaceName : evm_stations_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"OrgId":"JKY0001","OrgName":"科园维修店","OrgStatus":"1","Longitude":"113.943546","Latitude":"22.534116","CityCode":"440300","State":"广东","City":"深圳市","District":"南山区","IsMoving":"0"},{"OrgId":"JKY0002","OrgName":"深大维护店","OrgStatus":"1","Longitude":"113.942936","Latitude":"22.537090","CityCode":"440300","State":"广东","City":"深圳市","District":"南山区","IsMoving":"0"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrgId : JKY0001
     * OrgName : 科园维修店
     * OrgStatus : 1
     * Longitude : 113.943546
     * Latitude : 22.534116
     * CityCode : 440300
     * State : 广东
     * City : 深圳市
     * District : 南山区
     * IsMoving : 0
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        private String OrgId;
        private String OrgName;
        private String OrgStatus;
        private String Longitude;
        private String Latitude;
        private String CityCode;
        private String State;
        private String City;
        private String District;
        private String IsMoving;

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getOrgStatus() {
            return OrgStatus;
        }

        public void setOrgStatus(String OrgStatus) {
            this.OrgStatus = OrgStatus;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getCityCode() {
            return CityCode;
        }

        public void setCityCode(String CityCode) {
            this.CityCode = CityCode;
        }

        public String getState() {
            return State;
        }

        public void setState(String State) {
            this.State = State;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String City) {
            this.City = City;
        }

        public String getDistrict() {
            return District;
        }

        public void setDistrict(String District) {
            this.District = District;
        }

        public String getIsMoving() {
            return IsMoving;
        }

        public void setIsMoving(String IsMoving) {
            this.IsMoving = IsMoving;
        }
    }
}

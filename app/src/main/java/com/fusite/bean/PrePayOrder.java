package com.fusite.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by caolz on 2018/5/8.
 */

public class PrePayOrder implements Parcelable {
    /**
     * interfaceName : prePayOrder
     * returnStatus : 100
     * isSuccess : Y
     * data : {"extData":"1","timeStamp":"20180508151958","prepay_id":"wx0815200199075041e44704ef1691321797","packageValue":"Sign=WXPay","partnerId":"1503166781","appid":"wx871c093d359f0df9","nonce_str":"aB1NZ055EO6eKbVC"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    private DataBean data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * extData : 1
         * timeStamp : 20180508151958
         * prepay_id : wx0815200199075041e44704ef1691321797
         * packageValue : Sign=WXPay
         * partnerId : 1503166781
         * appid : wx871c093d359f0df9
         * nonce_str : aB1NZ055EO6eKbVC
         */

        private String extData;
        private String timeStamp;
        private String prepay_id;
        private String packageValue;
        private String partnerId;
        private String appid;
        private String nonce_str;

        public String getExtData() {
            return extData;
        }

        public void setExtData(String extData) {
            this.extData = extData;
        }

        public String getTimeStamp() {
            return timeStamp;
        }

        public void setTimeStamp(String timeStamp) {
            this.timeStamp = timeStamp;
        }

        public String getPrepay_id() {
            return prepay_id;
        }

        public void setPrepay_id(String prepay_id) {
            this.prepay_id = prepay_id;
        }

        public String getPackageValue() {
            return packageValue;
        }

        public void setPackageValue(String packageValue) {
            this.packageValue = packageValue;
        }

        public String getPartnerId() {
            return partnerId;
        }

        public void setPartnerId(String partnerId) {
            this.partnerId = partnerId;
        }

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getNonce_str() {
            return nonce_str;
        }

        public void setNonce_str(String nonce_str) {
            this.nonce_str = nonce_str;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.extData);
            dest.writeString(this.timeStamp);
            dest.writeString(this.prepay_id);
            dest.writeString(this.packageValue);
            dest.writeString(this.partnerId);
            dest.writeString(this.appid);
            dest.writeString(this.nonce_str);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.extData = in.readString();
            this.timeStamp = in.readString();
            this.prepay_id = in.readString();
            this.packageValue = in.readString();
            this.partnerId = in.readString();
            this.appid = in.readString();
            this.nonce_str = in.readString();
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.interfaceName);
        dest.writeString(this.returnStatus);
        dest.writeString(this.isSuccess);
        dest.writeParcelable(this.data, flags);
    }

    public PrePayOrder() {
    }

    protected PrePayOrder(Parcel in) {
        this.interfaceName = in.readString();
        this.returnStatus = in.readString();
        this.isSuccess = in.readString();
        this.data = in.readParcelable(DataBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<PrePayOrder> CREATOR = new Parcelable.Creator<PrePayOrder>() {
        @Override
        public PrePayOrder createFromParcel(Parcel source) {
            return new PrePayOrder(source);
        }

        @Override
        public PrePayOrder[] newArray(int size) {
            return new PrePayOrder[size];
        }
    };
}

package com.fusite.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Jelly on 2016/4/18.
 */
public class CarModels {

    /**
     * interfaceName : evr_models_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"ModelId":"M00000003","OrgId":"KJ0000001","Picture":"http://car3.autoimg.cn/cardfs/product/g14/M0A/52/8A/u_autohomecar__wKgH1VcNmMGAaeVwAAGaSgQTHZw749.jpg","Brand":"特斯拉","Series":"Model S","Counts":"5","Struct":"3厢","availableNum":"1","EnduranceMileage":"320","Price":"130.00"},{"ModelId":"M00000004","OrgId":"KJ0000001","Picture":"http://g.hiphotos.baidu.com/news/w%3D638/sign=fb44eabe972397ddd6799b076183b216/8435e5dde71190efe8ba4186c81b9d16fdfa6022.jpg","Brand":"特斯拉","Series":"Model 3","Counts":"5","Struct":"3厢","availableNum":"2","EnduranceMileage":"320","Price":"130.00"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * ModelId : M00000003
     * OrgId : KJ0000001
     * Picture : http://car3.autoimg.cn/cardfs/product/g14/M0A/52/8A/u_autohomecar__wKgH1VcNmMGAaeVwAAGaSgQTHZw749.jpg
     * Brand : 特斯拉
     * Series : Model S
     * Counts : 5
     * Struct : 3厢
     * availableNum : 1
     * EnduranceMileage : 320
     * Price : 130.00
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity implements Parcelable {
        private String ModelId;
        private String OrgId;
        private String Picture;
        private String Brand;
        private String Series;
        private String Counts;
        private String Struct;
        private String availableNum;
        private String EnduranceMileage;
        private String Price;

        public String getModelId() {
            return ModelId;
        }

        public void setModelId(String ModelId) {
            this.ModelId = ModelId;
        }

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getPicture() {
            return Picture;
        }

        public void setPicture(String Picture) {
            this.Picture = Picture;
        }

        public String getBrand() {
            return Brand;
        }

        public void setBrand(String Brand) {
            this.Brand = Brand;
        }

        public String getSeries() {
            return Series;
        }

        public void setSeries(String Series) {
            this.Series = Series;
        }

        public String getCounts() {
            return Counts;
        }

        public void setCounts(String Counts) {
            this.Counts = Counts;
        }

        public String getStruct() {
            return Struct;
        }

        public void setStruct(String Struct) {
            this.Struct = Struct;
        }

        public String getAvailableNum() {
            return availableNum;
        }

        public void setAvailableNum(String availableNum) {
            this.availableNum = availableNum;
        }

        public String getEnduranceMileage() {
            return EnduranceMileage;
        }

        public void setEnduranceMileage(String EnduranceMileage) {
            this.EnduranceMileage = EnduranceMileage;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.ModelId);
            dest.writeString(this.OrgId);
            dest.writeString(this.Picture);
            dest.writeString(this.Brand);
            dest.writeString(this.Series);
            dest.writeString(this.Counts);
            dest.writeString(this.Struct);
            dest.writeString(this.availableNum);
            dest.writeString(this.EnduranceMileage);
            dest.writeString(this.Price);
        }

        public DataEntity() {
        }

        protected DataEntity(Parcel in) {
            this.ModelId = in.readString();
            this.OrgId = in.readString();
            this.Picture = in.readString();
            this.Brand = in.readString();
            this.Series = in.readString();
            this.Counts = in.readString();
            this.Struct = in.readString();
            this.availableNum = in.readString();
            this.EnduranceMileage = in.readString();
            this.Price = in.readString();
        }

        public static final Parcelable.Creator<DataEntity> CREATOR = new Parcelable.Creator<DataEntity>() {
            @Override
            public DataEntity createFromParcel(Parcel source) {
                return new DataEntity(source);
            }

            @Override
            public DataEntity[] newArray(int size) {
                return new DataEntity[size];
            }
        };
    }
}

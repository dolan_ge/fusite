package com.fusite.bean;

/**
 * Created by Jelly on 2016/5/19.
 */
public class Photo {
    public String img;

    public Photo(String img){
        this.img = img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImg() {
        return img;
    }
}

package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/6/3.
 */
public class CollectList{

    /**
     * interfaceName : ev_myfavourites_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"CustId":"86201604070000000784","OrgId":"C00000001","OrgName":"深大站","Addr":"深圳市南山区科园路南山科技园","OrgClasses":"EVC"},{"CustId":"86201604070000000784","OrgId":"SZ013","OrgName":"九方购物中心","Addr":"龙华民治街道人民南路九方购物广场","OrgClasses":"EVC"},{"CustId":"86201604070000000784","OrgId":"SZ017","OrgName":"科兴科学园","Addr":"南山区科苑路15号科兴科学园","OrgClasses":"EVC"},{"CustId":"86201604070000000784","OrgId":"SZ018","OrgName":"十五峯花园","Addr":"南山区陶源龙珠六路十五峯花园","OrgClasses":"EVC"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * CustId : 86201604070000000784
     * OrgId : C00000001
     * OrgName : 深大站
     * Addr : 深圳市南山区科园路南山科技园
     * OrgClasses : EVC
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        private String CustId;
        private String OrgId;
        private String OrgName;
        private String Addr;
        private String OrgClasses;

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getAddr() {
            return Addr;
        }

        public void setAddr(String Addr) {
            this.Addr = Addr;
        }

        public String getOrgClasses() {
            return OrgClasses;
        }

        public void setOrgClasses(String OrgClasses) {
            this.OrgClasses = OrgClasses;
        }
    }
}

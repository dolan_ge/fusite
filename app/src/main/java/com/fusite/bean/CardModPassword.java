package com.fusite.bean;

/**
 * Created by Jelly on 2016/7/22.
 */
public class CardModPassword {

    /**
     * interfaceName : crm_account_pwdupd
     * returnStatus : 100
     * isSuccess : Y
     * data : {"AccountNo":"84807555001000000991","CustId":"86201604050000000728"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * AccountNo : 84807555001000000991
     * CustId : 86201604050000000728
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String AccountNo;
        private String CustId;

        public String getAccountNo() {
            return AccountNo;
        }

        public void setAccountNo(String AccountNo) {
            this.AccountNo = AccountNo;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }
    }
}

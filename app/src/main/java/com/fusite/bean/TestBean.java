package com.fusite.bean;

/**
 *
 * Created by Jelly on 2016/6/14.
 */
public class TestBean {
    public String imgUrl;
    public String title;
    public String content;
    public String intoUrl;

    public TestBean() {
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIntoUrl() {
        return intoUrl;
    }

    public void setIntoUrl(String intoUrl) {
        this.intoUrl = intoUrl;
    }
}

package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/6/26.
 */
public class News {

    /**
     * interfaceName : crm_message_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"MessageID":"25062016185606540","Title":"23123","Content":"345353DFDSF","MsgType":"系统通知","CustID":"86201604050000000718","Usericon":"http://www.genlex.com.cn/images/1464334739582.png","CustName":"玛帮新能源"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * MessageID : 25062016185606540
     * Title : 23123
     * Content : 345353DFDSF
     * MsgType : 系统通知
     * CustID : 86201604050000000718
     * Usericon : http://www.genlex.com.cn/images/1464334739582.png
     * CustName : 玛帮新能源
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity extends DateSort{
        private String MessageID;
        private String Title;
        private String Content;
        private String MsgType;
        private String CustID;
        private String Usericon;
        private String CustName;
        private String CreateDate;

        public String getMessageID() {
            return MessageID;
        }

        public void setMessageID(String MessageID) {
            this.MessageID = MessageID;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getContent() {
            return Content;
        }

        public void setContent(String Content) {
            this.Content = Content;
        }

        public String getMsgType() {
            return MsgType;
        }

        public void setMsgType(String MsgType) {
            this.MsgType = MsgType;
        }

        public String getCustID() {
            return CustID;
        }

        public void setCustID(String CustID) {
            this.CustID = CustID;
        }

        public String getUsericon() {
            return Usericon;
        }

        public void setUsericon(String Usericon) {
            this.Usericon = Usericon;
        }

        public String getCustName() {
            return CustName;
        }

        public void setCustName(String CustName) {
            this.CustName = CustName;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String createDate) {
            CreateDate = createDate;
        }

    }
}

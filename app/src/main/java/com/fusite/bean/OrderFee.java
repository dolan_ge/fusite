package com.fusite.bean;

/**
 * Created by Jelly on 2016/4/25.
 */
public class OrderFee {

    /**
     * interfaceName : evr_orderfee_get
     * returnStatus : 100
     * isSuccess : Y
     * data : {"PreRentAmt":"1820.00","ServiceAmt":"2.00","Deposit":"300.00","InsurenceAmt":"30.00"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * PreRentAmt : 1820.00
     * ServiceAmt : 2.00
     * Deposit : 300.00
     * InsurenceAmt : 30.00
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String PreRentAmt;
        private String ServiceAmt;
        private String Deposit;
        private String InsurenceAmt;

        public String getPreRentAmt() {
            return PreRentAmt;
        }

        public void setPreRentAmt(String PreRentAmt) {
            this.PreRentAmt = PreRentAmt;
        }

        public String getServiceAmt() {
            return ServiceAmt;
        }

        public void setServiceAmt(String ServiceAmt) {
            this.ServiceAmt = ServiceAmt;
        }

        public String getDeposit() {
            return Deposit;
        }

        public void setDeposit(String Deposit) {
            this.Deposit = Deposit;
        }

        public String getInsurenceAmt() {
            return InsurenceAmt;
        }

        public void setInsurenceAmt(String InsurenceAmt) {
            this.InsurenceAmt = InsurenceAmt;
        }
    }
}

package com.fusite.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jelly on 2016/4/21.
 */
public class RentOrder {

    /**
     * interfaceName : evr_order_set
     * returnStatus : 100
     * isSuccess : Y
     * data : {"OrderNo":"O-20160421-00693","OrderStatus":"0","CustId":"86201604070000000784","RentBeginTime":"2016/04/21 16:00:19","RentEndTime":"2016/04/28 16:00:19","OrgId":"KJ0000001","OrgName":"科技园租车","ReturnOrgId":"KJ0000001","ReturnOrgName":"科技园租车","Longitude":"113.943264","Latitude":"22.533556","ServiceAmt":"2.00","PreRentAmt":"3360.00","Deposit":"700.00","InsurenceAmt":"30.00","VehicleId":"V00000006","LicensePlate":"粤B83A84","ModelId":"M00000006","Brand":"特斯拉","Series":"Model 5","Struct":"3厢","Counts":"5","CreateDate":"2016-04-21 16:04:20"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrderNo : O-20160421-00693
     * OrderStatus : 0
     * CustId : 86201604070000000784
     * RentBeginTime : 2016/04/21 16:00:19
     * RentEndTime : 2016/04/28 16:00:19
     * OrgId : KJ0000001
     * OrgName : 科技园租车
     * ReturnOrgId : KJ0000001
     * ReturnOrgName : 科技园租车
     * Longitude : 113.943264
     * Latitude : 22.533556
     * ServiceAmt : 2.00
     * PreRentAmt : 3360.00
     * Deposit : 700.00
     * InsurenceAmt : 30.00
     * VehicleId : V00000006
     * LicensePlate : 粤B83A84
     * ModelId : M00000006
     * Brand : 特斯拉
     * Series : Model 5
     * Struct : 3厢
     * Counts : 5
     * CreateDate : 2016-04-21 16:04:20
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity implements Parcelable {
        private String OrderNo;
        private String OrderStatus;
        private String CustId;
        private String RentBeginTime;
        private String RentEndTime;
        private String OrgId;
        private String OrgName;
        private String ReturnOrgId;
        private String ReturnOrgName;
        private String Longitude;
        private String Latitude;
        private String ServiceAmt;
        private String PreRentAmt;
        private String Deposit;
        private String InsurenceAmt;
        private String VehicleId;
        private String LicensePlate;
        private String ModelId;
        private String Brand;
        private String Series;
        private String Struct;
        private String Counts;
        private String CreateDate;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getRentBeginTime() {
            return RentBeginTime;
        }

        public void setRentBeginTime(String RentBeginTime) {
            this.RentBeginTime = RentBeginTime;
        }

        public String getRentEndTime() {
            return RentEndTime;
        }

        public void setRentEndTime(String RentEndTime) {
            this.RentEndTime = RentEndTime;
        }

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getReturnOrgId() {
            return ReturnOrgId;
        }

        public void setReturnOrgId(String ReturnOrgId) {
            this.ReturnOrgId = ReturnOrgId;
        }

        public String getReturnOrgName() {
            return ReturnOrgName;
        }

        public void setReturnOrgName(String ReturnOrgName) {
            this.ReturnOrgName = ReturnOrgName;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getServiceAmt() {
            return ServiceAmt;
        }

        public void setServiceAmt(String ServiceAmt) {
            this.ServiceAmt = ServiceAmt;
        }

        public String getPreRentAmt() {
            return PreRentAmt;
        }

        public void setPreRentAmt(String PreRentAmt) {
            this.PreRentAmt = PreRentAmt;
        }

        public String getDeposit() {
            return Deposit;
        }

        public void setDeposit(String Deposit) {
            this.Deposit = Deposit;
        }

        public String getInsurenceAmt() {
            return InsurenceAmt;
        }

        public void setInsurenceAmt(String InsurenceAmt) {
            this.InsurenceAmt = InsurenceAmt;
        }

        public String getVehicleId() {
            return VehicleId;
        }

        public void setVehicleId(String VehicleId) {
            this.VehicleId = VehicleId;
        }

        public String getLicensePlate() {
            return LicensePlate;
        }

        public void setLicensePlate(String LicensePlate) {
            this.LicensePlate = LicensePlate;
        }

        public String getModelId() {
            return ModelId;
        }

        public void setModelId(String ModelId) {
            this.ModelId = ModelId;
        }

        public String getBrand() {
            return Brand;
        }

        public void setBrand(String Brand) {
            this.Brand = Brand;
        }

        public String getSeries() {
            return Series;
        }

        public void setSeries(String Series) {
            this.Series = Series;
        }

        public String getStruct() {
            return Struct;
        }

        public void setStruct(String Struct) {
            this.Struct = Struct;
        }

        public String getCounts() {
            return Counts;
        }

        public void setCounts(String Counts) {
            this.Counts = Counts;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.OrderNo);
            dest.writeString(this.OrderStatus);
            dest.writeString(this.CustId);
            dest.writeString(this.RentBeginTime);
            dest.writeString(this.RentEndTime);
            dest.writeString(this.OrgId);
            dest.writeString(this.OrgName);
            dest.writeString(this.ReturnOrgId);
            dest.writeString(this.ReturnOrgName);
            dest.writeString(this.Longitude);
            dest.writeString(this.Latitude);
            dest.writeString(this.ServiceAmt);
            dest.writeString(this.PreRentAmt);
            dest.writeString(this.Deposit);
            dest.writeString(this.InsurenceAmt);
            dest.writeString(this.VehicleId);
            dest.writeString(this.LicensePlate);
            dest.writeString(this.ModelId);
            dest.writeString(this.Brand);
            dest.writeString(this.Series);
            dest.writeString(this.Struct);
            dest.writeString(this.Counts);
            dest.writeString(this.CreateDate);
        }

        public DataEntity() {
        }

        protected DataEntity(Parcel in) {
            this.OrderNo = in.readString();
            this.OrderStatus = in.readString();
            this.CustId = in.readString();
            this.RentBeginTime = in.readString();
            this.RentEndTime = in.readString();
            this.OrgId = in.readString();
            this.OrgName = in.readString();
            this.ReturnOrgId = in.readString();
            this.ReturnOrgName = in.readString();
            this.Longitude = in.readString();
            this.Latitude = in.readString();
            this.ServiceAmt = in.readString();
            this.PreRentAmt = in.readString();
            this.Deposit = in.readString();
            this.InsurenceAmt = in.readString();
            this.VehicleId = in.readString();
            this.LicensePlate = in.readString();
            this.ModelId = in.readString();
            this.Brand = in.readString();
            this.Series = in.readString();
            this.Struct = in.readString();
            this.Counts = in.readString();
            this.CreateDate = in.readString();
        }

        public static final Parcelable.Creator<DataEntity> CREATOR = new Parcelable.Creator<DataEntity>() {
            @Override
            public DataEntity createFromParcel(Parcel source) {
                return new DataEntity(source);
            }

            @Override
            public DataEntity[] newArray(int size) {
                return new DataEntity[size];
            }
        };
    }
}

package com.fusite.bean;

/**
 * Created by caolz on 2018/5/9.
 */

public class ActivityInfo {

    /**
     * crm_activity_get : {"returnStatus":"100","isSuccess":"Y","ActivityNo":"ACT180504.000002","ActivityName":"五一充值活动","ActivityDesc":"会员充值满1000送100","BeginDateTime":"2018-05-04","EndDateTime":"2018-05-10"}
     */

    private CrmActivityGetBean crm_activity_get;

    public CrmActivityGetBean getCrm_activity_get() {
        return crm_activity_get;
    }

    public void setCrm_activity_get(CrmActivityGetBean crm_activity_get) {
        this.crm_activity_get = crm_activity_get;
    }

    public static class CrmActivityGetBean {
        /**
         * returnStatus : 100
         * isSuccess : Y
         * ActivityNo : ACT180504.000002
         * ActivityName : 五一充值活动
         * ActivityDesc : 会员充值满1000送100
         * BeginDateTime : 2018-05-04
         * EndDateTime : 2018-05-10
         */

        private String returnStatus;
        private String isSuccess;
        private String ActivityNo;
        private String ActivityName;
        private String ActivityDesc;
        private String BeginDateTime;
        private String EndDateTime;

        public String getReturnStatus() {
            return returnStatus;
        }

        public void setReturnStatus(String returnStatus) {
            this.returnStatus = returnStatus;
        }

        public String getIsSuccess() {
            return isSuccess;
        }

        public void setIsSuccess(String isSuccess) {
            this.isSuccess = isSuccess;
        }

        public String getActivityNo() {
            return ActivityNo;
        }

        public void setActivityNo(String ActivityNo) {
            this.ActivityNo = ActivityNo;
        }

        public String getActivityName() {
            return ActivityName;
        }

        public void setActivityName(String ActivityName) {
            this.ActivityName = ActivityName;
        }

        public String getActivityDesc() {
            return ActivityDesc;
        }

        public void setActivityDesc(String ActivityDesc) {
            this.ActivityDesc = ActivityDesc;
        }

        public String getBeginDateTime() {
            return BeginDateTime;
        }

        public void setBeginDateTime(String BeginDateTime) {
            this.BeginDateTime = BeginDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime;
        }

        public void setEndDateTime(String EndDateTime) {
            this.EndDateTime = EndDateTime;
        }
    }
}

package com.fusite.bean;

/**
 * Created by Jelly on 2016/4/20.
 */
public class Screen {
    public String title;
    public boolean isSelect;

    public Screen(String title, boolean isSelect) {
        this.title = title;
        this.isSelect = isSelect;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}

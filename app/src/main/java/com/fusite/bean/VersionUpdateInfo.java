package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/26.
 */
public class VersionUpdateInfo {

    /**
     * interfaceName : crm_androidapk_get
     * returnStatus : 100
     * isSuccess : Y
     * data : {"Version":"1.1","Path":"http://www.genlex.com.cn/image/files/2016/05/20/1463744235728/app-release.apk","VersionDesc":"这个版本有神器,快快更新吧"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * Version : 1.1
     * Path : http://www.genlex.com.cn/image/files/2016/05/20/1463744235728/app-release.apk
     * VersionDesc : 这个版本有神器,快快更新吧
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String Version;
        private String Path;
        private String VersionDesc;

        public String getVersion() {
            return Version;
        }

        public void setVersion(String Version) {
            this.Version = Version;
        }

        public String getPath() {
            return Path;
        }

        public void setPath(String Path) {
            this.Path = Path;
        }

        public String getVersionDesc() {
            return VersionDesc;
        }

        public void setVersionDesc(String VersionDesc) {
            this.VersionDesc = VersionDesc;
        }
    }
}

package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/5/16.
 */
public class GetServiceOrder {

    /**
     * interfaceName : evm_orders_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"OrgName":"科园维修店","OrderNo":"O-20160516-01025","CustId":"86201604050000000728","PlanTime":"2016-05-16 11:15:46","ServiceId":"S0000001","ServiceName":"汽车维修","Tel":"12345676","Manager":"老文"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrgName : 科园维修店
     * OrderNo : O-20160516-01025
     * CustId : 86201604050000000728
     * PlanTime : 2016-05-16 11:15:46
     * ServiceId : S0000001
     * ServiceName : 汽车维修
     * Tel : 12345676
     * Manager : 老文
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity extends DateSort{
        private String OrgName;
        private String OrderNo;
        private String OrderStatus;
        private String CustId;
        private String PlanTime;
        private String ServiceId;
        private String ServiceName;
        private String Tel;
        private String Manager;
        private String ServiceAmt;
        private String Deposit;


        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getPlanTime() {
            return PlanTime;
        }

        public void setPlanTime(String PlanTime) {
            this.PlanTime = PlanTime;
        }

        public String getServiceId() {
            return ServiceId;
        }

        public void setServiceId(String ServiceId) {
            this.ServiceId = ServiceId;
        }

        public String getServiceName() {
            return ServiceName;
        }

        public void setServiceName(String ServiceName) {
            this.ServiceName = ServiceName;
        }

        public String getTel() {
            return Tel;
        }

        public void setTel(String Tel) {
            this.Tel = Tel;
        }

        public String getManager() {
            return Manager;
        }

        public void setManager(String Manager) {
            this.Manager = Manager;
        }

        public String getServiceAmt() {
            return ServiceAmt;
        }

        public void setServiceAmt(String serviceAmt) {
            ServiceAmt = serviceAmt;
        }

        public String getDeposit() {
            return Deposit;
        }

        public void setDeposit(String deposit) {
            Deposit = deposit;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            OrderStatus = orderStatus;
        }
    }
}

package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/6/25.
 */
public class InvoiceHistory {

    /**
     * interfaceName : ev_invoice_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"DocNo":"12","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"I1233333","InvoiceTitle":"清华大学苏州汽车研究院(相城)","InvcAmt":"100.00","InvoiceNo":"SD1233333","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"2"},{"DocNo":"INV-20160625-017","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-018","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-019","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-020","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-021","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-022","CreateDate":"2016-06-25T15:13:59"},{"DocNo":"INV-20160625-027","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-029","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-030","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-031","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-032","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-033","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-034","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-035","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-036","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-037","InvoiceTitle":"清华大学","InvcAmt":"8.35","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-038","InvoiceTitle":"asasda","InvcAmt":"43.20","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-039","InvoiceTitle":"111111111","InvcAmt":"6.80","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-040","InvoiceTitle":"qasda","InvcAmt":"67.00","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"},{"DocNo":"INV-20160625-050","InvoiceTitle":"adsadsaasdsa","InvcAmt":"57.00","CreateDate":"2016-06-25T15:13:59","InvoiceStatus":"1"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * DocNo : 12
     * InvoiceTitle : 清华大学
     * InvcAmt : 8.35
     * CreateDate : 2016-06-25T15:13:59
     * InvoiceStatus : 1
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity extends DateSort{
        private String DocNo;
        private String InvoiceTitle;
        private String InvcAmt;
        private String CreateDate;
        private String InvoiceStatus;

        public String getDocNo() {
            return DocNo;
        }

        public void setDocNo(String DocNo) {
            this.DocNo = DocNo;
        }

        public String getInvoiceTitle() {
            return InvoiceTitle;
        }

        public void setInvoiceTitle(String InvoiceTitle) {
            this.InvoiceTitle = InvoiceTitle;
        }

        public String getInvcAmt() {
            return InvcAmt;
        }

        public void setInvcAmt(String InvcAmt) {
            this.InvcAmt = InvcAmt;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }

        public String getInvoiceStatus() {
            return InvoiceStatus;
        }

        public void setInvoiceStatus(String InvoiceStatus) {
            this.InvoiceStatus = InvoiceStatus;
        }
    }
}

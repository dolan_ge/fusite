package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/4/28.
 */
public class UploadImage {

    /**
     * interfaceName : uploadImage
     * data : [{"name":"avatar","url":"http://47.100.97.221/images/20180510/img/c256245a7cc63508c16de741e34faf0ar.png","key":"avatar.png"}]
     * returnStatus : 100
     * isSuccess : Y
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    private List<DataBean> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : avatar
         * url : http://47.100.97.221/images/20180510/img/c256245a7cc63508c16de741e34faf0ar.png
         * key : avatar.png
         */

        private String name;
        private String url;
        private String key;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }
}

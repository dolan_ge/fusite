package com.fusite.bean;

/**
 * Created by Jelly on 2016/5/16.
 */
public class SetServiceOrder {

    /**
     * interfaceName : evm_order_set
     * returnStatus : 100
     * isSuccess : Y
     * data : {"OrderNo":"O-20160516-01026","OrderStatus":"1","CustId":"86201604070000000784","PlanTime":"2016-05-16 11:23:01","OrgId":"JKY0001","OrgName":"科园维修店","Latitude":"22.534116","Longitude":"113.943546","ServiceAmt":"0.00","ServiceId":"S0000002","ServiceName":"汽车维护","Model":"111","Tel":"18617050557","CreateDate":"2016-05-16 11:23:01"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrderNo : O-20160516-01026
     * OrderStatus : 1
     * CustId : 86201604070000000784
     * PlanTime : 2016-05-16 11:23:01
     * OrgId : JKY0001
     * OrgName : 科园维修店
     * Latitude : 22.534116
     * Longitude : 113.943546
     * ServiceAmt : 0.00
     * ServiceId : S0000002
     * ServiceName : 汽车维护
     * Model : 111
     * Tel : 18617050557
     * CreateDate : 2016-05-16 11:23:01
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String OrderNo;
        private String OrderStatus;
        private String CustId;
        private String PlanTime;
        private String OrgId;
        private String OrgName;
        private String Latitude;
        private String Longitude;
        private String ServiceAmt;
        private String ServiceId;
        private String ServiceName;
        private String Model;
        private String Tel;
        private String CreateDate;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getPlanTime() {
            return PlanTime;
        }

        public void setPlanTime(String PlanTime) {
            this.PlanTime = PlanTime;
        }

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getServiceAmt() {
            return ServiceAmt;
        }

        public void setServiceAmt(String ServiceAmt) {
            this.ServiceAmt = ServiceAmt;
        }

        public String getServiceId() {
            return ServiceId;
        }

        public void setServiceId(String ServiceId) {
            this.ServiceId = ServiceId;
        }

        public String getServiceName() {
            return ServiceName;
        }

        public void setServiceName(String ServiceName) {
            this.ServiceName = ServiceName;
        }

        public String getModel() {
            return Model;
        }

        public void setModel(String Model) {
            this.Model = Model;
        }

        public String getTel() {
            return Tel;
        }

        public void setTel(String Tel) {
            this.Tel = Tel;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }
    }
}

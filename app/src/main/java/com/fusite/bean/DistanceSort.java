package com.fusite.bean;

/**
 * Created by Jelly on 2016/8/17.
 */
public class DistanceSort implements Comparable<DistanceSort>{

    public float distance;

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    @Override
    public int compareTo(DistanceSort another) {
        if(distance > another.distance){
            return 1;
        }

        if(distance == another.distance){
            return 0;
        }

        return -1;
    }
}

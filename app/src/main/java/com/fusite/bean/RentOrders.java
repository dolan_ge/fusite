package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/4/25.
 */
public class RentOrders{

    /**
     * interfaceName : evr_orders_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"OrderNo":"O-20160422-00737","OrderStatus":"1","OrgId":"KJ0000001","OrgName":"科技园租车","ReturnOrgId":"KJ0000001","ReturnOrgName":"科技园租车","CreateDate":"2016-04-22 17:34:49"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrderNo : O-20160422-00737
     * OrderStatus : 1
     * OrgId : KJ0000001
     * OrgName : 科技园租车
     * ReturnOrgId : KJ0000001
     * ReturnOrgName : 科技园租车
     * CreateDate : 2016-04-22 17:34:49
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity extends DateSort{
        private String OrderNo;
        private String OrderStatus;
        private String OrgId;
        private String OrgName;
        private String ReturnOrgId;
        private String ReturnOrgName;
        private String CreateDate;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getReturnOrgId() {
            return ReturnOrgId;
        }

        public void setReturnOrgId(String ReturnOrgId) {
            this.ReturnOrgId = ReturnOrgId;
        }

        public String getReturnOrgName() {
            return ReturnOrgName;
        }

        public void setReturnOrgName(String ReturnOrgName) {
            this.ReturnOrgName = ReturnOrgName;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }
    }
}

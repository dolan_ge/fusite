package com.fusite.bean;

/**
 * Created by Jelly on 2016/4/22.
 */
public class RentOrderInfo {

    /**
     * interfaceName : evr_order_get
     * returnStatus : 100
     * isSuccess : Y
     * data : {"OrderNo":"O-20160519-01124","OrderStatus":"4","OrgName":"科技园租车","ReturnOrgName":"科技园租车","RentBeginTime":"2016-05-19 12:00:32","RentEndTime":"2016-05-26 12:00:32","PreRentAmt":"3360.00","RealBeginTime":"2016-05-19 12:00","RealEndTime":"2016-05-26 09:31","InsurenceAmt":"30.00","ServiceAmt":"2.00","Deposit":"700.00","TotalAmt":"4092.00","RealAmt":"3392.00","ClearingAmt":"700.00","PayType":"1","VehicleId":"V00000006","LicensePlate":"粤B83A84","Brand":"特斯拉","Series":"Model 5","Picture":"http://g.hiphotos.baidu.com/news/w%3D638/sign=fb44eabe972397ddd6799b076183b216/8435e5dde71190efe8ba4186c81b9d16fdfa6022.jpg","Tel":"0752-12345678"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrderNo : O-20160519-01124
     * OrderStatus : 4
     * OrgName : 科技园租车
     * ReturnOrgName : 科技园租车
     * RentBeginTime : 2016-05-19 12:00:32
     * RentEndTime : 2016-05-26 12:00:32
     * PreRentAmt : 3360.00
     * RealBeginTime : 2016-05-19 12:00
     * RealEndTime : 2016-05-26 09:31
     * InsurenceAmt : 30.00
     * ServiceAmt : 2.00
     * Deposit : 700.00
     * TotalAmt : 4092.00
     * RealAmt : 3392.00
     * ClearingAmt : 700.00
     * PayType : 1
     * VehicleId : V00000006
     * LicensePlate : 粤B83A84
     * Brand : 特斯拉
     * Series : Model 5
     * Picture : http://g.hiphotos.baidu.com/news/w%3D638/sign=fb44eabe972397ddd6799b076183b216/8435e5dde71190efe8ba4186c81b9d16fdfa6022.jpg
     * Tel : 0752-12345678
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String OrderNo;
        private String OrderStatus;
        private String OrgName;
        private String ReturnOrgName;
        private String RentBeginTime;
        private String RentEndTime;
        private String PreRentAmt;
        private String RealBeginTime;
        private String RealEndTime;
        private String InsurenceAmt;
        private String ServiceAmt;
        private String Deposit;
        private String TotalAmt;
        private String RealAmt;
        private String ClearingAmt;
        private String PayType;
        private String VehicleId;
        private String LicensePlate;
        private String Brand;
        private String Series;
        private String Picture;
        private String Tel;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getReturnOrgName() {
            return ReturnOrgName;
        }

        public void setReturnOrgName(String ReturnOrgName) {
            this.ReturnOrgName = ReturnOrgName;
        }

        public String getRentBeginTime() {
            return RentBeginTime;
        }

        public void setRentBeginTime(String RentBeginTime) {
            this.RentBeginTime = RentBeginTime;
        }

        public String getRentEndTime() {
            return RentEndTime;
        }

        public void setRentEndTime(String RentEndTime) {
            this.RentEndTime = RentEndTime;
        }

        public String getPreRentAmt() {
            return PreRentAmt;
        }

        public void setPreRentAmt(String PreRentAmt) {
            this.PreRentAmt = PreRentAmt;
        }

        public String getRealBeginTime() {
            return RealBeginTime;
        }

        public void setRealBeginTime(String RealBeginTime) {
            this.RealBeginTime = RealBeginTime;
        }

        public String getRealEndTime() {
            return RealEndTime;
        }

        public void setRealEndTime(String RealEndTime) {
            this.RealEndTime = RealEndTime;
        }

        public String getInsurenceAmt() {
            return InsurenceAmt;
        }

        public void setInsurenceAmt(String InsurenceAmt) {
            this.InsurenceAmt = InsurenceAmt;
        }

        public String getServiceAmt() {
            return ServiceAmt;
        }

        public void setServiceAmt(String ServiceAmt) {
            this.ServiceAmt = ServiceAmt;
        }

        public String getDeposit() {
            return Deposit;
        }

        public void setDeposit(String Deposit) {
            this.Deposit = Deposit;
        }

        public String getTotalAmt() {
            return TotalAmt;
        }

        public void setTotalAmt(String TotalAmt) {
            this.TotalAmt = TotalAmt;
        }

        public String getRealAmt() {
            return RealAmt;
        }

        public void setRealAmt(String RealAmt) {
            this.RealAmt = RealAmt;
        }

        public String getClearingAmt() {
            return ClearingAmt;
        }

        public void setClearingAmt(String ClearingAmt) {
            this.ClearingAmt = ClearingAmt;
        }

        public String getPayType() {
            return PayType;
        }

        public void setPayType(String PayType) {
            this.PayType = PayType;
        }

        public String getVehicleId() {
            return VehicleId;
        }

        public void setVehicleId(String VehicleId) {
            this.VehicleId = VehicleId;
        }

        public String getLicensePlate() {
            return LicensePlate;
        }

        public void setLicensePlate(String LicensePlate) {
            this.LicensePlate = LicensePlate;
        }

        public String getBrand() {
            return Brand;
        }

        public void setBrand(String Brand) {
            this.Brand = Brand;
        }

        public String getSeries() {
            return Series;
        }

        public void setSeries(String Series) {
            this.Series = Series;
        }

        public String getPicture() {
            return Picture;
        }

        public void setPicture(String Picture) {
            this.Picture = Picture;
        }

        public String getTel() {
            return Tel;
        }

        public void setTel(String Tel) {
            this.Tel = Tel;
        }
    }
}

package com.fusite.bean;

/**
 * 充电卡充值
 * Created by Jelly on 2016/7/13.
 */
public class CardRecharge {

    /**
     * interfaceName : crm_accounts_rechg
     * returnStatus : 100
     * isSuccess : Y
     * data : {"AccountNo":"123456789","BalanceAmt":"110.00"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * AccountNo : 123456789
     * BalanceAmt : 110.00
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String AccountNo;
        private String BalanceAmt;

        public String getAccountNo() {
            return AccountNo;
        }

        public void setAccountNo(String AccountNo) {
            this.AccountNo = AccountNo;
        }

        public String getBalanceAmt() {
            return BalanceAmt;
        }

        public void setBalanceAmt(String BalanceAmt) {
            this.BalanceAmt = BalanceAmt;
        }
    }
}

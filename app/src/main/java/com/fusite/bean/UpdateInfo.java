package com.fusite.bean;

/**
 * Created by Jelly on 2016/7/22.
 */
public class UpdateInfo {

    /**
     * interfaceName : crm_customer_refresh
     * returnStatus : 100
     * isSuccess : Y
     * data : {"CustID":"86201604050000000728","BalanceAmt":"98843.83","isCertification":"1","HasMessage":"1"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * CustID : 86201604050000000728
     * BalanceAmt : 98843.83
     * isCertification : 1
     * HasMessage : 1
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String CustID;
        private String BalanceAmt;
        private String isCertification;
        private String HasMessage;

        public String getCustID() {
            return CustID;
        }

        public void setCustID(String CustID) {
            this.CustID = CustID;
        }

        public String getBalanceAmt() {
            return BalanceAmt;
        }

        public void setBalanceAmt(String BalanceAmt) {
            this.BalanceAmt = BalanceAmt;
        }

        public String getIsCertification() {
            return isCertification;
        }

        public void setIsCertification(String isCertification) {
            this.isCertification = isCertification;
        }

        public String getHasMessage() {
            return HasMessage;
        }

        public void setHasMessage(String HasMessage) {
            this.HasMessage = HasMessage;
        }
    }
}

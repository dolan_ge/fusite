package com.fusite.bean;

/**
 * Created by Jelly on 2016/5/23.
 */
public class CancelServiceOrder {

    /**
     * interfaceName : evm_order_cancel
     * returnStatus : 100
     * isSuccess : Ys
     * data : {"OrderNo":"O-20160523-01210","OrderStatus":"16"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrderNo : O-20160523-01210
     * OrderStatus : 16
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String OrderNo;
        private String OrderStatus;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getOrderStatus() {
            return OrderStatus;
        }

        public void setOrderStatus(String OrderStatus) {
            this.OrderStatus = OrderStatus;
        }
    }
}

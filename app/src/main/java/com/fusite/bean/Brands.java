package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/8/16.
 */
public class Brands {

    /**
     * interfaceName : crm_brands_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"Brand":"AC Schnitzer"},{"Brand":"知豆"},{"Brand":"中华"},{"Brand":"中欧奔驰房车"},{"Brand":"中通客车"},{"Brand":"中兴"},{"Brand":"众泰"},{"Brand":"重汽王牌"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * Brand : AC Schnitzer
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        private String Brand;

        public String getBrand() {
            return Brand;
        }

        public void setBrand(String Brand) {
            this.Brand = Brand;
        }
    }
}

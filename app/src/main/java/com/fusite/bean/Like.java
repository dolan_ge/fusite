package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/14.
 */
public class Like {

    /**
     * interfaceName : crm_forum_like
     * returnStatus : 100
     * isSuccess : Y
     * data : {"ForumNo":"O-20160613-01413","CustId":"86201604050000000728"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * ForumNo : O-20160613-01413
     * CustId : 86201604050000000728
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String ForumNo;
        private String CustId;

        public String getForumNo() {
            return ForumNo;
        }

        public void setForumNo(String ForumNo) {
            this.ForumNo = ForumNo;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }
    }
}

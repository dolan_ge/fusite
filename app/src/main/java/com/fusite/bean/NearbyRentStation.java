package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/4/18.
 */
public class NearbyRentStation {

    /**
     * interfaceName : evr_stationset_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"District":"福田区","Stations":[{"OrgId":"FT0000001","OrgName":"福田租车站","District":"福田区","Longitude":"113.959266","Latitude":"22.531010","Addr":"盐田区沙头角盐田区政府(海山路)"}]},{"District":"南山区","Stations":[{"OrgId":"KJ0000001","OrgName":"科技园租车","District":"南山区","Longitude":"113.943264","Latitude":"22.533556","Addr":"深圳市南山区科技园中山大学"},{"OrgId":"SD0000001","OrgName":"深圳大学租车","District":"南山区","Longitude":"113.936463","Latitude":"22.540172","Addr":"深圳市深圳大学北门"}]}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * District : 福田区
     * Stations : [{"OrgId":"FT0000001","OrgName":"福田租车站","District":"福田区","Longitude":"113.959266","Latitude":"22.531010","Addr":"盐田区沙头角盐田区政府(海山路)"}]
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        private String District;
        /**
         * OrgId : FT0000001
         * OrgName : 福田租车站
         * District : 福田区
         * Longitude : 113.959266
         * Latitude : 22.531010
         * Addr : 盐田区沙头角盐田区政府(海山路)
         */

        private List<StationsEntity> Stations;

        public String getDistrict() {
            return District;
        }

        public void setDistrict(String District) {
            this.District = District;
        }

        public List<StationsEntity> getStations() {
            return Stations;
        }

        public void setStations(List<StationsEntity> Stations) {
            this.Stations = Stations;
        }

        public static class StationsEntity {
            private String OrgId;
            private String OrgName;
            private String District;
            private String Longitude;
            private String Latitude;
            private String Addr;

            public String getOrgId() {
                return OrgId;
            }

            public void setOrgId(String OrgId) {
                this.OrgId = OrgId;
            }

            public String getOrgName() {
                return OrgName;
            }

            public void setOrgName(String OrgName) {
                this.OrgName = OrgName;
            }

            public String getDistrict() {
                return District;
            }

            public void setDistrict(String District) {
                this.District = District;
            }

            public String getLongitude() {
                return Longitude;
            }

            public void setLongitude(String Longitude) {
                this.Longitude = Longitude;
            }

            public String getLatitude() {
                return Latitude;
            }

            public void setLatitude(String Latitude) {
                this.Latitude = Latitude;
            }

            public String getAddr() {
                return Addr;
            }

            public void setAddr(String Addr) {
                this.Addr = Addr;
            }
        }
    }
}

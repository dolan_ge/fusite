package com.fusite.bean;

/**
 * Created by Jelly on 2016/4/21.
 */
public class CarModel {

    /**
     * interfaceName : evr_model_get
     * returnStatus : 100
     * isSuccess : Y
     * data : {"ModelId":"M00000004","Picture":"http://g.hiphotos.baidu.com/news/w%3D638/sign=fb44eabe972397ddd6799b076183b216/8435e5dde71190efe8ba4186c81b9d16fdfa6022.jpg","Brand":"特斯拉","Series":"Model 3","Counts":"5","Struct":"3厢","EnduranceMileage":"320","Price":"260.00"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * ModelId : M00000004
     * Picture : http://g.hiphotos.baidu.com/news/w%3D638/sign=fb44eabe972397ddd6799b076183b216/8435e5dde71190efe8ba4186c81b9d16fdfa6022.jpg
     * Brand : 特斯拉
     * Series : Model 3
     * Counts : 5
     * Struct : 3厢
     * EnduranceMileage : 320
     * Price : 260.00
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String ModelId;
        private String Picture;
        private String Brand;
        private String Series;
        private String Counts;
        private String Struct;
        private String EnduranceMileage;
        private String Price;

        public String getModelId() {
            return ModelId;
        }

        public void setModelId(String ModelId) {
            this.ModelId = ModelId;
        }

        public String getPicture() {
            return Picture;
        }

        public void setPicture(String Picture) {
            this.Picture = Picture;
        }

        public String getBrand() {
            return Brand;
        }

        public void setBrand(String Brand) {
            this.Brand = Brand;
        }

        public String getSeries() {
            return Series;
        }

        public void setSeries(String Series) {
            this.Series = Series;
        }

        public String getCounts() {
            return Counts;
        }

        public void setCounts(String Counts) {
            this.Counts = Counts;
        }

        public String getStruct() {
            return Struct;
        }

        public void setStruct(String Struct) {
            this.Struct = Struct;
        }

        public String getEnduranceMileage() {
            return EnduranceMileage;
        }

        public void setEnduranceMileage(String EnduranceMileage) {
            this.EnduranceMileage = EnduranceMileage;
        }

        public String getPrice() {
            return Price;
        }

        public void setPrice(String Price) {
            this.Price = Price;
        }
    }
}

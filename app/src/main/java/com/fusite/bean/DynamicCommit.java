package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/15.
 */
public class DynamicCommit {

    /**
     * interfaceName : crm_forum_reply
     * returnStatus : 100
     * isSuccess : Y
     * data : {"ForumNo":"O-20160613-01411","Item":"1"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * ForumNo : O-20160613-01411
     * Item : 1
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String ForumNo;
        private String Item;

        public String getForumNo() {
            return ForumNo;
        }

        public void setForumNo(String ForumNo) {
            this.ForumNo = ForumNo;
        }

        public String getItem() {
            return Item;
        }

        public void setItem(String Item) {
            this.Item = Item;
        }
    }
}

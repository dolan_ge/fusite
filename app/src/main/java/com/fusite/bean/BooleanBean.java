package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/23.
 */
public class BooleanBean {
    public boolean flag;

    public BooleanBean() {
    }

    public BooleanBean(boolean flag) {
        this.flag = flag;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}

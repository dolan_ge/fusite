package com.fusite.bean;

/**
 * Created by Jelly on 2016/4/21.
 */
public class CityName {

    /**
     * interfaceName : evr_cityname_get
     * returnStatus : 100
     * isSuccess : Y
     * data : {"Area":"深圳","SortOrder":"440300"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * Area : 深圳
     * SortOrder : 440300
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String Area;
        private String SortOrder;

        public String getArea() {
            return Area;
        }

        public void setArea(String Area) {
            this.Area = Area;
        }

        public String getSortOrder() {
            return SortOrder;
        }

        public void setSortOrder(String SortOrder) {
            this.SortOrder = SortOrder;
        }
    }
}

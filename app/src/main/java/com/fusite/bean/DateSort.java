package com.fusite.bean;

import java.util.Date;

/**
 * 时间排序父类
 * Created by Jelly on 2016/4/28.
 */
public class DateSort implements Comparable<DateSort>{
    public Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int compareTo(DateSort another) {
        return another.date.compareTo(this.date);
    }
}

package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/13.
 */
public class SendDynamic {

    /**
     * interfaceName : crm_forum_set
     * returnStatus : 100
     * isSuccess : Y
     * data : {"ForumNo":"O-20160613-01402","CreateDate":"2016-06-13 16:58:38"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * ForumNo : O-20160613-01402
     * CreateDate : 2016-06-13 16:58:38
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String ForumNo;
        private String CreateDate;

        public String getForumNo() {
            return ForumNo;
        }

        public void setForumNo(String ForumNo) {
            this.ForumNo = ForumNo;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }
    }
}

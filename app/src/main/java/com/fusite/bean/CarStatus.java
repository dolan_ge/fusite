package com.fusite.bean;

/**
 * Created by Jelly on 2016/5/18.
 */
public class CarStatus {

    /**
     * interfaceName : evr_vehiclestate_get
     * returnStatus : 100
     * isSuccess : Y
     * data : {"VehicleId":"V00000006","LicensePlate":"粤B83A84","Mileage":"262.0","MotorTemp":"50.0","Speed":"46.8125","BatteryCurrent":"35.69999","BatteryVoltage":"336.50001","SOC":"76.0","BatteryTemp":"27.0"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * VehicleId : V00000006
     * LicensePlate : 粤B83A84
     * Mileage : 262.0
     * MotorTemp : 50.0
     * Speed : 46.8125
     * BatteryCurrent : 35.69999
     * BatteryVoltage : 336.50001
     * SOC : 76.0
     * BatteryTemp : 27.0
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String VehicleId;
        private String LicensePlate;
        private String Mileage;
        private String MotorTemp;
        private String Speed;
        private String BatteryCurrent;
        private String BatteryVoltage;
        private String SOC;
        private String BatteryTemp;

        public String getVehicleId() {
            return VehicleId;
        }

        public void setVehicleId(String VehicleId) {
            this.VehicleId = VehicleId;
        }

        public String getLicensePlate() {
            return LicensePlate;
        }

        public void setLicensePlate(String LicensePlate) {
            this.LicensePlate = LicensePlate;
        }

        public String getMileage() {
            return Mileage;
        }

        public void setMileage(String Mileage) {
            this.Mileage = Mileage;
        }

        public String getMotorTemp() {
            return MotorTemp;
        }

        public void setMotorTemp(String MotorTemp) {
            this.MotorTemp = MotorTemp;
        }

        public String getSpeed() {
            return Speed;
        }

        public void setSpeed(String Speed) {
            this.Speed = Speed;
        }

        public String getBatteryCurrent() {
            return BatteryCurrent;
        }

        public void setBatteryCurrent(String BatteryCurrent) {
            this.BatteryCurrent = BatteryCurrent;
        }

        public String getBatteryVoltage() {
            return BatteryVoltage;
        }

        public void setBatteryVoltage(String BatteryVoltage) {
            this.BatteryVoltage = BatteryVoltage;
        }

        public String getSOC() {
            return SOC;
        }

        public void setSOC(String SOC) {
            this.SOC = SOC;
        }

        public String getBatteryTemp() {
            return BatteryTemp;
        }

        public void setBatteryTemp(String BatteryTemp) {
            this.BatteryTemp = BatteryTemp;
        }
    }
}

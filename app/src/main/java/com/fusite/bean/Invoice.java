package com.fusite.bean;

import java.util.List;

/**
 * Created by Jelly on 2016/6/23.
 */
public class Invoice {

    /**
     * interfaceName : ev_invoiceorder_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"OrderNo":"O-20160406-00524","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-04-06 18:40:35","CreateUsr":"18806002800000000019","begintime":"2016-04-06 18:41:16","endtime":"2016-04-06 19:02:09","Amt":"0.00"},{"OrderNo":"O-20160510-00962","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-10 17:32:00","CreateUsr":"18806002800000000019","begintime":"2016-05-10 17:35:24","endtime":"2016-05-10 17:37:22","Amt":"0.00"},{"OrderNo":"O-20160517-01068","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-17 13:48:00","CreateUsr":"18806002800000000019","begintime":"2016-05-17 13:51:55","endtime":"2016-05-17 13:54:04","Amt":"0.00"},{"OrderNo":"O-20160517-01069","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-17 14:11:25","CreateUsr":"18806002800000000019","begintime":"2016-05-17 14:15:26","endtime":"2016-05-17 15:45:28","Amt":"0.00"},{"OrderNo":"O-20160518-01081","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-18 15:26:47","CreateUsr":"18806002800000000019","begintime":"2016-05-18 15:30:55","endtime":"2016-05-18 15:31:05","Amt":"0.00"},{"OrderNo":"O-20160518-01083","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-18 15:50:59","CreateUsr":"18806002800000000019","begintime":"2016-05-18 15:55:08","endtime":"2016-05-18 15:55:29","Amt":"0.00"},{"OrderNo":"O-20160518-01084","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-18 16:15:43","CreateUsr":"18806002800000000019","begintime":"2016-05-18 16:20:03","endtime":"2016-05-18 16:20:12","Amt":"0.00"},{"OrderNo":"O-20160518-01086","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-18 17:13:50","CreateUsr":"18806002800000000019","begintime":"2016-05-18 17:17:50","endtime":"2016-05-18 17:22:02","Amt":"0.00"},{"OrderNo":"O-20160518-01087","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-18 17:24:12","CreateUsr":"18806002800000000019","begintime":"2016-05-18 17:28:11","endtime":"2016-05-18 17:28:56","Amt":"0.00"},{"OrderNo":"O-20160518-01090","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-18 17:56:40","CreateUsr":"18806002800000000019","begintime":"2016-05-18 18:00:42","endtime":"2016-05-18 18:05:00","Amt":"0.00"},{"OrderNo":"O-20160519-01118","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-19 10:32:18","CreateUsr":"18806002800000000019","begintime":"2016-05-19 10:36:04","endtime":"2016-05-19 10:36:39","Amt":"0.00"},{"OrderNo":"O-20160519-01125","orderType":"EVR","CustId":"18806002800000000019","OrgName":"科技园租车","OrgName2":"科技园租车","CreateDate":"2016-05-19 11:47:56","CreateUsr":"18806002800000000019","begintime":"2016-05-19 11:00","endtime":"2016-05-20 11:54","Amt":"202.00"},{"OrderNo":"O-20160519-01126","orderType":"EVR","CustId":"18806002800000000019","OrgName":"科技园租车","OrgName2":"科技园租车","CreateDate":"2016-05-19 11:58:21","CreateUsr":"18806002800000000019","begintime":"2016-05-19 14:14","endtime":"2016-05-23 14:48","Amt":"12312314.00"},{"OrderNo":"O-20160519-01156","orderType":"EVC","CustId":"18806002800000000019","OrgName":"深大站","CreateDate":"2016-05-19 18:05:37","CreateUsr":"18806002800000000019","begintime":"2016-05-19 18:09:52","endtime":"2016-05-19 18:10:33","Amt":"0.00"},{"OrderNo":"O-20160531-01330","orderType":"EVC","CustId":"18806002800000000019","OrgName":"盐田图书馆","CreateDate":"2016-05-31 13:47:52","CreateUsr":"18806002800000000019","begintime":"2016-05-31 14:07:09","endtime":"2016-05-31 14:07:17","Amt":"0.00"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * OrderNo : O-20160406-00524
     * orderType : EVC
     * CustId : 18806002800000000019
     * OrgName : 深大站
     * CreateDate : 2016-04-06 18:40:35
     * CreateUsr : 18806002800000000019
     * begintime : 2016-04-06 18:41:16
     * endtime : 2016-04-06 19:02:09
     * Amt : 0.00
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity {
        private String OrderNo;
        private String orderType;
        private String CustId;
        private String OrgName;
        private String OrgName2;
        private String CreateDate;
        private String CreateUsr;
        private String begintime;
        private String endtime;
        private String Amt;

        public String getOrderNo() {
            return OrderNo;
        }

        public void setOrderNo(String OrderNo) {
            this.OrderNo = OrderNo;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }

        public String getCreateUsr() {
            return CreateUsr;
        }

        public void setCreateUsr(String CreateUsr) {
            this.CreateUsr = CreateUsr;
        }

        public String getBegintime() {
            return begintime;
        }

        public void setBegintime(String begintime) {
            this.begintime = begintime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getAmt() {
            return Amt;
        }

        public void setAmt(String Amt) {
            this.Amt = Amt;
        }
    }
}

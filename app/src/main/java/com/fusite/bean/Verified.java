package com.fusite.bean;

/**
 * Created by Jelly on 2016/6/16.
 */
public class Verified {

    /**
     * interfaceName : crm_cert_get
     * returnStatus : 100
     * isSuccess : Y
     * data : {"CustID":"86201604050000000728","isCertification":"0","IdentityNum":"362321190002291329","Name":"毛寨主","AuditStatus":"32","Reason":"太帅了，不行"}
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * CustID : 86201604050000000728
     * isCertification : 0
     * IdentityNum : 362321190002291329
     * Name : 毛寨主
     * AuditStatus : 32
     * Reason : 太帅了，不行
     */

    private DataEntity data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        private String CustID;
        private String isCertification;
        private String IdentityNum;
        private String Name;
        private String AuditStatus;
        private String Reason;

        public String getCustID() {
            return CustID;
        }

        public void setCustID(String CustID) {
            this.CustID = CustID;
        }

        public String getIsCertification() {
            return isCertification;
        }

        public void setIsCertification(String isCertification) {
            this.isCertification = isCertification;
        }

        public String getIdentityNum() {
            return IdentityNum;
        }

        public void setIdentityNum(String IdentityNum) {
            this.IdentityNum = IdentityNum;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getAuditStatus() {
            return AuditStatus;
        }

        public void setAuditStatus(String AuditStatus) {
            this.AuditStatus = AuditStatus;
        }

        public String getReason() {
            return Reason;
        }

        public void setReason(String Reason) {
            this.Reason = Reason;
        }
    }
}

package com.fusite.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Jelly on 2016/5/10.
 */
public class FilterServiceStation {

    /**
     * interfaceName : evm_station_getbypos
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"CompanyID":"C01","OrgId":"th000001","Manager":"老王","Tel":"12345678","BookCount":"0","OrgName":"清华救援","Addr":"南山区","Longitude":"113.950109","Latitude":"22.532390","CityCode":"440300","State":"广东省","City":"深圳市","AuditDate":"2016-05-10T00:00:00","OrgDesc":"清华救援","ServiceName":"救援服务"}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * CompanyID : C01
     * OrgId : th000001
     * Manager : 老王
     * Tel : 12345678
     * BookCount : 0
     * OrgName : 清华救援
     * Addr : 南山区
     * Longitude : 113.950109
     * Latitude : 22.532390
     * CityCode : 440300
     * State : 广东省
     * City : 深圳市
     * AuditDate : 2016-05-10T00:00:00
     * OrgDesc : 清华救援
     * ServiceName : 救援服务
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity implements Parcelable {
        private String CompanyID;
        private String OrgId;
        private String Manager;
        private String Tel;
        private String BookCount;
        private String OrgName;
        private String Addr;
        private String Longitude;
        private String Latitude;
        private String CityCode;
        private String State;
        private String City;
        private String AuditDate;
        private String OrgDesc;
        private String ServiceName;

        public String getCompanyID() {
            return CompanyID;
        }

        public void setCompanyID(String CompanyID) {
            this.CompanyID = CompanyID;
        }

        public String getOrgId() {
            return OrgId;
        }

        public void setOrgId(String OrgId) {
            this.OrgId = OrgId;
        }

        public String getManager() {
            return Manager;
        }

        public void setManager(String Manager) {
            this.Manager = Manager;
        }

        public String getTel() {
            return Tel;
        }

        public void setTel(String Tel) {
            this.Tel = Tel;
        }

        public String getBookCount() {
            return BookCount;
        }

        public void setBookCount(String BookCount) {
            this.BookCount = BookCount;
        }

        public String getOrgName() {
            return OrgName;
        }

        public void setOrgName(String OrgName) {
            this.OrgName = OrgName;
        }

        public String getAddr() {
            return Addr;
        }

        public void setAddr(String Addr) {
            this.Addr = Addr;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getCityCode() {
            return CityCode;
        }

        public void setCityCode(String CityCode) {
            this.CityCode = CityCode;
        }

        public String getState() {
            return State;
        }

        public void setState(String State) {
            this.State = State;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String City) {
            this.City = City;
        }

        public String getAuditDate() {
            return AuditDate;
        }

        public void setAuditDate(String AuditDate) {
            this.AuditDate = AuditDate;
        }

        public String getOrgDesc() {
            return OrgDesc;
        }

        public void setOrgDesc(String OrgDesc) {
            this.OrgDesc = OrgDesc;
        }

        public String getServiceName() {
            return ServiceName;
        }

        public void setServiceName(String ServiceName) {
            this.ServiceName = ServiceName;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.CompanyID);
            dest.writeString(this.OrgId);
            dest.writeString(this.Manager);
            dest.writeString(this.Tel);
            dest.writeString(this.BookCount);
            dest.writeString(this.OrgName);
            dest.writeString(this.Addr);
            dest.writeString(this.Longitude);
            dest.writeString(this.Latitude);
            dest.writeString(this.CityCode);
            dest.writeString(this.State);
            dest.writeString(this.City);
            dest.writeString(this.AuditDate);
            dest.writeString(this.OrgDesc);
            dest.writeString(this.ServiceName);
        }

        public DataEntity() {
        }

        protected DataEntity(Parcel in) {
            this.CompanyID = in.readString();
            this.OrgId = in.readString();
            this.Manager = in.readString();
            this.Tel = in.readString();
            this.BookCount = in.readString();
            this.OrgName = in.readString();
            this.Addr = in.readString();
            this.Longitude = in.readString();
            this.Latitude = in.readString();
            this.CityCode = in.readString();
            this.State = in.readString();
            this.City = in.readString();
            this.AuditDate = in.readString();
            this.OrgDesc = in.readString();
            this.ServiceName = in.readString();
        }

        public static final Parcelable.Creator<DataEntity> CREATOR = new Parcelable.Creator<DataEntity>() {
            @Override
            public DataEntity createFromParcel(Parcel source) {
                return new DataEntity(source);
            }

            @Override
            public DataEntity[] newArray(int size) {
                return new DataEntity[size];
            }
        };
    }
}

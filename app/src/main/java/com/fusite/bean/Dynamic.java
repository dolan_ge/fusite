package com.fusite.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Jelly on 2016/6/13.
 */
public class Dynamic {

    /**
     * interfaceName : crm_forums_get
     * returnStatus : 100
     * isSuccess : Y
     * data : [{"ForumNo":"O-20160614-01448","CustId":"86201604060000000772","CustName":"马化腾","Usericon":"http://www.genlex.com.cn/images/1465891408076.png","Content":"刚刚闪退了","CreateDate":"2016-06-14 17:11:44","Liked":"0","Likes":"2","Replies":"1","ReplyContents":[{"Item":"1","Type":"0","From_Id":"86201604050000000728","From_Name":"毛寨主","To_Id":"","To_Name":"","Content":"haha，蔡工和翁工好帅","CreateDate":"2016-06-14 18:57:00"}],"Pictures":[{"url":"http://www.genlex.com.cn/images/1465895486211.png"}]}]
     */

    private String interfaceName;
    private String returnStatus;
    private String isSuccess;
    /**
     * ForumNo : O-20160614-01448
     * CustId : 86201604060000000772
     * CustName : 马化腾
     * Usericon : http://www.genlex.com.cn/images/1465891408076.png
     * Content : 刚刚闪退了
     * CreateDate : 2016-06-14 17:11:44
     * Liked : 0
     * Likes : 2
     * Replies : 1
     * ReplyContents : [{"Item":"1","Type":"0","From_Id":"86201604050000000728","From_Name":"毛寨主","To_Id":"","To_Name":"","Content":"haha，蔡工和翁工好帅","CreateDate":"2016-06-14 18:57:00"}]
     * Pictures : [{"url":"http://www.genlex.com.cn/images/1465895486211.png"}]
     */

    private List<DataEntity> data;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public static class DataEntity implements Parcelable {
        private String ForumNo;
        private String CustId;
        private String CustName;
        private String Usericon;
        private String Content;
        private String CreateDate;
        private String Liked;
        private String Likes;
        private String Replies;
        /**
         * Item : 1
         * Type : 0
         * From_Id : 86201604050000000728
         * From_Name : 毛寨主
         * To_Id :
         * To_Name :
         * Content : haha，蔡工和翁工好帅
         * CreateDate : 2016-06-14 18:57:00
         */

        private List<ReplyContentsEntity> ReplyContents;
        /**
         * url : http://www.genlex.com.cn/images/1465895486211.png
         */

        private List<PicturesEntity> Pictures;

        public String getForumNo() {
            return ForumNo;
        }

        public void setForumNo(String ForumNo) {
            this.ForumNo = ForumNo;
        }

        public String getCustId() {
            return CustId;
        }

        public void setCustId(String CustId) {
            this.CustId = CustId;
        }

        public String getCustName() {
            return CustName;
        }

        public void setCustName(String CustName) {
            this.CustName = CustName;
        }

        public String getUsericon() {
            return Usericon;
        }

        public void setUsericon(String Usericon) {
            this.Usericon = Usericon;
        }

        public String getContent() {
            return Content;
        }

        public void setContent(String Content) {
            this.Content = Content;
        }

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String CreateDate) {
            this.CreateDate = CreateDate;
        }

        public String getLiked() {
            return Liked;
        }

        public void setLiked(String Liked) {
            this.Liked = Liked;
        }

        public String getLikes() {
            return Likes;
        }

        public void setLikes(String Likes) {
            this.Likes = Likes;
        }

        public String getReplies() {
            return Replies;
        }

        public void setReplies(String Replies) {
            this.Replies = Replies;
        }

        public List<ReplyContentsEntity> getReplyContents() {
            return ReplyContents;
        }

        public void setReplyContents(List<ReplyContentsEntity> ReplyContents) {
            this.ReplyContents = ReplyContents;
        }

        public List<PicturesEntity> getPictures() {
            return Pictures;
        }

        public void setPictures(List<PicturesEntity> Pictures) {
            this.Pictures = Pictures;
        }

        public static class ReplyContentsEntity implements Parcelable {
            private String Item;
            private String Type;
            private String From_Id;
            private String From_Name;
            private String To_Id;
            private String To_Name;
            private String Content;
            private String CreateDate;

            public String getItem() {
                return Item;
            }

            public void setItem(String Item) {
                this.Item = Item;
            }

            public String getType() {
                return Type;
            }

            public void setType(String Type) {
                this.Type = Type;
            }

            public String getFrom_Id() {
                return From_Id;
            }

            public void setFrom_Id(String From_Id) {
                this.From_Id = From_Id;
            }

            public String getFrom_Name() {
                return From_Name;
            }

            public void setFrom_Name(String From_Name) {
                this.From_Name = From_Name;
            }

            public String getTo_Id() {
                return To_Id;
            }

            public void setTo_Id(String To_Id) {
                this.To_Id = To_Id;
            }

            public String getTo_Name() {
                return To_Name;
            }

            public void setTo_Name(String To_Name) {
                this.To_Name = To_Name;
            }

            public String getContent() {
                return Content;
            }

            public void setContent(String Content) {
                this.Content = Content;
            }

            public String getCreateDate() {
                return CreateDate;
            }

            public void setCreateDate(String CreateDate) {
                this.CreateDate = CreateDate;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.Item);
                dest.writeString(this.Type);
                dest.writeString(this.From_Id);
                dest.writeString(this.From_Name);
                dest.writeString(this.To_Id);
                dest.writeString(this.To_Name);
                dest.writeString(this.Content);
                dest.writeString(this.CreateDate);
            }

            public ReplyContentsEntity() {
            }

            protected ReplyContentsEntity(Parcel in) {
                this.Item = in.readString();
                this.Type = in.readString();
                this.From_Id = in.readString();
                this.From_Name = in.readString();
                this.To_Id = in.readString();
                this.To_Name = in.readString();
                this.Content = in.readString();
                this.CreateDate = in.readString();
            }

            public static final Parcelable.Creator<ReplyContentsEntity> CREATOR = new Parcelable.Creator<ReplyContentsEntity>() {
                @Override
                public ReplyContentsEntity createFromParcel(Parcel source) {
                    return new ReplyContentsEntity(source);
                }

                @Override
                public ReplyContentsEntity[] newArray(int size) {
                    return new ReplyContentsEntity[size];
                }
            };
        }

        public static class PicturesEntity implements Parcelable {
            private String url;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.url);
            }

            public PicturesEntity() {
            }

            protected PicturesEntity(Parcel in) {
                this.url = in.readString();
            }

            public static final Parcelable.Creator<PicturesEntity> CREATOR = new Parcelable.Creator<PicturesEntity>() {
                @Override
                public PicturesEntity createFromParcel(Parcel source) {
                    return new PicturesEntity(source);
                }

                @Override
                public PicturesEntity[] newArray(int size) {
                    return new PicturesEntity[size];
                }
            };
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.ForumNo);
            dest.writeString(this.CustId);
            dest.writeString(this.CustName);
            dest.writeString(this.Usericon);
            dest.writeString(this.Content);
            dest.writeString(this.CreateDate);
            dest.writeString(this.Liked);
            dest.writeString(this.Likes);
            dest.writeString(this.Replies);
            dest.writeTypedList(this.ReplyContents);
            dest.writeTypedList(this.Pictures);
        }

        public DataEntity() {
        }

        protected DataEntity(Parcel in) {
            this.ForumNo = in.readString();
            this.CustId = in.readString();
            this.CustName = in.readString();
            this.Usericon = in.readString();
            this.Content = in.readString();
            this.CreateDate = in.readString();
            this.Liked = in.readString();
            this.Likes = in.readString();
            this.Replies = in.readString();
            this.ReplyContents = in.createTypedArrayList(ReplyContentsEntity.CREATOR);
            this.Pictures = in.createTypedArrayList(PicturesEntity.CREATOR);
        }

        public static final Parcelable.Creator<DataEntity> CREATOR = new Parcelable.Creator<DataEntity>() {
            @Override
            public DataEntity createFromParcel(Parcel source) {
                return new DataEntity(source);
            }

            @Override
            public DataEntity[] newArray(int size) {
                return new DataEntity[size];
            }
        };
    }
}

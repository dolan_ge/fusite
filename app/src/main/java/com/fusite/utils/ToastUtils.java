package com.fusite.utils;

import android.content.Context;
import android.widget.Toast;

import com.fusite.pile.MyApplication;

/**
 * Created by caolz on 2018/4/25.
 */

public class ToastUtils {
    private static Toast toast = null; //Toast的对象！

    public static void showToast(String str) {
        if (toast == null) {
            toast = Toast.makeText(MyApplication.getInstance(), str, Toast.LENGTH_SHORT);
        }
        else {
            toast.setText(str);
        }
        toast.show();
    }
}

package com.fusite.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.fusite.bean.Error;
import com.fusite.handlerinterface.NetErrorHandlerListener;

/**
 * Created by Jelly on 2016/3/29.
 */
public class ToastUtil {
    /**
     * 单例对象
     */
    private static ToastUtil toastUtil = new ToastUtil();

    /**
     * 获得单例对象
     * @return
     */
    public static ToastUtil getInstance(){
        return toastUtil;
    }

    private ToastUtil(){}

    /**
     * 输出错误信息
     * @param context
     * @param errorDao
     * @return 返回是否已经提示
     */
    public boolean toastError(Context context,Error errorDao,NetErrorHandlerListener netErrorHandlerListener){
        boolean isToast = false;
        if(errorDao.isToast()){
            isToast = true;
            try {
                Toast.makeText(context,errorDao.getMsg(),Toast.LENGTH_SHORT).show();
            }catch (Exception e){
                LogUtil.e("error",e.getMessage());
            }
        }
        if(netErrorHandlerListener != null) {
            netErrorHandlerListener.handlerError(errorDao.getReturnState());
        }
        return isToast;
    }

    /**
     * 输出错误信息
     * @param context
     * @param errorDao
     * @return 返回是否已经提示
     */
    public boolean toastError(Context context,Error errorDao,String noHintState,NetErrorHandlerListener netErrorHandlerListener){
        boolean isToast = false;
        if(errorDao.isToast()){
            isToast = true;
            if(!TextUtils.equals(errorDao.getReturnState(),noHintState)){
                Toast.makeText(context,errorDao.getMsg(),Toast.LENGTH_SHORT).show();
            }
        }
        if(netErrorHandlerListener != null) {
            netErrorHandlerListener.handlerError(errorDao.getReturnState());
        }
        return isToast;
    }

}

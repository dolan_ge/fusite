package com.fusite.utils;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.fusite.activities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity的控制器
 * Created by Jelly on 2016/3/1.
 */
public class ActivityControl {
    /**
     * TAG
     */
    public static String TAG="ActivityControl";
    /**
     * 保存Activity的集合
     */
    private static List<BaseActivity> list = new ArrayList<BaseActivity>();

    /**
     * 增加Activity
     * @param activity
     */
    public static void add(BaseActivity activity){
        list.add(activity);
        Log.v(TAG,"List中还有"+list.size()+"个Activity");
    }

    /**
     * 删除Activity
     * @param activity
     */
    public static void remove(BaseActivity activity){
        list.remove(activity);
        Log.v(TAG,"List中还剩" + list.size());
    }

    /**
     * 根据TAG结束某个Activity
     * @param tag
     */
    public static void finish(String tag){
        for (int i = 0; i < list.size(); i++) {
            BaseActivity activity = list.get(i);
            if(TextUtils.equals(tag,activity.getTAG())){
                activity.finish();
            }
        }
    }

    /**
     * 结束Activity,带ResultCode
     * @param tag
     * @param resultCode
     */
    public static void finishResult(String tag,int resultCode){
        for (int i = 0; i < list.size(); i++) {
            BaseActivity activity = list.get(i);
            if(TextUtils.equals(tag,activity.getTAG())){
                activity.setResult(resultCode);
                activity.finish();
            }
        }
    }

    /**
     * 结束Activity
     * @param tag
     * @param resultCode
     * @param intent
     */
    public static void finishResult(String tag, int resultCode, Intent intent){
        for (int i = 0; i < list.size(); i++) {
            BaseActivity activity = list.get(i);
            if(TextUtils.equals(tag,activity.getTAG())){
                activity.setResult(resultCode,intent);
                activity.finish();
            }
        }
    }

    /**
     * 结束所有Activity
     */
    public static void finishAll(){
        for(BaseActivity activity : list){
            list.remove(activity);
            activity.finish();
        }
    }
}

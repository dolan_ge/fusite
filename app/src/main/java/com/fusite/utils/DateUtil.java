package com.fusite.utils;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jelly on 2016/4/9.
 */
public class DateUtil {

    /**
     * 能够转换成Date的日期格式
     */
    public static final String dateFormat1 = "yyyy-MM-dd HH:mm:ss";

    public static final String dateFormat2 = "yyyy-MM-dd HH:mm";

    public static final String now = "刚刚";

    /**
     * 根据字符串获取日期对象
     *
     * @param dateStr
     * @return
     */
    public static Date getDate(String dateStr) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat1);
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            LogUtil.v("异常事件",dateStr);
        }
        return date;
    }

    /**
     * 获取指定格式的日期字符串
     *
     * @param date
     * @param sdfStr
     * @return
     */
    public static String getSdfDate(Date date, String sdfStr) {
        SimpleDateFormat sdf = new SimpleDateFormat(sdfStr);
        String dateStr = sdf.format(date);
        return dateStr;
    }


    public static Date getDate(String dateStr, String sdfStr) {
        SimpleDateFormat sdf = new SimpleDateFormat(sdfStr);
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 获取指定格式的日期字符串
     *
     * @param dateStr
     * @param sdfStr
     * @return
     */
    public static String getSdfDate(String dateStr, String sdfStr) {

        if(TextUtils.isEmpty(dateStr)){
            return "";
        }

        SimpleDateFormat sdf = new SimpleDateFormat(sdfStr);
        Date date = getDate(dateStr);
        if(date != null){
            String result = sdf.format(date);
            return result;
        }
        return dateStr;
    }


    /**
     * 计算两个日期之差
     *
     * @param dateStr1
     * @param dateStr2
     * @return
     */
    public static String DifferDate(String dateStr1, String dateStr2) {
        Date date1 = getDate(dateStr1);
        Date date2 = getDate(dateStr2);
        Long dateL = date1.getTime() - date2.getTime();
        Date dateResult = new Date(dateL);
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        String result = sdf.format(dateResult);
        if (TextUtils.equals(result, "00")) {
            result = "60";
        }
        return result;
    }

    public static String DifferDate(String dateStr1, String dateStr2, String sdfStr) {
        Date date1 = getDate(dateStr1, sdfStr);
        Date date2 = getDate(dateStr2, sdfStr);
        Long dateL = date1.getTime() - date2.getTime();
        Date dateResult = new Date(dateL);
        SimpleDateFormat sdf = new SimpleDateFormat("mm");
        String result = sdf.format(dateResult);
        if (TextUtils.equals(result, "00")) {
            result = "60";
        }
        return result;
    }


    /**
     * @param dateStr1
     * @param dateStr2
     * @return
     */
    public static String getTwoDateDay(String dateStr1, String dateStr2) {
        Date date1 = getDate(dateStr1, dateFormat2);
        Date date2 = getDate(dateStr2, dateFormat2);
        Long dateL = date1.getTime() - date2.getTime();
        long hourmm = 1 * 60 * 60 * 1000;
        int day = (int) (dateL / hourmm / 24);
        int hour = (int) (dateL / hourmm % 24);
        return day + "天" + hour + "小时";
    }


    public static String getTimeDistance(String dateStr) {
        Date date = getDate(dateStr);
        Long dateL = new Date().getTime() - date.getTime();
        int minute = (int) (dateL / (60 * 1000));
        if (minute == 0) {
            return "刚刚";
        } else if (minute < 60) {
            return minute + "分钟";
        } else {
            int hour = minute / 60;
            if (hour < 24) {
                return hour + "小时";
            } else {
                int day = hour / 24;
                if (day < 30) {
                    return day + "天";
                } else {
                    int month = day / 30;
                    if (month < 12) {
                        return month + "个月";
                    } else {
                        int year = month / 12;
                        return year + "年";
                    }
                }
            }
        }
    }

    /**
     * 获得月份
     */
    public static int getMonth(String dateStr) {
        Date date = getDate(dateStr);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH);
    }

    public static String getChineseMonth(String monthStr) {
        String chinaMonthStr = "";
        switch (monthStr) {
            case "1":
                chinaMonthStr = "二月份";
                break;
            case "2":
                chinaMonthStr = "三月份";
                break;
            case "3":
                chinaMonthStr = "四月份";
                break;
            case "4":
                chinaMonthStr = "五月份";
                break;
            case "5":
                chinaMonthStr = "六月份";
                break;
            case "6":
                chinaMonthStr = "七月份";
                break;
            case "7":
                chinaMonthStr = "八月份";
                break;
            case "8":
                chinaMonthStr = "九月份";
                break;
            case "9":
                chinaMonthStr = "十月份";
                break;
            case "10":
                chinaMonthStr = "十一月份";
                break;
            case "11":
                chinaMonthStr = "十二月份";
                break;
            case "12":
                chinaMonthStr = "一月份";
                break;
            default:
                chinaMonthStr = "月份";
                break;
        }
        return chinaMonthStr;
    }

}

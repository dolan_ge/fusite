package com.fusite.utils;

import com.fusite.bean.PileStation;
import com.fusite.bean.RentStations;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jelly on 2016/3/31.
 */
public class SearchUtil {
    private static String TAG = "SearchUtil";
    /**
     * 单例对象
     */
    private static SearchUtil searchUtil = new SearchUtil();
    /**
     * 获得单例对象
     * @return
     */
    public static SearchUtil getInstance(){
        return searchUtil;
    }

    public List<PileStation> searchListOnRegExp(String condition,List<PileStation> list){
        List<PileStation> daos = new ArrayList<>();
        for(int i=0;i<list.size();i++){
            PileStation dao = list.get(i);
            LogUtil.v(TAG,"开始匹配:"+dao.getCondition()+"条件："+condition);
            if(RegExpUtil.regChar(dao.getCondition(),condition)){
                LogUtil.v(TAG,"结果:成功");
                daos.add(dao);
            }
        }
        return daos;
    }

    public List<RentStations.DataEntity> searchRentListOnRegExp(String condition , List<RentStations.DataEntity> list){
        List<RentStations.DataEntity> daos = new ArrayList<>();
        for(int i=0;i<list.size();i++){
            RentStations.DataEntity dataEntity = list.get(i);
            if(RegExpUtil.regChar(dataEntity.getCondition(),condition)){
                daos.add(dataEntity);
            }
        }
        return daos;
    }
}

package com.fusite.utils;

import com.fruitknife.bean.BasicNameValuePair;
import com.fruitknife.util.XmlTransform;
import com.fusite.constant.Keys;
import com.tencent.mm.sdk.modelpay.PayReq;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Jelly on 2016/5/3.
 */
public class WeChatUtil {

    /**
     * 获取订单信息的xml文件
     * @param body
     * @param detail
     * @param total_fee
     * @param out_trade_no
     * @return xml
     */
    public static String getOrderInfo(String body,String detail,String total_fee,String out_trade_no,String attach){
        List<BasicNameValuePair> listMap = new ArrayList<>();
        listMap.add(new BasicNameValuePair("appid", Keys.AppId));
        listMap.add(new BasicNameValuePair("attach", attach));
        listMap.add(new BasicNameValuePair("body", body));
        listMap.add(new BasicNameValuePair("detail", detail));
//        listMap.add(new BasicNameValuePair("device_info", "WEB"));
        listMap.add(new BasicNameValuePair("fee_type", "CNY"));
        listMap.add(new BasicNameValuePair("goods_tag", "WXG"));
        listMap.add(new BasicNameValuePair("limit_pay", "no_credit"));
        listMap.add(new BasicNameValuePair("mch_id", Keys.MchId));
        listMap.add(new BasicNameValuePair("nonce_str", (int) (Math.random() * 100000) + 1000000 + ""));
        listMap.add(new BasicNameValuePair("notify_url", "http://47.100.97.221/firstev/wxPayNotify"));
//        listMap.add(new BasicNameValuePair("notify_url", "http://www.myscm.cn/dhz/wxPayNotify"));
        listMap.add(new BasicNameValuePair("out_trade_no", out_trade_no));
        listMap.add(new BasicNameValuePair("spbill_create_ip", "127.0.0.1"));
        String time = getTimeStamp();
        listMap.add(new BasicNameValuePair("time_expire", getTimeStampAdd(time,6)));
        listMap.add(new BasicNameValuePair("time_start", time));
        listMap.add(new BasicNameValuePair("total_fee", total_fee));
        listMap.add(new BasicNameValuePair("trade_type", "APP"));
        listMap.add(new BasicNameValuePair("sign", getSign(listMap)));
        String xmlStr = new XmlTransform().mapToXml(listMap);
        return xmlStr;
    }

    /**
     * 获得调起支付的签名
     * @return
     */
    public static String getRequestSign(PayReq request){
        List<BasicNameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("appid",request.appId));
        list.add(new BasicNameValuePair("noncestr",request.nonceStr));
        list.add(new BasicNameValuePair("package",request.packageValue));
        list.add(new BasicNameValuePair("partnerid",request.partnerId));
        list.add(new BasicNameValuePair("prepayid",request.prepayId));
        list.add(new BasicNameValuePair("timestamp",request.timeStamp));
        return getSign(list);
    }

    /**
     * 签名
     *
     * @param list
     * @return
     */
    public static  String getSign(List<BasicNameValuePair> list) {
        String stringA = "";
        for (BasicNameValuePair pair : list) {
            stringA += pair.getName() + "=" + pair.getValue() + "&";
        }
        stringA = stringA.substring(0, stringA.length() - 1);
        String stringSignTemp = stringA + "&key=" + Keys.Key;
        String sign = MD5.getMessageDigest(stringSignTemp.getBytes()).toUpperCase();
        return sign;
    }

    /**
     * 增加时间
     *
     * @return
     */
    public static String getTimeStampAdd(String time,int addValue) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.MINUTE, addValue);
        Date resultDate = c.getTime();
        return sdf.format(resultDate);
    }

    ;

    /**
     * 获得当前时间
     * @return
     */
    public static String getTimeStamp() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(date);
    }

    /**
     * 获得订单号
     * @return
     */
    public static String genOutTradNo() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(date);
    }

}

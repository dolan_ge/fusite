package com.fusite.utils;

import com.fusite.bean.DateSort;
import com.fusite.bean.DistanceSort;

import java.util.Collections;
import java.util.List;

/**
 * Created by Jelly on 2016/7/15.
 */
public class ListUtil {

    /**
     * 时间排序
     * @param list
     * @return
     */
    public static List<DateSort> sortTime(List<DateSort> list){
        Collections.sort(list);
        return list;
    }

    public static List<DistanceSort> sortDistance(List<DistanceSort> list){
        Collections.sort(list);
        return list;
    }

}

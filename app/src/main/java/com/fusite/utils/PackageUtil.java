package com.fusite.utils;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by Jelly on 2016/6/24.
 */
public class PackageUtil {

    public static float getVersionCode(Context context){
        float versionCode = 0;
        try {
            versionCode = Float.parseFloat(context.getPackageManager().getPackageInfo(context.getPackageName(),0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }
}

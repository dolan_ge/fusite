package com.fusite.utils;

import android.app.Dialog;
import android.content.Context;

import com.fusite.handlerinterface.DialogListener;
import com.fusite.view.CustomDialog;
import com.fusite.view.CustomProgressDialog;
import com.fusite.view.CustomSureDialog;

/**
 * Created by Jelly on 2016/3/26.
 */
public class DialogUtil {

    private static class DialogInstance{
        private static final DialogUtil dialogUtil = new DialogUtil();
    }

    public static DialogUtil getInstance(){
        return DialogInstance.dialogUtil;
    }

    private DialogUtil(){};

    /**
     * 显示确认按钮监听事件的Dialog
     * @param context
     * @param hint 提示
     * @param onClickListener 点击确定按钮的监听事件
     */
    public Dialog showSureListenerDialog(Context context,String hint,DialogListener onClickListener){
        CustomSureDialog customSureDialog = new CustomSureDialog(context);
        customSureDialog.setHintText(hint);
        customSureDialog.setOkClickListener(onClickListener);
        return customSureDialog.show();
    }

    /**
     * 显示确认按钮监听事件的Dialog
     * @param context
     * @param hint 提示
     * @param okOnClickListener 点击确定按钮的监听事件
     * @param cancelOnClickListener 点击关闭按钮的监听事件
     */
    public Dialog showSureListenerDialog(Context context, String hint, DialogListener okOnClickListener, DialogListener cancelOnClickListener){
        CustomSureDialog customSureDialog = new CustomSureDialog(context);
        customSureDialog.setHintText(hint);
        customSureDialog.setOkClickListener(okOnClickListener);
        customSureDialog.setCancelClickListener(cancelOnClickListener);
        return customSureDialog.show();
    }

    private CustomDialog initLoading(Context context, String hint){
        CustomProgressDialog customProgressDialog = new CustomProgressDialog(context,hint);
        customProgressDialog.show();
        return customProgressDialog;
    }

    private CustomDialog initLoading(Context context, int hint){
        CustomProgressDialog customProgressDialog = new CustomProgressDialog(context,hint);
        customProgressDialog.show();
        return customProgressDialog;
    }

    public CustomDialog startLoading(Context context, CustomDialog loading, int hint){
        if(loading == null){
            return initLoading(context,hint);
        }else{
            loading.show();
            return loading;
        }
    }

    public CustomDialog startLoading(Context context, CustomDialog loading, String hint){
        if(loading == null){
            return initLoading(context,hint);
        }else{
            loading.show();
            return loading;
        }
    }

    public void dismiss(CustomDialog dialog){
        if(dialog != null){
            dialog.dismiss();
        }
    }

}



package com.fusite.utils;

import android.content.Context;
import android.text.TextUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 缓存工具
 * Created by Jelly on 2016/6/22.
 */
public class HttpCacheUtil {
    public String TAG = "HttpCacheUtil";
    public String cachePath; //缓存路径
    public String cacheDirName = "HttpCache";//缓存目录名
    public File cacheFile;

    public HttpCacheUtil(Context context) {
        if(TextUtils.isEmpty(cachePath)){
            cachePath = context.getCacheDir().getPath() + File.separator + cacheDirName;
        }
        if(cacheFile == null){
            cacheFile = new File(cachePath);
            if(!cacheFile.exists()){
                if(cacheFile.mkdir()){
                    LogUtil.v(TAG,"创建目录成功");
                }else{
                    LogUtil.v(TAG,"创建目录失败");
                };
            }
        }

    }

    public boolean cacheHttp(String json,String apiName){
        try {
            File file = new File(cachePath+File.separator+apiName+".json");
            if(!file.exists()){
                file.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(json.getBytes());
            fileOutputStream.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public String readHttp(String apiName){
        try {
            File file = new File(cachePath+File.separator+apiName+".json");
            if(!file.exists()){
                return null;
            }
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] b = new byte[2048];
            int length = 0;
            String result = "";
            while ((length = fileInputStream.read(b)) != -1){
                result += new String(b,0,length);
            }
            fileInputStream.close();
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 是否存在缓存
     */
    public boolean isExistCache(String apiName){
        File file = new File(cachePath+File.separator+apiName+".json");
        if(file.exists()){
            return true;
        }else{
            return false;
        }
    }

}

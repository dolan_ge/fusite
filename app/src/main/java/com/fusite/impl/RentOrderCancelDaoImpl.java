package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.RentOrderCancel;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * 删除租车订单
 * Created by Jelly on 2016/3/9.
 */
public class RentOrderCancelDaoImpl extends BaseDaoImpl {

    public String TAG = "RentOrderCancelDaoImpl";
    /**
     * 操作对象
     */
    public RentOrderCancel rentOrderCancel;

    public RentOrderCancelDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.order.cancel";
    }

    public RentOrderCancelDaoImpl(Context context) {
        super(context);
    }


    public void getRentOrderCancel(String Token,String custid,String OrderNo){
        if(NetParam.isEmpty(Token,OrderNo,custid)){
            LogUtil.v(TAG,"参数为空");
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("Token", Token);
        conditionMap.put("OrderNo",OrderNo);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,custid,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    rentOrderCancel = JsonUtil.objectFromJson(s,RentOrderCancel.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

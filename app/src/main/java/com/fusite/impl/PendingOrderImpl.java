package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;

import com.fusite.bean.PendingOrder;
import com.fusite.param.NetErrorEnum;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.jellycai.service.ResponseResultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jelly on 2016/3/10.
 */
public class PendingOrderImpl extends BaseDaoImpl {

    public String TAG = "PendingOrderImpl";

    public List<PendingOrder> pendingOrderDaos = new ArrayList<PendingOrder>();

    /**
     * 充电中
     */
    public static final String CHARGING = "2";
    /**
     * 待支付
     */
    public static final String PAYING = "4";



    public PendingOrderImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evc.orders.get";
    }

    public PendingOrderImpl(Context context) {
        super(context);
    }


    /**
     * 从网络上获取数据
     * @param Token
     * @param custid
     */
    public void getNetPendingOrderList(final String Token, String custid){
        if(NetParam.isEmpty(Token,custid)){
            return;
        }
        conditionMap.clear();
        timestamp = NetParam.getTime();
        conditionMap.put("Token",Token);
        conditionMap.put("OrderStatus1",CHARGING);
        conditionMap.put("OrderStatus2",PAYING);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,custid,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b,String s) {
                Log.v(TAG,s.trim());
                if (NetParam.isSuccess(b,s)) {
                    pendingOrderDaos = JsonUtil.arrayFormJson(s, PendingOrder[].class);
                    handler.sendEmptyMessage(msg);
                }else {
                    PendingOrder pendingOrder = new PendingOrder();
                    PendingOrder.EvcOrdersGetEntity evcOrdersGetEntity = new PendingOrder.EvcOrdersGetEntity();
                    evcOrdersGetEntity.setIsSuccess("N");
                    evcOrdersGetEntity.setReturnStatus(NetErrorEnum.暂无数据.getState());
                    pendingOrder.setEvc_orders_get(evcOrdersGetEntity);
                    pendingOrderDaos.add(pendingOrder);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

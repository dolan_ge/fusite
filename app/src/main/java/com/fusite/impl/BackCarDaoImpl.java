package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.fusite.bean.BackCar;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * Created by Jelly on 2016/3/24.
 */
public class BackCarDaoImpl extends BaseDaoImpl {

    public String TAG = "BackCarDaoImpl";

    public BackCar backCar = new BackCar();
    /**
     * 余额支付
     */
    public static final String BalancePay = "1";
    /**
     * 支付宝支付
     */
    public static final String AliPay = "2";
    /**
     * 微信支付
     */
    public static final String WPay = "3";


    public BackCarDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.order.clearing";
    }

    public BackCarDaoImpl(Context context) {
        super(context);
    }

    /**
     * 获得消费账单
     */
    public void getBackCar(String Token,String custid,String orderNo,String payOrderNo,String type,String payPassword){
        if(NetParam.isEmpty(Token,custid,orderNo)){
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("Token",Token);
        conditionMap.put("OrderNo",orderNo);
        if(TextUtils.equals(BalancePay,type)){
            conditionMap.put("Type",type);
            conditionMap.put("PayPassword",payPassword);
        }else if(TextUtils.equals(AliPay,type)){
            conditionMap.put("Type",type);
            conditionMap.put("PayOrderNo",payOrderNo);
        }else if(TextUtils.equals(WPay,type)){
            conditionMap.put("Type",type);
            conditionMap.put("PayOrderNo",payOrderNo);
        }
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode, mode, timestamp, custid, sign_method, sign, execmode, fields, condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                Log.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    backCar = JsonUtil.objectFromJson(s,BackCar.class);
                    handler.sendEmptyMessage(msg);
                }

            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }


}

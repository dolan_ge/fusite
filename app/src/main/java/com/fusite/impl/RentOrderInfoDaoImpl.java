package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.RentOrderInfo;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * Created by Jelly on 2016/4/11.
 */
public class RentOrderInfoDaoImpl extends BaseDaoImpl{
    public String TAG = "RentOrderInfoDaoImpl";
    /**
     * 操作的数据
     */
    public RentOrderInfo rentOrderInfo;

    /**
     * 已还车
     */
    public static final String BackedCar = "4";
    /**
     * 未取出
     */
    public static final String NotPickCar = "1";
    /**
     * 使用中
     */
    public static final String Using = "2";
    /**
     * 关闭
     */
    public static final String Cancel = "16";

    public RentOrderInfoDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.order.get";
    }

    public RentOrderInfoDaoImpl(Context context) {
        super(context);
    }

    public void getRentOrderInfo(String token,String custId,String orderNo){
        if(NetParam.isEmpty(token,orderNo)){
            LogUtil.v(TAG,"参数为空");
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("Token",token);
        conditionMap.put("OrderNo",orderNo);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,custId,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    rentOrderInfo = JsonUtil.objectFromJson(s,RentOrderInfo.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

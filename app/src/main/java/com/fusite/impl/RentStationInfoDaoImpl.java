package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.RentStationInfo;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * Created by Jelly on 2016/4/11.
 */
public class RentStationInfoDaoImpl extends BaseDaoImpl{
    /**
     * TAG
     */
    public String TAG = "RentStationInfoDaoImpl";
    public RentStationInfo rentStationInfo;

    public RentStationInfoDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.station.get";
    }

    public RentStationInfoDaoImpl(Context context) {
        super(context);
    }

    public void getStationInfo(String OrgId,String CustId){
        if(NetParam.isEmpty(OrgId,CustId)){
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("OrgId",OrgId);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,CustId,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    rentStationInfo = JsonUtil.objectFromJson(s,RentStationInfo.class);
                }
                handler.sendEmptyMessage(msg);
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

package com.fusite.impl;

import android.content.Context;
import android.os.Handler;

import com.fusite.param.NetParam;
import com.jellycai.service.URLService;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * 基础Impl
 * Created by Jelly on 2016/3/3.
 */
public abstract class BaseDaoImpl {
    /**
     * TAG
     */
    public static String TAG = "BaseDaoImpl";

    protected WeakReference<Context> mContext;
    /**
     * 网络访问操作
     */
    protected URLService service;
    /**
     * 网络访问参数
     */
    protected Map<String,String> param;
    /**
     * 访问路径
     */
    protected String path = NetParam.path;
    /**
     * 接口标示
     */
    protected String trancode = "APPAPI000";
    /**
     * 端口
     */
    protected String mode = "4";
    /**
     * 时间戳
     */
    protected String timestamp;
    /**
     * 签名方法
     */
    protected String sign_method = "MD5";
    /**
     * 签名结果
     */
    protected String sign = "sign";
    /**
     * 接口名称
     */
    protected String execmode;
    /**
     * 返回字段
     */
    protected String fields = "fields";
    /**
     * 请求条件
     */
    protected String condition;
    /**
     * 参数
     */
    protected Map<String,String> conditionMap;
    protected int msg;
    protected Handler handler;
    /**
     * 网络请求成功
     */
    public static final String Success = "Y";
    /**
     * 网络请求有误
     */
    public static final String Failure = "N";

    /**
     * 需要访问网络数据，需要实现的构造函数
     * @param context
     * @param handler
     * @param msg
     */
    public BaseDaoImpl(Context context, Handler handler, int msg){
        this.mContext = new WeakReference<Context>(context);
        service = new URLService(mContext.get());
        conditionMap = new HashMap(); //拼接条件
        this.handler = handler;
        this.msg = msg;
    }

    /**
     * 只需要获取缓数据，需要实现的构造函数
     * @param context
     */
    public BaseDaoImpl(Context context){
        this.mContext = new WeakReference<Context>(context);
    }

}

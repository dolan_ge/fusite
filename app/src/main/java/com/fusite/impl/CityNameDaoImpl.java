package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;

import com.fusite.bean.CityName;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * 预约订单
 * Created by Jelly on 2016/3/9.
 */
public class CityNameDaoImpl extends BaseDaoImpl {
    /**
     * 操作对象
     */
    public CityName cityName;

    public CityNameDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.cityname.get";
    }

    public CityNameDaoImpl(Context context) {
        super(context);
    }


    public void getCityName(String CityCode){
        if(TextUtils.isEmpty(CityCode)){
            return;
        }
        conditionMap.clear();
        timestamp = NetParam.getTime();
        conditionMap.put("CityCode", CityCode);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,"1",sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                if(NetParam.isSuccess(b,s)){
                    cityName = JsonUtil.objectFromJson(s,CityName.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

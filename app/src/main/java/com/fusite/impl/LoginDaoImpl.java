package com.fusite.impl;

import android.content.Context;
import android.os.Handler;

import com.fusite.bean.Login;
import com.fusite.utils.SharedPreferencesUtil;

/**
 * 登录Impl
 * Created by Jelly on 2016/3/3.
 */
public class LoginDaoImpl extends BaseDaoImpl {

    private SharedPreferencesUtil sharedPreferencesUtil = SharedPreferencesUtil.getInstance();

    /**
     * 构造函数
     * @param context
     */
    public LoginDaoImpl(Context context,Handler handler,int msg){
        super(context,handler,msg);
    }

    public LoginDaoImpl(Context context){
        super(context);
    }

    /**
     * 缓存对象
     */
    public void cacheObject(Login loginDao){
        sharedPreferencesUtil.saveObject(mContext.get(), loginDao);
    }

    /**
     * 获取缓存对象
     * @return
     */
    public Login getCacheObject(){
        Login login = new Login();
        login = (Login) sharedPreferencesUtil.readObject(mContext.get(),login.getClass().getName());
        if(login == null){
            return null;
        }
        return login;
    }

    /**
     * 清除缓存
     */
    public void clearCache(){
        Login login = new Login();
        sharedPreferencesUtil.clearObject(mContext.get(), login.getClass().getName());
    }
}

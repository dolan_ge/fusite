package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;

import com.fusite.bean.DeleteRentOrder;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * Created by Jelly on 2016/3/22.
 */
public class DeleteRentOrderDaoImpl extends BaseDaoImpl {

    public String TAG = "DeleteRentOrderDaoImpl";

    /**
     * 操作数据
     */
    public DeleteRentOrder deleteRentOrder;


    public DeleteRentOrderDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.order.delete";
    }

    public DeleteRentOrderDaoImpl(Context context) {
        super(context);
    }

    /**
     * 获取数据
     */
    public void getDeleteRentOrder(String Token,String custid,String orderNo){
        if(NetParam.isEmpty(Token,custid,orderNo)){
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("Token",Token);
        conditionMap.put("OrderNo",orderNo);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode, mode, timestamp, custid, sign_method, sign, execmode, fields, condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                Log.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    deleteRentOrder = JsonUtil.objectFromJson(s,DeleteRentOrder.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

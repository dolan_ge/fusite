package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;

import com.fusite.bean.GetChargeAppoint;
import com.fusite.param.NetErrorEnum;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * Created by Jelly on 2016/3/10.
 */
public class GetChargeAppointDaoImpl extends BaseDaoImpl {
    public String TAG = "GetChargeAppointDaoImpl";
    /**
     * 操作对象
     */
    public GetChargeAppoint getChargeAppointDao;

    public GetChargeAppointDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evc.orders.get";
    }

    public GetChargeAppointDaoImpl(Context context) {
        super(context);
    }

    public void getNetGetChargeAppointDao(String Token,String custid){
        if(TextUtils.isEmpty(Token) || TextUtils.isEmpty(custid)){
            return;
        }
        conditionMap.clear();
        timestamp = NetParam.getTime();
        conditionMap.put("Token",Token);
        conditionMap.put("OrderStatus1","1");
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,custid,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)) {
                    getChargeAppointDao = JsonUtil.arrayFormJson(s,GetChargeAppoint[].class).get(0);
                    handler.sendEmptyMessage(msg);
                }else {
                    getChargeAppointDao = new GetChargeAppoint();
                    GetChargeAppoint.EvcOrdersGetEntity entity = new GetChargeAppoint.EvcOrdersGetEntity();
                    entity.setIsSuccess("N");
                    entity.setReturnStatus(NetErrorEnum.暂无数据.getState());
                    getChargeAppointDao.setEvc_orders_get(entity);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

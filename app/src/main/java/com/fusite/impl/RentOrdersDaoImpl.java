package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.RentOrders;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * 预约订单
 * Created by Jelly on 2016/3/9.
 */
public class RentOrdersDaoImpl extends BaseDaoImpl {

    public String TAG = "RentOrdersDaoImpl";

    /**
     * 操作对象
     */
    public RentOrders rentOrders;
    /**
     * 未完成订单，参数类型
     */
    public static final String PendingOrder = "0";
    /**
     * 已完成订单，参数类型
     */
    public static final String FinishOrder = "1";
    /**
     * 待取车
     */
    public static final String PendPickCar = "1";
    /**
     * 车辆使用中
     */
    public static final String Caring = "2";
    /**
     * 以还车
     */
    public static final String ReturnedCar = "4";
    /**
     * 已完成租车订
     */
    public static final String FinishRent = "8";
    /**
     * 已取消租车订单
     */
    public static final String CancelRent = "16";

    public RentOrdersDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.orders.get";
    }

    public RentOrdersDaoImpl(Context context) {
        super(context);
    }


    public void getRentOrders(String Token,String custid,String orderType){
        if(NetParam.isEmpty(Token,custid,orderType)){
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("Token", Token);
        conditionMap.put("OrderType",orderType);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,custid,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    rentOrders = JsonUtil.objectFromJson(s,RentOrders.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

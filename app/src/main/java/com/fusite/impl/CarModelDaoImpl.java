package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.CarModel;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * Created by Jelly on 2016/4/11.
 */
public class CarModelDaoImpl extends BaseDaoImpl{
    /**
     * 操作的数据
     */
    public CarModel carModel;

    public CarModelDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.model.get";
    }

    public CarModelDaoImpl(Context context) {
        super(context);
    }

    public void getCarModel(String modelId,String token,String custId){
        if(NetParam.isEmpty(modelId,token,custId)){
            LogUtil.v(TAG,"参数为空");
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("ModelId",modelId);
        conditionMap.put("Token",token);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,custId,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    carModel = JsonUtil.objectFromJson(s,CarModel.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

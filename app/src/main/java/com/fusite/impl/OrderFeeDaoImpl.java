package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.OrderFee;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * 预约订单
 * Created by Jelly on 2016/3/9.
 */
public class OrderFeeDaoImpl extends BaseDaoImpl {

    public String TAG = "OrderFeeDaoImpl";

    /**
     * 操作对象
     */
    public OrderFee orderFee;


    public OrderFeeDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.orderfee.get";
    }

    public OrderFeeDaoImpl(Context context) {
        super(context);
    }


    public void getRentOrders(String Token,String custid,String orgId,String returnOrgId,String rentBeginTime,String rentEndTime,String modelId){
        if(NetParam.isEmpty(Token,custid,orgId,returnOrgId,rentBeginTime,rentEndTime,modelId)){
            LogUtil.v(TAG,"参数为空");
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("Token", Token);
        conditionMap.put("OrgId", orgId);
        conditionMap.put("ReturnOrgId",returnOrgId);
        conditionMap.put("RentBeginTime",rentBeginTime);
        conditionMap.put("RentEndTime",rentEndTime);
        conditionMap.put("ModelId",modelId);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,custid,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    orderFee = JsonUtil.objectFromJson(s,OrderFee.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

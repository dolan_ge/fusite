package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.StationInfo;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * Created by Jelly on 2016/4/11.
 */
public class StationInfoDaoImpl extends BaseDaoImpl{
    /**
     * TAG
     */
    public String TAG = "StationInfoDaoImpl";

    public StationInfo stationInfo;

    public static final String isCollect = "1";
    public static final String noCollect = "0";


    public StationInfoDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evc.station.get";
    }

    public StationInfoDaoImpl(Context context) {
        super(context);
    }

    public void getStationInfo(String OrgId,String CustId){
        if(NetParam.isEmpty(OrgId)){
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("OrgId",OrgId);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,CustId,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    stationInfo = JsonUtil.objectFromJson(s,StationInfo.class);
                }
                handler.sendEmptyMessage(msg);
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

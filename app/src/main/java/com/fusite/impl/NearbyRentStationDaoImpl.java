package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.NearbyRentStation;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * 获取某个地区的服务点
 * Created by Jelly on 2016/4/11.
 */
public class NearbyRentStationDaoImpl extends BaseDaoImpl{
    /**
     * 操作的数据
     */
    public NearbyRentStation nearbyRentStation;

    public NearbyRentStationDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = RequestParam.NearbyRentStation.InterfaceName;
    }

    public NearbyRentStationDaoImpl(Context context) {
        super(context);
    }

    public void getNearbyRentStation(String Position){
        if(NetParam.isEmpty(Position)){
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put(RequestParam.NearbyRentStation.Position,Position);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,"1",sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    nearbyRentStation = JsonUtil.objectFromJson(s,NearbyRentStation.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.CarModels;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * Created by Jelly on 2016/4/11.
 */
public class CarModelsDaoImpl extends BaseDaoImpl{

    public String TAG = "CarModelsDaoImpl";
    public CarModels carModels;

    public CarModelsDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.models.get";
    }

    public CarModelsDaoImpl(Context context) {
        super(context);
    }

    public void getCarModels(String OrgId, String Token, String CustId){
        if(NetParam.isEmpty(OrgId,Token,CustId)){
            LogUtil.v(TAG,"参数为空");
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("OrgId",OrgId);
        conditionMap.put("Token",Token);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,CustId,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                if(NetParam.isSuccess(b,s)){
                    carModels = JsonUtil.objectFromJson(s,CarModels.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

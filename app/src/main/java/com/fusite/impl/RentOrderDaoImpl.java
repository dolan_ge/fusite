package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.RentOrder;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * 预约订单
 * Created by Jelly on 2016/3/9.
 */
public class RentOrderDaoImpl extends BaseDaoImpl {

    public String TAG = "RentOrderDaoImpl";

    public RentOrder rentOrder;

    public RentOrderDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.order.set";
    }

    public RentOrderDaoImpl(Context context) {
        super(context);
    }


    public void getRentOrder(String token,String custId,String orgId,String returnOrgId,String rentBeginTime,String rentEndTime,String modelId){
        if(NetParam.isEmpty(token,custId,orgId,returnOrgId,rentBeginTime,rentEndTime,modelId)){
            LogUtil.v(TAG,"参数为空");
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("Token", token);
        conditionMap.put("OrgId",orgId);
        conditionMap.put("ReturnOrgId",returnOrgId);
        conditionMap.put("RentBeginTime",rentBeginTime);
        conditionMap.put("RentEndTime",rentEndTime);
        conditionMap.put("ModelId",modelId);
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,custId,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    rentOrder = JsonUtil.objectFromJson(s,RentOrder.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

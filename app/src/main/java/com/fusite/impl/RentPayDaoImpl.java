package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.RentPay;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * Created by Jelly on 2016/4/11.
 */
public class RentPayDaoImpl extends BaseDaoImpl{
    /**
     * 操作的数据
     */
    public RentPay rentPay;

    public static final int BalancePay = 1;

    public static final int AliPay = 2;

    public static final int WPay = 3;

    public RentPayDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.order.pay";
    }

    public RentPayDaoImpl(Context context) {
        super(context);
    }

    public void getRentPay(String token,String custId,String orderNo,int type,String payPassword,String payOrderNo){
        if(NetParam.isEmpty(token,orderNo)){
            LogUtil.v(TAG,"参数为空");
            return;
        }
        timestamp = NetParam.getTime();
        conditionMap.clear();
        conditionMap.put("Token",token);
        conditionMap.put("OrderNo",orderNo);
        conditionMap.put("Type",type+"");
        if(type == BalancePay){
            conditionMap.put("PayPassword",payPassword);
        }else if(type == AliPay){
            conditionMap.put("PayOrderNo",payOrderNo);
        }else if(type == WPay){
            conditionMap.put("PayOrderNo",payOrderNo);
        }
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode,mode,timestamp,custId,sign_method,sign,execmode,fields,condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                LogUtil.v(TAG,s.trim());
                if(NetParam.isSuccess(b,s)){
                    rentPay = JsonUtil.objectFromJson(s,RentPay.class);
                    handler.sendEmptyMessage(msg);
                }
            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

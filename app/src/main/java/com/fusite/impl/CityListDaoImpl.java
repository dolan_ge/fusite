package com.fusite.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;

import com.fusite.bean.CityList;
import com.fusite.param.NetParam;
import com.fusite.utils.JsonUtil;
import com.jellycai.service.ResponseResultHandler;

/**
 * 获取
 * Created by Jelly on 2016/3/23.
 */
public class CityListDaoImpl extends BaseDaoImpl {
    /**
     * 操作数据
     */
    public CityList citylist;

    public CityListDaoImpl(Context context, Handler handler, int msg) {
        super(context, handler, msg);
        execmode = "evr.citylist.get";
    }

    public CityListDaoImpl(Context context) {
        super(context);
    }

    /**
     * 获得数据
     */
    public void getCityList(){
        conditionMap.clear();
        timestamp = NetParam.getTime();
        condition = NetParam.spliceCondition(conditionMap);
        param = NetParam.getParamMap(trancode, mode, timestamp, "1", sign_method, sign, execmode, fields, condition);
        service.doPost(path, param, new ResponseResultHandler() {
            @Override
            public void response(boolean b, String s) {
                if(NetParam.isSuccess(b,s)){
                    citylist = JsonUtil.objectFromJson(s,CityList.class);
                    handler.sendEmptyMessage(msg);
                }

            }

            @Override
            public void responseBitmap(boolean b, Bitmap bitmap) {

            }
        });
    }

}

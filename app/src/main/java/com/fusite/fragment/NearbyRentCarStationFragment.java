package com.fusite.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.adpater.DistrictListAdapter;
import com.fusite.adpater.RentStationListAdapter;
import com.fusite.bean.Error;
import com.fusite.bean.NearbyRentStation;
import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.NetErrorHandlerListener;
import com.fusite.impl.NearbyRentStationDaoImpl;
import com.fusite.map.MyLocation;
import com.fusite.param.NetErrorEnum;
import com.fusite.param.RequestParam;
import com.fusite.pile.R;
import com.fusite.pile.RentCarSelectCityActivity;
import com.fusite.pile.RentStationInfoActivity;
import com.fusite.utils.HttpCacheUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 附近租车
 * Created by Jelly on 2016/4/12.
 */
public class NearbyRentCarStationFragment extends UtilFragment implements NetErrorHandlerListener {

    public String TAG = "NearbyRentCarStationFragment";
    @BindView(R.id.districtList)
    ListView districtList;
    @BindView(R.id.stationList)
    ListView stationList;
    @BindView(R.id.city)
    TextView city;

    private NearbyRentStationDaoImpl nearbyRentStationDao;

    private static final int msgNearby = 0x001;

    private DistrictListAdapter districtListAdapter;

    private RentStationListAdapter rentStationListAdapter;

    public static final int SelectCityRequestCode = 0x002;

    private int currDistrict = 0;

    private String cityCode;

    private HttpCacheUtil httpCacheUtil;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.nearby_rentcar_station, container, false);
            unbinder = ButterKnife.bind(this, root);
            init();
            loadObjectAttribute();
            setListener();
            setFragView();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }

        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {
        httpCacheUtil = new HttpCacheUtil(context);
        nearbyRentStationDao = new NearbyRentStationDaoImpl(context, new MyHandler(this), msgNearby);
    }

    @Override
    public void setListener() {
        setDistrictListItemListener();
        setStationListItemListener();
    }

    @Override
    public void setFragView() {

        if(httpCacheUtil.isExistCache(RequestParam.NearbyRentStation.InterfaceName)){
            nearbyRentStationDao.nearbyRentStation = JsonUtil.objectFromJson(httpCacheUtil.readHttp(RequestParam.NearbyRentStation.InterfaceName),NearbyRentStation.class);
            districtListAdapter = new DistrictListAdapter(nearbyRentStationDao.nearbyRentStation.getData(), context);
            districtList.setAdapter(districtListAdapter);
            rentStationListAdapter = new RentStationListAdapter(nearbyRentStationDao.nearbyRentStation.getData().get(0).getStations(), context);
            stationList.setAdapter(rentStationListAdapter);
        }

        MyLocation location = application.getLocation();

        if(location == null){
            Toast.makeText(context,"无法定位",Toast.LENGTH_SHORT).show();
            return;
        }

        String adCode = location.getAdCode();
        cityCode = adCode.substring(0, adCode.length() - 2) + "00";
        nearbyRentStationDao.getNearbyRentStation(cityCode);

        city.setText(location.getCity());
    }


    public void setDistrictListItemListener() {
        districtList.setOnItemClickListener((parent, view, position, id) -> {
            currDistrict = position;
            districtListAdapter.setSelectItem(position);
            districtListAdapter.notifyDataSetChanged();
            List<NearbyRentStation.DataEntity> dataEntity = nearbyRentStationDao.nearbyRentStation.getData();
            rentStationListAdapter.setList(dataEntity.get(position).getStations());
            rentStationListAdapter.notifyDataSetChanged();
        });
    }

    public void setStationListItemListener(){
        stationList.setOnItemClickListener((parent, view, position, id) -> {
            NearbyRentStation.DataEntity.StationsEntity stationsEntity = nearbyRentStationDao.nearbyRentStation.getData().get(currDistrict).getStations().get(position);
            RentStationInfoActivity.startActivity(context,stationsEntity.getOrgId());
        });
    }

    @OnClick(R.id.city_layout)
    public void onClick() {
        Intent intent = new Intent(this.getContext(), RentCarSelectCityActivity.class);
        getParentFragment().startActivityForResult(intent, SelectCityRequestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtil.v(TAG, "回调");
        switch (requestCode) {
            case SelectCityRequestCode:
                if (resultCode == RentCarSelectCityActivity.Success) {
                    nearbyRentStationDao.getNearbyRentStation(data.getStringExtra("CityCode"));
                    city.setText(data.getStringExtra("City"));
                    cityCode = data.getStringExtra("CityCode");
                    currDistrict = 0;
                } else {

                }
                break;
        }
    }

    private static class MyHandler extends Handler{
        private WeakReference<NearbyRentCarStationFragment> mFragment;

        private MyHandler(NearbyRentCarStationFragment fragment){
            mFragment = new WeakReference<NearbyRentCarStationFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            NearbyRentCarStationFragment fragment = mFragment.get();
            if(fragment == null){
                return;
            }
            switch (msg.what) {
                case msgNearby:
                    if (TextUtils.equals("N", fragment.nearbyRentStationDao.nearbyRentStation.getIsSuccess())) {
                        Error error = fragment.errorParamUtil.checkReturnState(fragment.nearbyRentStationDao.nearbyRentStation.getReturnStatus());
                        fragment.toastUtil.toastError(fragment.context, error, fragment);
                        return;
                    }
                    fragment.districtListAdapter = new DistrictListAdapter(fragment.nearbyRentStationDao.nearbyRentStation.getData(), fragment.context);
                    fragment.districtList.setAdapter(fragment.districtListAdapter);
                    fragment.rentStationListAdapter = new RentStationListAdapter(fragment.nearbyRentStationDao.nearbyRentStation.getData().get(0).getStations(), fragment.context);
                    fragment.stationList.setAdapter(fragment.rentStationListAdapter);
                    //缓存
                    fragment.httpCacheUtil.cacheHttp(JsonUtil.Object2Json(fragment.nearbyRentStationDao.nearbyRentStation),RequestParam.NearbyRentStation.InterfaceName);
                    break;
            }
        }
    }

    @Override
    public void handlerError(String returnState) {
        if (TextUtils.equals(returnState, NetErrorEnum.暂无数据.getState())) {
            if(districtListAdapter != null){
                districtListAdapter.clearList();
                districtListAdapter.notifyDataSetChanged();
            }
            if(rentStationListAdapter != null){
                rentStationListAdapter.clearList();
                rentStationListAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

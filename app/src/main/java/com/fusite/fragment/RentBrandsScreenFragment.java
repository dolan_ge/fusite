package com.fusite.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.fusite.adpater.RentScreenListAdapter;
import com.fusite.bean.CarModels;
import com.fusite.bean.Screen;
import com.fusite.fragments.UtilFragment;
import com.fusite.pile.R;
import com.fusite.pile.RentCarScreenCarActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 租车价格筛选
 * Created by Jelly on 2016/4/20.
 */
public class RentBrandsScreenFragment extends UtilFragment {

    public String TAG = "RentBrandsScreenFragment";
    @BindView(R.id.list)
    ListView list;

    private List<CarModels.DataEntity> modelList;

    private List<Screen> dataList = new ArrayList<>();

    private RentScreenListAdapter adapter;

    public static final String Brands1 = "品牌不限";
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        if(root == null){
            root = LayoutInflater.from(context).inflate(R.layout.rent_screen, container,false);
        }
        unbinder = ButterKnife.bind(this, root);
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void loadObjectAttribute() {
        modelList = ((RentCarScreenCarActivity)getActivity()).getList();
        if(dataList.size() == 0){
            if(TextUtils.equals(((RentCarScreenCarActivity)getActivity()).getBrandScreen(),Brands1)){
                dataList.add(new Screen(Brands1,true));
            }else {
                dataList.add(new Screen(Brands1, false));
            }
        }
        boolean flag = false;
        for(int i=0;i<modelList.size();i++){
            flag = false;
            for(int b=0;b<dataList.size();b++){
                if(TextUtils.equals(modelList.get(i).getBrand(),dataList.get(b).getTitle())){
                    flag = true;
                    break;
                }
            }
            if(!flag){
                if(TextUtils.equals(modelList.get(i).getBrand(),((RentCarScreenCarActivity)getActivity()).getBrandScreen())){
                    dataList.add(new Screen(modelList.get(i).getBrand(),true));
                } else {
                    dataList.add(new Screen(modelList.get(i).getBrand(), false));
                }
            }
        }
        adapter = new RentScreenListAdapter(context,dataList);
    }

    @Override
    public void setListener() {
        setScreenItemListener();
    }

    @Override
    public void setFragView() {
        list.setAdapter(adapter);
    }

    public void setScreenItemListener(){
        list.setOnItemClickListener((parent, view, position, id) -> {
            adapter.selectRadio(position);
            adapter.notifyDataSetChanged();
            RentCarScreenCarActivity screenCarActivity = (RentCarScreenCarActivity) getActivity();
            screenCarActivity.setBrandScreen(dataList.get(position).getTitle());
        });
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

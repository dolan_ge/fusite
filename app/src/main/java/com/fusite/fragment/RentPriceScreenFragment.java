package com.fusite.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.fusite.adpater.RentScreenListAdapter;
import com.fusite.bean.Screen;
import com.fusite.fragments.UtilFragment;
import com.fusite.pile.R;
import com.fusite.pile.RentCarScreenCarActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 租车价格筛选
 * Created by Jelly on 2016/4/20.
 */
public class RentPriceScreenFragment extends UtilFragment {

    public String TAG = "RentPriceScreenFragment";
    @BindView(R.id.list)
    ListView list;

    private RentScreenListAdapter adapter;

    private List<Screen> dataList = new ArrayList<>();

    public static final String Price1 = "价格不限";

    public static final String Price2 = "¥150以下";

    public static final String Price3 = "¥150-¥300";

    public static final String Price4 = "¥300-¥500";

    public static final String Price5 = "¥500以上";
    private Unbinder unbinder;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        if(root == null){
            root = LayoutInflater.from(context).inflate(R.layout.rent_screen, container,false);
            unbinder = ButterKnife.bind(this, root);
            lazyLoad();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void loadObjectAttribute() {
        if(dataList.size() == 0){
            if(TextUtils.equals(((RentCarScreenCarActivity)getActivity()).getPriceScreen(),Price1)){
                dataList.add(new Screen(Price1,true));
            }else{
                dataList.add(new Screen(Price1,false));
            }
            if(TextUtils.equals(((RentCarScreenCarActivity)getActivity()).getPriceScreen(),Price2)){
                dataList.add(new Screen(Price2,true));
            }else{
                dataList.add(new Screen(Price2,false));
            }
            if(TextUtils.equals(((RentCarScreenCarActivity)getActivity()).getPriceScreen(),Price3)){
                dataList.add(new Screen(Price3,true));
            }else{
                dataList.add(new Screen(Price3,false));
            }
            if(TextUtils.equals(((RentCarScreenCarActivity)getActivity()).getPriceScreen(),Price4)){
                dataList.add(new Screen(Price4,true));
            }else{
                dataList.add(new Screen(Price4,false));
            }
            if(TextUtils.equals(((RentCarScreenCarActivity)getActivity()).getPriceScreen(),Price5)){
                dataList.add(new Screen(Price5,true));
            }else{
                dataList.add(new Screen(Price5,false));
            }
        }
        if(adapter == null){
            adapter = new RentScreenListAdapter(context, dataList);
        }
    }

    @Override
    public void setListener() {
        setScreenItemListener();
    }

    @Override
    public void setFragView() {
        list.setAdapter(adapter);
    }

    /**
     * 设置筛选监听Item
     */
    public void setScreenItemListener(){
        list.setOnItemClickListener((parent, view, position, id) -> {
            adapter.selectRadio(position);
            adapter.notifyDataSetChanged();
            RentCarScreenCarActivity screenCarActivity = (RentCarScreenCarActivity) getActivity();
            screenCarActivity.setPriceScreen(dataList.get(position).getTitle());
        });
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

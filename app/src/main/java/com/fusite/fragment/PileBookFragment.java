package com.fusite.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.bean.Error;
import com.fusite.bean.GetChargeAppoint;
import com.fusite.bean.Login;
import com.fusite.bean.PileInfo;
import com.fusite.bean.StartCharge;
import com.fusite.constant.TypeDefined;
import com.fusite.fragments.UtilFragment;
import com.fusite.impl.FinishOrderDaoImpl;
import com.fusite.impl.GetChargeAppointDaoImpl;
import com.fusite.impl.PileInfoDaoImpl;
import com.fusite.impl.StartChargeDaoImpl;
import com.fusite.map.MyLocation;
import com.fusite.pile.NaviEmulatorActivity;
import com.fusite.pile.NewPileOperActivity;
import com.fusite.pile.R;
import com.fusite.utils.DateUtil;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 我的预约充电预约
 * Created by Jelly on 2016/3/10.
 */
public class PileBookFragment extends UtilFragment implements View.OnClickListener {
    /**
     * TAG
     */
    public String TAG = "PileBookFragment";
    @BindView(R.id.orgName)
    TextView orgName;
    @BindView(R.id.addr)
    TextView addr;
    @BindView(R.id.facilityName)
    TextView facilityName;
    @BindView(R.id.facilityType)
    TextView facilityType;
    @BindView(R.id.orderNo)
    TextView orderNo;
    @BindView(R.id.facilityMode)
    TextView facilityMode;
    @BindView(R.id.appoint_price_text)
    TextView appointPriceText;
    @BindView(R.id.phone_text)
    TextView phoneText;
    @BindView(R.id.start)
    TextView start;
    @BindView(R.id.nodata_show)
    LinearLayout nodataShow;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.startTime)
    TextView startTime;
    @BindView(R.id.endTime)
    TextView endTime;

    /**
     * GetChargeAppointDaoImpl
     */
    private GetChargeAppointDaoImpl getChargeAppointDao;
    /**
     * 获取预约消息
     */
    private static final int msgGetAppoint = 0x001;
    /**
     * 结束订单
     */
    private FinishOrderDaoImpl finishOrderDao;
    /**
     * 结束订单消息
     */
    private static final int msgFinish = 0x002;
    /**
     * StartChargeDaoImpl
     */
    private StartChargeDaoImpl startChargeDao;
    /**
     * 开始充电消息
     */
    private static final int msgStart = 0x003;
    /**
     * 加载数据PopupWindow
     */
    private CustomDialog loading;
    /**
     * 电桩详情
     */
    private PileInfoDaoImpl pileInfoDao;
    /**
     * 电桩详情消息
     */
    private static final int msgPileInfo = 0x005;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.pile_book_order, container, false);
            unbinder = ButterKnife.bind(this, root);
            lazyLoad();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }
        ViewGroup parent = (ViewGroup) root.getParent();
        if (parent != null) {
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void loadObjectAttribute() {
        MyHandler handler = new MyHandler(this);
        getChargeAppointDao = new GetChargeAppointDaoImpl(context, handler, msgGetAppoint);
        finishOrderDao = new FinishOrderDaoImpl(context, handler, msgFinish);
        startChargeDao = new StartChargeDaoImpl(context, handler, msgStart);
        pileInfoDao = new PileInfoDaoImpl(context, handler, msgPileInfo);
    }

    @Override
    public void setFragView() {
        loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
        Login.DataEntity data = application.getLogin().getData();
        getChargeAppointDao.getNetGetChargeAppointDao(data.getToken(), data.getCustID());
    }

    @OnClick({R.id.start, R.id.cancel, R.id.navigation, R.id.phone_btn})
    public void onClick(View view) {
        Login.DataEntity entity = application.getLogin().getData(); //用户数据
        switch (view.getId()) {
            case R.id.start:
                //开始充电,在开始充电之前
                if (getChargeAppointDao.getChargeAppointDao == null || TextUtils.equals(getChargeAppointDao.getChargeAppointDao.getEvc_orders_get().getIsSuccess(), "N")) {
                    Toast.makeText(context, "对不起，暂时无法开始充电！", Toast.LENGTH_SHORT).show();
                    return;
                }
                startChargeDao.getStartChargeDao(entity.getToken(), getChargeAppointDao.getChargeAppointDao.getEvc_orders_get().getOrderNo(), entity.getCustID());
                break;
            case R.id.cancel:
                if (getChargeAppointDao.getChargeAppointDao == null || TextUtils.equals(getChargeAppointDao.getChargeAppointDao.getEvc_orders_get().getIsSuccess(), "N")) {
                    Toast.makeText(context, "对不起，此订单暂时无法结束！", Toast.LENGTH_SHORT).show();
                    return;
                }
                dialogUtil.showSureListenerDialog(context, "取消预约后预约费不会退回,是否需要取消预约！", (dialog,index) -> {
                    dialog.dismiss();
                    Login.DataEntity entity1 = application.getLogin().getData();
                    finishOrderDao.getFinishOrderDao(entity1.getToken(), getChargeAppointDao.getChargeAppointDao.getEvc_orders_get().getOrderNo(), entity1.getCustID());
                });
                break;
            case R.id.navigation:
                MyLocation location = application.getLocation();
                GetChargeAppoint.EvcOrdersGetEntity entity1 = getChargeAppointDao.getChargeAppointDao.getEvc_orders_get();
                NaviEmulatorActivity.startAppActivity(context, Double.parseDouble(location.getLatitude()), Double.parseDouble(location.getLongitude()), Double.parseDouble(entity1.getLatitude()), Double.parseDouble(entity1.getLongitude()));
                break;
            case R.id.phone_btn:
                if(TextUtils.isEmpty(phoneText.getText())){
                    return;
                }
                Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:"+phoneText.getText().toString()));
                startActivity(intent);
                break;
        }
    }

    private static class MyHandler extends Handler{
        private WeakReference<PileBookFragment> mFragment;

        public MyHandler(PileBookFragment fragment){
            mFragment = new WeakReference<PileBookFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            PileBookFragment fragment = mFragment.get();
            if(fragment == null){
                return;
            }
            switch (msg.what) {
                case msgGetAppoint: //获得预约数据
                    if (TextUtils.equals(fragment.getChargeAppointDao.getChargeAppointDao.getEvc_orders_get().getIsSuccess(), "N")) {
                        if(fragment.nodataShow != null){
                            fragment.nodataShow.setVisibility(View.VISIBLE);
                        }
                        fragment.dialogUtil.dismiss(fragment.loading);
                        return;
                    }
                    fragment.nodataShow.setVisibility(View.GONE);
                    GetChargeAppoint.EvcOrdersGetEntity entity = fragment.getChargeAppointDao.getChargeAppointDao.getEvc_orders_get();
                    fragment.orderNo.setText(entity.getOrderNo());
                    fragment.phoneText.setText(entity.getTel());
                    fragment.appointPriceText.setText("¥" + entity.getPlanCost());
                    fragment.pileInfoDao.getPileInfoDao(entity.getFacilityID(), PileInfoDaoImpl.FACILITYID);
                    fragment.startTime.setText(DateUtil.getSdfDate(entity.getPlanBeginDateTime(), "HH:mm"));
                    fragment.endTime.setText(DateUtil.getSdfDate(entity.getPlanEndDateTime(), "HH:mm"));
                    if (TextUtils.equals("00", DateUtil.DifferDate(entity.getPlanEndDateTime(), entity.getPlanBeginDateTime()))) {
                        fragment.time.setText("60");
                    } else {
                        fragment.time.setText(DateUtil.DifferDate(entity.getPlanEndDateTime(), entity.getPlanBeginDateTime()));
                    }
                    break;
                case msgFinish: //结束预约
                    if (TextUtils.equals(fragment.finishOrderDao.finishOrderDao.getEvc_order_cancel().getIsSuccess(), "N")) {
                        Error errorDao = fragment.errorParamUtil.checkReturnState(fragment.finishOrderDao.finishOrderDao.getEvc_order_cancel().getReturnStatus());
                        fragment.toastUtil.toastError(fragment.context, errorDao, null);
                        return;
                    }
                    Toast.makeText(fragment.context, "成功取消预约！", Toast.LENGTH_SHORT).show();
                    fragment.getActivity().finish();
                    break;
                case msgStart: //开始充电
                    if (fragment.startChargeDao.startChargeDao == null || TextUtils.equals(fragment.startChargeDao.startChargeDao.getEvc_order_change().getIsSuccess(), "N")) {
                        Error errorDao = fragment.errorParamUtil.checkReturnState(fragment.startChargeDao.startChargeDao.getEvc_order_change().getReturnStatus());
                        fragment.toastUtil.toastError(fragment.context, errorDao, null);
                        return;
                    }
                    fragment.getActivity().finish();
                    StartCharge.EvcOrderChangeEntity entity1 = fragment.startChargeDao.startChargeDao.getEvc_order_change();
                    NewPileOperActivity.startActivity(fragment.context, entity1);
                    break;
                case msgPileInfo:
                    fragment.dialogUtil.dismiss(fragment.loading);
                    if (TextUtils.equals("N", fragment.pileInfoDao.pileInfoDao.getEvc_facility_get().getIsSuccess())) {
                        Error error = fragment.errorParamUtil.checkReturnState(fragment.pileInfoDao.pileInfoDao.getEvc_facility_get().getReturnStatus());
                        fragment.toastUtil.toastError(fragment.context, error, null);
                        return;
                    }
                    PileInfo.EvcFacilityGetEntity evcFacilityGetEntity = fragment.pileInfoDao.pileInfoDao.getEvc_facility_get();
                    fragment.orgName.setText(evcFacilityGetEntity.getOrgName());
                    fragment.facilityName.setText(evcFacilityGetEntity.getFacilityName());
                    if (TextUtils.equals(TypeDefined.FACILITY_TYPE_DC, evcFacilityGetEntity.getFacilityType())) {
                        fragment.facilityType.setText("直流桩");
                    } else if (TextUtils.equals(TypeDefined.FACILITY_TYPE_AC, evcFacilityGetEntity.getFacilityType())) {
                        fragment.facilityType.setText("交流桩");
                    }
                    fragment.addr.setText(evcFacilityGetEntity.getAddr());
                    break;
            }
        }
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

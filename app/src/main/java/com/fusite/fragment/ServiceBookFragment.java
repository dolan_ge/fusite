package com.fusite.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.bean.GetServiceOrder;
import com.fusite.bean.Login;
import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.PayActivity;
import com.fusite.pile.R;
import com.fusite.utils.DoubleUtil;
import com.fusite.view.CustomDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 服务预约
 * Created by Jelly on 2016/3/10.
 */
public class ServiceBookFragment extends UtilFragment {
    /**
     * TAG
     */
    public String TAG = "ServiceBookFragment";
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.orderId)
    TextView orderId;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.nodata_show)
    LinearLayout nodataShow;
    @BindView(R.id.serviceAmt)
    TextView serviceAmt;
    @BindView(R.id.deposit)
    TextView deposit;
    @BindView(R.id.pay)
    TextView pay;
    @BindView(R.id.cancel)
    TextView cancel;

    /**
     * 服务Id
     */
    private String orderNo;
    private String orderStatus;
    private CustomDialog loading;
    private String serviceAmtStr;
    private String depositStr;

    public static final int PayDepositRequest = 0x001;
    public static final int PayServiceAmtRequest = 0x002;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (root == null) {
            root = inflater.inflate(R.layout.my_service_book, container, false);
        }
        unbinder = ButterKnife.bind(this, root);
        ViewGroup parent = (ViewGroup) root.getParent();
        if (parent != null) {
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void setFragView() {
        loading = dialogUtil.startLoading(context, loading, "正在加载");
        Login.DataEntity dataEntity = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetServiceOrder.Token, dataEntity.getToken());
        map.put(RequestParam.GetServiceOrder.OrderType, RequestParam.GetServiceOrder.UNFINISHED);
        ApiService.getPileService().GetServiceOrder(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.GetServiceOrder.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(getServiceOrder -> {
                    if (TextUtils.equals(BeanParam.Failure, getServiceOrder.getIsSuccess())) {
                        return null;
                    }
                    return getServiceOrder;
                })
                .subscribe(getServiceOrder -> {
                    dialogUtil.dismiss(loading);
                    if (getServiceOrder == null) {
                        nodataShow.setVisibility(View.VISIBLE);
                        return;
                    }
                    nodataShow.setVisibility(View.GONE);
                    GetServiceOrder.DataEntity dataEntity1 = getServiceOrder.getData().get(0);
                    orderNo = dataEntity1.getOrderNo();
                    name.setText(dataEntity1.getOrgName());
                    time.setText(dataEntity1.getPlanTime());
                    orderId.setText(dataEntity1.getOrderNo());
                    phone.setText(dataEntity1.getTel());
                    if (!TextUtils.isEmpty(dataEntity1.getServiceAmt())) {
                        serviceAmt.setText(dataEntity1.getServiceAmt() + "元");
                    }
                    if (!TextUtils.isEmpty(dataEntity1.getDeposit())) {
                        deposit.setText(dataEntity1.getDeposit() + "元");
                    }

                    serviceAmtStr = dataEntity1.getServiceAmt();
                    depositStr = dataEntity1.getDeposit();
                    orderStatus = dataEntity1.getOrderStatus();

                    if(TextUtils.equals(orderStatus,BeanParam.ServiceOrder.State4)){
                        pay.setText("支付服务费");
                        cancel.setText("服务中");
                    }

                }, new ThrowableAction(context));
    }


    @OnClick({R.id.cancel, R.id.pay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel:

                if(TextUtils.equals(orderStatus,BeanParam.ServiceOrder.State4)){
                    Toast.makeText(context,"服务中，无法取消",Toast.LENGTH_SHORT).show();
                    return;
                }

                Login.DataEntity dataEntity = application.getLogin().getData();
                Map<String, String> map = new HashMap();
                map.put(RequestParam.CancelServiceOrder.Token, dataEntity.getToken());
                map.put(RequestParam.CancelServiceOrder.OrderNo, orderNo);
                ApiService.getPileService().CancelServiceOrder(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.CancelServiceOrder.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(cancelServiceOrder -> {
                            if (TextUtils.equals(BeanParam.Failure, cancelServiceOrder.getIsSuccess())) {
                                Error error = errorParamUtil.checkReturnState(cancelServiceOrder.getReturnStatus());
                                toastUtil.toastError(context, error, null);
                                return null;
                            }
                            return cancelServiceOrder;
                        })
                        .subscribe(cancelServiceOrder -> {
                            if (cancelServiceOrder == null) {
                                return;
                            }
                            Toast.makeText(context, "取消成功", Toast.LENGTH_SHORT).show();
                            Activity activity = (Activity) context;
                            activity.finish();
                        },new ThrowableAction(context));
                break;
            case R.id.pay:

                if(TextUtils.equals(orderStatus,BeanParam.ServiceOrder.State1)){
                    Toast.makeText(context,"请先确认金额",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.equals(orderStatus, BeanParam.ServiceOrder.State2)){ //支付预约金
                    PayActivity.startPayActivity(this,orderNo,PayActivity.ServiceBookPay,depositStr, PayDepositRequest);
                }

                if(TextUtils.equals(orderStatus,BeanParam.ServiceOrder.State4)){ //支付服务费
                    String price = DoubleUtil.delete(serviceAmtStr,depositStr);
                    PayActivity.startPayActivity(this,orderNo,PayActivity.ServiceAmtPay,price,PayServiceAmtRequest);
                }

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode){
            case PayDepositRequest:
                if(resultCode == PayActivity.Success){ //支付成功
                    Toast.makeText(context,"支付成功",Toast.LENGTH_SHORT).show();
                    cancel.setText("服务中");
                    pay.setText("服务中");
                    orderStatus = BeanParam.ServiceOrder.State4;
                    return;
                }
                break;
            case PayServiceAmtRequest:
                if(resultCode == PayActivity.Success){
                    Toast.makeText(context,"服务完成",Toast.LENGTH_SHORT).show();
                    setFragView();
                    return;
                }
                break;
        }
    }

    @Override
    public String getTAG() {
        return TAG;
    }



}

package com.fusite.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusite.bean.FilterServiceStation;
import com.fusite.fragments.UtilFragment;
import com.fusite.pile.R;
import com.fusite.pile.ServiceBookActivity;
import com.fusite.pile.ServiceStationInfoActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Jelly on 2016/5/6.
 */
public class ServiceDetailFragment extends UtilFragment {
    /**
     * TAG
     */
    public String TAG = "ServiceDetailFragment";
    @BindView(R.id.orgName)
    TextView orgName;
    @BindView(R.id.bookNum)
    TextView bookNum;
    @BindView(R.id.createTime)
    TextView createTime;
    @BindView(R.id.orgName1)
    TextView orgName1;
    @BindView(R.id.authenticate)
    TextView authenticate;
    @BindView(R.id.score1)
    ImageView score1;
    @BindView(R.id.score2)
    ImageView score2;
    @BindView(R.id.score3)
    ImageView score3;
    @BindView(R.id.score4)
    ImageView score4;
    @BindView(R.id.score5)
    ImageView score5;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.serviceType)
    TextView serviceType;
    @BindView(R.id.addr)
    TextView addr;
    @BindView(R.id.detail)
    TextView detail;
    @BindView(R.id.phoneName)
    TextView phoneName;

    private FilterServiceStation.DataEntity dataEntity;
    private String serviceId;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.service_station_detail, container, false);
            unbinder = ButterKnife.bind(this, root);
            lazyLoad();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void loadObjectAttribute() {
        ServiceStationInfoActivity activity = (ServiceStationInfoActivity) getActivity();
        if (dataEntity == null) {
            dataEntity = activity.dataEntity;
        }
        if(TextUtils.isEmpty(serviceId)){
            serviceId = activity.serviceId;
        }
    }

    @Override
    public void setFragView() {
        if(!TextUtils.isEmpty(dataEntity.getAddr())){
            addr.setText(dataEntity.getAddr());
        }
        if(!TextUtils.isEmpty(dataEntity.getOrgName())){
            orgName.setText(dataEntity.getOrgName());
            orgName1.setText(dataEntity.getOrgName());
        }
        if(!TextUtils.isEmpty(dataEntity.getAuditDate())){
            createTime.setText("发布时间" + dataEntity.getAuditDate().replace("T"," "));
            authenticate.setVisibility(View.VISIBLE);
        }else{
            authenticate.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(dataEntity.getTel())){
            phone.setText(dataEntity.getTel());
        }
        if(!TextUtils.isEmpty(dataEntity.getOrgDesc())){
            detail.setText(dataEntity.getOrgDesc());
        }
        if(!TextUtils.isEmpty(dataEntity.getServiceName())){
            serviceType.setText(dataEntity.getServiceName());
        }
        if(!TextUtils.isEmpty(dataEntity.getManager())){
            phoneName.setText(dataEntity.getManager());
        }
        if(!TextUtils.isEmpty(dataEntity.getBookCount())){
            bookNum.setText("累计"+dataEntity.getBookCount()+"人预约");
        }
    }

    @OnClick({R.id.call, R.id.book})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.call:
                Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + phone.getText().toString()));
                startActivity(intent);
                break;
            case R.id.book:
                Log.v(TAG,dataEntity.getOrgId()+""+serviceId);
                ServiceBookActivity.startActivity(context,dataEntity.getOrgId(),serviceId);
                break;
        }
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

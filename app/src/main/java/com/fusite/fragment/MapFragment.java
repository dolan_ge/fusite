package com.fusite.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.AMapOptions;
import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.fusite.api.Api;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.bean.FilterServiceStation;
import com.fusite.bean.PileStation;
import com.fusite.bean.RentStations;
import com.fusite.bean.ServiceStations;
import com.fusite.bean.ServiceTypes;
import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.map.MyLocation;
import com.fusite.param.BeanParam;
import com.fusite.param.DefaultParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.LoginActivity;
import com.fusite.pile.MainActivity;
import com.fusite.pile.NaviEmulatorActivity;
import com.fusite.pile.PileSelectPileActivity;
import com.fusite.pile.PileStationInfoActivity;
import com.fusite.pile.QRActivity;
import com.fusite.pile.R;
import com.fusite.pile.RentStationInfoActivity;
import com.fusite.pile.ServiceStationInfoActivity;
import com.fusite.utils.FloatUtil;
import com.fusite.utils.HttpCacheUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.fusite.utils.ToastUtils;
import com.fusite.view.CustomDialog;
import com.fusite.view.CustomOneHelpDialog;
import com.fusite.view.CustomSelectServiceDialog;
import com.fusite.view.SlideSwitch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 地图站点模块
 * Created by Jelly on 2016/2/25.
 */
public class MapFragment extends UtilFragment implements AMapLocationListener, AMap.InfoWindowAdapter, AMap.OnInfoWindowClickListener {
    /**
     * TAG
     */
    private String TAG = "MapFragment";

    @BindView(R.id.qrcode)
    ImageView qrcode;
    @BindView(R.id.screen)
    ImageView screen;
    @BindView(R.id.oneHelp)
    ImageView oneHelp;
    @BindView(R.id.location)
    ImageView location;
    @BindView(R.id.selectService)
    RelativeLayout selectService;
    public MapView mapView;
    @BindView(R.id.selectServiceBottom)
    ImageView selectServiceBottom;
    @BindView(R.id.typeNumBg)
    ImageView typeNumBg;
    @BindView(R.id.map)
    FrameLayout map;

    private AMap aMap;
    private AMapLocationClient locationClient;
    private AMapLocationClientOption locationClientOption;
    private LocationSource.OnLocationChangedListener locationChangedListener;

    private PopupWindow screenPw;
    private List<Marker> markers = new ArrayList<>();
    private boolean isFirst = true;

    private PopupWindow markerPw;
    private boolean isUse = false;

    private final float Anchor = 0.5f;

    private Animation rotateAnim;

    private ServiceStations serviceStations;

    private List<PileStation> pileStations;
    private List<PileStation> showPileStationsList;

    private RentStations rentStations;

    public static String ServiceType1 = "汽车服务";
    public static String ServiceType2 = "电桩维保";
    public static String ServiceType3 = "电桩安装";
    public static String ServiceType4 = "救援服务";

    private String serviceId = "";


    private CustomDialog loading;

    private HttpCacheUtil httpCacheUtil;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (root == null) {
            root = inflater.inflate(R.layout.station, container, false);
            unbinder = ButterKnife.bind(this, root);
            init();
            loadObjectAttribute();
            loadMap(savedInstanceState); //加载地图
            setListener();
            setFragView();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }
        ViewGroup parent = (ViewGroup) root.getParent();
        if (parent != null) {
            parent.removeView(root);
        }
        return root;
    }


    /**
     * 保存状态
     *
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 获取焦点
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }
//        aMap.clear(true);
//        setFragView(); //每次获取焦点时，重新刷新充电点
    }

    /**
     * 暂停
     */
    @Override
    public void onPause() {
        super.onPause();
        if (mapView != null) {
            mapView.onPause();
        }
        //必须调用高德地图的这个方法
        if (locationClient != null) {
            locationClient.stopLocation(); //停止定位
        }
        MainActivity activity = (MainActivity) getActivity();
        activity.switchDrawLayout(false); //关闭左边的侧滑栏
        isFirst = true; //把isFirst设为true，切换模块的时候就不会在调整距离了
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mapView != null) {
            mapView.onDestroy();
        }
        if (null != locationClient) {
            locationClient.onDestroy();
        }
        screenPw = null;
        markerPw = null;
        root = null;
        loading = null;
        map = null;
        mapView = null;
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    /**
     * 获取视图中的控件
     */
    @Override
    public void init() {
        mapView = new MapView(getActivity().getApplicationContext());
        map.addView(mapView);
    }

    @Override
    public void loadObjectAttribute() {
        httpCacheUtil = new HttpCacheUtil(context);
    }

    @Override
    public void setListener() {
        setMarkerClickListener();
    }

    @Override
    public void setFragView() {
        try {
            loadPosition();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 加载电桩位置
     */
    public void loadPosition() throws Exception {
        Api api = ApiService.getPileService();
        if (application.getCurrTab() == MainActivity.pileTab) { //加载充电点

            if (httpCacheUtil.isExistCache(RequestParam.PileStations.InterfaceName)) {
                pileStations = JsonUtil.arrayFormJson(httpCacheUtil.readHttp(RequestParam.PileStations.InterfaceName), PileStation[].class);
                showPileStationsList = pileStations;
                if (application.getCurrTab() == MainActivity.pileTab) {
                    setPileStationsOnMap(pileStations);
                }
            }
            //执行网络请求
            Map<String, String> map = new HashMap<>();
            map.put(RequestParam.PileStations.Position, RequestParam.PileStations.PositionValue);
            api.getPileStations(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.PileStations.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(pileStations -> {
                        this.pileStations = pileStations;
                        this.showPileStationsList = pileStations;
                        if (application.getCurrTab() == MainActivity.pileTab) {
                            setPileStationsOnMap(pileStations);
                        }
                        //缓存
                        httpCacheUtil.cacheHttp(JsonUtil.Object2Json(pileStations), RequestParam.PileStations.InterfaceName);
                    }, new ThrowableAction(context));
        } else if (application.getCurrTab() == MainActivity.serviceTab) {
            if (httpCacheUtil.isExistCache(RequestParam.ServiceStations.InterfaceName)) {
                serviceStations = JsonUtil.objectFromJson(httpCacheUtil.readHttp(RequestParam.ServiceStations.InterfaceName), ServiceStations.class);
                if (application.getCurrTab() == MainActivity.serviceTab) {
                    setServiceStationsOnMap(serviceStations.getData());
                }
            }
            Map<String, String> map = new HashMap<>();
            map.put(RequestParam.ServiceStations.Position, RequestParam.ServiceStations.PositionValue);
            api.getServiceStations(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.ServiceStations.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(serviceStations -> {
                        MapFragment.this.serviceStations = serviceStations;
                        if (application.getCurrTab() == MainActivity.serviceTab) {
                            setServiceStationsOnMap(serviceStations.getData());
                        }
                        httpCacheUtil.cacheHttp(JsonUtil.Object2Json(serviceStations), RequestParam.ServiceStations.InterfaceName);
                    }, throwable -> {
                    });
        }
    }

    /**
     * 设置地图上所有电桩的位置
     */
    public void setPileStationsOnMap(List<PileStation> list) {
        markers.clear();
        aMap.clear(true);
        MarkerOptions markerOptions = null;
        for (PileStation dao : list) {
            markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng(Double.parseDouble(dao.getEvc_stations_getall().getLatitude()), Double.parseDouble(dao.getEvc_stations_getall().getLongitude()));
            if (TextUtils.equals(dao.getEvc_stations_getall().getIsAvailable(), BeanParam.PileStations.OPEN)) { //可用
                markerOptions.anchor(Anchor, Anchor).draggable(false).position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_marker));
            } else {
                markerOptions.anchor(Anchor, Anchor).draggable(false).position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.red_marker));
            }
            markers.add(aMap.addMarker(markerOptions));
        }
    }

    /**
     * 设置所有的租车服务点在地图上
     */
    public void setRentStationsOnMap(List<RentStations.DataEntity> list) {
        markers.clear();
        aMap.clear(true);
        MarkerOptions markerOptions = null;
        for (RentStations.DataEntity dataEntity : list) {
            markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng(Double.parseDouble(dataEntity.getLatitude()), Double.parseDouble(dataEntity.getLongitude()));
            if (TextUtils.equals(dataEntity.getOrgStatus(), BeanParam.RentStations.OPEN)) {
                markerOptions.anchor(Anchor, Anchor).draggable(false).position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.rent_yellow_marker));
            } else {
                markerOptions.anchor(Anchor, Anchor).draggable(false).position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.rent_red_marker));
            }
            Marker marker = aMap.addMarker(markerOptions);
            marker.setTitle(dataEntity.getOrgName());
            marker.setObject(dataEntity);
            markers.add(marker);
        }
    }


    public void setServiceStationsOnMap(List<ServiceStations.DataEntity> list) {
        if (list == null) {
            list = new ArrayList<>();
        }
        markers.clear();
        aMap.clear(true);
        MarkerOptions markerOptions = null;
        for (ServiceStations.DataEntity dataEntity : list) {
            markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng(Double.parseDouble(dataEntity.getLatitude()), Double.parseDouble(dataEntity.getLongitude()));
            if (TextUtils.equals(BeanParam.ServiceStations.IsMoving, dataEntity.getIsMoving())) {
                markerOptions.anchor(Anchor, Anchor).draggable(false).position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.help_station_icon));
            } else {
                markerOptions.anchor(Anchor, Anchor).draggable(false).position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.fix_station_icon));
            }
            Marker marker = aMap.addMarker(markerOptions);
            markers.add(marker);
        }
    }

    /**
     * 移动到当前位置
     */
    public void onLocation() {
        try {
            aMap.animateCamera(
                    new CameraUpdateFactory().newLatLngZoom(new LatLng(aMap.getMyLocation().getLatitude(), aMap.getMyLocation().getLongitude()), aMap.getCameraPosition().zoom));
        } catch (NullPointerException e) {
            Log.e(TAG, "当前定位点为空");
        }
    }

    public List<PileStation> screenUse(List<PileStation> list) {
        List<PileStation> show = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (TextUtils.equals("1", list.get(i).getEvc_stations_getall().getIsAvailable())) {
                show.add(list.get(i));
            }
        }
        return show;
    }

    /**
     * 设置点击开关的父容器的布局的时候能够改变开关状态
     */
    public void setSwitchListener() {
        LinearLayout use_layout = (LinearLayout) screenPw.getContentView().findViewById(R.id.use_layout);
        final SlideSwitch use_ss = (SlideSwitch) screenPw.getContentView().findViewById(R.id.use_switch);
        use_ss.setEnabled(false);
        if (isUse) {
            use_ss.changeSwitchStatus();
        }
        use_layout.setOnClickListener(v -> use_ss.changeSwitchStatus());
        use_ss.setOnSwitchChangedListener((obj, status) -> {
            if (status == 1) {
                isUse = true;
                showPileStationsList = screenUse(pileStations);
            } else {
                isUse = false;
                showPileStationsList = pileStations;
            }
            setPileStationsOnMap(showPileStationsList);
        });
        LinearLayout enough_layout = (LinearLayout) screenPw.getContentView().findViewById(R.id.enough_layout);
        final SlideSwitch enough_ss = (SlideSwitch) screenPw.getContentView().findViewById(R.id.enough_switch);
        enough_ss.setEnabled(false);
        enough_layout.setOnClickListener(v -> enough_ss.changeSwitchStatus());
    }

    /**
     * 设置筛选菜单的关闭
     */
    public void setScreenClose() {
        ImageView close = (ImageView) screenPw.getContentView().findViewById(R.id.close);
        close.setOnClickListener(v -> screenPw.dismiss());
    }

    /**
     * 设置Marker的点击事件
     */
    public void setMarkerClickListener() {
        aMap.setOnMarkerClickListener(marker -> {
            if (marker == null) { //marker不能为空
                return true;
            }
            int index = markers.indexOf(marker);
            if (index == -1) { //集合中必须有
                return true;
            }
            if (application.getCurrTab() == MainActivity.pileTab) {
                showPileStationPw(root, showPileStationsList.get(index));
            } else {

            }
            return false;
        });
    }


    @Override
    public View getInfoWindow(Marker marker) {
        RelativeLayout infoView = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.rent_custom_info, null);
        TextView title = (TextView) infoView.findViewById(R.id.title);
        title.setText(marker.getTitle());
        return infoView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        RentStations.DataEntity dataEntity = (RentStations.DataEntity) marker.getObject();
        RentStationInfoActivity.startActivity(context, dataEntity.getOrgId());
    }

    /**
     * 显示电桩，所有列表
     *
     * @param v
     * @param dao
     */
    public void showPileStationPw(View v, final PileStation dao) {
        ViewHolder viewHolder = null;
        View pwRoot = null;
        if (markerPw != null) {
            pwRoot = markerPw.getContentView();
            viewHolder = (ViewHolder) pwRoot.getTag();
        } else {
            viewHolder = new ViewHolder();
            pwRoot = getActivity().getLayoutInflater().inflate(R.layout.pile_station_pw_layout, null);
            viewHolder.orgName = (TextView) pwRoot.findViewById(R.id.orgName);
            viewHolder.open_text = (TextView) pwRoot.findViewById(R.id.open_text);
            viewHolder.addr = (TextView) pwRoot.findViewById(R.id.addr);
            viewHolder.sum_text = (TextView) pwRoot.findViewById(R.id.sum_text);
            viewHolder.spare_text = (TextView) pwRoot.findViewById(R.id.spare_text);
            viewHolder.distance_text = (TextView) pwRoot.findViewById(R.id.distance_text);
            viewHolder.nav_layout = (LinearLayout) pwRoot.findViewById(R.id.nav_layout);
            viewHolder.appoint_layout = (LinearLayout) pwRoot.findViewById(R.id.appoint_layout);
            markerPw = popupWindowUtil.getPopupWindow(context, pwRoot, windowUtil.getScreenWidth(getActivity()), ViewGroup.LayoutParams.WRAP_CONTENT);
            pwRoot.setTag(viewHolder);
        }

        final PileStation.EvcStationsGetallEntity entity = dao.getEvc_stations_getall();

        //无法定位
        if(application.getLocation() == null){
            Toast.makeText(context,"无法定位",Toast.LENGTH_SHORT).show();
            return;
        }

        LatLng startLatLng = new LatLng(Double.parseDouble(application.getLocation().getLatitude()), Double.parseDouble(application.getLocation().getLongitude()));
        LatLng endLatLng = new LatLng(Double.parseDouble(entity.getLatitude()), Double.parseDouble(entity.getLongitude()));
        viewHolder.distance_text.setText(FloatUtil.mToKm(AMapUtils.calculateLineDistance(startLatLng, endLatLng)) + "km");

        viewHolder.orgName.setText(entity.getOrgName());

        if (TextUtils.equals(entity.getIsAvailable(), "1")) {
            viewHolder.open_text.setText("有空闲");
        } else {
            viewHolder.open_text.setText("繁忙中");
        }

        viewHolder.addr.setText(entity.getAddr());

        String sum = "0";
        if (!TextUtils.isEmpty(entity.getAvailableNum()) && !TextUtils.isEmpty(entity.getUnavailableNum())) {
            sum = Integer.parseInt(entity.getAvailableNum()) + Integer.parseInt(entity.getUnavailableNum()) + "";
        }
        viewHolder.sum_text.setText(sum);
        if (!TextUtils.equals(sum, "0")) {
            viewHolder.spare_text.setText(entity.getAvailableNum());
        } else {
            viewHolder.spare_text.setText("0");
        }

        viewHolder.nav_layout.setOnClickListener(v1 -> {
            popupWindowUtil.dismissPopupWindow(markerPw);
            NaviEmulatorActivity.startAppActivity(context, aMap.getMyLocation().getLatitude(), aMap.getMyLocation().getLongitude(), Double.parseDouble(entity.getLatitude()), Double.parseDouble(entity.getLongitude()));
        });

        viewHolder.appoint_layout.setOnClickListener(v1 -> { //预约
            popupWindowUtil.dismissPopupWindow(markerPw);
            if (!application.isLogin()) {
                LoginActivity.startActivity(context);
            } else {
                PileSelectPileActivity.startActivity(context, entity.getOrgId(), entity.getAddr());
            }
        });

        pwRoot.setOnClickListener(v1 -> {
            popupWindowUtil.dismissPopupWindow(markerPw);
            PileStationInfoActivity.startActivity(context, entity.getOrgId());
        });

        markerPw.setAnimationStyle(R.style.markerpw_anim);
        markerPw.showAtLocation(v, Gravity.BOTTOM, 0, 0);
    }

    /**
     * 加载地图
     *
     * @param savedInstanceState 退出界面时缓存的数据
     */
    public void loadMap(Bundle savedInstanceState) {
        //下面两句话在加载地图时是必须得写的
        mapView.onCreate(savedInstanceState);
        aMap = mapView.getMap(); //获取地图对象
        aMap.setInfoWindowAdapter(this); //设置可以显示InfoWindow
        aMap.setOnInfoWindowClickListener(this);
        aMap.animateCamera(CameraUpdateFactory.newCameraPosition(application.getCameraPosition())); //初始化位置和缩放
        setLocation();
        aMap.getUiSettings().setZoomControlsEnabled(false); //设置缩放按钮不可以见
        aMap.getUiSettings().setLogoPosition(AMapOptions.LOGO_POSITION_BOTTOM_LEFT); //设置地图logo在中间显示
        aMap.setMyLocationEnabled(true); //显示定位层，并且可以触发定位
        aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE); //只在第一次定位移动到地图中心点。
        // 自定义系统定位蓝点
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        // 自定义定位蓝点图标
        myLocationStyle.myLocationIcon(BitmapDescriptorFactory.fromResource(R.drawable.location));
        myLocationStyle.radiusFillColor(resourceUtil.getResourceColor(context, R.color.location_round_fill_color));//设置圆形边框的填充颜色
        myLocationStyle.strokeColor(resourceUtil.getResourceColor(context, R.color.location_round_fill_color)); //设置圆形边框的边框颜色
        aMap.setMyLocationStyle(myLocationStyle);
    }


    /**
     * 设置定位
     */
    public void setLocation() {
        aMap.setLocationSource(new LocationSource() {
            @Override
            public void activate(OnLocationChangedListener onLocationChangedListener) {
                if (onLocationChangedListener == null) { //不能为空
                    return;
                }
                //初始化定位参数
                locationChangedListener = onLocationChangedListener;
                locationClient = new AMapLocationClient(context);
                locationClientOption = new AMapLocationClientOption();
                locationClientOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
                locationClientOption.setNeedAddress(true); //设置需要返回地址
                locationClient.setLocationListener(MapFragment.this);
                locationClient.setLocationOption(locationClientOption);
                locationClient.startLocation();
            }

            @Override
            public void deactivate() {
                locationChangedListener = null;
                if (locationClient != null) {
                    locationClient.stopLocation();
                    locationClient.onDestroy();
                }
                locationClient = null;
            }
        });
    }

    /**
     * 定位改变后的监听事件
     * @param aMapLocation
     */
    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {

        if (TextUtils.isEmpty(aMapLocation.getCity())) { //定位失败
            return;
        }

        String address = aMapLocation.getAddress().replace(aMapLocation.getCity(), "").replace(aMapLocation.getProvince(), "").replace(aMapLocation.getDistrict(), "");

        MyLocation location = new MyLocation(aMapLocation.getAdCode(), aMapLocation.getLatitude() + "", address, aMapLocation.getLongitude() + "", aMapLocation.getCity(), aMapLocation.getProvince(), aMapLocation.getDistrict());
        spCacheUtil.cacheMyLocation(context, location);
        application.setLocation(location);
        locationChangedListener.onLocationChanged(aMapLocation);
        //缓存当前位置和城市代码
        if (isFirst) {
            try {
                loadPosition(); //加载充电点
            } catch (Exception e) {
                LogUtil.v(TAG, e.getMessage());
            }
            aMap.moveCamera(CameraUpdateFactory.zoomTo(DefaultParam.ZOOM)); //修改缩放位置
            isFirst = !isFirst;
        }
    }


    public void switchTab(int tab) {
        TabFragment tabFragment = (TabFragment) getParentFragment();
        if (tab == MainActivity.pileTab) { //充电
            mapView.setVisibility(View.VISIBLE);
            location.setVisibility(View.VISIBLE);
            qrcode.setVisibility(View.VISIBLE);
            screen.setVisibility(View.VISIBLE);
            oneHelp.setVisibility(View.GONE);
            selectService.setVisibility(View.GONE);
            selectServiceBottom.setVisibility(View.GONE);
            tabFragment.search.setVisibility(View.VISIBLE);
            tabFragment.map.setVisibility(View.VISIBLE);
            tabFragment.centerTitle.setVisibility(View.GONE);
            typeNumBg.clearAnimation();
            if (showPileStationsList != null) {
                setPileStationsOnMap(showPileStationsList);
            } else {
                try {
                    loadPosition();
                } catch (Exception e) {
                    LogUtil.v(TAG, e.getMessage());
                }
            }
        } else if (tab == MainActivity.serviceTab) { //服务
            mapView.setVisibility(View.GONE);
            location.setVisibility(View.GONE);
            qrcode.setVisibility(View.GONE);
            screen.setVisibility(View.GONE);
            oneHelp.setVisibility(View.GONE);
            selectService.setVisibility(View.GONE);
            selectServiceBottom.setVisibility(View.GONE);
            tabFragment.search.setVisibility(View.GONE);
            tabFragment.map.setVisibility(View.GONE);
            tabFragment.centerTitle.setVisibility(View.VISIBLE);
            tabFragment.centerTitle.setText(R.string.afterSaleService);
            if (rotateAnim == null) {
                rotateAnim = AnimationUtils.loadAnimation(context, R.anim.rotate_bg);
            }
            typeNumBg.startAnimation(rotateAnim);
            if (serviceStations != null) {
                setServiceStationsOnMap(serviceStations.getData());
            } else {
                try {
                    loadPosition();
                } catch (Exception e) {
                    LogUtil.v(TAG, e.getMessage());
                }

            }
        }
        //移动到当前位置
        onLocation();
    }


    @Override
    public String getTAG() {
        return TAG;
    }

    @OnClick({R.id.screen, R.id.selectService, R.id.location, R.id.oneHelp, R.id.qrcode})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.screen:
                if (screenPw != null && !screenPw.isShowing()) {
                    int[] location1 = windowUtil.getViewLocation(view); //获取筛选按钮的x坐标
                    //设置显示的属性，x=筛选按钮在屏幕的x坐标-PopupWindow的宽度+筛选按钮的宽度，y=筛选按钮在屏幕的y坐标
                    screenPw.showAtLocation(view, Gravity.NO_GRAVITY, location1[0] - screenPw.getWidth() + view.getWidth(), location1[1]);
                    return;
                }
                //打开PopupWindow,显示操作栏
                View screenPwView  = LayoutInflater.from(context).inflate(R.layout.screen_layout, null);
                screenPw = popupWindowUtil.getPopupWindow(getActivity(), screenPwView, view.getHeight());
                int[] location1 = windowUtil.getViewLocation(view); //获取筛选按钮的x坐标
                //设置显示的属性，x=筛选按钮在屏幕的x坐标-PopupWindow的宽度+筛选按钮的宽度，y=筛选按钮在屏幕的y坐标
                screenPw.showAtLocation(view, Gravity.NO_GRAVITY, location1[0] - screenPw.getWidth() + view.getWidth(), location1[1]);
                setScreenClose(); //设置筛选关闭按钮的监听事件
                setSwitchListener();
                break;
            case R.id.selectService:
                if (!application.isLogin()) {
                    LoginActivity.startActivity(context);
                    return;
                }
                CustomSelectServiceDialog customSelectServiceDialog = new CustomSelectServiceDialog(context);
                customSelectServiceDialog.show();
                customSelectServiceDialog.setFixClickListener((dialog, index) -> {
                    loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
                    getServiceObserve(index)
                            .map(filterServiceStation -> {
                                dialogUtil.dismiss(loading);
                                return filterServiceStation;
                            })
                            .subscribe(filterServiceStation -> {
                                if (filterServiceStation != null) {
                                    ServiceStationInfoActivity.startActivity(context, filterServiceStation.getData().get(0), serviceId);
                                }
                            }, throwable -> {
                                Log.v(TAG, throwable.getMessage());
                            });
                });
                customSelectServiceDialog.setMaintainClickListener((dialog, index) -> {
                    loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
                    getServiceObserve(index)
                            .map(filterServiceStation -> {
                                dialogUtil.dismiss(loading);
                                return filterServiceStation;
                            })
                            .subscribe(filterServiceStation -> {
                                if (filterServiceStation != null) {
                                    ServiceStationInfoActivity.startActivity(context, filterServiceStation.getData().get(0), serviceId);
                                }
                            }, new ThrowableAction(context));
                });
                customSelectServiceDialog.setMountClickListener((dialog, index) -> {
                    loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
                    getServiceObserve(index)
                            .map(filterServiceStation -> {
                                dialogUtil.dismiss(loading);
                                return filterServiceStation;
                            })
                            .subscribe(filterServiceStation -> {
                                if (filterServiceStation != null) {
                                    ServiceStationInfoActivity.startActivity(context, filterServiceStation.getData().get(0), serviceId);
                                }
                            }, new ThrowableAction(context));
                });
                break;
            case R.id.location:
                onLocation();
                break;
            case R.id.oneHelp: //一键救援
                if (!application.isLogin()) {
                    LoginActivity.startActivity(context);
                    return;
                }
                CustomOneHelpDialog customOneHelpDialog = new CustomOneHelpDialog(context);
                View root = customOneHelpDialog.getRoot();
                TextView name = (TextView) root.findViewById(R.id.name);
                TextView license = (TextView) root.findViewById(R.id.license);
                TextView distance = (TextView) root.findViewById(R.id.distance);
                TextView time = (TextView) root.findViewById(R.id.time);
                TextView ok = (TextView) root.findViewById(R.id.ok);
                TextView cancel = (TextView) root.findViewById(R.id.cancel);
                getServiceObserve(3)
                        .subscribe(filterServiceStation -> {
                            if (filterServiceStation != null) {
                                FilterServiceStation.DataEntity dataEntity = filterServiceStation.getData().get(0);
                                name.setText(dataEntity.getOrgName());
                                ok.setOnClickListener(v -> {
                                    if(TextUtils.isEmpty(dataEntity.getTel())){
                                        Toast.makeText(context,"电话号码不能为空",Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + dataEntity.getTel()));
                                    startActivity(intent);
                                });
                                cancel.setOnClickListener(v -> {
                                    dialogUtil.dismiss(customOneHelpDialog);
                                });
                                customOneHelpDialog.show();
                            }
                        }, new ThrowableAction(context));
                break;
            case R.id.qrcode:
                if (!application.isLogin()) { //没有登录
                    LoginActivity.startActivity(context);
                    return;
                }
                QRActivity.startActivity(context);
                break;
        }
    }


    /**
     * 筛选服务
     * @param index
     * @return
     */
    public Observable<FilterServiceStation> getServiceObserve(int index) {
        Map<String, String> map = new HashMap();
        map.put(RequestParam.ServiceTypes.Token, application.getLogin().getData().getToken());
        return ApiService.getPileService().getServiceTypes(NetParam.trancode, NetParam.mode, NetParam.getTime(), application.getLogin().getData().getCustID(), NetParam.sign_method, RequestParam.ServiceTypes.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .flatMap(serviceTypes -> {
                    List<ServiceTypes.DataEntity> list = serviceTypes.getData();

                    for (int i = 0; i < list.size(); i++) {
                        ServiceTypes.DataEntity dataEntity = list.get(i);
                        if (index == 0) {
                            if (TextUtils.equals(dataEntity.getServiceName(), ServiceType1)) {
                                serviceId = dataEntity.getServiceId();
                                break;
                            }
                        } else if (index == 1) {
                            if (TextUtils.equals(dataEntity.getServiceName(), ServiceType2)) {
                                serviceId = dataEntity.getServiceId();
                                break;
                            }
                        } else if (index == 2) {
                            if (TextUtils.equals(dataEntity.getServiceName(), ServiceType3)) {
                                serviceId = dataEntity.getServiceId();
                                break;
                            }
                        } else if (index == 3) {
                            if (TextUtils.equals(dataEntity.getServiceName(), ServiceType4)) {
                                serviceId = dataEntity.getServiceId();
                                break;
                            }
                        }
                    }
                    MyLocation myLocation = application.getLocation();

                    if (myLocation == null || TextUtils.isEmpty(myLocation.getLatitude()) || TextUtils.isEmpty(myLocation.getLongitude())) {
                        Toast.makeText(context, "请先加载定位", Toast.LENGTH_SHORT).show();
                    }
                    map.clear();
                    map.put(RequestParam.filterServiceStation.ServiceId, serviceId);
                    map.put(RequestParam.filterServiceStation.Longitude, myLocation.getLongitude());
                    map.put(RequestParam.filterServiceStation.Latitude, myLocation.getLatitude());
                    Observable<FilterServiceStation> observable = ApiService.getPileService().filterServiceStation(NetParam.trancode, NetParam.mode, NetParam.getTime(), application.getLogin().getData().getCustID(), NetParam.sign_method, RequestParam.filterServiceStation.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map));
                    return observable;
                })
                .observeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(filterServiceStation -> {
                    if (TextUtils.equals(BeanParam.Failure, filterServiceStation.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(filterServiceStation.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return filterServiceStation;
                });
    }

    /**
     * 用于缓存PopupWindow中的控件
     */
    private class ViewHolder {
        TextView orgName;
        TextView open_text;
        TextView addr;
        TextView sum_text;
        TextView spare_text;
        TextView distance_text;
        LinearLayout nav_layout;
        LinearLayout appoint_layout;
    }
}

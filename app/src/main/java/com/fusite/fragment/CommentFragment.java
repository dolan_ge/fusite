package com.fusite.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.fusite.adpater.CommitListAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.Commit;
import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.OrgIdFragmentListener;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 电站评论
 * Created by Jelly on 2016/4/10.
 */
public class CommentFragment extends UtilFragment {
    /**
     * TAG
     */
    public String TAG = "PileStationCommentFragment";
    @BindView(R.id.list)
    ListView list;

    private CommitListAdapter adapter;
    private List<Commit.DataEntity> dataList;
    private OrgIdFragmentListener orgIdFragmentListener;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.comment_fragment, container, false);
        }
        unbinder = ButterKnife.bind(this, root);
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        orgIdFragmentListener = (OrgIdFragmentListener) context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setFragView() {
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetCommit.OrgId, orgIdFragmentListener.getOrgId());
        ApiService.getPileService().GetCommit(NetParam.trancode, NetParam.mode, NetParam.getTime(),NetParam.custid, NetParam.sign_method, RequestParam.GetCommit.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(commit -> {
                    if(TextUtils.equals(BeanParam.Failure,commit.getIsSuccess())){
                        return null;
                    }
                    return commit;
                })
                .subscribe(commit -> {
                    if(commit == null){
                        return;
                    }
                    dataList = commit.getData();
                    adapter = new CommitListAdapter(dataList,context);
                    if(list == null){
                        return;
                    }
                    list.setAdapter(adapter);
                });
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

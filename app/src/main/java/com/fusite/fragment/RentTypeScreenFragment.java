package com.fusite.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.fusite.adpater.RentScreenListAdapter;
import com.fusite.bean.CarModels;
import com.fusite.bean.Screen;
import com.fusite.fragments.UtilFragment;
import com.fusite.pile.R;
import com.fusite.pile.RentCarScreenCarActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Jelly on 2016/4/20.
 */
public class RentTypeScreenFragment extends UtilFragment {
    /**
     * TAG
     */
    public String TAG = "RentTypeScreenFragment";
    @BindView(R.id.list)
    ListView list;

    private List<CarModels.DataEntity> modellist;

    private List<Screen> dataList = new ArrayList<>();

    private RentScreenListAdapter adapter;

    public static final String Type1 = "类型不限";
    private Unbinder unbinder;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        if(root == null){
            root = LayoutInflater.from(context).inflate(R.layout.rent_screen, container,false);
        }
        unbinder = ButterKnife.bind(this, root);
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void loadObjectAttribute() {
        modellist = ((RentCarScreenCarActivity) getActivity()).getList();
        if(dataList.size() == 0){
            if(TextUtils.equals(Type1,((RentCarScreenCarActivity) getActivity()).getTypeScreen())){
                dataList.add(new Screen(Type1,true));
            }else {
                dataList.add(new Screen(Type1, false));
            }
        }
        boolean flag = false;
        for (int i = 0; i < modellist.size(); i++) {
            flag = false;
            for (int b = 0; b < dataList.size(); b++) {
                if (TextUtils.equals(modellist.get(i).getStruct(), dataList.get(b).getTitle())) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                if(TextUtils.equals(modellist.get(i).getStruct(),((RentCarScreenCarActivity) getActivity()).getTypeScreen())){
                    dataList.add(new Screen(modellist.get(i).getStruct(),true));
                }else {
                    dataList.add(new Screen(modellist.get(i).getStruct(), false));
                }
            }
        }
        adapter = new RentScreenListAdapter(context, dataList);
    }

    @Override
    public void setListener() {
        setScreenItemListener();
    }

    @Override
    public void setFragView() {
        list.setAdapter(adapter);
    }

    public void setScreenItemListener(){
        list.setOnItemClickListener((parent, view, position, id) -> {
            adapter.selectRadio(position);
            adapter.notifyDataSetChanged();
            RentCarScreenCarActivity screenCarActivity = (RentCarScreenCarActivity) getActivity();
            screenCarActivity.setTypeScreen(dataList.get(position).getTitle());
        });
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

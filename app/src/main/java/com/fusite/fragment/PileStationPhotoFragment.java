package com.fusite.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.fusite.adpater.PhotoAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.StationImage;
import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.OrgIdFragmentListener;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.ImageActivity;
import com.fusite.pile.R;
import com.fusite.utils.LogUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 电站图片
 * Created by Jelly on 2016/4/10.
 */
public class PileStationPhotoFragment extends UtilFragment {
    /**
     * TAG
     */
    public String TAG = "PileStationPhotoFragment";
    @BindView(R.id.gv)
    GridView gv;

    private PhotoAdapter adapter;
    private List<String> images;
    private OrgIdFragmentListener orgIdFragmentListener;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.station_photo_fragment, container, false);
        }
        unbinder = ButterKnife.bind(this, root);
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        orgIdFragmentListener = (OrgIdFragmentListener) context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setListener() {
        setOnItemListener();
    }

    @Override
    public void setFragView() {
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetPileStationImage.OrgId, orgIdFragmentListener.getOrgId());
        ApiService.getPileService().GetStationImage(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.GetPileStationImage.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(pileStationImage -> {
                    if(TextUtils.equals(BeanParam.Failure, pileStationImage.getIsSuccess())){
                        return null;
                    }
                    LogUtil.v(TAG,new Gson().toJson(pileStationImage));
                    return pileStationImage;
                })
                .map(pileStationImage -> {
                    if(pileStationImage == null){
                        return null;
                    }
                    List<String> list1 = new ArrayList<>();
                    StationImage.DataEntity dataEntity = pileStationImage.getData();
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl1())){
                        list1.add(dataEntity.getFileUrl1());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl2())){
                        list1.add(dataEntity.getFileUrl2());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl3())){
                        list1.add(dataEntity.getFileUrl3());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl4())){
                        list1.add(dataEntity.getFileUrl4());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl5())){
                        list1.add(dataEntity.getFileUrl5());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl6())){
                        list1.add(dataEntity.getFileUrl6());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl7())){
                        list1.add(dataEntity.getFileUrl7());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl7())){
                        list1.add(dataEntity.getFileUrl7());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl8())){
                        list1.add(dataEntity.getFileUrl8());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl9())){
                        list1.add(dataEntity.getFileUrl9());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl10())){
                        list1.add(dataEntity.getFileUrl10());
                    }
                    return list1;
                })
                .subscribe(strings -> {
                    if (strings == null) {
                        return;
                    }
                    images = strings;
                    adapter = new PhotoAdapter(context, strings);
                    gv.setAdapter(adapter);
                }, new ThrowableAction(context));
    }




    public void setOnItemListener(){
        gv.setOnItemClickListener((parent, view, position, id) -> {
            ImageActivity.startActivity(context,images.get(position));
        });
    }

    @Override
    public String getTAG() {
        return TAG;
    }

}

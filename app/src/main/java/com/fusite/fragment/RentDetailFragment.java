package com.fusite.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.RentStationInfo;
import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.OrgIdFragmentListener;
import com.fusite.impl.RentStationInfoDaoImpl;
import com.fusite.impl.StationInfoDaoImpl;
import com.fusite.pile.NaviEmulatorActivity;
import com.fusite.pile.R;
import com.fusite.pile.RentCarTimeActivity;
import com.fusite.pile.RentStationInfoActivity;
import com.fusite.utils.FloatUtil;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Jelly on 2016/4/18.
 */
public class RentDetailFragment extends UtilFragment {
    /**
     * TAG
     */
    public static String TAG = "RentDetailFragment";
    @BindView(R.id.orgName)
    TextView orgName;
    @BindView(R.id.hours_text)
    TextView hoursText;
    @BindView(R.id.distance_text)
    TextView distanceText;
    @BindView(R.id.addr)
    TextView addr;
    @BindView(R.id.sum_text)
    TextView sumText;
    @BindView(R.id.spare_text)
    TextView spareText;
    @BindView(R.id.company_text)
    TextView companyText;
    @BindView(R.id.phone_text)
    TextView phoneText;
    @BindView(R.id.company)
    TextView company;
    @BindView(R.id.appoint_layout)
    LinearLayout appointLayout;
    @BindView(R.id.nav_layout)
    RelativeLayout navLayout;

    private View root;
    /**
     * 获取服务点ID
     */
    private OrgIdFragmentListener orgIdFragmentListener;
    /**
     * 获取租车服务点详情
     */
    private RentStationInfoDaoImpl rentStationInfoDao;

    private static final int msgRentStation = 0x001;

    private CustomDialog loading;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = LayoutInflater.from(context).inflate(R.layout.rent_station_detail_fragment, container, false);
            unbinder = ButterKnife.bind(this, root);
            lazyLoad();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void loadObjectAttribute() {
        rentStationInfoDao = new RentStationInfoDaoImpl(context, new MyHandler(this), msgRentStation);
        orgIdFragmentListener = (RentStationInfoActivity) getActivity();
    }

    @Override
    public void setFragView() {
        loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
        Login login = application.getLogin();
        rentStationInfoDao.getStationInfo(orgIdFragmentListener.getOrgId(),login == null?"1":login.getData().getCustID());
    }

    @OnClick(R.id.phone_btn)
    public void onClick() {
        Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + phoneText.getText().toString()));
        startActivity(intent);
    }

    private static class MyHandler extends Handler{
        private WeakReference<RentDetailFragment> mFragment;

        private MyHandler(RentDetailFragment fragment){
            mFragment = new WeakReference<RentDetailFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            RentDetailFragment fragment = mFragment.get();
            if(fragment == null){
                return;
            }
            switch (msg.what) {
                case msgRentStation:
                    fragment.dialogUtil.dismiss(fragment.loading);
                    if(fragment.rentStationInfoDao.rentStationInfo == null){
                        Toast.makeText(fragment.context,"网络异常",Toast.LENGTH_SHORT).show();
                        break;
                    }
                    if (TextUtils.equals(fragment.rentStationInfoDao.rentStationInfo.getIsSuccess(), "N")) {
                        Error error = fragment.errorParamUtil.checkReturnState(fragment.rentStationInfoDao.rentStationInfo.getReturnStatus());
                        fragment.toastUtil.toastError(fragment.context, error, null);
                        return;
                    }
                    final RentStationInfo.DataEntity dataEntity = fragment.rentStationInfoDao.rentStationInfo.getData();
                    fragment.orgName.setText(dataEntity.getOrgName());
                    //没有定位
                    if(fragment.application.getLocation() == null){
                        Toast.makeText(fragment.context,"无法定位",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    LatLng startLatLng = new LatLng(Double.parseDouble(fragment.application.getLocation().getLatitude()), Double.parseDouble(fragment.application.getLocation().getLongitude()));
                    LatLng endLatLng = new LatLng(Double.parseDouble(dataEntity.getLatitude()), Double.parseDouble(dataEntity.getLongitude()));
                    fragment.distanceText.setText(FloatUtil.mToKm(AMapUtils.calculateLineDistance(startLatLng, endLatLng)) + "km");
                    fragment.addr.setText(dataEntity.getAddr());

                    String sum = "0";
                    if (!TextUtils.isEmpty(dataEntity.getAvailableNum()) && !TextUtils.isEmpty(dataEntity.getUnavailableNum())) {
                        sum = Integer.parseInt(dataEntity.getAvailableNum()) + Integer.parseInt(dataEntity.getUnavailableNum()) + "";
                    }
                    fragment.sumText.setText(sum);
                    if (!TextUtils.equals(sum, "0")) {
                        fragment.spareText.setText(dataEntity.getAvailableNum());
                    } else {
                        fragment.spareText.setText("0");
                    }

                    fragment.company.setText(dataEntity.getCompanyName());
                    fragment.companyText.setText("该充电点由" + dataEntity.getCompanyName() + "负责运营");
                    fragment.phoneText.setText(dataEntity.getTel());

                    RentStationInfoActivity rentStationInfoActivity = (RentStationInfoActivity) fragment.getActivity();
                    rentStationInfoActivity.setCityCode(dataEntity.getCityCode());
                    if(TextUtils.equals(StationInfoDaoImpl.isCollect,dataEntity.getFavourite())){
                        rentStationInfoActivity.setCollect(true);
                    }else{
                        rentStationInfoActivity.setCollect(false);
                    }

                    fragment.navLayout.setOnClickListener(v -> NaviEmulatorActivity.startAppActivity(fragment.context, Double.parseDouble(fragment.application.getLocation().getLatitude()), Double.parseDouble(fragment.application.getLocation().getLongitude()), Double.parseDouble(dataEntity.getLatitude()), Double.parseDouble(dataEntity.getLongitude())));

                    fragment.appointLayout.setOnClickListener(v -> {
                        RentCarTimeActivity.startActivity(fragment.context,dataEntity.getOrgId(),rentStationInfoActivity.getCityCode(),dataEntity.getOrgName());
                    });
                    break;
            }
        }
    }

    @Override
    public String getTAG() {
        return TAG;
    }

}

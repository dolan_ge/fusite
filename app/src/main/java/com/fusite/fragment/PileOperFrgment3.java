package com.fusite.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.FragDataListener;
import com.fusite.pile.R;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Jelly on 2016/7/11.
 */
public class PileOperFrgment3 extends UtilFragment {

    public String TAG = "PileOperFrgment1";
    @BindView(R.id.facilityType)
    TextView facilityType;
    @BindView(R.id.chargingVoltage)
    TextView chargingVoltage;
    @BindView(R.id.chargingCurrent)
    TextView chargingCurrent;

    private FragDataListener fragDataListener;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.pile_oper_frag3, container, false);
            unbinder = ButterKnife.bind(this, root);
            lazyLoad();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void setFragView() {
        if(fragDataListener == null){
            return;
        }
        Map<String,String> dataMap = fragDataListener.getData();

        for(String key:dataMap.keySet()){
            Log.v("data=","key:" + key + ",value:" + dataMap.get(key));
        }

        if(!TextUtils.isEmpty(dataMap.get("FacilityType")) && facilityType != null){
            facilityType.setText(dataMap.get("FacilityType"));
        }

        if(!TextUtils.isEmpty(dataMap.get("ChargingVoltage")) && chargingVoltage != null){
            chargingVoltage.setText(dataMap.get("ChargingVoltage") + "伏");
        }

        if(!TextUtils.isEmpty(dataMap.get("ChargingCurrent")) && chargingCurrent != null){
            chargingCurrent.setText(dataMap.get("ChargingCurrent") + "安");
        }


    }


    /**
     * 设置获取数据监听器
     *
     * @param listener
     */
    public PileOperFrgment3 setDataListener(FragDataListener listener) {
        this.fragDataListener = listener;
        return this;
    }

    /**
     * 更新界面
     */
    public void updateView() {
        setFragView();
    }

}

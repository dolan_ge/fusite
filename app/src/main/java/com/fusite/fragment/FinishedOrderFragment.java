package com.fusite.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.fusite.adpater.FinishOrderListAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.CompleteOrder;
import com.fusite.bean.DateSort;
import com.fusite.bean.Error;
import com.fusite.bean.GetServiceOrder;
import com.fusite.bean.Login;
import com.fusite.bean.RentOrders;
import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.impl.CompleteOrderDaoImpl;
import com.fusite.impl.RentOrdersDaoImpl;
import com.fusite.param.BeanParam;
import com.fusite.param.NetErrorEnum;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.PileOrderInfoActivity;
import com.fusite.pile.R;
import com.fusite.pile.RentCarOrderInfoActivity;
import com.fusite.pile.ServiceOrderInfoActivity;
import com.fusite.utils.DateUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.ListUtil;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * 已完成订单列表
 * Created by Jelly on 2016/3/10.
 */
public class FinishedOrderFragment extends UtilFragment {
    /**
     * TAG
     */
    public String TAG = "FinishOrderFragment";
    @BindView(R.id.nodata_show)
    LinearLayout nodataShow;
    @BindView(R.id.finish_list)
    ListView finishList;
    /**
     *
     */
    private FinishOrderListAdapter adapter;
    /**
     * Loading
     */
    private CustomDialog loading;

    private RentOrdersDaoImpl rentOrdersDao;

    private static final int msgRentOrders = 0x002;

    private List<DateSort> showList;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (root == null) {
            root = inflater.inflate(R.layout.finished_order, container, false);
        }
        unbinder = ButterKnife.bind(this, root);
        ViewGroup parent = (ViewGroup) root.getParent();
        if (parent != null) {
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void loadObjectAttribute() {
        MyHandler handler = new MyHandler(this);
        rentOrdersDao = new RentOrdersDaoImpl(context, handler, msgRentOrders);
    }

    @Override
    public void setListener() {
        setListItemClickListener();
    }

    @Override
    public void setFragView() {
        loading = dialogUtil.startLoading(context, loading, R.string.loadingText);

        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetServiceOrder.Token, loginData.getToken());
        map.put(RequestParam.GetServiceOrder.OrderType, RequestParam.GetServiceOrder.FINISHED);
        ApiService.getPileService().GetServiceOrder(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.GetServiceOrder.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(getServiceOrder -> {
                    if (TextUtils.equals(BeanParam.Failure, getServiceOrder.getIsSuccess())) {
                        if (TextUtils.equals(NetErrorEnum.暂无数据.getState(), getServiceOrder.getReturnStatus())) {
                            return getServiceOrder;
                        }
                        return null;
                    }
                    return getServiceOrder;
                })
                .subscribe(getServiceOrder -> {


                    Log.v("jsonService", JsonUtil.Object2Json(getServiceOrder));

                    //数据异常
                    if (getServiceOrder == null) {
                        return;
                    }

                    //暂无数据
                    if (getServiceOrder.getData() == null || getServiceOrder.getData().size() == 0) {
                        getPileOrder();
                        return;
                    }

                    for (GetServiceOrder.DataEntity orderData : getServiceOrder.getData()) {
                        orderData.setDate(DateUtil.getDate(orderData.getPlanTime()));
                    }

                    if (showList == null) {
                        showList = new ArrayList<DateSort>();
                    }

                    showList.addAll(getServiceOrder.getData());

                    getPileOrder();

                }, new ThrowableAction(context, loading));
    }

    /**
     * 设置List的Item的点击事件
     */
    public void setListItemClickListener() {
        finishList.setOnItemClickListener((parent, view, position, id) -> {
            Object object = adapter.getItem(position);
            if (object instanceof CompleteOrder) {
                CompleteOrder.EvcOrdersGetEntity evcOrdersGetEntity = ((CompleteOrder) adapter.getItem(position)).getEvc_orders_get();
                PileOrderInfoActivity.startActivity(context, evcOrdersGetEntity.getOrderNo());
            } else if (object instanceof RentOrders.DataEntity) {
                RentOrders.DataEntity dataEntity = (RentOrders.DataEntity) object;
                RentCarOrderInfoActivity.startActivity(context, dataEntity.getOrderNo());
            } else if (object instanceof GetServiceOrder.DataEntity) {
                GetServiceOrder.DataEntity dataEntity = (GetServiceOrder.DataEntity) object;
                ServiceOrderInfoActivity.startActivity(context, dataEntity.getOrderNo());
            }
        });
    }

    public void getPileOrder() {
        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> conditionMap = new HashMap<>();
        conditionMap.put("Token", loginData.getToken());
        conditionMap.put("OrderStatus1", "8");
        conditionMap.put("OrderStatus2", "16");
        String condition = NetParam.spliceCondition(conditionMap);
        Map<String, String> param = NetParam.getParamMap("evc.orders.get",loginData.getCustID(),condition);
        ApiService.getPileService().CompleteOrder(param)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<CompleteOrder>>() {
                    @Override
                    public void call(List<CompleteOrder> completeOrders) {

                        Log.v("json",JsonUtil.Object2Json(completeOrders));

                        if (TextUtils.equals(completeOrders.get(0).getEvc_orders_get().getIsSuccess(), "N")) {

                            if (TextUtils.equals(NetErrorEnum.暂无数据.getState(), completeOrders.get(0).getEvc_orders_get().getReturnStatus())) {
                                Login.DataEntity dataEntity = application.getLogin().getData();
                                rentOrdersDao.getRentOrders(dataEntity.getToken(), dataEntity.getCustID(), RentOrdersDaoImpl.FinishOrder);
                                return;
                            }

                            Error errorDao = errorParamUtil.checkReturnState(completeOrders.get(0).getEvc_orders_get().getReturnStatus());
                            toastUtil.toastError(context, errorDao, NetErrorEnum.暂无数据.getState(), null);
                            Login.DataEntity dataEntity = application.getLogin().getData();
                            rentOrdersDao.getRentOrders(dataEntity.getToken(), dataEntity.getCustID(), RentOrdersDaoImpl.FinishOrder);

                            return;
                        }
                        for (CompleteOrder completeOrder : completeOrders) {
                            CompleteOrder.EvcOrdersGetEntity evcOrdersGetEntity = completeOrder.getEvc_orders_get();
                            if (TextUtils.equals(evcOrdersGetEntity.getOrderStatus(), CompleteOrderDaoImpl.CompleteOrder)) {
                                completeOrder.setDate(DateUtil.getDate(evcOrdersGetEntity.getRealBeginDateTime()));
                            } else {
                                completeOrder.setDate(DateUtil.getDate(evcOrdersGetEntity.getPlanBeginDateTime() == null ? evcOrdersGetEntity.getRealBeginDateTime() : evcOrdersGetEntity.getPlanBeginDateTime()));
                            }

                            if (showList == null) {
                                showList = new ArrayList<>();
                            }

                            showList.add(completeOrder);
                        }

                        Login.DataEntity dataEntity = application.getLogin().getData();
                        rentOrdersDao.getRentOrders(dataEntity.getToken(), dataEntity.getCustID(), RentOrdersDaoImpl.FinishOrder);
                    }
                }, new ThrowableAction(context, loading));
    }

    private static class MyHandler extends Handler {

        private WeakReference<FinishedOrderFragment> mFragment;

        private MyHandler(FinishedOrderFragment fragment) {
            this.mFragment = new WeakReference<FinishedOrderFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            FinishedOrderFragment fragment = mFragment.get();
            if (fragment == null) {
                return;
            }
            switch (msg.what) {
                case msgRentOrders:
                    fragment.dialogUtil.dismiss(fragment.loading);
                    if (TextUtils.equals("N", fragment.rentOrdersDao.rentOrders.getIsSuccess())) {

                        Error error = fragment.errorParamUtil.checkReturnState(fragment.rentOrdersDao.rentOrders.getReturnStatus());
                        fragment.toastUtil.toastError(fragment.context, error, NetErrorEnum.暂无数据.getState(), null);

                    }

                    if (fragment.showList == null) {
                        fragment.showList = new ArrayList<>();
                    }

                    if (TextUtils.equals("Y", fragment.rentOrdersDao.rentOrders.getIsSuccess())) {
                        for (RentOrders.DataEntity temp : fragment.rentOrdersDao.rentOrders.getData()) {
                            temp.setDate(DateUtil.getDate(temp.getCreateDate()));
                            fragment.showList.add(temp);
                        }
                    }


                    if (fragment.showList.size() > 0) {
                        ListUtil.sortTime(fragment.showList);
                        if (fragment.adapter == null) {
                            fragment.adapter = new FinishOrderListAdapter(fragment.context, fragment.showList, fragment.application.getLogin());
                        }
                        fragment.finishList.setAdapter(fragment.adapter);
                    }

                    if (fragment.showList.size() <= 0) {
                        fragment.nodataShow.setVisibility(View.VISIBLE);
                    }

                    break;
            }
        }
    }


    @Override
    public String getTAG() {
        return TAG;
    }

}


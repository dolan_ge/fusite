package com.fusite.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.FragDataListener;
import com.fusite.pile.R;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Jelly on 2016/7/11.
 */
public class PileOperFrgment2 extends UtilFragment {

    public String TAG = "PileOperFrgment1";
    @BindView(R.id.remainTime)
    TextView remainTime;
    @BindView(R.id.chargingAmt)
    TextView chargingAmt;
    @BindView(R.id.serviceAmt)
    TextView serviceAmt;

    private FragDataListener fragDataListener;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.pile_oper_frag2, container, false);
        }
        unbinder = ButterKnife.bind(this, root);
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setFragView() {
        if(fragDataListener == null){
            return;
        }

        Map<String,String> dataMap = fragDataListener.getData();
        for(String key:dataMap.keySet()){
            Log.v("data=","key:" + key + ",value:" + dataMap.get(key));
        }
        if(!TextUtils.isEmpty(dataMap.get("RemainTime")) && remainTime != null){
            remainTime.setText(dataMap.get("RemainTime") + "分钟");
        }

        if(!TextUtils.isEmpty(dataMap.get("ChargingAmt")) && chargingAmt != null){
            chargingAmt.setText(dataMap.get("ChargingAmt") + "元");
        }

        if(!TextUtils.isEmpty(dataMap.get("ServiceAmt")) && serviceAmt != null){
            serviceAmt.setText(dataMap.get("ServiceAmt") + "元");
        }

    }

    /**
     * 设置获取数据监听器
     *
     * @param listener
     */
    public PileOperFrgment2 setDataListener(FragDataListener listener) {
        this.fragDataListener = listener;
        return this;
    }

    /**
     * 更新界面
     */
    public void updateView() {
        setFragView();
    }

}

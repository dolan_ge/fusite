package com.fusite.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fusite.fragments.BaseFragment;
import com.fusite.fragments.SimpleFragment;
import com.fusite.pile.MainActivity;
import com.fusite.pile.PileStationSearchActivity;
import com.fusite.pile.R;
import com.fusite.pile.RentCarSearchActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 模块一
 * Created by Jelly on 2016/3/24.
 */
public class TabFragment extends SimpleFragment {
    /**
     * TAG
     */
    public String TAG = "Tab1Fragment";
    @BindView(R.id.actionBar_layout)
    RelativeLayout actionBarLayout;
    @BindView(R.id.search)
    TextView search;
    @BindView(R.id.centerTitle)
    TextView centerTitle;
    @BindView(R.id.map)
    TextView map;
    /**
     * 根布局
     */
    private View root;
    /**
     * 所有电桩的fragment
     */
    private MapFragment mapFragment = new MapFragment();
    /**
     * 附近的电桩的Fragment
     */
    private NearbyPileStationFragment nearbyStationFragment = new NearbyPileStationFragment();
    /**
     * 附近服务点
     */
    private NearbyRentCarStationFragment nearbyRentCarFragment = new NearbyRentCarStationFragment();
    /**
     * ActionBar的高度
     */
    private int actionBarHeight;

    private String NearbyTitle = "附近";

    private String MapTitle = "地图";
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.tab, container, false);
            unbinder = ButterKnife.bind(this, root);
            init();
            loadObjectAttribute();
            setListener();
            setFragView();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            mapFragment.mapView.setVisibility(View.INVISIBLE);
        } else {
            mapFragment.mapView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapFragment = null;
        root = null;
    }

    @Override
    public void init() {
    }

    @Override
    public void loadObjectAttribute() {
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setFragView() {
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        if (mapFragment.isAdded()) {
            ft.show(mapFragment);
        } else if (nearbyStationFragment.isAdded()) {
            ft.show(nearbyStationFragment);
        } else {
            ft.add(R.id.content, mapFragment);
        }
        ft.commit();
    }

    public MapFragment getMapFragment() {
        return mapFragment;
    }

    @OnClick({R.id.member, R.id.map, R.id.search})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.member:
                MainActivity activity = (MainActivity) getActivity();
                activity.switchDrawLayout(true); //打开左边抽屉
                break;
            case R.id.map:
                TextView tv = (TextView) view;
                if (TextUtils.equals(tv.getText().toString(), MapTitle)) {
                    tv.setText(NearbyTitle);
                } else {
                    tv.setText(MapTitle);
                }
                FragmentTransaction ft = getChildFragmentManager().beginTransaction();
                if (application.getCurrTab() == MainActivity.pileTab) {
                    if (mapFragment.isVisible()) {
                        ft.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out);
                        switchFragment(ft, mapFragment, nearbyStationFragment);
                    } else {
                        ft.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_right_out);
                        switchFragment(ft, nearbyStationFragment, mapFragment);
                    }
                }
                break;
            case R.id.search:
                if (application.getCurrTab() == MainActivity.pileTab) {
                    PileStationSearchActivity.startActivity(context);
                }
                break;
        }
    }

    /**
     * 不需重新加载切换Fragment
     *
     * @param ft
     * @param from
     * @param to
     */
    public void switchFragment(FragmentTransaction ft, BaseFragment from, BaseFragment to) {
        if (!to.isAdded()) {
            ft.hide(from);
            ft.add(R.id.content, to);
        } else {
            ft.hide(from);
            ft.show(to);
        }
        ft.commit();
    }

    /**
     * 是否可以切换List
     */
    public boolean isSwitchList() {
        if (nearbyStationFragment.isVisible() || nearbyRentCarFragment.isVisible()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 切换ListFragment
     */
    public void switchListFragment() {
        if (!isSwitchList()) {
            return;
        }
        if (nearbyStationFragment.isVisible()) {
            FragmentTransaction ft = getChildFragmentManager().beginTransaction();
            ft.hide(nearbyStationFragment);
            ft.show(mapFragment);
            ft.commit();
        } else if (nearbyRentCarFragment.isVisible()) {
            FragmentTransaction ft = getChildFragmentManager().beginTransaction();
            ft.hide(nearbyRentCarFragment);
            ft.show(mapFragment);
            ft.commit();
        }
    }

    /**
     * 获得ActionBar的高度
     *
     * @return
     */
    public int getActionBarHeight() {
        if (actionBarHeight != 0) {
            return actionBarHeight;
        }
        if (actionBarLayout != null) {
            actionBarHeight = actionBarLayout.getHeight();
        }
        return actionBarHeight;
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

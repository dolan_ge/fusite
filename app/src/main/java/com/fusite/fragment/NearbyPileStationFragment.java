package com.fusite.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.fusite.adpater.NearbyListItemAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.DistanceSort;
import com.fusite.bean.NearbyPileStation;
import com.fusite.fragments.UtilFragment;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.PileStationInfoActivity;
import com.fusite.pile.R;
import com.fusite.utils.HttpCacheUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.ListUtil;
import com.fusite.view.SlideSwitch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 附近电桩
 * Created by Jelly on 2016/3/24.
 */
public class NearbyPileStationFragment extends UtilFragment {
    /**
     * TAG
     */
    public String TAG = "NearbyPileStationFragment";
    @BindView(R.id.arrow)
    ImageView arrow;
    @BindView(R.id.screen)
    RelativeLayout screen;
    @BindView(R.id.list)
    ListView list;


    private NearbyListItemAdapter adapter;
    private PopupWindow screenPw;
    private boolean isShow = false;

    private List<NearbyPileStation> pileStations;
    private List<NearbyPileStation> showList = new ArrayList<>();

    private boolean isUse = false;

    private HttpCacheUtil httpCacheUtil;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.nearby_pile_station, container, false);
            unbinder = ButterKnife.bind(this, root);
            init();
            loadObjectAttribute();
            setListener();
            setFragView();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {
        httpCacheUtil = new HttpCacheUtil(context);
    }

    @Override
    public void setListener() {
        setScreenListener();
        setListItemClickListener();
    }

    @Override
    public void setFragView() {

        if(httpCacheUtil.isExistCache(RequestParam.PileStations.NearbyInterfaceName)){
            pileStations = JsonUtil.arrayFormJson(httpCacheUtil.readHttp(RequestParam.PileStations.NearbyInterfaceName),NearbyPileStation[].class);
            showList = pileStations;
            adapter = new NearbyListItemAdapter(context, pileStations);
            list.setAdapter(adapter);
        }
        if (application.getLocation() != null) {
            String adCode = application.getLocation().getAdCode();
            adCode = adCode.substring(0, adCode.length() - 2) + "00";
            Map<String, String> map = new HashMap<>();
            map.put(RequestParam.PileStations.Position, adCode);
            ApiService.getPileService().getNearbyPileStations(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.PileStations.NearbyInterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(pileStations -> {

                        List<DistanceSort> distanceSorts = new ArrayList<DistanceSort>();
                        LatLng latLng0 = new LatLng(Double.parseDouble(application.getLocation().getLatitude()) ,Double.parseDouble(application.getLocation().getLongitude()));

                        for(int i=0;i<pileStations.size();i++){
                            LatLng latLng1 = new LatLng(Double.parseDouble(pileStations.get(i).getEvc_stations_get().getLatitude()),Double.parseDouble(pileStations.get(i).getEvc_stations_get().getLongitude()));
                            pileStations.get(i).setDistance(AMapUtils.calculateLineDistance(latLng0,latLng1));
                            distanceSorts.add(pileStations.get(i));
                        }

                        ListUtil.sortDistance(distanceSorts);

                        pileStations.clear();

                        for(int i=0;i<distanceSorts.size();i++){
                            pileStations.add((NearbyPileStation) distanceSorts.get(i));
                        }

                        NearbyPileStationFragment.this.pileStations = pileStations;
                        NearbyPileStationFragment.this.showList = pileStations;
                        adapter = new NearbyListItemAdapter(context, pileStations);
                        list.setAdapter(adapter);
                        //缓存
                        httpCacheUtil.cacheHttp(JsonUtil.Object2Json(pileStations),RequestParam.PileStations.NearbyInterfaceName);
                    }, throwable -> {

                    });
        }
    }

    /**
     * 设置List的点击监听器
     */
    public void setListItemClickListener() {
        list.setOnItemClickListener((parent, view, position, id) -> {
            if (showList.get(position) != null) {
                PileStationInfoActivity.startActivity(context, showList.get(position).getEvc_stations_get().getOrgId());
            }
        });
    }

    /**
     * 设置筛选按钮的监听器
     */
    public void setScreenListener() {
        screen.setOnClickListener(v -> {
            if (screenPw != null && isShow) {
                isShow = popupWindowUtil.dismissPopupWindow(screenPw, isShow);
                arrow.setImageResource(R.drawable.down);
                return;
            } else if (screenPw != null && !isShow) {
                screenPw.showAsDropDown(v);
                arrow.setImageResource(R.drawable.up);
                isShow = !isShow;
                return;
            }
            View root1 = LayoutInflater.from(context).inflate(R.layout.nearby_pile_station_screen_layout, null);
            final RelativeLayout use = (RelativeLayout) root1.findViewById(R.id.use);
            RelativeLayout enough = (RelativeLayout) root1.findViewById(R.id.enough);
            final SlideSwitch use_switch = (SlideSwitch) root1.findViewById(R.id.use_switch);
            final SlideSwitch enough_switch = (SlideSwitch) root1.findViewById(R.id.enough_switch);
            use_switch.setEnabled(false);
            enough_switch.setEnabled(false);
            if (isUse) {
                use_switch.changeSwitchStatus();
            }
            use.setOnClickListener(v1 -> use_switch.changeSwitchStatus());

            enough.setOnClickListener(v1 -> enough_switch.changeSwitchStatus());

            use_switch.setOnSwitchChangedListener((obj, status) -> {
                if (status == 1) {
                    isUse = true;
                    showList = screenUse(pileStations);
                } else {
                    isUse = false;
                    showList = pileStations;
                }
                if (adapter != null) {
                    adapter.setList(showList);
                    adapter.notifyDataSetChanged();
                }
            });

            ImageView bg = (ImageView) root1.findViewById(R.id.bg);
            bg.setOnClickListener(v1 -> {
                if (screenPw != null && screenPw.isShowing()) {
                    screenPw.dismiss();
                    isShow = false;
                }
            });
            arrow.setImageResource(R.drawable.up);
            TabFragment tab1Fragment = (TabFragment) getParentFragment();
            screenPw = popupWindowUtil.getPopupWindow(context, root1, windowUtil.getScreenWidth(getActivity()), windowUtil.getScreenHeight(getActivity()) - tab1Fragment.getActionBarHeight() - screen.getHeight() - windowUtil.getStateBarHeight(context));
            screenPw.showAsDropDown(v);
            isShow = true;
        });
    }

    public List<NearbyPileStation> screenUse(List<NearbyPileStation> list) {
        List<NearbyPileStation> show = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (TextUtils.equals(BeanParam.PileStations.OPEN, list.get(i).getEvc_stations_get().getIsAvailable())) {
                show.add(list.get(i));
            }
        }
        return show;
    }


    @Override
    public String getTAG() {
        return TAG;
    }
}

package com.fusite.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.StationInfo;
import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.OrgIdFragmentListener;
import com.fusite.impl.StationInfoDaoImpl;
import com.fusite.pile.LoginActivity;
import com.fusite.pile.NaviEmulatorActivity;
import com.fusite.pile.PileSelectPileActivity;
import com.fusite.pile.PileStationInfoActivity;
import com.fusite.pile.R;
import com.fusite.utils.FloatUtil;
import com.fusite.utils.ToastUtils;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 电站详情
 * Created by Jelly on 2016/4/10.
 */
public class PileStationDetailFragment extends UtilFragment {
    /**
     * TAG
     */
    public String TAG = "PileStationDetailFragment";

    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.orgName)
    TextView orgName;
    @BindView(R.id.open_text)
    TextView openText;
    @BindView(R.id.distance_text)
    TextView distanceText;
    @BindView(R.id.addr)
    TextView addr;
    @BindView(R.id.nav_layout)
    RelativeLayout navLayout;
    @BindView(R.id.appoint_layout)
    LinearLayout appointLayout;
    @BindView(R.id.sum_text)
    TextView sumText;
    @BindView(R.id.spare_text)
    TextView spareText;
    @BindView(R.id.company)
    TextView company;
    @BindView(R.id.company_text)
    TextView companyText;
    @BindView(R.id.phone_text)
    TextView phoneText;

    private StationInfoDaoImpl stationInfoDao;
    private static final int msgStationInfo = 0x001;

    private OrgIdFragmentListener orgIdFragmentListener;

    private CustomDialog loading;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.pile_station_detail_fragment, container, false);
            unbinder = ButterKnife.bind(this, root);
            lazyLoad();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        orgIdFragmentListener = (OrgIdFragmentListener) context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void loadObjectAttribute() {
        stationInfoDao = new StationInfoDaoImpl(context, new MyHandler(this), msgStationInfo);
    }

    @Override
    public void setFragView() {
        loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
        Login login = application.getLogin();
        stationInfoDao.getStationInfo(orgIdFragmentListener.getOrgId(),login == null?"1":login.getData().getCustID());
    }

    @OnClick(R.id.phone_btn)
    public void onClick() {
        Intent intent=new Intent("android.intent.action.CALL", Uri.parse("tel:"+phoneText.getText().toString()));
        startActivity(intent);
    }

    private static class MyHandler extends Handler{
        private WeakReference<PileStationDetailFragment> mFragment;

        private MyHandler(PileStationDetailFragment fragment){
            mFragment = new WeakReference(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            PileStationDetailFragment fragment = mFragment.get();
            if(fragment == null || fragment.context == null){
                return;
            }
            switch (msg.what) {
                case msgStationInfo:

                    fragment.dialogUtil.dismiss(fragment.loading);

                    if(fragment.stationInfoDao.stationInfo == null){
                        Toast.makeText(fragment.context,"网络异常",Toast.LENGTH_SHORT).show();
                        break;
                    }
                    if (TextUtils.equals("N", fragment.stationInfoDao.stationInfo.getIsSuccess())) {
                        Error error = fragment.errorParamUtil.checkReturnState(fragment.stationInfoDao.stationInfo.getReturnStatus());
                        fragment.toastUtil.toastError(fragment.context, error, null);
                        break;
                    }
                    final StationInfo.DataEntity dataEntity = fragment.stationInfoDao.stationInfo.getData();
                    fragment.orgName.setText(dataEntity.getOrgName());

                    if (TextUtils.equals(dataEntity.getIsAvailable(), "1")) {
                        fragment.openText.setText("有空闲");
                    } else {
                        fragment.openText.setText("繁忙中");
                    }

                    LatLng startLatLng = new LatLng(Double.parseDouble(fragment.application.getLocation().getLatitude()), Double.parseDouble(fragment.application.getLocation().getLongitude()));
                    LatLng endLatLng = new LatLng(Double.parseDouble(dataEntity.getLatitude()), Double.parseDouble(dataEntity.getLongitude()));
                    fragment.distanceText.setText(FloatUtil.mToKm(AMapUtils.calculateLineDistance(startLatLng, endLatLng)) + "km");

                    fragment.addr.setText(dataEntity.getAddr());

                    String sum = "0";
                    if (!TextUtils.isEmpty(dataEntity.getAvailableNum()) && !TextUtils.isEmpty(dataEntity.getUnavailableNum())) {
                        sum = Integer.parseInt(dataEntity.getAvailableNum()) + Integer.parseInt(dataEntity.getUnavailableNum()) + "";
                    }
                    fragment.sumText.setText(sum);
                    if (!TextUtils.equals(sum, "0")) {
                        fragment.spareText.setText(dataEntity.getAvailableNum());
                    } else {
                        fragment.spareText.setText("0");
                    }

                    fragment.company.setText(dataEntity.getCompanyName());
                    fragment.companyText.setText("该充电点由" + dataEntity.getCompanyName() + "负责运营");
                    fragment.phoneText.setText(dataEntity.getTel());

                    //设置收藏
                    PileStationInfoActivity pileStationInfoActivity = (PileStationInfoActivity) fragment.getActivity();

                    if(TextUtils.equals(StationInfoDaoImpl.isCollect,dataEntity.getFavourite() == null ? "0":dataEntity.getFavourite())){
                        pileStationInfoActivity.setCollect(true);
                    }else{
                        pileStationInfoActivity.setCollect(false);
                    }

                    fragment.navLayout.setOnClickListener(v -> NaviEmulatorActivity.startAppActivity(fragment.context, Double.parseDouble(fragment.application.getLocation().getLatitude()), Double.parseDouble(fragment.application.getLocation().getLongitude()), Double.parseDouble(dataEntity.getLatitude()), Double.parseDouble(dataEntity.getLongitude())));

                    fragment.appointLayout.setOnClickListener(v -> { //预约
                        if (!fragment.application.isLogin()) {
                            LoginActivity.startActivity(fragment.context);
                        } else {
                            PileSelectPileActivity.startActivity(fragment.context, dataEntity.getOrgId(), dataEntity.getAddr());
                        }
                    });
                    break;
            }
        }
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

package com.fusite.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.fusite.adpater.PhotoAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.StationImage;
import com.fusite.fragments.UtilFragment;
import com.fusite.handlerinterface.OrgIdFragmentListener;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.ImageActivity;
import com.fusite.pile.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/5/30.
 */
public class RentPhotoFragment extends UtilFragment{
    /**
     * TAG
     */
    public String TAG = "RentPhotoFragment";
    @BindView(R.id.gv)
    GridView gv;

    private PhotoAdapter adapter;
    private List<String> images;

    private OrgIdFragmentListener orgIdFragmentListener;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.station_photo_fragment, container, false);
        }
        unbinder = ButterKnife.bind(this, root);
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        orgIdFragmentListener = (OrgIdFragmentListener) context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setListener() {
        setOnItemClickListener();
    }

    @Override
    public void setFragView() {
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetPileStationImage.OrgId, orgIdFragmentListener.getOrgId());
        ApiService.getPileService().GetStationImage(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.GetRentStationImage.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(stationImage -> {
                    if(TextUtils.equals(BeanParam.Failure, stationImage.getIsSuccess())){
                        return null;
                    }
                    return stationImage;
                })
                .map(stationImage -> {
                    if(stationImage == null){
                        return null;
                    }
                    List<String> list1 = new ArrayList<>();
                    StationImage.DataEntity dataEntity = stationImage.getData();
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl1())){
                        list1.add(dataEntity.getFileUrl1());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl2())){
                        list1.add(dataEntity.getFileUrl2());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl3())){
                        list1.add(dataEntity.getFileUrl3());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl4())){
                        list1.add(dataEntity.getFileUrl4());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl5())){
                        list1.add(dataEntity.getFileUrl5());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl6())){
                        list1.add(dataEntity.getFileUrl6());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl7())){
                        list1.add(dataEntity.getFileUrl7());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl7())){
                        list1.add(dataEntity.getFileUrl7());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl8())){
                        list1.add(dataEntity.getFileUrl8());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl9())){
                        list1.add(dataEntity.getFileUrl9());
                    }
                    if(!TextUtils.isEmpty(dataEntity.getFileUrl10())){
                        list1.add(dataEntity.getFileUrl10());
                    }
                    return list1;
                })
                .subscribe(strings -> {
                    if(strings == null){
                        return;
                    }
                    images = strings;
                    adapter = new PhotoAdapter(context,strings);
                    gv.setAdapter(adapter);
                });
    }

    public void setOnItemClickListener(){
        gv.setOnItemClickListener((parent, view, position, id) -> ImageActivity.startActivity(context,images.get(position)));
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

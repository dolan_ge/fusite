package com.fusite.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.fusite.adpater.PendingOrderListAdapter;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.PendingOrder;
import com.fusite.bean.RentOrders;
import com.fusite.fragments.UtilFragment;
import com.fusite.impl.PendingOrderImpl;
import com.fusite.impl.RentOrdersDaoImpl;
import com.fusite.param.NetErrorEnum;
import com.fusite.pile.NewPileOperActivity;
import com.fusite.pile.R;
import com.fusite.pile.RentOrderInfoActivity;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 未完成订单
 * Created by Jelly on 2016/3/10.
 */
public class PendingOrderFragment extends UtilFragment {
    /**
     * TAG
     */
    public String TAG = "PendingOrderFragment";
    @BindView(R.id.nodata_show)
    LinearLayout nodataShow;
    @BindView(R.id.pending_list)
    ListView pendingList;

    private PendingOrderListAdapter adapter;

    private PendingOrderImpl pendingOrder;
    private static final int msgPenging = 0x001;

    private RentOrdersDaoImpl rentOrdersDao;
    private static final int msgRentPending = 0x002;

    private CustomDialog loading;

    private List<Object> showList;

    public static final int RentCarOrderRequest = 0x003;
    private Unbinder unbinder;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.pending_order, container, false);
            unbinder = ButterKnife.bind(this, root);
            lazyLoad();
        } else {
            unbinder = ButterKnife.bind(this, root);
        }
        ViewGroup parent = (ViewGroup) root.getParent();
        if(parent != null){
            parent.removeView(root);
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStop() {
        super.onStop();
        dialogUtil.dismiss(loading);
    }


    @Override
    public void loadObjectAttribute() {
        MyHandler handler = new MyHandler(this);
        pendingOrder = new PendingOrderImpl(context, handler, msgPenging);
        rentOrdersDao = new RentOrdersDaoImpl(context, handler, msgRentPending);
    }

    @Override
    public void setListener() {
        setListItemClickListener();
    }

    @Override
    public void setFragView() {
        loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
        Login.DataEntity data = application.getLogin().getData();
        pendingOrder.getNetPendingOrderList(data.getToken(), data.getCustID());
    }

    /**
     * 设置列表的点击事件
     */
    public void setListItemClickListener() {
        pendingList.setOnItemClickListener((parent, view, position, id) -> {
            Object object = showList.get(position);
            if(object instanceof PendingOrder){
                getActivity().finish();
                PendingOrder.EvcOrdersGetEntity data = pendingOrder.pendingOrderDaos.get(position).getEvc_orders_get();
                NewPileOperActivity.startActivity(context, data);
            }else if(object instanceof RentOrders.DataEntity){
                RentOrders.DataEntity dataEntity = (RentOrders.DataEntity) object;
                RentOrderInfoActivity.startActivity(this,dataEntity.getOrderNo(),RentCarOrderRequest);
            }
        });
    }

    private static class MyHandler extends Handler{
        private WeakReference<PendingOrderFragment> mFragment;

        private MyHandler(PendingOrderFragment fragment){
            this.mFragment = new WeakReference<PendingOrderFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            PendingOrderFragment fragment = mFragment.get();
            if(fragment == null){
                return;
            }
            switch (msg.what){
                case msgPenging: //充电订单
                    if (TextUtils.equals(fragment.pendingOrder.pendingOrderDaos.get(0).getEvc_orders_get().getIsSuccess(), "N")) {
                        Error errorDao = fragment.errorParamUtil.checkReturnState(fragment.pendingOrder.pendingOrderDaos.get(0).getEvc_orders_get().getReturnStatus());
                        fragment.toastUtil.toastError(fragment.context,errorDao, NetErrorEnum.暂无数据.getState(),null);
                        Login.DataEntity dataEntity = fragment.application.getLogin().getData();
                        fragment.rentOrdersDao.getRentOrders(dataEntity.getToken(),dataEntity.getCustID(),RentOrdersDaoImpl.PendingOrder + "");
                        return;
                    }
                    if(fragment.showList == null){
                        fragment.showList = new ArrayList<>();
                    }
                    fragment.showList.addAll(fragment.pendingOrder.pendingOrderDaos);
                    Login.DataEntity dataEntity = fragment.application.getLogin().getData();
                    fragment.rentOrdersDao.getRentOrders(dataEntity.getToken(),dataEntity.getCustID(),RentOrdersDaoImpl.PendingOrder);
                    break;
                case msgRentPending: //租车订单
                    fragment.dialogUtil.dismiss(fragment.loading);
                    if(TextUtils.equals(fragment.rentOrdersDao.rentOrders.getIsSuccess(),"N")){
                        Error error = fragment.errorParamUtil.checkReturnState(fragment.rentOrdersDao.rentOrders.getReturnStatus());
                        fragment.toastUtil.toastError(fragment.context,error,NetErrorEnum.暂无数据.getState(),null);
                    }
                    if(fragment.showList == null){
                        fragment.showList = new ArrayList<>();
                    }
                    if(TextUtils.equals("Y",fragment.rentOrdersDao.rentOrders.getIsSuccess())){
                        fragment.showList.addAll(fragment.rentOrdersDao.rentOrders.getData());
                    }
                    if(fragment.showList.size() > 0){
                        if(fragment.adapter == null){
                            fragment.adapter = new PendingOrderListAdapter(fragment.context, fragment.showList);
                        }
                        fragment.pendingList.setAdapter(fragment.adapter);
                    }
                    if(fragment.showList.size() <= 0){
                        fragment.nodataShow.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RentCarOrderRequest:
                if(resultCode == RentOrderInfoActivity.Success){
                    showList.removeAll(rentOrdersDao.rentOrders.getData());
                    adapter.reloadList(showList);
                }
                break;
        }
    }

    @Override
    public String getTAG() {
        return super.getTAG();
    }

}


package com.fusite.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.fusite.handlerinterface.ShareListener;
import com.fusite.pile.R;
import com.fusite.constant.Keys;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.io.ByteArrayOutputStream;

/**
 * Created by Jelly on 2016/6/12.
 */
public class CustomShareDialog {

    private View root;
    private Dialog dialog;
    private LinearLayout share1;
    private LinearLayout share2;
    private LinearLayout share3;
    private LinearLayout close;
    private String url = "coming soon...";
    private String title = "玛帮新能源电桩";
    private String description = "我在玛帮新能源电桩充电，快来看看吧";
    private String type = "webpage";

    private ShareListener listener;

    public CustomShareDialog(Context context) {
        dialog = new Dialog(context, R.style.CustomFeaturesDialog);
        dialog.setTitle("");
        root = LayoutInflater.from(context).inflate(R.layout.share_dialog, null);
        share1 = (LinearLayout) root.findViewById(R.id.share1);
        share1.setOnClickListener(v -> {
            listener.share();
            WXWebpageObject wxWebpageObject = new WXWebpageObject();
            wxWebpageObject.webpageUrl = url;
            WXMediaMessage message = new WXMediaMessage(wxWebpageObject);
            message.title = title;
            message.description = description;
            Bitmap thumb = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
            message.thumbData = Bitmap2Bytes(thumb);
            SendMessageToWX.Req req = new SendMessageToWX.Req();
            req.transaction = buildTransaction(type);
            req.message = message;
            req.scene = 0;
            IWXAPI api = WXAPIFactory.createWXAPI(context, Keys.AppId, false);
            api.registerApp(Keys.AppId); //把应用注册到微信
            api.sendReq(req);
        });
        share2 = (LinearLayout) root.findViewById(R.id.share2);
        share2.setOnClickListener(v -> {
            listener.share();
            WXWebpageObject wxWebpageObject1 = new WXWebpageObject();
            wxWebpageObject1.webpageUrl = url;
            WXMediaMessage message1 = new WXMediaMessage(wxWebpageObject1);
            message1.title = title;
            message1.description = description;
            Bitmap thumb1 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
            message1.thumbData = Bitmap2Bytes(thumb1);
            SendMessageToWX.Req req1 = new SendMessageToWX.Req();
            req1.transaction = buildTransaction(type);
            req1.message = message1;
            req1.scene = 1;
            IWXAPI api1 = WXAPIFactory.createWXAPI(context, Keys.AppId, false);
            api1.registerApp(Keys.AppId); //把应用注册到微信
            api1.sendReq(req1);
        });
        share3 = (LinearLayout) root.findViewById(R.id.share3);
        share3.setOnClickListener(v -> {
            listener.share();
            WXWebpageObject wxWebpageObject2 = new WXWebpageObject();
            wxWebpageObject2.webpageUrl = url;
            WXMediaMessage message2 = new WXMediaMessage(wxWebpageObject2);
            message2.title = title;
            message2.description = description;
            Bitmap thumb2 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
            message2.thumbData = Bitmap2Bytes(thumb2);
            SendMessageToWX.Req req2 = new SendMessageToWX.Req();
            req2.transaction = buildTransaction(type);
            req2.message = message2;
            req2.scene = 2;
            IWXAPI api2 = WXAPIFactory.createWXAPI(context, Keys.AppId, false);
            api2.registerApp(Keys.AppId); //把应用注册到微信
            api2.sendReq(req2);
        });
        close = (LinearLayout) root.findViewById(R.id.close);
        close.setOnClickListener(v -> dialog.dismiss());
        dialog.setContentView(root);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
        dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.MATCH_PARENT;
    }


    private String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }


    private byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public View getRoot() {
        return root;
    }

    public void setListener(ShareListener listener) {
        this.listener = listener;
    }

    public Dialog show() {
        if (dialog == null) {
            return null;
        }
        dialog.show();
        return dialog;
    }

}

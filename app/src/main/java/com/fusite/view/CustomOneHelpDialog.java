package com.fusite.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.fusite.pile.R;

/**
 * 自定义的服务选择框Dialog
 * Created by Jelly on 2016/5/5.
 */
public class CustomOneHelpDialog extends CustomDialog{

    private View root;
    private ImageView close;

    public CustomOneHelpDialog(Context context){
        dialog = new Dialog(context, R.style.CustomFeaturesDialog);
        dialog.setTitle("");
        root = LayoutInflater.from(context).inflate(R.layout.onehelp_dialog,null);
        close = (ImageView) root.findViewById(R.id.close);
        close.setOnClickListener(v -> dialog.dismiss());
        dialog.setContentView(root);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
    }

    public View getRoot() {
        return root;
    }

}

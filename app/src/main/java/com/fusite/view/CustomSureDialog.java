package com.fusite.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.fusite.handlerinterface.DialogListener;
import com.fusite.pile.R;

/**
 * 自定义系统确认选择框
 * Created by Jelly on 2016/5/6.
 */
public class CustomSureDialog {

    private Dialog dialog;
    private String okText = "确认";
    private String cancelText = "取消";
    private String hintText = "是否取消操作？";
    private TextView ok;
    private TextView cancel;
    private TextView hint;
    private DialogListener okClickListener;
    private DialogListener cancelClickListener;



    public CustomSureDialog(Context context){
        dialog = new Dialog(context, R.style.CustomFeaturesDialog);
        dialog.setTitle("");
        View view = LayoutInflater.from(context).inflate(R.layout.sure_dialog,null);
        cancel = (TextView) view.findViewById(R.id.cancel);
        hint = (TextView) view.findViewById(R.id.hint);
        ok = (TextView) view.findViewById(R.id.ok);
        cancel.setOnClickListener(v -> {
            dialog.dismiss();
            if(cancelClickListener != null){
                cancelClickListener.handle(dialog,0);
            }
        });
        ok.setOnClickListener(v -> {
            if(okClickListener != null){
                okClickListener.handle(dialog,0);
            }else{
                dialog.dismiss();
            }
        });
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
    }

    public Dialog show(){
        if(dialog == null){
            return null;
        }
        hint.setText(hintText);
        dialog.show();
        return dialog;
    }

    public void dismiss(){
        if(dialog != null){
            dialog.dismiss();
        }
    }

    public void setOkText(String okText) {
        this.okText = okText;
    }

    public void setCancelText(String cancelText) {
        this.cancelText = cancelText;
    }

    public void setHintText(String hintText){
        this.hintText = hintText;
    }

    public void setOkClickListener(DialogListener okClickListener){
        this.okClickListener = okClickListener;
    }

    public void setCancelClickListener(DialogListener cancelClickListener){
        this.cancelClickListener = cancelClickListener;
    }

}

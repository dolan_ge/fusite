package com.fusite.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

import com.fusite.pile.R;
import com.fusite.utils.BitmapUtil;

/**
 * Created by Jelly on 2016/5/18.
 */
public class SpeedWatch extends View{

    private Bitmap pointer;
    private Bitmap watch;
    private float width;
    private float height;
    private float outOvalR; //大圆和表盘的差

    private float ovalR; //大圆的半径

    private float ovalWidth; //小实心圆的半径
    private float stokeWidth; //大圆的线宽

    private int gra = 0;
    private int tempGra = 0;

    private double sumDress = 240; //总刻度的度数
    private int sumGra = 200; //总刻度
    private double oneGraDress; //一个刻度代表的度数

    public SpeedWatch(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUp(context,attrs);
    }

    public SpeedWatch(Context context) {
        super(context);
    }

    public SpeedWatch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUp(context,attrs);
    }


   public void setUp(Context context,AttributeSet attrs){
       TypedArray typedArray = context.obtainStyledAttributes(attrs,R.styleable.SpeedWatch);
       ovalR = typedArray.getDimension(R.styleable.SpeedWatch_ovalR,200);
       outOvalR = typedArray.getDimension(R.styleable.SpeedWatch_outOvalR,5);
       ovalWidth = typedArray.getDimension(R.styleable.SpeedWatch_ovalWidth,5);
       stokeWidth = typedArray.getDimension(R.styleable.SpeedWatch_stokeWidth,2);
       init();
   }



    public void init(){
        Resources resources = getResources();
        width = ovalR * 2 - outOvalR * 2;
        height = width;
        pointer = BitmapFactory.decodeResource(resources, R.drawable.pointer);
        pointer = BitmapUtil.getInstance().zoomImage(pointer,width,height);
        watch = BitmapFactory.decodeResource(resources,R.drawable.watch);
        watch = BitmapUtil.getInstance().zoomImage(watch,width,height);
        oneGraDress = sumDress/sumGra;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension((int) (ovalR * 2 + ovalWidth),(int) (ovalR * 2 + ovalWidth));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();

        paint.setColor(getResources().getColor(R.color.whiteLineColor));
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(stokeWidth);
        RectF rectF = new RectF(0+ovalWidth,0+ovalWidth,ovalR*2 - ovalWidth,ovalR*2 - ovalWidth);
        canvas.drawArc(rectF,-210,240,false,paint); //绘制浅色轮廓
        paint.setColor(Color.WHITE);
        canvas.drawArc(rectF,-210, (float) (gra*oneGraDress),false,paint); //绘制白色轮廓

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);

        paint.setTextSize(70);
        if(gra == 0){
            canvas.drawText("0",ovalR - 60,ovalR + 150,paint);//速度
        }else{
            canvas.drawText(gra + "",ovalR - 60,ovalR + 150,paint);//速度
        }
        paint.setTextSize(30);
        canvas.drawText("km/h",ovalR - 50 + 70,ovalR + 145,paint); //单位
        canvas.drawText("车辆当前时速",ovalR - 70,ovalR + 190,paint); //描述

        canvas.drawBitmap(watch,0+outOvalR,0+outOvalR,paint); //表盘

        canvas.rotate((float) (gra*oneGraDress),ovalR,ovalR); //指针角度
        canvas.drawBitmap(pointer,0+outOvalR,0+outOvalR,paint); //指针

        canvas.rotate(-30,ovalR,ovalR);
        canvas.drawCircle(0 + ovalWidth,ovalR,ovalWidth,paint); //圆点

    }

    public void setGra(int graIndex) {
        if(graIndex < 0){
            return;
        }
        tempGra = graIndex;
        new Thread(new Runnable() {
            @Override
            public void run() {

                if(tempGra > gra){ //加
                    for(int i=gra;i<=tempGra;i++){
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        gra = i;
                        handler.sendEmptyMessage(0x000);
                    }
                }else if(tempGra<gra){ //减
                    for(int i=gra;i>=tempGra;i--){
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        gra = i;
                        handler.sendEmptyMessage(0x000);
                    }
                }
            }
        }).start();
    }


    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            invalidate();
        }
    };
}

package com.fusite.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.fusite.pile.R;

/**
 * Created by Jelly on 2016/7/19.
 */
public class CustomSelectInvoiceDialog extends CustomDialog{

    private View.OnClickListener commitInvoice;
    private View.OnClickListener specialInvoice;
    private Button commitBtn;
    private Button specialBtn;

    public CustomSelectInvoiceDialog(Context context){
        dialog = new Dialog(context, R.style.CustomFeaturesDialog);
        dialog.setTitle("");
        root = LayoutInflater.from(context).inflate(R.layout.select_invoice_dialog,null);
        commitBtn = (Button) root.findViewById(R.id.commitBtn);
        specialBtn = (Button) root.findViewById(R.id.specialBtn);
        root.findViewById(R.id.close).setOnClickListener(v -> {
            dismiss();
        });
        dialog.setContentView(root);
    }

    public void setCommitInvoice(View.OnClickListener listener){
        this.commitInvoice = listener;
        commitBtn.setOnClickListener(listener);
    }

    public void setSpecialInvoice(View.OnClickListener listener){
        this.specialInvoice = listener;
        specialBtn.setOnClickListener(listener);
    }


}

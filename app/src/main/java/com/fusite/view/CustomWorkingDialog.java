package com.fusite.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;

import com.fusite.pile.R;

/**
 * Created by Jelly on 2016/5/20.
 */
public class CustomWorkingDialog {

    private Dialog dialog;
    private View root;

    public CustomWorkingDialog(Context context){
        dialog = new Dialog(context, R.style.CustomFeaturesDialog);
        dialog.setTitle("");
        root = LayoutInflater.from(context).inflate(R.layout.working_dialog,null);
        root.setOnClickListener(v -> dialog.dismiss());
        dialog.setContentView(root);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
    }

    public Dialog show(){
        if(dialog == null){
            return null;
        }
        dialog.show();
        return dialog;
    }

}

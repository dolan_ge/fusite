package com.fusite.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.fusite.handlerinterface.DialogListener;
import com.fusite.pile.R;

/**
 * 自定义的服务选择框Dialog
 * Created by Jelly on 2016/5/5.
 */
public class CustomSelectServiceDialog {

    private Dialog dialog;
    private RelativeLayout fix;
    private RelativeLayout maintain;
    private RelativeLayout mount;
    private DialogListener fixClickListener;
    private DialogListener maintainClickListener;
    private DialogListener mountClickListener;

    public CustomSelectServiceDialog(Context context){
        dialog = new Dialog(context, R.style.CustomFeaturesDialog);
        dialog.setTitle("");
        View view = LayoutInflater.from(context).inflate(R.layout.select_service_dialog,null);
        fix = (RelativeLayout) view.findViewById(R.id.fix);
        maintain = (RelativeLayout) view.findViewById(R.id.maintain);
        mount = (RelativeLayout) view.findViewById(R.id.mount);
        ImageView close = (ImageView) view.findViewById(R.id.close);
        close.setOnClickListener(v -> dialog.dismiss());
        fix.setOnClickListener(v -> {
            dialog.dismiss();
            if(fixClickListener != null){
                fixClickListener.handle(dialog,0);
            }
        });
        maintain.setOnClickListener(v -> {
            dialog.dismiss();
            if(maintainClickListener != null){
                maintainClickListener.handle(dialog,1);
            }
        });
        mount.setOnClickListener(v -> {
            dialog.dismiss();
            if(mountClickListener != null){
                mountClickListener.handle(dialog,2);
            }
        });
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
    }

    public Dialog show(){
        if(dialog == null){
            return null;
        }
        dialog.show();
        return dialog;
    }

    public void setFixClickListener(DialogListener fixClickListener){
        this.fixClickListener = fixClickListener;
    }

    public void setMaintainClickListener(DialogListener maintainClickListener) {
        this.maintainClickListener = maintainClickListener;
    }

    public void setMountClickListener(DialogListener mountClickListener) {
        this.mountClickListener = mountClickListener;
    }
}

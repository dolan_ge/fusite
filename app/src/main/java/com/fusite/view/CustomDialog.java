package com.fusite.view;

import android.app.Dialog;
import android.view.View;

/**
 * Created by Jelly on 2016/7/20.
 */
public class CustomDialog {

    protected Dialog dialog;
    protected View root;

    public boolean show(){
        if(dialog != null && !dialog.isShowing()){
            dialog.show();
            return true;
        }
        return false;
    }

    public boolean dismiss(){
        if(dialog != null && dialog.isShowing()){
            dialog.dismiss();
            return true;
        }
        return false;
    }

}

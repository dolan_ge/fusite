package com.fusite.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fusite.api.ChinaService;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.DialogFreshListener;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.MyApplication;
import com.fusite.pile.R;

import java.util.HashMap;
import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/15.
 */
public class SendDynamicDialog {

    private View root;
    private Dialog dialog;
    private Button send;
    private EditText input;
    private ProgressBar progress;
    private String toId;
    private String forumNo;
    private DialogFreshListener listener;

    public SendDynamicDialog(Context context, DialogFreshListener listener) {
        this.listener = listener;
        dialog = new Dialog(context, R.style.CustomFeaturesDialog);
        dialog.setTitle("");
        root = LayoutInflater.from(context).inflate(R.layout.send_dynamic_commit, null);
        send = (Button) root.findViewById(R.id.send);
        input = (EditText) root.findViewById(R.id.input);
        progress = (ProgressBar) root.findViewById(R.id.progress);
        send.setOnClickListener(v -> {
            String content = input.getText().toString();
            if(TextUtils.isEmpty(content)){
                Toast.makeText(context,"内容不能为空",Toast.LENGTH_SHORT).show();
                return;
            }else if(content.length() > 100){
                Toast.makeText(context,"内容不能超过100个字",Toast.LENGTH_SHORT).show();
                return;
            }
            MyApplication application = (MyApplication) ((Activity) context).getApplication();
            Login.DataEntity dataEntity = application.getLogin().getData();
            Map<String, String> map = new HashMap<String, String>();
            map.put(RequestParam.DynamicCommit.Token, dataEntity.getToken());
            map.put(RequestParam.DynamicCommit.Content,content);
            map.put(RequestParam.DynamicCommit.ForumNo, forumNo);
            if(!TextUtils.isEmpty(toId)){
                map.put(RequestParam.DynamicCommit.ToId, toId);
            }
            Map<String, String> mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, NetParam.sign, RequestParam.DynamicCommit.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));
            send.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
            ChinaService.SendDynamicCommit(mapParam)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(dynamicCommit -> {
                        send.setVisibility(View.VISIBLE);
                        progress.setVisibility(View.INVISIBLE);
                        return dynamicCommit;
                    })
                    .map(dynamicCommit -> {
                        if(TextUtils.equals(dynamicCommit.getIsSuccess(), BeanParam.Failure)){
                            Toast.makeText(context,"发送失败",Toast.LENGTH_SHORT).show();
                            return null;
                        }
                        return dynamicCommit;
                    })
                    .subscribe(dynamicCommit -> {
                        if(dynamicCommit == null){
                            return;
                        }
                        listener.refresh();
                        input.setText("");
                        dialog.dismiss();
                        Toast.makeText(context,"发送成功",Toast.LENGTH_SHORT).show();
                    }, throwable -> {
                        Toast.makeText(context,"系统错误",Toast.LENGTH_SHORT).show();
                    });
        });

        dialog.setContentView(root);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
        dialog.getWindow().getAttributes().width = ViewGroup.LayoutParams.MATCH_PARENT;
    }

    public View getRoot() {
        return root;
    }

    public Dialog show(String forumNo, String toId ,String toName) {
        this.forumNo = forumNo;
        this.toId = toId;

        if(!TextUtils.isEmpty(this.toId)){
            input.setHint("回复"+toName+":");
        }

        if (dialog == null) {
            return null;
        }
        if(!dialog.isShowing()){
            dialog.show();
        }
        return dialog;
    }

}

package com.fusite.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.fusite.pile.R;

/**
 *  自定义的进度条
 * Created by Jelly on 2016/4/27.
 */
public class CustomProgressDialog extends CustomDialog{

    private int hintInt;
    private String hintString;
    private TextView hintTv;


    public CustomProgressDialog(Context context,String hintString){
        this.hintString = hintString;
        dialog = new Dialog(context, R.style.CustomProgressDialog);
        dialog.setTitle("");
        root = LayoutInflater.from(context).inflate(R.layout.loading,null);
        hintTv = (TextView) root.findViewById(R.id.hint);
        hintTv.setText(this.hintString);
        dialog.setContentView(root);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
    }


    public CustomProgressDialog(Context context,int hintInt){
        this.hintInt = hintInt;
        dialog = new Dialog(context, R.style.CustomProgressDialog);
        dialog.setTitle("");
        View view = LayoutInflater.from(context).inflate(R.layout.loading,null);
        hintTv = (TextView) view.findViewById(R.id.hint);
        hintTv.setText(hintInt);
        dialog.setContentView(view);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
    }

    public void setHint(String hintString){
        this.hintString = hintString;
        hintTv.setText(hintString);
    }

    public void setHint(int hintInt){
        this.hintInt = hintInt;
        hintTv.setText(hintInt);
    }
}

package com.fusite.api;

import com.fusite.bean.ActivityInfo;
import com.fusite.bean.Appoint;
import com.fusite.bean.Balance;
import com.fusite.bean.Bill;
import com.fusite.bean.Brands;
import com.fusite.bean.CancelCollect;
import com.fusite.bean.CancelServiceOrder;
import com.fusite.bean.CarStatus;
import com.fusite.bean.CardModPassword;
import com.fusite.bean.CardRecharge;
import com.fusite.bean.ChargeCardItem;
import com.fusite.bean.CheckCode;
import com.fusite.bean.Code;
import com.fusite.bean.Collect;
import com.fusite.bean.CollectList;
import com.fusite.bean.Commit;
import com.fusite.bean.CompleteOrder;
import com.fusite.bean.DeleteChargeCard;
import com.fusite.bean.Dynamic;
import com.fusite.bean.FilterServiceStation;
import com.fusite.bean.FreezeCard;
import com.fusite.bean.GetServiceOrder;
import com.fusite.bean.Invoice;
import com.fusite.bean.InvoiceHistory;
import com.fusite.bean.InvoiceSet;
import com.fusite.bean.Like;
import com.fusite.bean.Login;
import com.fusite.bean.Logout;
import com.fusite.bean.NearbyPileStation;
import com.fusite.bean.News;
import com.fusite.bean.OneDynamic;
import com.fusite.bean.PileStation;
import com.fusite.bean.RegUser;
import com.fusite.bean.RentOrders;
import com.fusite.bean.RentStations;
import com.fusite.bean.ResetLoginPwd;
import com.fusite.bean.ServiceOrderDelete;
import com.fusite.bean.ServiceOrderInfo;
import com.fusite.bean.ServicePay;
import com.fusite.bean.ServiceStations;
import com.fusite.bean.ServiceTypes;
import com.fusite.bean.StationImage;
import com.fusite.bean.UpdateInfo;
import com.fusite.bean.UpdatePassword;
import com.fusite.bean.UpdateUserInfo;
import com.fusite.bean.UpdateVerifiedInfo;
import com.fusite.bean.UploadImage;
import com.fusite.bean.Verified;
import com.fusite.bean.VersionUpdateInfo;
import com.fusite.bean.Withdraw;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by Jelly on 2016/4/29.
 */
public interface Api {
    /**
     * 上传文件
     *
     * @param file
     * @return
     */
    @POST("uploadFile")
    Observable<UploadImage> upload(@Body RequestBody file);

    @POST("unifiedorder")
    Observable<ResponseBody> getGenprePay(@Body RequestBody xml);

    @POST("interface/ev_app_cnt_api.do")
    Observable<ServiceStations> getServiceStations(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<List<PileStation>> getPileStations(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<List<NearbyPileStation>> getNearbyPileStations(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<RentStations> getRentStations(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("server/sendsms")
    Observable<List<Code>> getCode(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("server/prePayOrder")
    Observable<List<Code>> prePay(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("sendSms")
    Observable<List<Code>> testCode(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<FilterServiceStation> filterServiceStation(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<ServiceTypes> getServiceTypes(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<Login> Login(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<List<RegUser>> RegUser(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<CheckCode> CheckCode(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<ResetLoginPwd> ResetLoginPwd(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<List<Logout>> Logout(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @Headers("Content-Type: text/html;charset=GBK")
    @GET("interface/ev_app_cnt_api.do")
    Observable<UpdateUserInfo> UpdateUserInfo(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<List<Balance>> GetBalance(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<ServiceOrderInfo> GetServiceOrderInfo(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<GetServiceOrder> GetServiceOrder(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<List<ChargeCardItem>> GetChargeCardItem(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<List<Bill>> GetBill(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<CarStatus> GetCarStatus(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<RentOrders> GetRentCarOrder(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<CancelServiceOrder> CancelServiceOrder(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<StationImage> GetStationImage(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<Commit> GetCommit(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<Collect> SetCollect(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<CollectList> GetCollect(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<CancelCollect> CancelCollect(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<UpdatePassword> UpdatePassword(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<Dynamic> GetDynamic(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<Like> Like(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<OneDynamic> OneDynamic(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<Verified> GetVerified(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<UpdateVerifiedInfo> UpdateVerifiedInfo(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<Invoice> GetInvoice(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<InvoiceSet> InvoiceSet(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<InvoiceHistory> InvoiceHistory(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<News> GetNews(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<VersionUpdateInfo> GetVersionUpdateInfo(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<Appoint> AppointPile(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<CardRecharge> CardRecharge(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<List<DeleteChargeCard>> DeleteChargeCard(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<CardModPassword> CardModPassword(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<FreezeCard> FreezeCard(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<UpdateInfo> UpdateInfo(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<ServicePay> ServicePay(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<ServiceOrderInfo> ServiceOrderInfo(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<ServiceOrderDelete> ServiceOrderDelete(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<Brands> Brands(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );

    @POST("interface/ev_app_cnt_api.do")
    Observable<List<CompleteOrder>> CompleteOrder(@QueryMap Map<String,String> map);

    @POST("interface/ev_app_cnt_api.do")
    Observable<List<ActivityInfo>> activityInfo(@QueryMap Map<String,String> map);

    @POST("interface/ev_app_cnt_api.do")
    Observable<Withdraw> WithdrawService(
            @Query("trancode") String trancode
            ,@Query("mode") String mode
            ,@Query("timestamp") String timestamp
            ,@Query("custid") String custid
            ,@Query("sign_method") String sign_method
            ,@Query("execmode") String execmode
            ,@Query("sign") String sign
            ,@Query("fields") String fields
            ,@Query("condition") String condition
    );
}

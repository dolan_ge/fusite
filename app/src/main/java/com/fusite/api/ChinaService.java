package com.fusite.api;

import android.util.Log;

import com.fusite.bean.DynamicCommit;
import com.fusite.bean.InvoiceSet;
import com.fusite.bean.SendCommit;
import com.fusite.bean.SendDynamic;
import com.fusite.bean.SetServiceOrder;
import com.fusite.bean.UpdateUserInfo;
import com.fusite.utils.JsonUtil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Jelly on 2016/5/14.
 */
public class ChinaService {

    public static Observable<UpdateUserInfo> UpdateUserInfo(Map<String, String> map) {
        return Observable.create(new Observable.OnSubscribe<UpdateUserInfo>() {

            @Override
            public void call(Subscriber<? super UpdateUserInfo> subscriber) {
                try {
                    String param = spliceParam(map);
                    URL url = new URL(ApiService.AppPath);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.setUseCaches(false);
                    connection.connect();
                    DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                    out.write(param.getBytes("GBK"));
                    out.flush();
                    out.close();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "GBK"));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                    reader.close();
                    subscriber.onNext(JsonUtil.objectFromJson(buffer.toString(), UpdateUserInfo.class));
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static Observable<SetServiceOrder> SetServiceOrder(Map<String, String> map) {
        return Observable.create(new Observable.OnSubscribe<SetServiceOrder>() {

            @Override
            public void call(Subscriber<? super SetServiceOrder> subscriber) {
                try {
                    String param = spliceParam(map);
                    URL url = new URL(ApiService.AppPath);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.setUseCaches(false);
                    connection.connect();
                    DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                    out.write(param.getBytes("GBK"));
                    out.flush();
                    out.close();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "GBK"));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                    reader.close();
                    subscriber.onNext(JsonUtil.objectFromJson(buffer.toString(), SetServiceOrder.class));
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static Observable<SendCommit> SendCommit(Map<String, String> map) {
        return Observable.create(new Observable.OnSubscribe<SendCommit>() {

            @Override
            public void call(Subscriber<? super SendCommit> subscriber) {
                try {
                    String param = spliceParam(map);
                    URL url = new URL(ApiService.AppPath);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.setUseCaches(false);
                    connection.connect();
                    DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                    out.write(param.getBytes("GBK"));
                    out.flush();
                    out.close();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "GBK"));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                    reader.close();
                    Log.v("json",buffer.toString());
                    subscriber.onNext(JsonUtil.objectFromJson(buffer.toString(), SendCommit.class));
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static Observable<SendDynamic> SendDynamic(Map<String, String> map){
        return Observable.create(new Observable.OnSubscribe<SendDynamic>() {

            @Override
            public void call(Subscriber<? super SendDynamic> subscriber) {
                try {
                    String param = spliceParam(map);
                    URL url = new URL(ApiService.AppPath);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.setUseCaches(false);
                    connection.connect();
                    DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                    out.write(param.getBytes("GBK"));
                    out.flush();
                    out.close();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "GBK"));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                    reader.close();
                    Log.v("json",buffer.toString());
                    subscriber.onNext(JsonUtil.objectFromJson(buffer.toString(), SendDynamic.class));
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static Observable<DynamicCommit> SendDynamicCommit(Map<String, String> map){
        return Observable.create(new Observable.OnSubscribe<DynamicCommit>() {

            @Override
            public void call(Subscriber<? super DynamicCommit> subscriber) {
                try {
                    String param = spliceParam(map);
                    URL url = new URL(ApiService.AppPath);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.setUseCaches(false);
                    connection.connect();
                    DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                    out.write(param.getBytes("GBK"));
                    out.flush();
                    out.close();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "GBK"));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                    reader.close();
                    Log.v("json",buffer.toString());
                    subscriber.onNext(JsonUtil.objectFromJson(buffer.toString(), DynamicCommit.class));
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static Observable<InvoiceSet> InvoiceSet(Map<String, String> map){
        return Observable.create(new Observable.OnSubscribe<InvoiceSet>() {

            @Override
            public void call(Subscriber<? super InvoiceSet> subscriber) {
                try {
                    String param = spliceParam(map);
                    URL url = new URL(ApiService.AppPath);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.setUseCaches(false);
                    connection.connect();
                    DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                    out.write(param.getBytes("GBK"));
                    out.flush();
                    out.close();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "GBK"));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                    reader.close();
                    subscriber.onNext(JsonUtil.objectFromJson(buffer.toString(), InvoiceSet.class));
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static <T> Observable<T> chinaRequest(Map<String, String> map,Class<T> tClass){
        return Observable.create(new Observable.OnSubscribe<T>() {

            @Override
            public void call(Subscriber<? super T> subscriber) {
                try {
                    String param = spliceParam(map);
                    URL url = new URL(ApiService.AppPath);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setRequestMethod("POST");
                    connection.setUseCaches(false);
                    connection.connect();
                    DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                    out.write(param.getBytes("GBK"));
                    out.flush();
                    out.close();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "GBK"));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                    reader.close();
                    subscriber.onNext(JsonUtil.objectFromJson(buffer.toString(), tClass));
                    subscriber.onCompleted();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 拼接参数
     * @param map
     * @return
     */
    public static String spliceParam(Map<String, String> map) {
        String result = "";
        for (String key : map.keySet()) {
            result += key + "=" + map.get(key) + "&";
        }
        result = result.substring(0, result.length() - 1);
        return result;
    }

}

package com.fusite.handlerinterface;

/**
 * 获取OrgId
 */
public interface OrgIdFragmentListener {
    String getOrgId();
}
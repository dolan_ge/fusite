package com.fusite.handlerinterface;

import android.app.Dialog;

/**
 * Created by Jelly on 2016/5/6.
 */
public interface DialogListener {
    void handle(Dialog dialog,int index);
}

package com.fusite.handlerinterface;

import android.content.Context;
import android.widget.Toast;

import com.fusite.utils.DialogUtil;
import com.fusite.view.CustomDialog;

import rx.functions.Action;
import rx.functions.Action1;

/**
 * Created by Jelly on 2016/7/21.
 */
public class ThrowableAction implements Action1<Throwable> {

    public String TAG = "ThrowableAction";

    private Context context;
    private CustomDialog loading;

    public ThrowableAction(Context context) {
        this.context = context;
    }

    public ThrowableAction(Context context, CustomDialog loading){
        this.context = context;
        this.loading = loading;
    }

    @Override
    public void call(Throwable throwable) {
        Toast.makeText(context,"加载失败",Toast.LENGTH_SHORT).show();
        if (loading != null) {
            DialogUtil.getInstance().dismiss(loading);
        }
    }
}

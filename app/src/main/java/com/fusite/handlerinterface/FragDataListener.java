package com.fusite.handlerinterface;

import java.util.Map;

/**
 * Created by Jelly on 2016/7/11.
 */
public interface FragDataListener {

    Map<String,String> getData();

}

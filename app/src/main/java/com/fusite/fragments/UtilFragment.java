package com.fusite.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fusite.utils.DialogUtil;
import com.fusite.utils.ErrorParamUtil;
import com.fusite.utils.PopupWindowUtil;
import com.fusite.utils.ResourceUtil;
import com.fusite.utils.SPCacheUtil;
import com.fusite.utils.ToastUtil;
import com.fusite.utils.WindowUtil;

/**
 * Created by Jelly on 2016/3/30.
 */
public abstract class UtilFragment extends SimpleFragment{
    /**
     * PopupWindow操作工具
     */
    public PopupWindowUtil popupWindowUtil = PopupWindowUtil.getInstance();
    /**
     * 资源操作工具
     */
    public ResourceUtil resourceUtil = ResourceUtil.getInstance();
    /**
     * Window操作工具
     */
    public WindowUtil windowUtil = WindowUtil.getInstance();
    /**
     * 错误码操作工具
     */
    public ErrorParamUtil errorParamUtil = ErrorParamUtil.getInstance();
    /**
     * Toast操作工具
     */
    public ToastUtil toastUtil = ToastUtil.getInstance();
    /**
     * Dialog操作工具
     */
    public DialogUtil dialogUtil = DialogUtil.getInstance();
    /**
     * SharePreference缓存操作工具
     */
    public SPCacheUtil spCacheUtil = SPCacheUtil.getInstance();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}

package com.fusite.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * BaseFragment
 * Created by Jelly on 2016/3/2.
 */
public abstract class BaseFragment extends Fragment{

    public String TAG="BaseFragment";
    public View root; //显示的界面

    public boolean isVisible = false; //是否可见
    public boolean isLoad = false; //是否已经加载数据
    public boolean isPrepared; //控件是否已经初始化


    public BaseFragment(){
        if(getArguments() == null){
            setArguments(new Bundle());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        isPrepared = true;
        return root;
    }


    /**
     * 初始化控件
     */
    public void init(){};

    /**
     * 加载Activity中的属性,比如：Application和数据持久化的对象
     */
    public void loadObjectAttribute(){};

    /**
     * 在这里设置各种各样的控件监听器
     */
    public void setListener(){};

    /**
     * 设置Activity需要显示的数据
     */
    public abstract void setFragView();


    /**
     * 
     * @param isVisibleToUser
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(getUserVisibleHint()) {
            isVisible = true;
            lazyLoad();
        } else {
            isVisible = false;
        }
    }


    protected void lazyLoad(){
        if(!isPrepared || !isVisible || isLoad){
            return;
        }
        init();
        loadObjectAttribute();
        setListener();
        setFragView();
        isLoad = true;
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getTAG() {
        return TAG;
    }
}
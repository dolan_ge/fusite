package com.fusite.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fusite.pile.MyApplication;

import java.util.List;

/**
 * Created by Jelly on 2016/3/30.
 */
public abstract class SimpleFragment extends BaseFragment{
    /**
     * Application
     */
    public MyApplication application;
    /**
     * Context
     */
    public Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        initFragment();
        return root;
    }

    /**
     * 初始化数据
     */
    public void initFragment(){
        application = (MyApplication) getActivity().getApplication();
        context = getContext();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        application = null;
        context = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final FragmentManager childFragmentManager = getChildFragmentManager();
        if (childFragmentManager != null) {
            final List<Fragment> nestedFragments = childFragmentManager.getFragments();
            if (nestedFragments == null || nestedFragments.size() == 0) return;
            for (Fragment childFragment : nestedFragments) {
                if (childFragment != null && !childFragment.isDetached() && !childFragment.isRemoving()) {
                    childFragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }
}

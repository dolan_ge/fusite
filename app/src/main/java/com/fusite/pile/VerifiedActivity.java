package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.Verified;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.LogUtil;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/16.
 */
public class VerifiedActivity extends UtilActivity {
    public String TAG = "VerifiedActivity";
    @BindView(R.id.stateHint1)
    TextView stateHint1;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.cardNo)
    TextView cardNo;
    @BindView(R.id.stateHint2)
    TextView stateHint2;
    @BindView(R.id.arrow)
    ImageView arrow;
    @BindView(R.id.top)
    RelativeLayout top;
    @BindView(R.id.verifiedState)
    RelativeLayout verifiedState;

    private Verified verified;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.verified_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {
        Login.DataEntity dataEntity = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetDynamic.Token, dataEntity.getToken());
        ApiService.getPileService().GetVerified(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.GetVerified.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(verified -> {
                    if (TextUtils.equals(BeanParam.Failure, verified.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(verified.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    LogUtil.v(TAG,new Gson().toJson(verified));
                    return verified;
                })
                .subscribe(verified -> {
                    if (verified == null) {
                        return;
                    }
                    this.verified = verified;
                    Verified.DataEntity verifiedData = verified.getData();
                    if (TextUtils.equals(BeanParam.Verified.State1, verifiedData.getAuditStatus())) { //认证中
                        stateHint2.setText("认证中");
                        stateHint1.setText("您的信息正在认证");
                        top.setClickable(false);
                    } else if (TextUtils.equals(BeanParam.Verified.State2, verifiedData.getAuditStatus())) { //认证通过
                        stateHint2.setText("已认证");
                        stateHint1.setText("您已通过实名认证");
                        name.setText(verifiedData.getName());
                        cardNo.setText(verifiedData.getIdentityNum());
                        top.setClickable(false);
                        //更新账户缓存
                        Login.DataEntity loginData = application.getLogin().getData();
                        loginData.setVerified(BeanParam.Login.isVerified);
                        application.cacheLogin();
                    } else if (TextUtils.equals(BeanParam.Verified.State4, verifiedData.getAuditStatus())) { //认证失败
                        stateHint2.setText("认证失败，查看原因");
                        stateHint1.setText("点击重新认证");
                        arrow.setVisibility(View.VISIBLE);
                        top.setClickable(true);
                        verifiedState.setOnClickListener(v -> HintTextActivity.startActivity(context,verifiedData.getReason()));
                    }else{
                        top.setClickable(true);
                    }
                });

    }

    @OnClick({R.id.top})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.top:
                UploadVerifiedInfoActivity.startActivity(context);
                break;
        }
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, VerifiedActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.CarStatus;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.LogUtil;
import com.fusite.view.SpeedWatch;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 车辆操作界面
 * Created by Jelly on 2016/5/17.
 */
public class CarOperActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "CarOperActivity";

    @BindView(R.id.sw)
    SpeedWatch sw;
    public String vehicleId;
    @BindView(R.id.SOC)
    TextView SOC;
    @BindView(R.id.motorTemp)
    TextView motorTemp;
    @BindView(R.id.batteryTemp)
    TextView batteryTemp;
    @BindView(R.id.batteryVoltage)
    TextView batteryVoltage;
    @BindView(R.id.batteryCurrent)
    TextView batteryCurrent;
    @BindView(R.id.licenseIcon)
    TextView licenseIcon;
    @BindView(R.id.license)
    TextView license;
    @BindView(R.id.mileage)
    TextView mileage;

    private boolean isPause;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.car_oper);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }


    @Override
    protected void onPause() {
        super.onPause();
        isPause = !isPause;
    }

    @Override
    public void loadObjectAttribute() {
        if (TextUtils.isEmpty(vehicleId)) {
            vehicleId = intent.getStringExtra("vehicleId");
        }
    }

    @Override
    public void setActivityView() {
        load();
    }

    public void load() {
        Login.DataEntity dataEntity = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetCarStatus.Token, dataEntity.getToken());
        map.put(RequestParam.GetCarStatus.VehicleId, vehicleId);
        ApiService.getPileService().GetCarStatus(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.GetCarStatus.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(carStatus -> {
                    if (TextUtils.equals(carStatus.getIsSuccess(), BeanParam.Failure)) {
                        Error error = errorParamUtil.checkReturnState(carStatus.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return carStatus;
                })
                .subscribe(carStatus -> {
                    if (carStatus == null) {
                        return;
                    }
                    CarStatus.DataEntity dataEntity1 = carStatus.getData();
                    sw.setGra((int) Double.parseDouble(dataEntity1.getSpeed()));
                    SOC.setText((int) Double.parseDouble(dataEntity1.getSOC()) + "%");
                    motorTemp.setText((int) Double.parseDouble(dataEntity1.getMotorTemp()) + "度");
                    batteryTemp.setText((int) Double.parseDouble(dataEntity1.getBatteryTemp()) + "度");
                    batteryVoltage.setText((int) (Double.parseDouble(dataEntity1.getBatteryVoltage())) + "V");
                    batteryCurrent.setText((int) (Double.parseDouble(dataEntity1.getBatteryCurrent())) + "A");
                    licenseIcon.setText(dataEntity1.getLicensePlate().substring(0, 1));
                    license.setText(dataEntity1.getLicensePlate());
                    new Thread(() -> {
                        try {
                            Thread.sleep(6000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if(!isPause){
                            load();
                        }
                    }).start();
                }, throwable -> {
                    LogUtil.v(TAG,throwable.getMessage());
                });
    }

    public static void startActivity(Context context, String vehicleId) {
        Intent intent = new Intent(context, CarOperActivity.class);
        intent.putExtra("vehicleId", vehicleId);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

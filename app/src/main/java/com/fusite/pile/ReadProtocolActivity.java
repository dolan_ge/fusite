package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fusite.activities.UtilActivity;

/**
 * Created by Jelly on 2016/7/28.
 */
public class ReadProtocolActivity extends UtilActivity{
    public String TAG = "ReadProtocolActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.read_protcol);
    }

    @Override
    public void setActivityView() {

    }

    public static void startActivity(Context context){
        Intent intent = new Intent(context,ReadProtocolActivity.class);
        context.startActivity(intent);
    }

}

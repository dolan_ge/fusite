package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.api.ChinaService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.SetServiceOrder;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.map.MyLocation;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.AssetsUtils;
import com.fusite.utils.BitmapUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.fusite.view.CustomDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import cn.qqtheme.framework.picker.AddressPicker;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/5/6.
 */
public class ServiceBookActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "ServiceBookActivity";
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.model)
    EditText model;
    @BindView(R.id.nickName)
    TextView nickName;
    @BindView(R.id.area)
    TextView area;
    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.image1)
    ImageView image1;
    @BindView(R.id.image2)
    ImageView image2;
    @BindView(R.id.image3)
    ImageView image3;
    @BindView(R.id.notes)
    TextView notes;

    private String serviceId;
    private String orgId;

    public static final int MaxPhotoNum = 3;
    private ImageView[] images = new ImageView[3];
    private List<PhotoInfo> resultList;
    private List<String> urls;

    private String areaStr; //区域
    private String addressStr; //详细地址
    private String notesStr;
    private String telStr;
    private String modelStr;
    private CustomDialog loading;
    private static final int EditErrorRequest = 0x001;
    private MyLocation location;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.service_book);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void init() {
        images[0] = image1;
        images[1] = image2;
        images[2] = image3;
    }

    @Override
    public void loadObjectAttribute() {
        location = application.getLocation();
        if (TextUtils.isEmpty(serviceId)) {
            serviceId = intent.getStringExtra("serviceId");
        }
        if (TextUtils.isEmpty(orgId)) {
            orgId = intent.getStringExtra("orgId");
        }
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {
        Login.DataEntity dataEntity = application.getLogin().getData();
        nickName.setText(dataEntity.getCustName());
        phone.setText(dataEntity.getMobile());
        //设置默认区域
        areaStr = location.getProvince() + "    " + location.getCity() + "     " + location.getDistrict();
        area.setText(areaStr);
        addressStr = location.getAddress();
        address.setText(addressStr);
    }

    /**
     * 打开图库
     */
    public void startGallery(int index) {
        int max = MaxPhotoNum - index;
        //修改最大的图片选择数量
        FunctionConfig config = new FunctionConfig.Builder()
                .setMutiSelectMaxSize(max)
                .build();
        GalleryFinal.openGalleryMuti(0x001, config, new MyHandler(this));
    }

    private static class MyHandler implements GalleryFinal.OnHanlderResultCallback {

        private WeakReference<ServiceBookActivity> mActivity;

        private MyHandler(ServiceBookActivity activity) {
            mActivity = new WeakReference<ServiceBookActivity>(activity);
        }

        @Override
        public void onHanlderFailure(int requestCode, String errorMsg) {
            LogUtil.v(mActivity.get().TAG, errorMsg);
        }

        @Override
        public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
            ServiceBookActivity activity = mActivity.get();
            if (activity.resultList == null) {
                activity.resultList = new ArrayList<>();
            }

            for (int i = 0; i < resultList.size(); i++) {
                activity.resultList.add(resultList.get(i));
            }
            activity.setImage();
        }
    }

    public void setImage() {
        for (int i = 0; i < resultList.size(); i++) {
            Picasso.with(context).load(new File(resultList.get(i).getPhotoPath())).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).into(images[i]);
            if (images[i].isClickable()) {
                images[i].setClickable(false);
            }
        }

        if (resultList.size() < 3) {
            images[resultList.size()].setImageResource(R.drawable.add_img);
            images[resultList.size()].setOnClickListener(v -> startGallery(resultList.size()));
        }

    }

    @OnClick({R.id.areaLayout, R.id.ok, R.id.image1, R.id.notesLayout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.areaLayout:
                ArrayList<AddressPicker.Province> data = new ArrayList<>();
                String json = AssetsUtils.readText(this, "city.json");
                data.addAll(JsonUtil.arrayFormJson(json, AddressPicker.Province[].class));
                AddressPicker picker = new AddressPicker(this, data);
                picker.setSelectedItem(location.getProvince(), location.getCity(), location.getDistrict());
                //picker.setHideProvince(true);//加上此句举将只显示地级及县级
                //picker.setHideCounty(true);//加上此句举将只显示省级及地级

                picker.setOnAddressPickListener((province, city, county) -> {
                    area.setText(province + "    " + city + "    " + county);
                    areaStr = province + city + county;
                });

                picker.show();
                break;
            case R.id.ok:
                areaStr = area.getText().toString();
                addressStr = address.getText().toString();
                telStr = phone.getText().toString();
                modelStr = model.getText().toString();

                if (NetParam.isEmpty(areaStr, addressStr, telStr, modelStr, notesStr)) {
                    Toast.makeText(context, "请填写完整信息", Toast.LENGTH_SHORT).show();
                    return;
                }

                loading = dialogUtil.startLoading(context,loading,R.string.loadingText);

                if (resultList == null || resultList.size() == 0) { //没有图片
                    uploadServiceInfo();
                }else{
                    uploadServiceInfo(0);//上传图片
                }



                break;
            case R.id.image1:
                startGallery(0);
                break;
            case R.id.notesLayout:
                EditErrorActivity.startActivity(context, EditErrorRequest);
                break;
        }
    }

    public void uploadServiceInfo() {
        Login.DataEntity dataEntity = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.SetServiceOrder.Token, dataEntity.getToken());
        map.put(RequestParam.SetServiceOrder.Tel, telStr);
        map.put(RequestParam.SetServiceOrder.Model, modelStr);
        map.put(RequestParam.SetServiceOrder.OrgId, orgId);
        map.put(RequestParam.SetServiceOrder.ServiceId, serviceId);
        map.put(RequestParam.SetServiceOrder.Area, areaStr);
        map.put(RequestParam.SetServiceOrder.Adress, addressStr);
        map.put(RequestParam.SetServiceOrder.Notes, notesStr);


        for (int i = 0; i < (urls == null ? 0 : urls.size()); i++) {
            map.put(RequestParam.SetServiceOrder.Picture + (i + 1), urls.get(i));
        }

        Map<String, String> mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, NetParam.sign, RequestParam.SetServiceOrder.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));
        ChinaService.chinaRequest(mapParam, SetServiceOrder.class)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(setServiceOrder -> {
                    if (TextUtils.equals(BeanParam.Failure, setServiceOrder.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(setServiceOrder.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return setServiceOrder;
                })
                .subscribe(setServiceOrder -> {
                    dialogUtil.dismiss(loading);
                    if (setServiceOrder == null) {
                        return;
                    }
                    finish();
                }, new ThrowableAction(this));
    }

    public void uploadServiceInfo(int index) {
        Observable.just(resultList.get(index).getPhotoPath())
                .observeOn(Schedulers.newThread())
                .map(s -> {
                    BitmapUtil bitmapUtil1 = BitmapUtil.getInstance();
                    return bitmapUtil1.Bitmap2Bytes(bitmapUtil1.getimage(s,400));
                })
                .flatMap(bytes ->
                        ApiService.getPileService().upload(new MultipartBody.Builder().setType(MultipartBody.FORM)
                                .addFormDataPart("serviceInfo", "serviceInfo.png", RequestBody.create(MediaType.parse("image/png/jpg; charset=utf-8"), bytes)).build())
                )
                .observeOn(AndroidSchedulers.mainThread())
                .map(uploadImage -> {
                    if (TextUtils.equals(BeanParam.Failure, uploadImage.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(uploadImage.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return uploadImage;
                })
                .subscribe(uploadImage -> {
                    if (uploadImage == null) {
                        dialogUtil.dismiss(loading);
                        return;
                    }

                    if (urls == null) {
                        urls = new ArrayList<>();
                    }

                    urls.add(uploadImage.getData().get(0).getUrl());

                    if (index + 1 == resultList.size()) {
                        uploadServiceInfo();
                    }else{ //继续上传下一张照片
                        uploadServiceInfo(index + 1);
                    }

                }, new ThrowableAction(this));
    }

    public static void startActivity(Context context, String orgId, String serviceId) {
        Intent intent = new Intent(context, ServiceBookActivity.class);
        intent.putExtra("orgId", orgId);
        intent.putExtra("serviceId", serviceId);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case EditErrorRequest:
                if (resultCode == EditErrorActivity.Success) {
                    notesStr = data.getStringExtra("notes");
                    notes.setText(notesStr);
                }
                break;
        }

    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

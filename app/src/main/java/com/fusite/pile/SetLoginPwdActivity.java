package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.view.CustomDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 设置密码
 * Created by Jelly on 2016/3/1.
 */
public class SetLoginPwdActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "SetLoginPwdActivity";
    @BindView(R.id.set_pwd_et01_clear)
    ImageView setPwdEt01Clear;
    @BindView(R.id.set_pwd_et01)
    EditText setPwdEt01;
    @BindView(R.id.set_pwd_et02_clear)
    ImageView setPwdEt02Clear;
    @BindView(R.id.set_pwd_et02)
    EditText setPwdEt02;


    private String phone;
    private String code;
    private String password;

    private CustomDialog loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.set_pwd);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void loadObjectAttribute() {
        phone = getIntent().getStringExtra("phone");
        code = getIntent().getStringExtra("code");
    }

    @Override
    public void init() {
    }


    @Override
    public void setListener() {
        setEtClearListener();
    }

    @Override
    public void setActivityView() {

    }

    /**
     * 设置输入框的清空按钮监听事件
     */
    public void setEtClearListener() {
        setPwdEt01.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (setPwdEt01.getText().length() > 0) {
                    if (!setPwdEt01Clear.isShown()) {
                        setPwdEt01Clear.setVisibility(View.VISIBLE);
                    }
                    if (!setPwdEt01Clear.isClickable()) {
                        setPwdEt01Clear.setOnClickListener(v -> setPwdEt01.setText(""));
                    }
                } else {
                    setPwdEt01Clear.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        setPwdEt02.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (setPwdEt02.getText().length() > 0) {
                    if (!setPwdEt02Clear.isShown()) {
                        setPwdEt02Clear.setVisibility(View.VISIBLE);
                    }
                    if (!setPwdEt02Clear.isClickable()) {
                        setPwdEt02Clear.setOnClickListener(v -> setPwdEt02.setText(""));
                    }
                } else {
                    setPwdEt02Clear.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick(R.id.okBtn)
    public void onClick() {
        String password1 = setPwdEt01.getText().toString();
        String password2 = setPwdEt02.getText().toString();

        if(TextUtils.isEmpty(password1) || TextUtils.isEmpty(password2)){
            Toast.makeText(this,"两次输入的密码不能为空",Toast.LENGTH_SHORT).show();
            return;
        }

        if (!TextUtils.equals(password1, password2)) {
            Toast.makeText(this, R.string.twoPasswordError, Toast.LENGTH_SHORT).show();
            return;
        }

        loading = dialogUtil.startLoading(this,loading,R.string.loadingText);

        password = password1;
        Map<String, String> map = new HashMap();
        map.put(RequestParam.ResetLoginPwd.Mobile, phone);
        map.put(RequestParam.ResetLoginPwd.Password, password);
        map.put(RequestParam.ResetLoginPwd.Code, code);
        ApiService.getPileService().ResetLoginPwd(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.ResetLoginPwd.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(resetLoginPwd -> {
                    dialogUtil.dismiss(loading);
                    if (TextUtils.equals(resetLoginPwd.getIsSuccess(), BeanParam.Failure)) {
                        Error error = errorParamUtil.checkReturnState(resetLoginPwd.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return resetLoginPwd;
                })
                .observeOn(Schedulers.newThread())
                .flatMap(resetLoginPwd -> {
                    if (resetLoginPwd == null) {
                        return null;
                    }
                    map.clear();
                    map.put(RequestParam.Login.Mobile, phone);
                    map.put(RequestParam.Login.Password, password);
                    map.put(RequestParam.Login.Type, RequestParam.Login.PasswordLogin);
                    Observable<Login> observe = ApiService.getPileService().Login(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.Login.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map));
                    return observe;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .map(login -> {
                    if (login == null) {
                        return null;
                    }
                    if (TextUtils.equals(BeanParam.Failure, login.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(login.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return login;
                })
                .subscribe(login -> {
                    if (login == null) {
                        return;
                    }
                    application.setLogin(login);
                    application.cacheLogin();
                    MainActivity.startActivity(context);
                }, new ThrowableAction(this));
    }



    public static void startActivity(Context context, String phone, String code) {
        Intent intent = new Intent(context, SetLoginPwdActivity.class);
        intent.putExtra("phone", phone);
        intent.putExtra("code", code);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.ChargeCardListAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.ChargeCardItem;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.HttpCacheUtil;
import com.fusite.utils.JsonUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * 充电卡显示
 * Created by Jelly on 2016/3/23.
 */
public class PileCardActivity extends UtilActivity {

    public String TAG = "PileCardActivity";
    @BindView(R.id.list)
    ListView list;


    private List<ChargeCardItem> daos;
    private int type; //0 :正常，1：选择卡
    private ChargeCardListAdapter adapter;
    public static final int ADD = 0x001;
    public static final int DELETE = 0x002;
    public static final int CHANGE_BALANCE = 0x003;
    private HttpCacheUtil httpCacheUtil;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.pile_card);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void loadObjectAttribute() {
        application = (MyApplication) getApplication();
        daos = getIntent().getParcelableArrayListExtra("daos");
        // 测试阶段使用
        if (daos.size() ==0) {
//            ChargeCardItem item=new ChargeCardItem();
//            ChargeCardItem.CrmAccountsGetEntity itemEntity = new ChargeCardItem.CrmAccountsGetEntity();
//            itemEntity.setAccountNo("123456789009876543210");
//            itemEntity.setBalanceAmt("20");
//            item.setCrm_accounts_get(itemEntity);
//            daos.add(item);
        }
        type = getIntent().getIntExtra("type", 0);
        adapter = new ChargeCardListAdapter(this, daos);
    }

    @Override
    public void setListener() {
        setCardItemClickListener();
    }

    @Override
    public void setActivityView() {
        list.setAdapter(adapter);
    }

    /**
     * 设置单个列表的点击事件
     */
    public void setCardItemClickListener() {
        list.setOnItemClickListener((parent, view, position, id) ->
                {
                    if (type ==1 ) {
                        WithdrawActivity.startActivity(PileCardActivity.this,CHANGE_BALANCE,daos.get(position),0);
                        return;
                    }
                    if (type ==2){
                        WithdrawActivity.startActivity(PileCardActivity.this,CHANGE_BALANCE,daos.get(position),1);
                        return;
                    }

                    PileCardOperActivity.startActivity(PileCardActivity.this, daos.get(position), DELETE);
                });
    }



    /**
     * 在每次获取焦点的时候更新列表
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * 增加充电卡
     * @param view
     * @return
     */
    public View addCard(View view) {
        AddPileCardActivity.startActivity(this, ADD);
        return view;
    }

    public void load(){
        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetBalance.Token, loginData.getToken());
        //获取充电卡
        ApiService.getPileService().GetChargeCardItem(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.GetChargeItemCard.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(chargeCardItems1 -> {
                    if (TextUtils.equals(BeanParam.Failure, chargeCardItems1.get(0).getCrm_accounts_get().getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(chargeCardItems1.get(0).getCrm_accounts_get().getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return chargeCardItems1;
                })
                .subscribe(chargeCardItems1 -> {
                    if (chargeCardItems1 == null) return;

                    if (daos == null) {
                        daos = chargeCardItems1;
                    } else {
                        daos.clear();
                        daos.addAll(chargeCardItems1);
                    }

                    adapter.notifyDataSetChanged(); //更新数据
                    //缓存
                    if (httpCacheUtil == null) {
                        httpCacheUtil = new HttpCacheUtil(context);
                    }
                    httpCacheUtil.cacheHttp(JsonUtil.Object2Json(chargeCardItems1), RequestParam.GetChargeItemCard.InterfaceName);
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if(daos == null){
                            daos = new ArrayList<ChargeCardItem>();
                        }else{
                            daos.clear();
                        }
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ADD:
                if (resultCode == AddPileCardActivity.Success) { //添加充电卡成功
                    load();
                }
                break;
            case DELETE:
                if (resultCode == PileCardOperActivity.SUCCESS) {//删除充电卡成功
                    load();
                }
                break;
            case CHANGE_BALANCE:
                load();
                break;
        }
    }

    /**
     * 通过数据Dao获取数据集合中的Index
     * @param chargeCardItemDao
     * @return
     */
    public int getIndexFromList(ChargeCardItem chargeCardItemDao) {
        for (int i = 0; i < daos.size(); i++) {
            if (TextUtils.equals(daos.get(i).getCrm_accounts_get().getAccountID(), chargeCardItemDao.getCrm_accounts_get().getAccountID())) {
                return i;
            }
        }
        return 0;
    }

    /**
     * 开启界面
     */
    public static void startActivity(Context context, ArrayList<ChargeCardItem> daos) {
        Intent intent = new Intent(context, PileCardActivity.class);
        intent.putParcelableArrayListExtra("daos", daos);
        context.startActivity(intent);
    }

    /**
     * 开启界面
     */
    public static void startActivity(Context context, ArrayList<ChargeCardItem> daos,int type) {
        Intent intent = new Intent(context, PileCardActivity.class);
        intent.putParcelableArrayListExtra("daos", daos);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.bean.AppointCost;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.PayingAppointOrder;
import com.fusite.bean.PileListItem;
import com.fusite.constant.TypeDefined;
import com.fusite.handlerinterface.NetErrorHandlerListener;
import com.fusite.impl.AppointCostDaoImpl;
import com.fusite.impl.AppointDaoImpl;
import com.fusite.impl.CancelAppointPayDaoImpl;
import com.fusite.impl.OrderInfoDaoImpl;
import com.fusite.impl.PayingAppointOrderDaoImpl;
import com.fusite.view.CustomDialog;
import com.jellycai.service.ThreadManage;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 预约界面
 * Created by Jelly on 2016/3/9.
 */
public class PileBookActivity extends UtilActivity implements View.OnClickListener, NetErrorHandlerListener {

    public String TAG = "PileBookActivity";
    @BindView(R.id.time10)
    TextView time10;
    @BindView(R.id.time15)
    TextView time15;
    @BindView(R.id.time30)
    TextView time30;
    @BindView(R.id.orgName)
    TextView orgName;
    @BindView(R.id.addr)
    TextView addr;
    @BindView(R.id.facilityName)
    TextView facilityName;
    @BindView(R.id.facilityType)
    TextView facilityType;
    @BindView(R.id.facilityMode)
    TextView facilityMode;
    @BindView(R.id.applicableCar)
    TextView applicableCar;
    @BindView(R.id.appoint_price)
    TextView appointPrice;
    @BindView(R.id.appoint)
    Button appoint;

    private TextView selectTime;

    private AppointDaoImpl appointDao;

    private static final int msgAppoint = 0x001;

    private CustomDialog loading;

    private OrderInfoDaoImpl orderInfoDao;

    private static final int msgInfo = 0x002;

    private static final int msgLoading = 0x003;

    private static boolean isPause = false;

    private static boolean isSecond = false;

    private static boolean isSuccess = false;

    public static final int SUCCESS = 0x004;

    public static final int FAILURE = 0x005;

    private AppointCostDaoImpl appointCostDao;

    private static final int msgAppointCost = 0x006;

    private PayingAppointOrderDaoImpl payingAppointOrderDao;

    private static final int msgPayingAppoint = 0x007;

    public static final int PAYREQUEST = 0x008;

    private CancelAppointPayDaoImpl cancelAppointPayDao;

    private static final int msgCancelAppointPay = 0x009;

    private MyHandler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.pile_book);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPause = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void init() {
    }

    @Override
    public void loadObjectAttribute() {
        handler = new MyHandler(this);
        appointDao = new AppointDaoImpl(this, handler, msgAppoint);
        orderInfoDao = new OrderInfoDaoImpl(this, handler, msgInfo);
        appointCostDao = new AppointCostDaoImpl(this, handler, msgAppointCost);
        payingAppointOrderDao = new PayingAppointOrderDaoImpl(this, handler, msgPayingAppoint);
        cancelAppointPayDao = new CancelAppointPayDaoImpl(this, handler, msgCancelAppointPay);
    }

    @Override
    public void setListener() {
    }

    @Override
    public void setActivityView() {
        PileListItem.EvcFacilitiesGetEntity entity = intent.getParcelableExtra("Entity");
        orgName.setText(entity.getOrgName());
        addr.setText(intent.getStringExtra("Addr"));
        facilityName.setText(entity.getFacilityName());
        if (TextUtils.equals(TypeDefined.FACILITY_TYPE_DC, entity.getFacilityType())) {
            facilityType.setText("直流桩");
        } else if (TextUtils.equals(TypeDefined.FACILITY_TYPE_AC, entity.getFacilityType())) {
            facilityType.setText("交流桩");
        }
        applicableCar.setText(entity.getApplicableCar());
    }

    /**
     * 选择时间
     * @param time
     */
    public void selectTime(TextView time) {
        time.setBackgroundResource(R.drawable.actionbar_color_btn_bg);
        if (selectTime != null && selectTime != time) {
            selectTime.setBackgroundResource(R.drawable.gray_color_btn_bg);
        }
        selectTime = time;
    }

    @OnClick({R.id.time10, R.id.time15, R.id.time30,R.id.appoint})
    public void onClick(View view) {
        TextView time = (TextView) view;
        switch (view.getId()) {
            case R.id.time10:
                selectTime(time);
                getAppointPrice();
                break;
            case R.id.time15:
                selectTime(time);
                getAppointPrice();
                break;
            case R.id.time30:
                selectTime(time);
                getAppointPrice();
                break;
            case R.id.appoint:
                if (selectTime == null) {
                    Toast.makeText(this, "请选择预约时间", Toast.LENGTH_SHORT).show();
                    break;
                }
                Login.DataEntity entity = application.getLogin().getData();
                PileListItem.EvcFacilitiesGetEntity entity1 = intent.getParcelableExtra("Entity");
                payingAppointOrderDao.getDao(entity.getToken(), entity.getCustID(), entity1.getFacilityID(), selectTime.getText().toString().replace("分钟", ""), appointPrice.getText().toString().replace("¥", ""));
                appoint.setEnabled(false);
                break;
        }
    }

    public void getAppointPrice(){
        Login.DataEntity loginData = application.getLogin().getData();
        appoint.setEnabled(false);
        appointPrice.setText("正在获取实时价格");
        appointCostDao.getDao(loginData.getToken(), loginData.getCustID(), selectTime.getText().toString().replace("分钟", ""));
    }


    private static class MyHandler extends Handler {
        private WeakReference<PileBookActivity> mActivity;

        public MyHandler(PileBookActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            PileBookActivity activity = mActivity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case msgAppoint:
                    if (TextUtils.equals(activity.appointDao.dao.getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.appointDao.dao.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, null);
                        activity.setResult(FAILURE);
                        activity.finish();
                        return;
                    }

//                    activity.loading = activity.dialogUtil.startLoading(activity.context,activity.loading,R.string.appointLoadingText);
//                    ThreadManage.start(() -> {
//                        try {
//                            Thread.sleep(5000);
//                            activity.handler.sendEmptyMessage(msgLoading);
//                            Thread.sleep(5000);
//                            if (!activity.isSuccess) {
//                                activity.isSecond = true;
//                                activity.handler.sendEmptyMessage(msgLoading);
//                            }
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    });
                    break;
                case msgLoading:
                    Login.DataEntity entity = activity.application.getLogin().getData();
                    activity.orderInfoDao.getOrderInfo(entity.getToken(), activity.payingAppointOrderDao.dao.getData().getOrderNo(), entity.getCustID());
                    break;
                case msgInfo:
                    if (activity.orderInfoDao.orderInfo == null || TextUtils.equals(activity.orderInfoDao.orderInfo.getEvc_order_get().getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.orderInfoDao.orderInfo.getEvc_order_get().getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, null);
                        return;
                    }
                    if (TextUtils.equals(activity.orderInfoDao.orderInfo.getEvc_order_get().getOrderStatus(), "1")) {
                        activity.isSuccess = true;
                        Toast.makeText(activity, "预约成功，请进入我的预约界面查看预约结果！", Toast.LENGTH_SHORT).show();
                        activity.setResult(SUCCESS);
                        activity.appointSuccess();
                    } else {
                        if (!activity.isSecond) {
                            return;
                        }
                        activity.isSecond = false;
                        activity.setResult(FAILURE);
                        activity.appointSuccess();
                    }
                    break;
                case msgAppointCost: //获取预约金额
                    if (TextUtils.equals(activity.appointCostDao.dao.getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.appointCostDao.dao.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, null);
                        return;
                    }
                    AppointCost.DataEntity dataEntity = activity.appointCostDao.dao.getData();
                    activity.appointPrice.setText("¥" + dataEntity.getCost());
                    activity.appoint.setEnabled(true);
                    break;
                case msgPayingAppoint:
                    if(activity.payingAppointOrderDao.dao == null){ //不能为空
                        return;
                    }
                    if (TextUtils.equals(activity.payingAppointOrderDao.dao.getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.payingAppointOrderDao.dao.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, null);
                        activity.appoint.setEnabled(true);
                        return;
                    }
                    PayingAppointOrder.DataEntity dataEntity1 = activity.payingAppointOrderDao.dao.getData();

                    //判断一下是否为不需要支付状态，如果是零金额，则不需要支付
                    if (TextUtils.equals(activity.payingAppointOrderDao.dao.getData().getOrderStatus(), "1")) {
                        Toast.makeText(activity, "预约成功，请进入我的预约界面查看预约结果！", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    } else {
                        PayActivity.startPayActivity(activity.context, dataEntity1.getOrderNo(), PayActivity.AppointPay, dataEntity1.getPlanCost(), PAYREQUEST);
                    }

                    break;
                case msgCancelAppointPay:
                    if (TextUtils.equals(activity.cancelAppointPayDao.dao.getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.cancelAppointPayDao.dao.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, null);
                        return;
                    }
                    Toast.makeText(activity, "成功取消预约支付", Toast.LENGTH_SHORT).show();
                    activity.finish();
                    break;
            }
        }
    }

    @Override
    public void handlerError(String returnState) {

    }

    /**
     * 预约充电成功，结束进度条和当前界面
     */
    public void appointSuccess() {
        finish(); //杀掉当前界面
        if (!isPause) {
            dialogUtil.dismiss(loading);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PAYREQUEST:
                Login.DataEntity entity = application.getLogin().getData();
                if (resultCode == PayActivity.Success) { //支付成功
                    Toast.makeText(context, "付款成功,正在预约中，请不要退出界面", Toast.LENGTH_SHORT).show();
                    if(!TextUtils.isEmpty(data.getStringExtra("OrderNo"))){
                        appointDao.getAppointDao(entity.getToken(),data.getStringExtra("OrderNo"), entity.getCustID());
                    }
                    loading = dialogUtil.startLoading(context,loading,R.string.appointLoadingText);
                    ThreadManage.start(() -> {
                        try {
                            Thread.sleep(5000);
                            handler.sendEmptyMessage(msgLoading);
                            Thread.sleep(5000);
                            if (!isSuccess) {
                                isSecond = true;
                                handler.sendEmptyMessage(msgLoading);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
                } else if (resultCode == PayActivity.Failure) { //支付失败
                    cancelAppointPayDao.getDao(entity.getToken(), entity.getCustID(), data.getStringExtra("OrderNo"));
                }
                break;

        }
    }

    public static void startActivity(Context context, PileListItem.EvcFacilitiesGetEntity entity, String addr, int requestCode) {
        Intent intent = new Intent(context, PileBookActivity.class);
        intent.putExtra("Entity", entity);
        intent.putExtra("Addr", addr);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

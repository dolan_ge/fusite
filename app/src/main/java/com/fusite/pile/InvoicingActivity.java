package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.ExpandableListViewAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.BooleanBean;
import com.fusite.bean.Error;
import com.fusite.bean.Invoice;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.DateUtil;
import com.fusite.utils.DoubleUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.view.CustomSelectInvoiceDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 开票列表
 * Created by Jelly on 2016/6/23.
 */
public class InvoicingActivity extends UtilActivity {
    public String TAG = "InvoicingActivity";
    @BindView(R.id.expendableList)
    ExpandableListView expendableList;
    @BindView(R.id.selectIcon)
    ImageView selectIcon;

    private ExpandableListViewAdapter adapter;
    private List<String> gruopList;
    private List<List<Invoice.DataEntity>> childList;

    private List<List<BooleanBean>> isList;//是否选中

    private boolean isAllSelect; //是的全选

    private String type;

    private CustomSelectInvoiceDialog dialog;

    public static final int RequestCode = 0x001;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.invoicing_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void loadObjectAttribute() {
        gruopList = new ArrayList<>();
        childList = new ArrayList<>();
        isList = new ArrayList<>();

        type = intent.getStringExtra("type");

    }

    @Override
    public void setListener() {
        setExpendableListClick();
    }

    @Override
    public void setActivityView() {
        gruopList.clear();
        childList.clear();
        isList.clear();

        Login.DataEntity loginEntity = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetInvoicing.Token, loginEntity.getToken());
        ApiService.getPileService().GetInvoice(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginEntity.getCustID(), NetParam.sign_method, RequestParam.GetInvoicing.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(invoice -> {
                    if (TextUtils.equals(BeanParam.Failure, invoice.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(invoice.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    Log.v(TAG, JsonUtil.Object2Json(invoice));
                    return invoice;
                })
                .subscribe(invoice -> {
                    if (invoice == null) {
                        return;
                    }
                    List<Invoice.DataEntity> dataList = new ArrayList<>();
                    //筛选类型
                    List<Invoice.DataEntity> temp = invoice.getData();
                    for (int i = 0; i < temp.size(); i++) {
                        if (TextUtils.equals(temp.get(i).getOrderType(), type)) {
                            dataList.add(temp.get(i));
                        }
                    }

                    Map<String, List<Invoice.DataEntity>> childMap = new HashMap<>();

                    for (int i = 0; i < dataList.size(); i++) {
                        int month = DateUtil.getMonth(dataList.get(i).getCreateDate()); //获得月份
                        boolean flag = false;
                        for (int j = 0; j < gruopList.size(); j++) { //判断是否重复
                            if (TextUtils.equals(month + "", gruopList.get(j))) {
                                flag = true;
                            }
                        }
                        if (!flag) {
                            gruopList.add(month + "");
                        }
                        if (childMap.get(month + "") == null) {
                            List<Invoice.DataEntity> entityList = new ArrayList<>();
                            entityList.add(dataList.get(i));
                            childMap.put(month + "", entityList);
                        } else {
                            childMap.get(month + "").add(dataList.get(i));
                        }
                    }
                    //添加数据到ChildList
                    for (String key : childMap.keySet()) {
                        childList.add(childMap.get(key));
                        List<BooleanBean> bList = new ArrayList<>();
                        for (int i = 0; i < childMap.get(key).size(); i++) {
                            bList.add(new BooleanBean(false));
                        }
                        isList.add(bList);
                    }


                    adapter = new ExpandableListViewAdapter(context, gruopList, childList, isList);
                    expendableList.setAdapter(adapter);
                    for (int i = 0; i < gruopList.size(); i++) {
                        expendableList.expandGroup(i);
                    }
                }, new ThrowableAction(this));
    }

    @OnClick({R.id.allSelect, R.id.okBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.allSelect:

                if (isList == null) {
                    return;
                }

                for (int i = 0; i < isList.size(); i++) {
                    for (int j = 0; j < isList.get(i).size(); j++) {
                        if (!isAllSelect) {
                            isList.get(i).get(j).setFlag(true);
                        } else {
                            isList.get(i).get(j).setFlag(false);
                        }
                    }
                }
                //修改按钮
                if (isAllSelect) {
                    selectIcon.setImageResource(R.drawable.invoice_no_select);
                } else {
                    selectIcon.setImageResource(R.drawable.invoice_select);
                }
                //更改状态
                isAllSelect = !isAllSelect;
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                break;
            case R.id.okBtn:

                String orderNoStr = spliceOrderNo();
                String moneyStr = spliceMoney();

                if (TextUtils.isEmpty(moneyStr) || TextUtils.isEmpty(orderNoStr)) {
                    Toast.makeText(context, "请选择需要开票的订单！", Toast.LENGTH_SHORT).show();
                    return;
                }

                //弹出选择发票的框
                if (dialog == null) {
                    dialog = new CustomSelectInvoiceDialog(context);
                }

                dialog.setCommitInvoice(v -> {
                    dialog.dismiss();
                    CommitInvoiceActivity.startActivity(context, moneyStr, orderNoStr, type);
                });

                dialog.setSpecialInvoice(v -> {
                    dialog.dismiss();
                    SpecialInvoiceActivity.startActivity(context, moneyStr, orderNoStr, type, RequestCode);
                });

                dialog.show();


                break;
        }
    }

    public String spliceMoney() {
        String money = "0.00";
        for (int i = 0; i < isList.size(); i++) {
            for (int j = 0; j < isList.get(i).size(); j++) {
                if (isList.get(i).get(j).isFlag()) {
                    money = DoubleUtil.add(money, childList.get(i).get(j).getAmt());
                }
            }
        }
        return money;
    }

    public String spliceOrderNo() {
        String orderNo = "";
        for (int i = 0; i < isList.size(); i++) {
            for (int j = 0; j < isList.get(i).size(); j++) {
                if (isList.get(i).get(j).isFlag()) {
                    orderNo += childList.get(i).get(j).getOrderNo() + "@" + childList.get(i).get(j).getOrderType() + ";";
                }
            }
        }
        return orderNo;
    }

    public void clearOrderNo(){
        for (int i = 0; i < isList.size(); i++) {
            for (int j = 0; j < isList.get(i).size(); j++) {
                if (isList.get(i).get(j).isFlag()) {
                   isList.get(i).remove(j);
                }
            }
        }
    }

    public void setExpendableListClick() {
        expendableList.setOnGroupClickListener((parent, v, groupPosition, id) -> true);
        expendableList.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            isList.get(groupPosition).get(childPosition).setFlag(!isList.get(groupPosition).get(childPosition).isFlag());
            adapter.notifyDataSetChanged();
            return false;
        });
    }


    public static void startActivity(Context context, String type) {
        Intent intent = new Intent(context, InvoicingActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RequestCode:
                clearOrderNo();
                adapter.notifyDataSetChanged();
                break;
        }
    }
}

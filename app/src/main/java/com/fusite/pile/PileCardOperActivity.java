package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.bean.ChargeCardItem;
import com.fusite.param.BeanParam;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jelly on 2016/3/23.
 */
public class PileCardOperActivity extends UtilActivity{

    public String TAG = "DeletePileCardActivity";

    private ChargeCardItem dao;
    public static final int SUCCESS = 0x002;
    public static final int FAILURE = 0x003;

    public static final int RechargeRequest = 0x004;

    private String accountNo; //账户No

    public static final int Unbund = 1;
    public static final int ModPassword = 2;
    public static final int Loss = 3;

    public static final int UnbindRequest = 0x001;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.pile_card_oper);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void loadObjectAttribute() {
        dao = intent.getParcelableExtra("ChargeCardItemDao");
        accountNo = dao.getCrm_accounts_get().getAccountNo();
    }

    @Override
    public void setActivityView() {

    }


    @OnClick({R.id.rechargeBtn,R.id.unbund,R.id.modPassword,R.id.loss})
    public void onClick(View v) {

        if(TextUtils.equals(BeanParam.PileCard.Freeze,dao.getCrm_accounts_get().getAccountStatus())){
            Toast.makeText(context,"充电卡已经冻结",Toast.LENGTH_SHORT).show();
            return;
        }

        switch (v.getId()){
            case R.id.rechargeBtn:
                RechargeActivity.startActivity(context,RechargeRequest,RechargeActivity.CardAmt,accountNo);
                break;
            case R.id.unbund:
                CardInputPasswordActivity.startActivity(context,dao.getCrm_accounts_get().getAccountID(),accountNo,Unbund, UnbindRequest);
                break;
            case R.id.modPassword:
                CardInputPasswordActivity.startActivity(context,dao.getCrm_accounts_get().getAccountID(),accountNo,ModPassword);
                break;
            case R.id.loss:
                CardInputPasswordActivity.startActivity(context,dao.getCrm_accounts_get().getAccountID(),accountNo,Loss);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case UnbindRequest:
                if(resultCode == CardInputPasswordActivity.Success){
                    setResult(SUCCESS);
                    finish();
                }
                break;
        }
    }

    /**
     * 开启界面
     */
    public static void startActivity(Context context, ChargeCardItem dao, int resquestCode) {
        Intent intent = new Intent(context, PileCardOperActivity.class);
        intent.putExtra("ChargeCardItemDao", dao);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, resquestCode);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

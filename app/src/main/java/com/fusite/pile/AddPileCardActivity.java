package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.NetErrorHandlerListener;
import com.fusite.impl.AddChargeCardDaoImpl;
import com.fusite.param.NetErrorEnum;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 绑定充电卡
 * Created by Jelly on 2016/3/23.
 */
public class AddPileCardActivity extends UtilActivity implements NetErrorHandlerListener {
    /**
     * TAG
     */
    public String TAG = "AddPileCardActivity";

    @BindView(R.id.no)
    EditText no;
    @BindView(R.id.pwd)
    EditText pwd;
    @BindView(R.id.ok)
    TextView ok;

    private AddChargeCardDaoImpl addChargeCardDao;
    private static final int msgAdd = 0x001;

    public static final int Success = 0x002;
    public static final int Failure = 0x003;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.add_pile_card);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
    }

    @Override
    public void loadObjectAttribute() {
        application = (MyApplication) getApplication();
        addChargeCardDao = new AddChargeCardDaoImpl(this, new MyHandler(this), msgAdd);
    }


    @Override
    public void setActivityView() {

    }

    @OnClick(R.id.ok)
    public void onClick() {
        String nos = no.getText().toString();
        String pwds = pwd.getText().toString();
        if (TextUtils.isEmpty(nos) || TextUtils.isEmpty(pwds)) {
            Toast.makeText(AddPileCardActivity.this, "卡号和密码不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        Login.DataEntity entity = application.getLogin().getData();
        addChargeCardDao.addAddChargeCardDao(entity.getToken(), nos, pwds, entity.getCustID());
    }

    private static class MyHandler extends Handler {
        private WeakReference<AddPileCardActivity> mActivity;

        public MyHandler(AddPileCardActivity activity) {
            mActivity = new WeakReference<AddPileCardActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            AddPileCardActivity activity = mActivity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case msgAdd:
                    if (activity.addChargeCardDao.addChargeCardDao == null || TextUtils.equals(activity.addChargeCardDao.addChargeCardDao.getCrm_accounts_insert().getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.addChargeCardDao.addChargeCardDao.getCrm_accounts_insert().getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, activity);
                        activity.setResult(Failure);
                        return;
                    }
                    activity.setResult(Success);
                    activity.finish();
                    break;
            }
        }
    }

    @Override
    public void handlerError(String returnState) {
        if (TextUtils.equals(returnState, NetErrorEnum.充电卡已经被绑定.getState())) {
            clearET();
        } else if (TextUtils.equals(returnState, NetErrorEnum.充电卡不存在.getState())) {
            clearET();
        } else if (TextUtils.equals(returnState, NetErrorEnum.充电卡密码错误.getState())) {
            pwd.setText("");
        }
    }

    /**
     * 清空输入框
     */
    public void clearET() {
        no.setText("");
        pwd.setText("");
    }


    public static void startActivity(Context context, int requestCode) {
        Intent intent = new Intent(context, AddPileCardActivity.class);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, requestCode);
    }


    @Override
    public String getTAG() {
        return TAG;
    }
}

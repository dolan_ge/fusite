package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Login;
import com.fusite.bean.ServiceOrderInfo;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/7/26.
 */
public class ServiceOrderInfoActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "ServiceOrderInfoActivity";
    @BindView(R.id.orgName)
    TextView orgName;
    @BindView(R.id.brand)
    TextView brand;
    @BindView(R.id.startTime)
    TextView startTime;
    @BindView(R.id.finishTime)
    TextView finishTime;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.serviceType)
    TextView serviceType;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.payType)
    TextView payType;
    @BindView(R.id.phone)
    TextView phone;

    private String orderNo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.service_order_info);
        ButterKnife.bind(this);
        loadObjectAttribute();
        setActivityView();
    }

    @Override
    public void loadObjectAttribute() {
        orderNo = intent.getStringExtra("orderNo");
    }

    @Override
    public void setActivityView() {
        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.ServiceOrderInfo.Token, loginData.getToken());
        map.put(RequestParam.ServiceOrderInfo.OrderNo, orderNo);
        ApiService.getPileService().ServiceOrderInfo(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.ServiceOrderInfo.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(serviceOrderInfo -> {
                    if (TextUtils.equals(BeanParam.Failure, serviceOrderInfo.getIsSuccess())) {
                        return null;
                    }
                    return serviceOrderInfo;
                })
                .subscribe(serviceOrderInfo -> {
                    if (serviceOrderInfo == null) {
                        return;
                    }
                    ServiceOrderInfo.DataEntity serviceOrderData = serviceOrderInfo.getData();
                    orgName.setText(serviceOrderData.getOrgName());
                    brand.setText(serviceOrderData.getModel());
                    startTime.setText(serviceOrderData.getPlanTime());
                    finishTime.setText(serviceOrderData.getUpdateDate());
                    serviceType.setText(serviceOrderData.getServiceName());
                    address.setText(serviceOrderData.getAddr());
                    price.setText(serviceOrderData.getServiceAmt());
                    phone.setText(serviceOrderData.getTel());
                    switch (serviceOrderData.getServicePayType()) {
                        case BeanParam.ServiceOrder.BalancePay:
                            payType.setText("余额支付");
                            break;
                        case BeanParam.ServiceOrder.AliPay:
                            payType.setText("支付宝");
                            break;
                        case BeanParam.ServiceOrder.WXPay:
                            payType.setText("微信");
                            break;
                        default:
                            payType.setText("玛帮新能源消费");
                            break;
                    }
                });
    }

    @OnClick(R.id.sendCommitLayout)
    public void onClick() {
        SendCommitActivity.startActivity(context,SendCommitActivity.ServiceCommit,orderNo);
    }

    public static void startActivity(Context context, String orderNo) {
        Intent intent = new Intent(context, ServiceOrderInfoActivity.class);
        intent.putExtra("orderNo", orderNo);
        context.startActivity(intent);
    }


}

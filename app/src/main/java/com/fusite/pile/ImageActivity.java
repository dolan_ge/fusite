package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.fusite.activities.UtilActivity;
import com.fusite.utils.WindowUtil;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.finalteam.galleryfinal.widget.zoonview.PhotoViewAttacher;

/**
 * 图片浏览
 * Created by Jelly on 2016/5/31.
 */
public class ImageActivity extends UtilActivity {

    public String TAG = "ImageActivity";
    @BindView(R.id.image)
    ImageView image;

    private String imageUrl;
    private PhotoViewAttacher photoViewAttacher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.image);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {
        imageUrl = intent.getStringExtra("imageUrl");
        photoViewAttacher = new PhotoViewAttacher(image);
    }

    @Override
    public void setListener() {
        setImageClickListener();
    }

    @Override
    public void setActivityView() {
        Picasso.with(context).load(imageUrl).error(R.drawable.square_viewplace).resize(WindowUtil.getInstance().getScreenWidth(this),WindowUtil.getInstance().getScreenHeight(this)/3 * 2).into(image, new Callback() {
            @Override
            public void onSuccess() {
                photoViewAttacher.update();
            }

            @Override
            public void onError() {

            }
        });
    }

    public void setImageClickListener(){
        photoViewAttacher.setOnPhotoTapListener((view, x, y) -> finish());
    }

    public static void startActivity(Context context, String imageUrl) {
        Intent intent = new Intent(context, ImageActivity.class);
        intent.putExtra("imageUrl", imageUrl);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;

import com.fusite.activities.UtilActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jelly on 2016/6/26.
 */
public class EditErrorActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "EditErrorActivity";
    @BindView(R.id.input)
    EditText input;

    public static final int Success = 0x001;
    public static final int Failure = 0x002;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.edit_error);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void setActivityView() {

    }

    @OnClick(R.id.ok)
    public void onClick() {
        String notes = input.getText().toString();
        intent.putExtra("notes",notes);
        setResult(Success,intent);
        finish();
    }

    public static void startActivity(Context context,int requestCode){
        Intent intent = new Intent(context,EditErrorActivity.class);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent,requestCode);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

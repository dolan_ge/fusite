package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.FriendListAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.Dynamic;
import com.fusite.bean.Login;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.HttpCacheUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bingoogolapple.refreshlayout.BGANormalRefreshViewHolder;
import cn.bingoogolapple.refreshlayout.BGARefreshLayout;
import cn.bingoogolapple.refreshlayout.BGARefreshViewHolder;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/13.
 */
public class FriendActivity extends UtilActivity implements BGARefreshLayout.BGARefreshLayoutDelegate {
    /**
     * TAG
     */
    public String TAG = "FriendActivity";
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.freshLayout)
    BGARefreshLayout freshLayout;
    @BindView(R.id.screen)
    TextView screen;

    private FriendListAdapter adpater;
    private List<Dynamic.DataEntity> dataList;
    public static final int start = 1;
    public static final int loadMore = 2;
    private boolean isScreen = false;

    private HttpCacheUtil httpCacheUtil;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.friend);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void init() {
        freshLayout.setDelegate(this);
        BGARefreshViewHolder refreshViewHolder = new BGANormalRefreshViewHolder(this, true);
        freshLayout.setRefreshViewHolder(refreshViewHolder);
        httpCacheUtil = new HttpCacheUtil(context);
    }

    @Override
    public void loadObjectAttribute() {

    }

    @Override
    public void setListener() {
        setListViewItemClick();
        setOnScrollListener();
    }

    @Override
    public void setActivityView() {
        if (httpCacheUtil.isExistCache(RequestParam.GetDynamic.InterfaceName)) { //读取缓存中的数据
            Dynamic dynamic = JsonUtil.objectFromJson(httpCacheUtil.readHttp(RequestParam.GetDynamic.InterfaceName), Dynamic.class);
            dataList = dynamic.getData();
            adpater = new FriendListAdapter(context, dataList);
            list.setAdapter(adpater);
            //开启下拉
            freshLayout.beginRefreshing();
        } else {
            loadData(start, NetParam.getTime()).subscribe(dynamic -> {
                if (dynamic == null) {
                    return;
                }
                dataList = dynamic.getData();
                adpater = new FriendListAdapter(context, dataList);
                list.setAdapter(adpater);
                //缓存数据
                httpCacheUtil.cacheHttp(JsonUtil.Object2Json(dynamic), RequestParam.GetDynamic.InterfaceName);
            }, throwable -> {

            });
        }
    }

    public Observable<Dynamic> loadData(int direction, String forumNo) {
        Login.DataEntity dataEntity = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetDynamic.Token, dataEntity.getToken());
        switch (direction) {
            case start: //刷新数据
                map.put(RequestParam.GetDynamic.ForumNo, "");
                break;
            case loadMore: //加载更多
                map.put(RequestParam.GetDynamic.ForumNo, forumNo);
                break;
        }
        return ApiService.getPileService().GetDynamic(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.GetDynamic.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(dynamic -> {
                    if (TextUtils.equals(dynamic.getIsSuccess(), BeanParam.Failure)) {
                        return null;
                    }
                    return dynamic;
                });
    }

    public void setListViewItemClick() {
        list.setOnItemClickListener((parent, view, position, id) -> DynamicActivity.startActivity(context, dataList.get(position)));
    }

    @OnClick({R.id.screen, R.id.send})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.screen: //筛选
                isScreen = !isScreen;
                if(isScreen){
                    screen.setText("所有");
                }else{
                    screen.setText("我的");
                }
                freshLayout.beginRefreshing();
                break;
            case R.id.send:
                SendDynamicActivity.startActivity(context);
                break;
        }
    }

    @Override
    public void onBGARefreshLayoutBeginRefreshing(BGARefreshLayout refreshLayout) {
        loadData(start, dataList == null || dataList.size() == 0 ? "" : dataList.get(0).getForumNo()).subscribe(dynamic -> {
            refreshLayout.endRefreshing();
            if (dynamic == null) {
                return;
            }
            LogUtil.v(TAG, JsonUtil.Object2Json(dynamic));
            this.dataList.clear();

            //筛选数据
            if (isScreen) {
                dynamic.setData(screenData(dynamic.getData()));
            }

            this.dataList.addAll(0, dynamic.getData());
            adpater.notifyDataSetChanged();
            //缓存数据
            httpCacheUtil.cacheHttp(JsonUtil.Object2Json(dynamic), RequestParam.GetDynamic.InterfaceName);
        }, throwable -> {
            refreshLayout.endRefreshing();
        });
    }

    @Override
    public boolean onBGARefreshLayoutBeginLoadingMore(BGARefreshLayout refreshLayout) {
        loadData(loadMore, dataList == null || dataList.size() == 0 ? "" : dataList.get(dataList.size() - 1).getForumNo()).subscribe(dynamic -> {
            refreshLayout.endLoadingMore();
            if (dynamic == null) {
                return;
            }

            //筛选
            if (isScreen) {
                dynamic.setData(screenData(dynamic.getData()));
            }

            this.dataList.addAll(dynamic.getData());
            adpater.notifyDataSetChanged();
        }, throwable -> {
            refreshLayout.endLoadingMore();
        });
        return true;
    }

    /**
     * 筛选自己的动态
     */
    public List<Dynamic.DataEntity> screenData(List<Dynamic.DataEntity> startList) {
        String custId = application.getLogin().getData().getCustID();
        List<Dynamic.DataEntity> endList = new ArrayList<>();
        for (Dynamic.DataEntity dataEntity : startList) {
            if (TextUtils.equals(custId, dataEntity.getCustId())) {
                endList.add(dataEntity);
            }
        }
        return endList;
    }

    public void setImage(ImageView image){
        if(image != null && image.getTag() != null && !TextUtils.equals(image.getTag().toString(),"1") && image.isShown()){
            Picasso.with(context).load(image.getTag().toString()).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).into(image);
            image.setTag("1");
        }
    }

    public void setOnScrollListener(){
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState){
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE: //滑动停止
                        LogUtil.v(TAG,"停止滚动");
                        adpater.setScroll(false);
                        int count = view.getChildCount();
                        LogUtil.v(TAG,count+"");
                        for (int i = 0; i < count; i++) {
                            View item = view.getChildAt(i);
                            ImageView img1 = (ImageView) item.findViewById(R.id.image1);
                            ImageView img2 = (ImageView) item.findViewById(R.id.image2);
                            ImageView img3 = (ImageView) item.findViewById(R.id.image3);
                            ImageView img4 = (ImageView) item.findViewById(R.id.image4);
                            ImageView img5 = (ImageView) item.findViewById(R.id.image5);
                            ImageView img6 = (ImageView) item.findViewById(R.id.image6);
                            ImageView img7 = (ImageView) item.findViewById(R.id.image7);
                            ImageView img8 = (ImageView) item.findViewById(R.id.image8);
                            ImageView img9 = (ImageView) item.findViewById(R.id.image9);

                            setImage(img1);
                            setImage(img2);
                            setImage(img3);
                            setImage(img4);
                            setImage(img5);
                            setImage(img5);
                            setImage(img6);
                            setImage(img7);
                            setImage(img8);
                            setImage(img9);
                        }

                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING: //滚动做出了抛出动作
                        LogUtil.v(TAG,"滚动抛出");
                        adpater.setScroll(true);
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL: //正在滚动
                        LogUtil.v(TAG,"正在滚动");
                        adpater.setScroll(true);
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }

    /**
     * 更新ListView中的某一项的点赞数据
     *
     * @param itemIndex
     * @param dataEntity
     * @param isAdd
     */
    public void updateView(int itemIndex, Dynamic.DataEntity dataEntity, boolean isAdd) {
        int visiblePosition = list.getFirstVisiblePosition();
        View v = list.getChildAt(itemIndex - visiblePosition);//Do something fancy with your listitem view
        FriendListAdapter.ViewHolder viewHolder = (FriendListAdapter.ViewHolder) v.getTag();
        if (isAdd) {
            viewHolder.like.setImageResource(R.drawable.onlike);
        } else {
            viewHolder.like.setImageResource(R.drawable.like);
        }
        adpater.setLikeListener(viewHolder, itemIndex, dataEntity);
        viewHolder.likeNum.setText(dataEntity.getLikes());
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, FriendActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }

}

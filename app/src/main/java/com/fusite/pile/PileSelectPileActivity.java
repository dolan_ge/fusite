package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ListView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.SelectPileListAdapter;
import com.fusite.bean.PileListItem;
import com.fusite.impl.PileListItemDaoImpl;
import com.fusite.utils.ToastUtils;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jelly on 2016/3/15.
 */
public class PileSelectPileActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "PileSelectPileActivity";
    @BindView(R.id.pile_list)
    ListView pileList;

    private PileListItemDaoImpl pileListItemDao;
    public static final int msgPiles = 0x001;
    private static final int APPOINT = 0x002;
    private CustomDialog loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.select_pile);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void loadObjectAttribute() {
        pileListItemDao = new PileListItemDaoImpl(this, new MyHandler(this), msgPiles);
    }

    @Override
    public void setListener() {
        setOnItemClickListener();
    }

    @Override
    public void setActivityView() {
        loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
        pileListItemDao.getPiles(intent.getStringExtra("OrgId"));
    }

    /**
     * 设置列表的点击监听
     */
    public void setOnItemClickListener() {
        pileList.setOnItemClickListener((parent, view, position, id) -> {
            PileListItem.EvcFacilitiesGetEntity data = pileListItemDao.piles.get(position).getEvc_facilities_get();
            if (!TextUtils.equals("0", data.getFacilityStatus())) {
                Toast.makeText(PileSelectPileActivity.this, "有人正在使用，请您稍等片刻", Toast.LENGTH_SHORT).show();
                return;
            }
            PileBookActivity.startActivity(PileSelectPileActivity.this, data, intent.getStringExtra("Addr"), APPOINT);
        });
    }

    private static class MyHandler extends Handler {

        private WeakReference<PileSelectPileActivity> mActivity;

        public MyHandler(PileSelectPileActivity activity) {
            mActivity = new WeakReference<PileSelectPileActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            PileSelectPileActivity activity = mActivity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case msgPiles: //获取充电点中的电桩列表
                    activity.dialogUtil.dismiss(activity.loading);
                    if (activity.pileListItemDao.piles.size() == 0) {
                        return;
                    }
                    activity.pileList.setAdapter(new SelectPileListAdapter(activity, activity.pileListItemDao.piles));
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case APPOINT:
                if (resultCode == PileBookActivity.SUCCESS) {
                    finish();
                    MyAppointActivity.startActivity(PileSelectPileActivity.this);
                } else if (resultCode == PileBookActivity.FAILURE) {
                    setActivityView();
                }
                break;
        }
    }

    /**
     * 开启界面
     */
    public static void startActivity(Context context, String OrgId, String addr) {
        ToastUtils.showToast("暂未开放，敬请期待");
//        Intent intent = new Intent(context, PileSelectPileActivity.class);
//        intent.putExtra("OrgId", OrgId);
//        intent.putExtra("Addr", addr);
//        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

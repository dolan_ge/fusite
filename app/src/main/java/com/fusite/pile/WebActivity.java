package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fusite.activities.UtilActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jelly on 2016/6/14.
 */
public class WebActivity extends UtilActivity {
    public String TAG = "WebActivity";
    @BindView(R.id.web)
    WebView web;

    private String webUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.web_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {
        webUrl = intent.getStringExtra("webUrl");
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {
        WebSettings webSettings = web.getSettings();
        webSettings.setJavaScriptEnabled(true);
        web.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        web.loadUrl(webUrl);
    }

    public static void startActivity(Context context,String webUrl){
        Intent intent = new Intent(context,WebActivity.class);
        intent.putExtra("webUrl",webUrl);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

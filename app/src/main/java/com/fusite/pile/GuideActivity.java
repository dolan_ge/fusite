package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.GuidePageAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 引导页
 * Created by Jelly on 2016/6/21.
 */
public class GuideActivity extends UtilActivity {

    public String TAG = "FirstActivity";
    @BindView(R.id.viewPage)
    ViewPager viewPage;
    @BindView(R.id.indicator)
    TextView indicator;
    @BindView(R.id.btnOk)
    TextView btnOk;

    private List<View> viewList;
    private GuidePageAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.guide_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void loadObjectAttribute() {
        if (viewList == null) {
            viewList = new ArrayList<>();
            viewList.add(LayoutInflater.from(context).inflate(R.layout.guide1, viewPage, false));
            viewList.add(LayoutInflater.from(context).inflate(R.layout.guide2, viewPage, false));
            viewList.add(LayoutInflater.from(context).inflate(R.layout.guide3, viewPage, false));
        }

        if (adapter == null) {
            adapter = new GuidePageAdapter(viewList, context);
        }
    }

    @Override
    public void setListener() {
        setPageChangeListener();
    }

    @Override
    public void setActivityView() {
        viewPage.setAdapter(adapter);
    }

    public void setPageChangeListener() {
        viewPage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        indicator.setText("● ○ ○");
                        btnOk.setVisibility(View.GONE);
                        break;
                    case 1:
                        indicator.setText("○ ● ○");
                        btnOk.setVisibility(View.GONE);
                        break;
                    case 2:
                        btnOk.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @OnClick(R.id.btnOk)
    public void onClick() {
        finish();
        MainActivity.startActivity(context);
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, GuideActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

package com.fusite.pile;

import android.app.Activity;
import android.app.Dialog;
import android.companion.WifiDeviceFilter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.ChargeCardItem;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.DialogListener;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.CashierInputFilterUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class WithdrawActivity extends UtilActivity {
    public String TAG = "WithdrawActivity";

    private int type; //0 :卡充 //1 ：提现
    private ChargeCardItem chargeCardItem;
    @BindView(R.id.title)
    TextView titleText;
    @BindView(R.id.inputMoneyDes)
    TextView inputMoneyDes;
    @BindView(R.id.inputMoney)
    EditText inputMoney;
    @BindView(R.id.money)
    TextView money;
    @BindView(R.id.cardNo)
    TextView cardNo;
    @BindView(R.id.cardMoney)
    TextView cardMoney;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.withdraw);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void loadObjectAttribute() {
        chargeCardItem = (ChargeCardItem) getIntent().getParcelableExtra("dao");
        type =getIntent().getIntExtra("type",0);
        if (type==1) {
           titleText.setText("提现");
           inputMoneyDes.setText("提现金额(元)");
        }
        if (chargeCardItem.getCrm_accounts_get().getAccountNo()!=null){
            cardNo.setText(chargeCardItem.getCrm_accounts_get().getAccountNo());
        }
        if (chargeCardItem.getCrm_accounts_get().getBalanceAmt()==null || chargeCardItem.getCrm_accounts_get().getBalanceAmt().equals("")){
            chargeCardItem.getCrm_accounts_get().setBalanceAmt("0.00");
        }
        cardMoney.setText("¥" +chargeCardItem.getCrm_accounts_get().getBalanceAmt());
    }

    @Override
    public void setActivityView() {
        InputFilter[] filters = {new CashierInputFilterUtil()};
        inputMoney.setFilters(filters);
        Login.DataEntity entity = application.getLogin().getData();
        if (!TextUtils.isEmpty(entity.getBalanceAmt())) {
            money.setText("¥" + entity.getBalanceAmt());
        }
    }

    @Override
    public void setListener() {

    }

    @OnClick({R.id.confirm})
    public void OnClick (View view){
        switch (view.getId()){
            case R.id.confirm :
                BigDecimal moneyIn = new BigDecimal(inputMoney.getText().toString());
                if (type ==0){
                    // TODO: 2018/5/24 卡充 和卡内余额进行比较
                    BigDecimal cardMoneyBalance = new BigDecimal(chargeCardItem.getCrm_accounts_get().getBalanceAmt());
                    if (moneyIn.compareTo(cardMoneyBalance)==1){
                        Toast.makeText(this, R.string.cardChargeError, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    dialogUtil.showSureListenerDialog(this, "确定充值" + moneyIn.toString() + "元至账户余额", new DialogListener() {
                        @Override
                        public void handle(Dialog dialog, int index) {
                            dialog.dismiss();
                            startWithdraw("2",moneyIn);
//                            ApiService.getPileService().WithdrawService(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.Login.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
//                                    .subscribeOn(Schedulers.newThread())
//                                    .observeOn(AndroidSchedulers.mainThread())
//                                    .map(withdraw -> {
//                                        return withdraw;
//                                    }).subscribe(withdraw -> {
//
//                            }),new ThrowableAction(WithdrawActivity.this));

//                            // 卡额度减少
//                            chargeCardItem.getCrm_accounts_get().setBalanceAmt(cardMoneyBalance.subtract(moneyIn).toString());
//                            WithdrawActivity.this.cardMoney.setText("¥" +chargeCardItem.getCrm_accounts_get().getBalanceAmt());
//                            // 余额增加
//                            Login.DataEntity entity = application.getLogin().getData();
//                            BigDecimal balanceMoney = new BigDecimal("0");
//                            if (!TextUtils.isEmpty(entity.getBalanceAmt())) {
//                                balanceMoney = new BigDecimal(entity.getBalanceAmt());
//                            }
//                            entity.setBalanceAmt(balanceMoney.add(moneyIn).toString());
//                            WithdrawActivity.this.money.setText("¥" + entity.getBalanceAmt());
                        }
                    });

                } else {
                    // TODO: 2018/5/24 提现 和账户余额比较
                    Login.DataEntity entity = application.getLogin().getData();
                    BigDecimal balanceMoney = new BigDecimal("0");
                    if (!TextUtils.isEmpty(entity.getBalanceAmt())) {
                        balanceMoney = new BigDecimal(entity.getBalanceAmt());
                    }
                    if (moneyIn.compareTo(balanceMoney)==1){
                        Toast.makeText(this, R.string.withdrawError, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    BigDecimal finalBalanceMoney = balanceMoney;
                    dialogUtil.showSureListenerDialog(this,"确定提现"+moneyIn+"元至充电卡",(dialog, index)->{
                       dialog.dismiss();
                        // 余额减少
                        startWithdraw("1",moneyIn);
//                        entity.setBalanceAmt(finalBalanceMoney.subtract(moneyIn).toString());
//                        WithdrawActivity.this.money.setText("¥" + entity.getBalanceAmt());
//                        // 卡额度增加
//                        BigDecimal cardMoneyBalance = new BigDecimal(chargeCardItem.getCrm_accounts_get().getBalanceAmt());
//                        chargeCardItem.getCrm_accounts_get().setBalanceAmt(cardMoneyBalance.add(moneyIn).toString());
//                        WithdrawActivity.this.cardMoney.setText("¥" +chargeCardItem.getCrm_accounts_get().getBalanceAmt());
//
//                        Intent intent = new Intent();
//                        intent.putExtra("dao",chargeCardItem);
//                        setResult(PileCardActivity.CHANGE_BALANCE,intent);
                    });

                }
                //System.out.println(moneyIn);
                break;
        }
    }

    public void startWithdraw (String type, BigDecimal moneyIn) {
        Map<String, String> map = new HashMap<>();
        Login.DataEntity dataEntity =  application.getLogin().getData();
        map.put(RequestParam.WithdrawInfo.Token, dataEntity.getToken());
        map.put(RequestParam.WithdrawInfo.AccountNo, chargeCardItem.getCrm_accounts_get().getAccountNo());
        map.put(RequestParam.WithdrawInfo.Amount, moneyIn.toString());
        map.put(RequestParam.WithdrawInfo.Type, type);
        ApiService.getPileService().WithdrawService(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.WithdrawInfo.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(withdraw -> {
                    if (TextUtils.equals(withdraw.getIsSuccess(), BeanParam.Failure)) {
                        Error errorDao = errorParamUtil.checkReturnState(withdraw.getReturnStatus());
                        toastUtil.toastError(context, errorDao, null);
                        return null;
                    }
                    return withdraw;
                })
                .subscribe(withdraw -> {
                    if (withdraw != null) {
                        Intent intent = new Intent();
                        intent.putExtra("dao",chargeCardItem);
                        setResult(PileCardActivity.CHANGE_BALANCE,intent);
                        String msg = "提现成功，请耐心等待审核";
                        if (type == "2") {
                            msg = "充值成功";
                        }
                        Toast.makeText(WithdrawActivity.this,msg,Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }, new ThrowableAction(WithdrawActivity.this));
    }

    public static void startActivity(Context context,int resultCode,ChargeCardItem item, int type) {
        Intent intent = new Intent(context, WithdrawActivity.class);
       intent.putExtra("dao",item);
        intent.putExtra("type",type);
        Activity activity = (Activity)context;
        activity.startActivityForResult(intent,resultCode);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

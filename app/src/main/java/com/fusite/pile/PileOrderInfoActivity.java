package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.OrderInfo;
import com.fusite.constant.TypeDefined;
import com.fusite.impl.OrderInfoDaoImpl;
import com.fusite.param.BeanParam;
import com.fusite.utils.DateUtil;
import com.fusite.utils.DoubleUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 充电订单详情
 * Created by Jelly on 2016/4/9.
 */
public class PileOrderInfoActivity extends UtilActivity {

    public String TAG = "PileOrderInfoActivity";


    @BindView(R.id.orgName)
    TextView orgName;
    @BindView(R.id.facilityName)
    TextView facilityName;
    @BindView(R.id.facilityType)
    TextView facilityType;
    @BindView(R.id.facilityMode)
    TextView facilityMode;
    @BindView(R.id.facilityId)
    TextView facilityId;
    @BindView(R.id.startTime)
    TextView startTime;
    @BindView(R.id.endTime)
    TextView endTime;
    @BindView(R.id.chargingQty)
    TextView chargingQty;
    @BindView(R.id.amt)
    TextView amt;
    @BindView(R.id.payWay)
    TextView payWay;
    @BindView(R.id.mobile)
    TextView mobile;
    @BindView(R.id.use_time)
    TextView useTime;
    @BindView(R.id.use_time_title)
    TextView useTimeTitle;
    @BindView(R.id.orgSign)
    ImageView orgSign;
    @BindView(R.id.plan)
    TextView plan;

    /**
     * 订单详情
     */
    private OrderInfoDaoImpl orderInfoDao;
    /**
     * 订单详情消息
     */
    private static final int msgOrderInfo = 0x001;

    private static final String DateFormat = "MM月dd日 HH:mm";

    private CustomDialog loading;

    private String orgNo;
    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.pile_order_info);
        unbinder = ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        dialogUtil.dismiss(loading);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void loadObjectAttribute() {
        orgNo = intent.getStringExtra("OrderNo");
        orderInfoDao = new OrderInfoDaoImpl(this, new MyHandler(this), msgOrderInfo);
    }

    @Override
    public void setActivityView() {
        loading = dialogUtil.startLoading(context, loading, R.string.loadingText);
        Login.DataEntity dataEntity = application.getLogin().getData();
        if (!TextUtils.isEmpty(orgNo)) {
            orderInfoDao.getOrderInfo(dataEntity.getToken(), orgNo, dataEntity.getCustID());
        }
    }

    @OnClick({R.id.sendCommitLayout, R.id.call})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendCommitLayout:
                SendCommitActivity.startActivity(context, SendCommitActivity.PileCommit, orgNo);
                break;
            case R.id.call:
                Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + mobile.getText().toString()));
                startActivity(intent);
                break;
        }
    }


    private static class MyHandler extends Handler {
        private WeakReference<PileOrderInfoActivity> mActivity;

        public MyHandler(PileOrderInfoActivity activity) {
            mActivity = new WeakReference<PileOrderInfoActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            PileOrderInfoActivity activity = mActivity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case msgOrderInfo:
                    activity.dialogUtil.dismiss(activity.loading);
                    if (activity.orderInfoDao.orderInfo == null) {
                        return;
                    }

                    Log.v("json", JsonUtil.Object2Json(activity.orderInfoDao.orderInfo));

                    if (TextUtils.equals("N", activity.orderInfoDao.orderInfo.getEvc_order_get().getIsSuccess())) {
                        Error error = activity.errorParamUtil.checkReturnState(activity.orderInfoDao.orderInfo.getEvc_order_get().getReturnStatus());
                        activity.toastUtil.toastError(activity.context, error, null);
                    }
                    OrderInfo.EvcOrderGetEntity evcOrderGetEntity = activity.orderInfoDao.orderInfo.getEvc_order_get();
                    activity.orgName.setText(evcOrderGetEntity.getOrgName());
                    activity.facilityName.setText(evcOrderGetEntity.getFacilityName());
                    activity.facilityId.setText(evcOrderGetEntity.getFacilityID());

                    if (TextUtils.equals(OrderInfoDaoImpl.CANCEL, evcOrderGetEntity.getOrderStatus())) {
                        activity.startTime.setText(DateUtil.getSdfDate(evcOrderGetEntity.getPlanBeginDateTime(), DateFormat));
                        activity.endTime.setText(DateUtil.getSdfDate(evcOrderGetEntity.getPlanEndDateTime(), DateFormat));
                        activity.useTimeTitle.setText("预约时间：");

                        try {
                            activity.useTime.setText(DateUtil.DifferDate(evcOrderGetEntity.getPlanEndDateTime(),evcOrderGetEntity.getPlanBeginDateTime()) + "分钟");
                        }catch (Exception e){
                            activity.useTime.setText("数据异常");
                        }

                        activity.orgSign.setImageResource(R.drawable.ordercancel);
                    } else if (TextUtils.equals(OrderInfoDaoImpl.COMPLETE, evcOrderGetEntity.getOrderStatus())) {
                        activity.startTime.setText(DateUtil.getSdfDate(evcOrderGetEntity.getRealBeginDateTime(), DateFormat));
                        activity.endTime.setText(DateUtil.getSdfDate(evcOrderGetEntity.getRealEndDateTime(), DateFormat));
                        activity.useTimeTitle.setText("充电时间：");
                        activity.useTime.setText(evcOrderGetEntity.getChargingTime() + "分钟");
                        activity.orgSign.setImageResource(R.drawable.orderfinish);
                    }

                    activity.chargingQty.setText(evcOrderGetEntity.getChargingQty() + "度");
                    activity.amt.setText(DoubleUtil.add(evcOrderGetEntity.getChargingAmt(), evcOrderGetEntity.getServiceAmt()) + "元");
                    if(TextUtils.isEmpty(evcOrderGetEntity.getPlanCost())){
                        activity.plan.setText("无预约");
                    }else{
                        activity.plan.setText(evcOrderGetEntity.getPlanCost() + "元");
                    }
                    activity.mobile.setText(evcOrderGetEntity.getTel());

                    //设置设备类型
                    if(TextUtils.isEmpty(evcOrderGetEntity.getFacilityType())){
                        activity.facilityType.setText("未知");
                    }else if(TextUtils.equals(TypeDefined.FACILITY_TYPE_AC,evcOrderGetEntity.getFacilityType())){
                        activity.facilityType.setText("交流桩");
                    }else if(TextUtils.equals(TypeDefined.FACILITY_TYPE_DC,evcOrderGetEntity.getFacilityType())){
                        activity.facilityType.setText("直流桩");
                    }

                    //设置支付方式
                    switch ((TextUtils.isEmpty(evcOrderGetEntity.getPayType()) ? TextUtils.isEmpty(evcOrderGetEntity.getPlanPayType()) ? "0" : evcOrderGetEntity.getPlanPayType() : evcOrderGetEntity.getPayType())) {
                        case BeanParam.EVCOrderInfo.BalancePay:
                            activity.payWay.setText("余额支付");
                            break;
                        case BeanParam.EVCOrderInfo.AliPay:
                            activity.payWay.setText("支付宝支付");
                            break;
                        case BeanParam.EVCOrderInfo.WXPay:
                            activity.payWay.setText("微信支付");
                            break;
                        default:
                            activity.payWay.setText("玛帮新能源消费");
                            break;
                    }


                    break;
            }
        }
    }

    /**
     * 开启界面
     *
     * @param context
     * @param OrderNo
     */
    public static void startActivity(Context context, String OrderNo) {
        Intent intent = new Intent(context, PileOrderInfoActivity.class);
        intent.putExtra("OrderNo", OrderNo);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }

}

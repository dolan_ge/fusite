package com.fusite.pile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.bean.CarModel;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.OrderFee;
import com.fusite.impl.BitmapImpl;
import com.fusite.impl.CarModelDaoImpl;
import com.fusite.impl.OrderFeeDaoImpl;
import com.fusite.impl.RentOrderCancelDaoImpl;
import com.fusite.impl.RentOrderDaoImpl;
import com.fusite.param.BeanParam;
import com.fusite.utils.DateUtil;
import com.fusite.utils.DoubleUtil;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 支付前显示订单
 * Created by Jelly on 2016/4/18.
 */
public class RentCarPayOrderActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "RentCarPayOrderActivity";
    @BindView(R.id.brand)
    TextView brand;
    @BindView(R.id.carType)
    TextView carType;
    @BindView(R.id.battery)
    TextView battery;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.pickCarCity)
    TextView pickCarCity;
    @BindView(R.id.returnCarCity)
    TextView returnCarCity;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.pickCarDate)
    TextView pickCarDate;
    @BindView(R.id.pickCarTime)
    TextView pickCarTime;
    @BindView(R.id.returnCarDate)
    TextView returnCarDate;
    @BindView(R.id.returnCarTime)
    TextView returnCarTime;
    @BindView(R.id.rentCarPrice)
    TextView rentCarPrice;
    @BindView(R.id.insurancePrice)
    TextView insurancePrice;
    @BindView(R.id.servicePrice)
    TextView servicePrice;
    @BindView(R.id.deposit)
    TextView deposit;
    @BindView(R.id.sum)
    TextView sum;


    private final String PickReturnDateFormat = "MM-dd";

    private final String PickReturnTimeFormat = "HH:mm";
    @BindView(R.id.carIcon)
    ImageView carIcon;

    private CarModelDaoImpl carModelDao;

    private static final int msgCarModel = 0x001;

    private BitmapImpl bitmap;

    private static final int msgBitmap = 0x002;

    private String sumPrice;

    public static final int PayRequest = 0x003;

    private static final String PriceUnit = "元";

    private OrderFeeDaoImpl orderFeeDao;

    private static final int msgOrderFee = 0x004;

    private RentOrderDaoImpl rentOrderDao;

    private static final int msgRentOrder = 0x005;

    private Data data;

    private RentOrderCancelDaoImpl rentOrderCancelDao;

    private static final int msgOrderCancel = 0x006;

    private CustomDialog loading;

    public static final int RentCarOrderRequest = 0x007;

    public static final int Success = 0x007;

    public static final int Failure = 0x008;

    private Dialog sureDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.rent_car_order);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onStop() {
        super.onStop();
        dialogUtil.dismiss(loading);
    }


    @Override
    public void loadObjectAttribute() {
        MyHandler handler = new MyHandler(this);
        carModelDao = new CarModelDaoImpl(this, handler, msgCarModel);
        bitmap = new BitmapImpl(this, handler, msgBitmap);
        orderFeeDao = new OrderFeeDaoImpl(this,handler,msgOrderFee);
        rentOrderDao = new RentOrderDaoImpl(this,handler,msgRentOrder);
        rentOrderCancelDao = new RentOrderCancelDaoImpl(this,handler,msgOrderCancel);
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {
        data = intent.getParcelableExtra("data");
        pickCarCity.setText(data.getOrgName());
        returnCarCity.setText(data.getReturnOrgName());
        pickCarDate.setText(DateUtil.getSdfDate(data.getRentBeginTime(), PickReturnDateFormat));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtil.getDate(data.getRentBeginTime()));
        pickCarTime.setText(RentCarTimeActivity.getDayOfWeek(calendar) + " " + DateUtil.getSdfDate(calendar.getTime(), PickReturnTimeFormat));
        returnCarDate.setText(DateUtil.getSdfDate(data.getRentEndTime(), PickReturnDateFormat));
        calendar.setTime(DateUtil.getDate(data.getRentEndTime()));
        returnCarTime.setText(RentCarTimeActivity.getDayOfWeek(calendar) + " " + DateUtil.getSdfDate(calendar.getTime(), PickReturnTimeFormat));

        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(DateUtil.getDate(data.getRentBeginTime()));
        calendar.setTime(DateUtil.getDate(data.getRentEndTime()));
        long result = calendar.getTimeInMillis() - calendar1.getTimeInMillis();
        calendar.setTimeInMillis(result);
        if (calendar.get(Calendar.DAY_OF_MONTH) - 1 == 0) {
            time.setText(calendar.get(Calendar.DAY_OF_MONTH) + "");
        } else {
            time.setText(calendar.get(Calendar.DAY_OF_MONTH) - 1 + "");
        }
        loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
        Login.DataEntity dataEntity = application.getLogin().getData();
        orderFeeDao.getRentOrders(dataEntity.getToken(),dataEntity.getCustID(),data.getOrgId(),data.getReturnOrgId(),data.getRentBeginTime(),data.getRentEndTime(),data.getModelId());
        carModelDao.getCarModel(data.getModelId(), dataEntity.getToken(), dataEntity.getCustID());
    }

    @OnClick(R.id.payBtn)
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.payBtn:
                if(TextUtils.isEmpty(sumPrice)) {
                    return;
                }

                Login.DataEntity loginData = application.getLogin().getData();
                if(TextUtils.equals(BeanParam.Login.noVerified,loginData.getVerified())){
                    if(sureDialog == null){
                        sureDialog = dialogUtil.showSureListenerDialog(context, "您还未实名认证，是否去实名认证", (dialog, index) -> {
                            dialog.dismiss();
                            VerifiedActivity.startActivity(context);
                        });
                    }else{
                        sureDialog.show();
                    }
                }else{
                    Login.DataEntity dataEntity = application.getLogin().getData();
                    rentOrderDao.getRentOrder(dataEntity.getToken(),dataEntity.getCustID(),data.getOrgId(),data.getReturnOrgId(),data.getRentBeginTime(),data.getRentEndTime(),data.getModelId());
                }
                break;
        }
    }

    private static class MyHandler extends Handler{

        private WeakReference<RentCarPayOrderActivity> mActivity;

        public MyHandler(RentCarPayOrderActivity activity){
            mActivity = new WeakReference<RentCarPayOrderActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            RentCarPayOrderActivity activity = mActivity.get();
            if(activity == null){
                return;
            }
            switch (msg.what) {
                case msgCarModel:
                    if (TextUtils.equals("N", activity.carModelDao.carModel.getIsSuccess())) {
                        Error error = activity.errorParamUtil.checkReturnState(activity.carModelDao.carModel.getReturnStatus());
                        activity.toastUtil.toastError(activity.context,error,null);
                        return;
                    }
                    CarModel.DataEntity dataEntity = activity.carModelDao.carModel.getData();
                    activity.brand.setText(dataEntity.getBrand());
                    activity.carType.setText(dataEntity.getStruct() + "|" + dataEntity.getSeries() + "|乘坐" + dataEntity.getCounts() + "人");
                    activity.battery.setText("可续航约" + dataEntity.getEnduranceMileage() + "km");
                    activity.price.setText(dataEntity.getPrice() + PriceUnit);
                    if(!TextUtils.isEmpty(dataEntity.getPicture())){
                        activity.bitmap.getBitmap(dataEntity.getPicture());
                    }
                    break;
                case msgBitmap:
                    if (activity.bitmap.img == null) {
                        return;
                    }
                    activity.carIcon.setImageBitmap(activity.bitmap.img);
                    break;
                case msgOrderFee:
                    activity.dialogUtil.dismiss(activity.loading);
                    if(TextUtils.equals("N",activity.orderFeeDao.orderFee.getIsSuccess())){
                        Error error = activity.errorParamUtil.checkReturnState(activity.orderFeeDao.orderFee.getReturnStatus());
                        activity.toastUtil.toastError(activity.context,error,null);
                        return;
                    }
                    OrderFee.DataEntity dataEntity1 = activity.orderFeeDao.orderFee.getData();
                    activity.rentCarPrice.setText(dataEntity1.getPreRentAmt() + PriceUnit);
                    activity.insurancePrice.setText(dataEntity1.getInsurenceAmt() + PriceUnit);
                    activity.servicePrice.setText(dataEntity1.getServiceAmt() + PriceUnit);
                    activity.deposit.setText(dataEntity1.getDeposit() + PriceUnit);
                    activity.sumPrice = DoubleUtil.add(dataEntity1.getPreRentAmt(), dataEntity1.getInsurenceAmt(), dataEntity1.getServiceAmt(), dataEntity1.getDeposit());
                    activity.sum.setText(activity.sumPrice + PriceUnit);
                    break;
                case msgRentOrder:
                    if(TextUtils.equals("N",activity.rentOrderDao.rentOrder.getIsSuccess())){
                        Error error = activity.errorParamUtil.checkReturnState(activity.rentOrderDao.rentOrder.getReturnStatus());
                        activity.toastUtil.toastError(activity.context,error,null);
                        return;
                    }
                    PayActivity.startPayActivity(activity.context,activity.rentOrderDao.rentOrder.getData().getOrderNo(),PayActivity.RentCarPay,activity.sumPrice,PayRequest);
                    break;
                case msgOrderCancel:
                    activity.dialogUtil.dismiss(activity.loading);
                    if (TextUtils.equals("N",activity.rentOrderCancelDao.rentOrderCancel.getIsSuccess())) {
                        Error error = activity.errorParamUtil.checkReturnState(activity.rentOrderCancelDao.rentOrderCancel.getReturnStatus());
                        activity.toastUtil.toastError(activity.context,error,null);
                        return;
                    }
                    Toast.makeText(activity.context,"取消订单成功",Toast.LENGTH_SHORT).show();
                    activity.finish();
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case PayRequest:
                if(resultCode == PayActivity.Success){
                    Toast.makeText(this,"支付成功",Toast.LENGTH_SHORT).show();
                    RentOrderInfoActivity.startActivity(context,data.getStringExtra("OrderNo"),RentCarOrderRequest);
                }else if(resultCode == PayActivity.Failure){ //取消支付调用删除订单的接口
                    loading = dialogUtil.startLoading(context,loading,R.string.handleLoadingText);
                    Login.DataEntity dataEntity = application.getLogin().getData();
                    rentOrderCancelDao.getRentOrderCancel(dataEntity.getToken(),dataEntity.getCustID(),data.getStringExtra("OrderNo"));
                }
                break;
            case RentCarOrderRequest:
                setResult(Success);
                finish();
                break;
        }
    }

    public static void startActivity(Context context,Data data,int requestCode) {
        Intent intent = new Intent(context, RentCarPayOrderActivity.class);
        intent.putExtra("data",data);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent,requestCode);
    }


    static class Data implements Parcelable {
        public String rentBeginTime;
        public String rentEndTime;
        public String orgId;
        public String orgName;
        public String returnOrgId;
        public String returnOrgName;
        public String modelId;

        public Data() {
        }

        public String getRentBeginTime() {
            return rentBeginTime;
        }

        public void setRentBeginTime(String rentBeginTime) {
            this.rentBeginTime = rentBeginTime;
        }

        public String getRentEndTime() {
            return rentEndTime;
        }

        public void setRentEndTime(String rentEndTime) {
            this.rentEndTime = rentEndTime;
        }

        public String getOrgId() {
            return orgId;
        }

        public void setOrgId(String orgId) {
            this.orgId = orgId;
        }

        public String getOrgName() {
            return orgName;
        }

        public void setOrgName(String orgName) {
            this.orgName = orgName;
        }

        public String getReturnOrgId() {
            return returnOrgId;
        }

        public void setReturnOrgId(String returnOrgId) {
            this.returnOrgId = returnOrgId;
        }

        public String getReturnOrgName() {
            return returnOrgName;
        }

        public void setReturnOrgName(String returnOrgName) {
            this.returnOrgName = returnOrgName;
        }

        public String getModelId() {
            return modelId;
        }

        public void setModelId(String modelId) {
            this.modelId = modelId;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.rentBeginTime);
            dest.writeString(this.rentEndTime);
            dest.writeString(this.orgId);
            dest.writeString(this.orgName);
            dest.writeString(this.returnOrgId);
            dest.writeString(this.returnOrgName);
            dest.writeString(this.modelId);
        }

        protected Data(Parcel in) {
            this.rentBeginTime = in.readString();
            this.rentEndTime = in.readString();
            this.orgId = in.readString();
            this.orgName = in.readString();
            this.returnOrgId = in.readString();
            this.returnOrgName = in.readString();
            this.modelId = in.readString();
        }

        public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel source) {
                return new Data(source);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

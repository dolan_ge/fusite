package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.SelectCarTypeAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.Brands;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * 选择车型
 * Created by Jelly on 2016/8/16.
 */
public class SelectCarTypeActivity extends UtilActivity {

    public static String TAG = "SelectCarTypeActivity";

    @BindView(R.id.list)
    ListView list;

    private SelectCarTypeAdapter adapter;
    private List<Brands.DataEntity> dataList;

    public static final int ModCarRequest = 0x001;
    public static final int Success = 0x002;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.select_car_type);
        ButterKnife.bind(this);
        setActivityView();
    }

    @Override
    public void setActivityView() {
        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.Brands.Token, loginData.getToken());
        ApiService.getPileService().Brands(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.Brands.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Brands>() {
                    @Override
                    public void call(Brands brands) {
                        if (TextUtils.equals(BeanParam.Failure, brands.getIsSuccess())) {
                            return;
                        }
                        dataList = brands.getData();
                        adapter = new SelectCarTypeAdapter(SelectCarTypeActivity.this,brands.getData());
                        list.setAdapter(adapter);
                        setItemClick();
                    }
                }, new ThrowableAction(this));
    }

    public void setItemClick(){
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputCarActivity.startActivity(context,dataList.get(position).getBrand(), ModCarRequest);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ModCarRequest:
                if(resultCode == InputCarActivity.Success){
                    setResult(Success);
                    finish();
                }
                break;
        }
    }

    public static void startActivity(Context context,int requestCode){
        Intent intent = new Intent(context,SelectCarTypeActivity.class);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent,requestCode);
    }

}

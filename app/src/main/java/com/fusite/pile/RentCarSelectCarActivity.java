package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.SelectCarListAdapter;
import com.fusite.bean.CarModels;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.fragment.RentBrandsScreenFragment;
import com.fusite.fragment.RentPriceScreenFragment;
import com.fusite.fragment.RentTypeScreenFragment;
import com.fusite.impl.CarModelsDaoImpl;
import com.fusite.utils.LogUtil;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jelly on 2016/4/18.
 */
public class RentCarSelectCarActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "RentCarSelectCarActivity";

    View root;
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.showCanRentIcon)
    TextView showCanRentIcon;
    @BindView(R.id.showCanRentTitle)
    TextView showCanRentTitle;

    private SelectCarListAdapter adapter;

    private CarModelsDaoImpl carModelDao;

    private static final int msgCarModel = 0x001;

    public static final int ScreenRequest = 0x002;

    private List<CarModels.DataEntity> showList;
    private List<CarModels.DataEntity> tempList;

    private String priceStr = RentPriceScreenFragment.Price1;
    private String type = RentTypeScreenFragment.Type1;
    private String brand = RentBrandsScreenFragment.Brands1;

    public static final String DateFormat = "yyyy-MM-dd HH:mm:ss";

    private String orgId;
    private String orgName;
    private String returnOrgId;
    private String returnOrgName;
    private String rentBeginTime;
    private String rentEndTime;


    private boolean isShowCanRent;

    public static final int RentCarOrderRequest = 0x003;

    public static final int Success = 0x004;

    public static final int Failure = 0x005;

    private CustomDialog loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.select_car);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loading = null;
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {
        carModelDao = new CarModelsDaoImpl(this, new MyHandler(this), msgCarModel);
        orgId = intent.getStringExtra("orgId");
        orgName = intent.getStringExtra("orgName");
        returnOrgId = intent.getStringExtra("returnOrgId");
        returnOrgName = intent.getStringExtra("returnOrgName");
        rentBeginTime = intent.getStringExtra("rentBeginTime");
        rentEndTime = intent.getStringExtra("rentEndTime");
    }

    @Override
    public void setListener() {
        setItemListener();
    }

    @Override
    public void setActivityView() {
        loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
        Login.DataEntity dataEntity = application.getLogin().getData();
        carModelDao.getCarModels(intent.getStringExtra("orgId"), dataEntity.getToken(), dataEntity.getCustID());
    }

    /**
     * 设置Item的点击事件
     */
    public void setItemListener() {
        list.setOnItemClickListener((parent, view, position, id) -> {
            CarModels.DataEntity dataEntity = carModelDao.carModels.getData().get(position);
            if(Integer.parseInt(dataEntity.getAvailableNum()) <= 0){
                Toast.makeText(context,"请选择有空闲车辆的服务点",Toast.LENGTH_SHORT).show();
                return;
            }
            RentCarPayOrderActivity.Data data = new RentCarPayOrderActivity.Data();
            data.setModelId(dataEntity.getModelId());
            data.setOrgId(orgId);
            data.setOrgName(orgName);
            data.setReturnOrgId(returnOrgId);
            data.setReturnOrgName(returnOrgName);
            data.setRentBeginTime(rentBeginTime);
            data.setRentEndTime(rentEndTime);
            RentCarPayOrderActivity.startActivity(context,data,RentCarOrderRequest);
        });
    }

    @OnClick({R.id.screen_layout,R.id.showCanRentLayout})
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.screen_layout:
                if (carModelDao == null || TextUtils.equals("N", carModelDao.carModels.getIsSuccess())) {
                    Toast.makeText(RentCarSelectCarActivity.this, "数据为空，不能筛选！", Toast.LENGTH_SHORT).show();
                    return;
                }
                RentCarScreenCarActivity.startActivity(context, ScreenRequest, (ArrayList<CarModels.DataEntity>) carModelDao.carModels.getData(), priceStr, type, brand);
                break;
            case R.id.showCanRentLayout:

                if (carModelDao == null || TextUtils.equals("N", carModelDao.carModels.getIsSuccess())) {
                    Toast.makeText(RentCarSelectCarActivity.this, "数据为空，不能筛选！", Toast.LENGTH_SHORT).show();
                    return;
                }

                isShowCanRent = !isShowCanRent;

                if(!isShowCanRent){
                    showList = tempList;
                    adapter.setList(showList);
                    adapter.notifyDataSetChanged();
                    showCanRentIcon.setBackgroundColor(resourceUtil.getResourceColor(context,R.color.bestGrayBg));
                    showCanRentTitle.setTextColor(resourceUtil.getResourceColor(context,R.color.lowTextColor));
                    break;
                }

                List<CarModels.DataEntity> list = showList;
                List<CarModels.DataEntity> listTemp = new ArrayList<>();
                for(CarModels.DataEntity dataEntity : list){
                    if(Integer.parseInt(dataEntity.getAvailableNum()) > 0){
                        listTemp.add(dataEntity);
                    }else{

                    }
                }
                tempList = showList;
                showList = listTemp;
                adapter.setList(showList);
                adapter.notifyDataSetChanged();
                showCanRentIcon.setBackgroundColor(resourceUtil.getResourceColor(context,R.color.actionbar_bg));
                showCanRentTitle.setTextColor(resourceUtil.getResourceColor(context,R.color.actionbar_bg));
                break;
        }

    }

    private static class MyHandler extends Handler{
        private WeakReference<RentCarSelectCarActivity> mActivity;

        public MyHandler(RentCarSelectCarActivity activity){
            mActivity = new WeakReference<RentCarSelectCarActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            RentCarSelectCarActivity activity = mActivity.get();
            if(activity == null){
                return;
            }
            switch (msg.what) {
                case msgCarModel:
                    activity.dialogUtil.dismiss(activity.loading);
                    if (TextUtils.equals("N", activity.carModelDao.carModels.getIsSuccess())) {
                        Error error = activity.errorParamUtil.checkReturnState(activity.carModelDao.carModels.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, error, null);
                        return;
                    }
                    activity.showList = activity.carModelDao.carModels.getData();
                    if (activity.adapter == null) {
                        activity.adapter = new SelectCarListAdapter(activity.context,activity.showList);
                    }
                    activity.list.setAdapter(activity.adapter);
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ScreenRequest:
                if (resultCode == RentCarScreenCarActivity.Success) {
                    priceStr = data.getStringExtra("Price");
                    type = data.getStringExtra("Type");
                    brand = data.getStringExtra("Brand");

                    int priceStart = 0;
                    int priceEnd = 0;
                    if (TextUtils.equals(RentPriceScreenFragment.Price2, priceStr)) {
                        priceEnd = 150;
                    } else if (TextUtils.equals(RentPriceScreenFragment.Price3, priceStr)) {
                        priceStart = 150;
                        priceEnd = 300;
                    } else if (TextUtils.equals(RentPriceScreenFragment.Price4, priceStr)) {
                        priceStart = 300;
                        priceEnd = 500;
                    } else if (TextUtils.equals(RentPriceScreenFragment.Price5, priceStr)) {
                        priceStart = 500;
                        priceEnd = 10000;
                    }
                    List<CarModels.DataEntity> list = carModelDao.carModels.getData();
                    List<CarModels.DataEntity> listTemp = new ArrayList<>();
                    boolean flag1 = false;
                    boolean flag2 = false;
                    boolean flag3 = false;
                    for (CarModels.DataEntity dataEntity : list) {

                        flag1 = false;
                        flag2 = false;
                        flag3 = false;

                        //flag1
                        if (Double.parseDouble(dataEntity.getPrice()) <= priceEnd && Double.parseDouble(dataEntity.getPrice()) >= priceStart) {
                            flag1 = true;
                        }
                        if (priceStart == priceEnd && priceStart == 0) {
                            flag1 = true;
                        }
                        //flag2
                        if (TextUtils.equals(dataEntity.getBrand(), brand)) {
                            flag2 = true;
                        }
                        if (TextUtils.equals(RentBrandsScreenFragment.Brands1, brand)) {
                            flag2 = true;
                        }
                        //flag3
                        if (TextUtils.equals(dataEntity.getStruct(), type)) {
                            LogUtil.v(TAG, dataEntity.getStruct());
                            flag3 = true;
                        }
                        if (TextUtils.equals(RentTypeScreenFragment.Type1, type)) {
                            flag3 = true;
                        }

                        if (flag1 && flag2 && flag3) {
                            listTemp.add(dataEntity);
                        }
                    }
                    showList = listTemp;
                    adapter.setList(showList);
                    adapter.notifyDataSetChanged();
                }
                break;
            case RentCarOrderRequest:
                if(resultCode == RentCarPayOrderActivity.Success){
                    setResult(Success);
                    finish();
                }
                break;
        }
    }

    public static void startActivity(Context context, String orgId,String orgName,String returnOrgId,String returnOrgName,String rentBeginTime, String rentEndTime,int requestCode) {
        Intent intent = new Intent(context, RentCarSelectCarActivity.class);
        intent.putExtra("orgId", orgId);
        intent.putExtra("orgName",orgName);
        intent.putExtra("returnOrgId", returnOrgId);
        intent.putExtra("returnOrgName",returnOrgName);
        intent.putExtra("rentBeginTime", rentBeginTime);
        intent.putExtra("rentEndTime", rentEndTime);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent,requestCode);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

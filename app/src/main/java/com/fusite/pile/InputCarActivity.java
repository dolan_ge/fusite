package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ChinaService;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/8/16.
 */
public class InputCarActivity extends UtilActivity {

    public static String TAG = "InputCarActivity";
    @BindView(R.id.input_et)
    EditText inputEt;

    private String carType;

    public static final int Success = 0x001;

    public static final int Failure = 0x002;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.input_car);
        ButterKnife.bind(this);
        setActivityView();
    }


    @Override
    public void setActivityView() {
        carType = intent.getStringExtra("carType");
    }

    @OnClick(R.id.btn)
    public void onClick() {

        String series = inputEt.getText().toString();

        if(TextUtils.isEmpty(series)){
            Toast.makeText(context,"请输入车型",Toast.LENGTH_SHORT).show();
            return;
        }

        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.UpdateUserInfo.Token, loginData.getToken());
        Log.v("Brand",carType);
        map.put(RequestParam.UpdateUserInfo.Brand, carType);
        map.put(RequestParam.UpdateUserInfo.Series,series);
        Map<String, String> mapParam = new HashMap();
        mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, NetParam.sign, RequestParam.UpdateUserInfo.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));
        ChinaService.UpdateUserInfo(mapParam)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(updateUserInfo -> {
                    if (updateUserInfo == null) {
                        return;
                    }
                    application.getLogin().getData().setBrand(carType);
                    application.getLogin().getData().setSeries(series);
                    application.cacheLogin();

                    setResult(Success);

                    finish();

                },new ThrowableAction(this));
    }


    public static void startActivity(Context context, String carType , int requestCode) {
        Intent intent = new Intent(context, InputCarActivity.class);
        intent.putExtra("carType", carType);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent,requestCode);
    }


}

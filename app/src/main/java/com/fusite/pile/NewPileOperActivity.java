package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.ViewPageAdapter;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.OrderInfo;
import com.fusite.bean.PendingOrder;
import com.fusite.bean.PileInfo;
import com.fusite.bean.StartCharge;
import com.fusite.constant.TypeDefined;
import com.fusite.fragment.PileOperFrgment1;
import com.fusite.fragment.PileOperFrgment2;
import com.fusite.fragment.PileOperFrgment3;
import com.fusite.fragments.BaseFragment;
import com.fusite.handlerinterface.FragDataListener;
import com.fusite.impl.ChargeOrderDaoImpl;
import com.fusite.impl.FinishChargeDaoImpl;
import com.fusite.impl.OrderInfoDaoImpl;
import com.fusite.impl.PendingOrderImpl;
import com.fusite.impl.PileInfoDaoImpl;
import com.fusite.utils.DoubleUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jelly on 2016/7/11.
 */
public class NewPileOperActivity extends UtilActivity implements FragDataListener {

    public String TAG = "NewPileOperActivity";
    @BindView(R.id.vp)
    ViewPager vp;
    @BindView(R.id.selectImage1)
    ImageView selectImage1;
    @BindView(R.id.selectImage2)
    ImageView selectImage2;
    @BindView(R.id.selectImage3)
    ImageView selectImage3;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.btn)
    TextView btn;
    @BindView(R.id.chargingQty)
    TextView chargingQty;
    @BindView(R.id.facilityName)
    TextView facilityName;

    private ImageView currSelectImage;
    private BaseFragment[] fragments;
    private PileOperFrgment1 pileOperFrgment1;
    private PileOperFrgment2 pileOperFrgment2;
    private PileOperFrgment3 pileOperFrgment3;
    private ViewPageAdapter viewPageAdapter;

    private Map<String, String> dataMap;

    private FinishChargeDaoImpl finishChargeDao;
    private static final int msgFinishCharge = 0x002;
    public static final String NOCHARGE = "开始充电";
    /**
     * 正在充电，点击结束充电
     */
    public static final String CHARGEING = "正在充电";
    /**
     * 完成充电，点击支付
     */
    public static final String FINISHCHARGE = "完成充电";
    /**
     * 完成支付，点击无效，显示状态
     */
    public static final String FINISHPAY = "完成支付";
    /**
     * 电桩上传账单中，无法点击支付
     */
    public static final String NoNet = "上传账单";
    /**
     * 充电状态
     */
    private String chargeState = NOCHARGE;
    private static final int PAYREQUEST = 0x001;
    private PileInfoDaoImpl pileInfoDao;
    private static final int msgPileInfo = 0x003; //二维码获取电桩详情消息
    private ChargeOrderDaoImpl chargeOrderDao;
    private static final int msgCharge = 0x004;
    private String OrderNo;
    private String FacilityID;
    private String GunID;
    private String GunStatus;
    private CustomDialog loading;
    private OrderInfoDaoImpl orderInfoDao;
    private static final int msgInfo = 0x006; //获取详情消息
    private OrderInfoDaoImpl startOrderInfoDao;
    private static final int startOrderInfo = 0x007; //在刚进来界面时获取订单详情的消息
    private MyHandler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.new_pileoper_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void init() {
        currSelectImage = selectImage1;
    }

    @Override
    public void loadObjectAttribute() {
        if (fragments == null) {
            pileOperFrgment1 = new PileOperFrgment1().setDataListener(this);
            pileOperFrgment2 = new PileOperFrgment2().setDataListener(this);
            pileOperFrgment3 = new PileOperFrgment3().setDataListener(this);
            fragments = new BaseFragment[]{pileOperFrgment1, pileOperFrgment2, pileOperFrgment3};
        }

        viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager(), fragments);

        handler = new MyHandler(this);
        finishChargeDao = new FinishChargeDaoImpl(this, handler, msgFinishCharge);
        chargeOrderDao = new ChargeOrderDaoImpl(this, handler, msgCharge);
        pileInfoDao = new PileInfoDaoImpl(this, handler, msgPileInfo);
        orderInfoDao = new OrderInfoDaoImpl(this, handler, msgInfo);
        startOrderInfoDao = new OrderInfoDaoImpl(this, handler, startOrderInfo);
        application = (MyApplication) getApplication();
        pileInfoDao = new PileInfoDaoImpl(this, handler, msgPileInfo);

        dataMap = new HashMap<>();
    }

    @Override
    public void setListener() {
        setViewPageSelectListener();
    }

    @Override
    public void setActivityView() {
        vp.setAdapter(viewPageAdapter);
        if (!TextUtils.isEmpty(intent.getStringExtra("QRId"))) { //读取二维码数据
            pileInfoDao.getPileInfoDao(intent.getStringExtra("QRId"), PileInfoDaoImpl.QRCODE);
            return;
        } else {

        }

        if (intent.getParcelableExtra("Entity") instanceof PendingOrder.EvcOrdersGetEntity) {
            PendingOrder.EvcOrdersGetEntity entity = intent.getParcelableExtra("Entity");
            if (TextUtils.equals(PendingOrderImpl.PAYING, entity.getOrderStatus()) && TextUtils.equals(OrderInfoDaoImpl.Uploaded,entity.getBillUploaded())) {
                chargeState = FINISHCHARGE;
                btn.setText("立即支付");
            } else if (TextUtils.equals(PendingOrderImpl.CHARGING, entity.getOrderStatus())) {
                chargeState = CHARGEING;
                btn.setText("停止充电");
                Login.DataEntity dataEntity = application.getLogin().getData();
                startOrderInfoDao.getOrderInfo(dataEntity.getToken(), entity.getOrderNo(), dataEntity.getCustID());
            } else if(TextUtils.equals(PendingOrderImpl.PAYING, entity.getOrderStatus()) && !TextUtils.equals(OrderInfoDaoImpl.Uploaded,entity.getBillUploaded())){
                chargeState = NoNet;
                btn.setText("上传账单");
            }

            dataMap.put("ChargingTime", entity.getChargingTime());
            dataMap.put("ChargingQty", entity.getChargingQty());
            dataMap.put("ChargingAmt", entity.getChargingAmt());
            dataMap.put("ServiceAmt", entity.getServiceAmt());
            LogUtil.d(TAG, "EvcOrdersGetEntity: GunID=" + entity.getGunID());
            dataMap.put("GunID", entity.getGunID());

            facilityName.setText(entity.getFacilityName());

            OrderNo = entity.getOrderNo();
            pileInfoDao.getPileInfoDao(entity.getFacilityID(), PileInfoDaoImpl.FACILITYID);

        } else if (intent.getParcelableExtra("Entity") instanceof StartCharge.EvcOrderChangeEntity) {
            StartCharge.EvcOrderChangeEntity entity = intent.getParcelableExtra("Entity");
            pileInfoDao.getPileInfoDao(entity.getFacilityID(), PileInfoDaoImpl.FACILITYID);
            OrderNo = entity.getOrderNo();
            chargeState = CHARGEING;
            btn.setText("停止充电");

            Login.DataEntity dataEntity = application.getLogin().getData();
            startOrderInfoDao.getOrderInfo(dataEntity.getToken(), entity.getOrderNo(), dataEntity.getCustID());
        }

    }

    @OnClick(R.id.btn)
    public void onClick() {
        if (TextUtils.equals(chargeState, NOCHARGE)) { //如果电桩还没有还是充电，点击开始充电
            if (TextUtils.isEmpty(FacilityID)) {
                return;
            }
            Login.DataEntity entity = application.getLogin().getData();
            chargeOrderDao.getChargeOrderDao(FacilityID, GunID, entity.getToken(), entity.getCustID());
        } else if (TextUtils.equals(chargeState, CHARGEING)) { //充电桩正在充电，结束充电
            dialogUtil.showSureListenerDialog(this, "是否结束本次充电！", (dialog, index) -> {
                Login.DataEntity data = application.getLogin().getData();
                if (!TextUtils.isEmpty(OrderNo)) {
                    finishChargeDao.getFinishChargeDao(data.getToken(), OrderNo, data.getCustID());
                }
                dialog.dismiss();
            });
        } else if (TextUtils.equals(chargeState, FINISHCHARGE)) { //已经完成充电等待支付
            String price = DoubleUtil.add(dataMap.get("ChargingAmt"),dataMap.get("ServiceAmt"));
            Log.v("price","");
            PayActivity.startPayActivity(this, OrderNo, PayActivity.ChargePay, price, PAYREQUEST);
        } else if(TextUtils.equals(chargeState,NoNet)){
            Toast.makeText(this,"充电站正在上传账单，请稍后重试",Toast.LENGTH_SHORT).show();
        }
    }

    public void setViewPageSelectListener() {
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (currSelectImage != null) {
                    currSelectImage.setImageResource(R.drawable.pile_oper_noselect);
                }
                switch (position) {
                    case 0:
                        selectImage1.setImageResource(R.drawable.pile_oper_select);
                        currSelectImage = selectImage1;
                        break;
                    case 1:
                        selectImage2.setImageResource(R.drawable.pile_oper_select);
                        currSelectImage = selectImage2;
                        break;
                    case 2:
                        selectImage3.setImageResource(R.drawable.pile_oper_select);
                        currSelectImage = selectImage3;
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public Map<String, String> getData() {
        return dataMap;
    }

    private static class MyHandler extends Handler{
        private WeakReference<NewPileOperActivity> mActivity;

        public MyHandler(NewPileOperActivity activity){
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {

            NewPileOperActivity activity = mActivity.get();
            if(activity == null){
                return;
            }

            Login.DataEntity loginEntity = activity.application.getLogin().getData();
            switch (msg.what) {
                case msgCharge: //点击充电
                    if (activity.chargeOrderDao.chargeOrderDao == null || TextUtils.equals(activity.chargeOrderDao.chargeOrderDao.getEvc_order_set().getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.chargeOrderDao.chargeOrderDao.getEvc_order_set().getReturnStatus());
                        if (activity.toastUtil.toastError(activity.context, errorDao, null)) {
                            return;
                        }
                        activity.finish();
                        return;
                    }
                    activity.chargeState = CHARGEING;
                    activity.btn.setText("停止充电");
                    activity.OrderNo = activity.chargeOrderDao.chargeOrderDao.getEvc_order_set().getOrderNo();
                    Login.DataEntity dataEntity = activity.application.getLogin().getData();
                    activity.startOrderInfoDao.getOrderInfo(dataEntity.getToken(), activity.chargeOrderDao.chargeOrderDao.getEvc_order_set().getOrderNo(), dataEntity.getCustID());
                    break;
                case msgFinishCharge: //完成充电
                    if (activity.finishChargeDao.finishChargeDao == null || TextUtils.equals(activity.finishChargeDao.finishChargeDao.getEvc_order_change().getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.finishChargeDao.finishChargeDao.getEvc_order_change().getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, null);
                        return;
                    }
                    activity.loading = activity.dialogUtil.startLoading(activity.context,activity.loading,R.string.handleLoadingText); //开启进度条
                    activity.orderInfoDao.getOrderInfo(loginEntity.getToken(), activity.finishChargeDao.finishChargeDao.getEvc_order_change().getOrderNo(), loginEntity.getCustID());
                    break;
                case msgPileInfo: //根据二维码或者电桩id获取电桩详情
                    if (activity.pileInfoDao.pileInfoDao == null || TextUtils.equals(activity.pileInfoDao.pileInfoDao.getEvc_facility_get().getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.pileInfoDao.pileInfoDao.getEvc_facility_get().getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, null);
                        activity.finish();
                        return;
                    }
                    PileInfo.EvcFacilityGetEntity entity = activity.pileInfoDao.pileInfoDao.getEvc_facility_get();
                    activity.title.setText(entity.getOrgName());
                    activity.dataMap.put("FacilityName",entity.getFacilityName());
                    activity.dataMap.put("FacilityID",entity.getFacilityID());
                    LogUtil.d(activity.TAG, "case msgPileInfo GunID=" + entity.getGunID());
                    activity.dataMap.put("GunID",entity.getGunID());

                    if (TextUtils.equals(TypeDefined.FACILITY_TYPE_DC, entity.getFacilityType())) {
                        activity.dataMap.put("FacilityType","直流桩");
                    } else if (TextUtils.equals(TypeDefined.FACILITY_TYPE_AC, entity.getFacilityType())) {
                        activity.dataMap.put("FacilityType","交流桩");
                    }

                    activity.FacilityID = entity.getFacilityID();
                    activity.GunID = entity.getGunID();

                    //显示电桩号
                    activity.facilityName.setText(entity.getFacilityName());

                    activity.updateView();
                    break;
                case msgInfo:
                    if (activity.orderInfoDao.orderInfo == null || TextUtils.equals(activity.orderInfoDao.orderInfo.getEvc_order_get().getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.pileInfoDao.pileInfoDao.getEvc_facility_get().getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, null);
                        return;
                    }
                    OrderInfo.EvcOrderGetEntity evcOrderGetEntity = activity.orderInfoDao.orderInfo.getEvc_order_get();
                    if (TextUtils.equals(OrderInfoDaoImpl.Uploaded, evcOrderGetEntity.getBillUploaded()) && TextUtils.equals(OrderInfoDaoImpl.Paying, evcOrderGetEntity.getOrderStatus())) {
                        activity.chargeState = FINISHCHARGE;
                        activity.btn.setText("立即支付");
                        //充电完成后跳到支付界面
                        String sum = DoubleUtil.add(evcOrderGetEntity.getServiceAmt(), evcOrderGetEntity.getChargingAmt());
                        PayActivity.startPayActivity(activity.context, activity.orderInfoDao.orderInfo.getEvc_order_get().getOrderNo(), PayActivity.ChargePay, sum, PAYREQUEST);
                        activity.finish();
                    } else if (TextUtils.equals(OrderInfoDaoImpl.Uploaded, evcOrderGetEntity.getBillUploaded()) && TextUtils.equals(OrderInfoDaoImpl.COMPLETE, evcOrderGetEntity.getOrderStatus())) {
                        Toast.makeText(activity.context, "订单已自动结算，感谢您的使用！", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    } else if (!TextUtils.equals(OrderInfoDaoImpl.Uploaded, evcOrderGetEntity.getBillUploaded())) {
                        new Thread(() -> {
                            try {
                                Thread.sleep(3000); //延迟3秒继续请求
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            activity.orderInfoDao.getOrderInfo(loginEntity.getToken(), activity.orderInfoDao.orderInfo.getEvc_order_get().getOrderNo(), loginEntity.getCustID());
                        }).start();
                    }
                    break;
                case startOrderInfo: //进入界面时获取订单详情
                    if (activity.startOrderInfoDao.orderInfo == null || TextUtils.equals(activity.startOrderInfoDao.orderInfo.getEvc_order_get().getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.startOrderInfoDao.orderInfo.getEvc_order_get().getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, null);
                        return;
                    }
                    OrderInfo.EvcOrderGetEntity evcOrderGetEntity1 = activity.startOrderInfoDao.orderInfo.getEvc_order_get();

                    Log.v("json1", JsonUtil.Object2Json(evcOrderGetEntity1));

                    activity.dataMap.put("FacilityName",evcOrderGetEntity1.getFacilityName());
                    LogUtil.d(activity.TAG, "case startOrderInfo GunID=" + evcOrderGetEntity1.getGunID());
                    activity.dataMap.put("GunID",evcOrderGetEntity1.getGunID());
                    activity.dataMap.put("ChargingQty",evcOrderGetEntity1.getChargingQty());
                    activity.dataMap.put("ChargingTime",evcOrderGetEntity1.getChargingTime());
                    activity.dataMap.put("ChargingAmt",evcOrderGetEntity1.getChargingAmt());
                    activity.dataMap.put("ServiceAmt",evcOrderGetEntity1.getServiceAmt());
                    activity.dataMap.put("RemainTime",evcOrderGetEntity1.getRemainTime());
                    activity.dataMap.put("ChargingPower",evcOrderGetEntity1.getChargingPower());
                    activity.dataMap.put("PostStateOfCharge",evcOrderGetEntity1.getPostStateOfCharge());
                    activity.dataMap.put("ChargingCurrent",evcOrderGetEntity1.getChargingCurrent());
                    activity.dataMap.put("ChargingVoltage",evcOrderGetEntity1.getChargingVoltage());

                    //显示充电进度
                    double soc = Double.parseDouble(evcOrderGetEntity1.getPostStateOfCharge());

                    activity.chargingQty.setText( (int)(soc) + "%");

                    activity.updateView();

                    if ( activity.chargeState == CHARGEING || TextUtils.equals(OrderInfoDaoImpl.Chargeing, evcOrderGetEntity1.getOrderStatus())) {
                        new Thread(() -> {
                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if(activity.application.getLogin() == null) return;
                            Login.DataEntity dataEntity1 = activity.application.getLogin().getData();
                            activity.startOrderInfoDao.getOrderInfo(dataEntity1.getToken(), evcOrderGetEntity1.getOrderNo(), dataEntity1.getCustID());

                        }).start();
                    }

                    break;
            }


        }
    }

    public void updateView(){
        pileOperFrgment1.updateView();
        pileOperFrgment2.updateView();
        pileOperFrgment3.updateView();
    }

    /**
     * 返回结果
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PAYREQUEST:
                if (resultCode == PayActivity.Success) {
                    chargeState = FINISHPAY;
                    btn.setText("完成充电");
                    finish();
                } else if (resultCode == PayActivity.Failure) {

                }
                break;
        }
    }

    /**
     * 开启充电界面
     * @param context
     * @param entity
     */
    public static void startActivity(Context context, StartCharge.EvcOrderChangeEntity entity) {
        Intent intent = new Intent(context, NewPileOperActivity.class);
        intent.putExtra("Entity", entity);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, PendingOrder.EvcOrdersGetEntity entity) {
        Intent intent = new Intent(context, NewPileOperActivity.class);
        intent.putExtra("Entity", entity);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String QRId) {
        Intent intent = new Intent(context, NewPileOperActivity.class);
        intent.putExtra("QRId", QRId);
        context.startActivity(intent);
    }


}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.amap.api.navi.AMapNaviView;
import com.amap.api.navi.enums.NaviType;
import com.amap.api.navi.model.NaviLatLng;

/**
 * Created by Jelly on 2016/3/8.
 */
public class NaviEmulatorActivity extends BaseNavActivity{
    /**
     * TAG
     */
    public String TAG = "NaviEmulatorActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.standernavimap);
        mAMapNaviView = (AMapNaviView) findViewById(R.id.map);
        mAMapNaviView.onCreate(savedInstanceState);
        mAMapNaviView.setAMapNaviViewListener(this);

        //获得数据
        Intent intent = getIntent();
        double startLat = intent.getDoubleExtra("startlat", 0);
        double startLong = intent.getDoubleExtra("startlong", 0);
        double endLat = intent.getDoubleExtra("endlat", 0);
        double endLong = intent.getDoubleExtra("endlong",0);
        mStartLatlng = new NaviLatLng(startLat,startLong);
        mEndLatlng = new NaviLatLng(endLat,endLong);
        sList.add(mStartLatlng);
        eList.add(mEndLatlng);

    }

    @Override
    public void onInitNaviSuccess() {
        super.onInitNaviSuccess();
        /**
         * 方法: int strategy=mAMapNavi.strategyConvert(congestion, avoidhightspeed, cost, hightspeed, multipleroute); 参数:
         *
         * @congestion 躲避拥堵
         * @avoidhightspeed 不走高速
         * @cost 避免收费
         * @hightspeed 高速优先
         * @multipleroute 多路径
         *
         *  说明: 以上参数都是boolean类型，其中multipleroute参数表示是否多条路线，如果为true则此策略会算出多条路线。
         *  注意: 不走高速与高速优先不能同时为true 高速优先与避免收费不能同时为true
         */
        int strategy = 0;
        try {
            //再次强调，最后一个参数为true时代表多路径，否则代表单路径
            strategy = mAMapNavi.strategyConvert(true, false, false, false, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAMapNavi.calculateDriveRoute(sList, eList, mWayPointList, strategy);

    }

    @Override
    public void onCalculateRouteSuccess() {
        super.onCalculateRouteSuccess();
        mAMapNavi.startNavi(NaviType.GPS);
    }

    /**
     * 开启界面
     */
    public static void startAppActivity(Context context,double startLat,double startLong,double endLat,double endLong){
        Intent intent = new Intent(context,NaviEmulatorActivity.class);
        intent.putExtra("startlat",startLat);
        intent.putExtra("startlong",startLong);
        intent.putExtra("endlat",endLat);
        intent.putExtra("endlong",endLong);
        context.startActivity(intent);
    }

}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.SearchRentSearchAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.RentStations;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.LogUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/4/20.
 */
public class RentCarSearchActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "RentCarSearchActivity";

    @BindView(R.id.search_et)
    EditText searchEt;
    @BindView(R.id.list)
    ListView list;
    /**
     * 搜索集合
     */
    private RentStations rentStations;
    /**
     * 搜索出来的数据
     */
    private List<RentStations.DataEntity> searchs;
    /**
     * 适配器
     */
    private SearchRentSearchAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.search);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {
    }

    @Override
    public void loadObjectAttribute() {
    }

    @Override
    public void setListener() {
        setInputListener();
        setSearchListItemClickListener();
    }

    @Override
    public void setActivityView() {
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.RentStations.Position, RequestParam.RentStations.PositionValue);
        ApiService.getPileService().getRentStations(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.RentStations.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .map(rentStations -> {
                    List<RentStations.DataEntity> list1 = setCondition(rentStations.getData());
                    rentStations.setData(list1);
                    return rentStations;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rentStations -> RentCarSearchActivity.this.rentStations = rentStations,
                        throwable -> {
                            LogUtil.v(TAG, throwable.getMessage());
                        });
    }

    /**
     * 设置搜索列表的点击监听事件
     */
    public void setSearchListItemClickListener() {
        list.setOnItemClickListener((parent, view, position, id) -> {

        });
    }

    /**
     * 设置输入监听
     */
    public void setInputListener() {
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchText = searchEt.getText().toString();
                LogUtil.v(TAG, "开始搜索：" + searchText);
                if (rentStations == null) {
                    return;
                }
                try {
                    searchs = searchUtil.searchRentListOnRegExp(searchText, rentStations.getData());
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
                if (searchs != null) {
                    adapter = new SearchRentSearchAdapter(context, application, searchs);
                    list.setAdapter(adapter);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * 设置条件
     */
    public List<RentStations.DataEntity> setCondition(List<RentStations.DataEntity> daos) {
        for (int i = 0; i < daos.size(); i++) {
            RentStations.DataEntity dataEntity = daos.get(i);
            dataEntity.setCondition(dataEntity.getOrgName() + dataEntity.getAddr() + dataEntity.getCity());
        }
        return daos;
    }


    /**
     * 从附近界面跳入到搜索界面
     *
     * @param context
     */
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, RentCarSearchActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

package com.fusite.pile;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.PackageUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;
import com.zhy.http.okhttp.request.RequestCall;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/5/6.
 */
public class SplashActivity extends UtilActivity{
    /**
     * TAG
     */
    public static String TAG = "SplashActivity";

    private final int SPLASH_DISPLAY_LENGHT = 500; // 延迟1秒

    public static final String VersionCodeName = "guide";
    public static final String VersionCodeKey = "versionCode";

    private ProgressDialog progressDialog;
    private String downloadUrl;

    public static final int WRITE_EXTERNAL_STORAGE = 0x001; //写入文件的权限申请
    public static final int LOCATION = 0x002; //定位的权限申请
    private int to;

    private String[] needPermissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.CAMERA,Manifest.permission.CALL_PHONE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        
        //当系统版本为4.4或者4.4以上时可以使用沉浸式状态栏
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //透明导航栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        new Handler().postDelayed(() -> {

            float versionCode = PackageUtil.getVersionCode(this);
            SharedPreferences sharedPreferences = getSharedPreferences(VersionCodeName,MODE_PRIVATE);
            float spVersionCode = sharedPreferences.getFloat(VersionCodeKey,0);

            Map<String, String> map = new HashMap<>();
            ApiService.getPileService().GetVersionUpdateInfo(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.GetVersionUpdateInfo.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(versionUpdateInfo -> {
                        if (TextUtils.equals(BeanParam.Failure, versionUpdateInfo.getIsSuccess())) {
                            Error error = errorParamUtil.checkReturnState(versionUpdateInfo.getReturnStatus());
                            toastUtil.toastError(context, error, null);
                            return null;
                        }
                        return versionUpdateInfo;
                    })
                    .subscribe(versionUpdateInfo -> {
                        if (versionUpdateInfo == null) {
                            return;
                        }

                        float currVersion = PackageUtil.getVersionCode(context);
                        float netVersion = Float.valueOf(versionUpdateInfo.getData().getVersion());
                        // TODO 后续需要开启检查更新功能
//                        if (netVersion > currVersion && currVersion != 0) { //开始更新
//                            dialogUtil.showSureListenerDialog(this, "发现新版本是否开始更新？", new DialogListener() {
//                                @Override
//                                public void handle(Dialog dialog, int index) {
//                                    if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                                        //无权限
//                                        downloadUrl = versionUpdateInfo.getData().getPath();
//                                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE);
//                                    }else{
//                                        //有权限
//                                        startUpdate(versionUpdateInfo.getData().getPath());
//                                    }
//
//                                }
//                            }, new DialogListener() {
//                                @Override
//                                public void handle(Dialog dialog, int index) {
//                                    //cancel
//                                    finish();
//                                }
//                            });
//                        } else {

                            if (!checkPermission(needPermissions)) {
                                //无权限
                                if (versionCode > spVersionCode) { //第一次启动
                                    //保存版本号
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putFloat(VersionCodeKey, versionCode);
                                    editor.commit();
                                    //打开引导页
                                    to = 0; //打开引导页
                                } else {
                                    to = 1; //打开主界面
                                }

                                ActivityCompat.requestPermissions((Activity) context, needPermissions, LOCATION);
                            }else{
                                finish();
                                //有权限
//                                if (versionCode > spVersionCode) { //第一次启动
//                                    //保存版本号
//                                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                                    editor.putFloat(VersionCodeKey, versionCode);
//                                    editor.commit();
//                                    //打开引导页
//                                    GuideActivity.startActivity(this);
//                                } else {
                                    MainActivity.startActivity(this);
//                                }
                            }
//                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            to = 1;
                            if (!checkPermission(needPermissions)) {
                                //无权限
                                ActivityCompat.requestPermissions((Activity) context, needPermissions, LOCATION);
                            }else{
                                finish();
                                MainActivity.startActivity(context);
                            }
                        }
                    });

        }, SPLASH_DISPLAY_LENGHT);
    }

    @Override
    public void setActivityView() {

    }

    /**
     * 检查权限
     * @param permissions
     */
    public boolean checkPermission(String[] permissions){
        for(String permission :  permissions){
            if(ContextCompat.checkSelfPermission(context,permission) != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }
        return true;
    }

    public boolean getIsSuccess(int[] grantResults){
        for(int result : grantResults){
            if(result != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && getIsSuccess(grantResults)) {  //成功
                    startUpdate(downloadUrl);
                } else { //失败
                    Toast.makeText(context,"获取权限失败",Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            case LOCATION:
                if (grantResults.length > 0 && getIsSuccess(grantResults)) {  //成功
                    finish();
//                    if(to == 0){
//                        GuideActivity.startActivity(context);
//                    }else if(to == 1){
                        MainActivity.startActivity(context);
//                    }
                } else { //失败
                    Toast.makeText(context,"获取权限失败",Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }
    }

    public void startUpdate(String downloadPath) {
        //构造请求
        RequestCall requestCall = OkHttpUtils
                .get()
                .url(downloadPath)
                .build();

        //打开进度栏
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("下载进度");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "取消下载", (dialog, which) -> {
            progressDialog.dismiss();
        });
        progressDialog.setOnDismissListener(dialog -> requestCall.cancel());
        progressDialog.show();
        //开始下载
        requestCall.execute(new FileCallBack(Environment.getExternalStorageDirectory().getAbsolutePath(), "fusite_android.apk") {
            /**
             * 下载出现错误
             * @param call
             * @param e
             * @param id
             */
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.v(TAG,e.getMessage());
                progressDialog.dismiss();
            }

            /**
             * 下载完成调用
             * @param response
             * @param id
             */
            @Override
            public void onResponse(File response, int id) {
                progressDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(response),
                        "application/vnd.android.package-archive");
                context.startActivity(intent);
            }

            /**
             * 加载进度
             * @param progress
             * @param total
             * @param id
             */
            @Override
            public void inProgress(float progress, long total, int id) {
                progressDialog.setProgress((int) (progress * 100));
            }
        });
    }

}

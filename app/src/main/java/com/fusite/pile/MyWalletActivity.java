package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Balance;
import com.fusite.bean.Bill;
import com.fusite.bean.ChargeCardItem;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.CircleTransform;
import com.fusite.utils.HttpCacheUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bingoogolapple.badgeview.BGABadgeImageView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 我的钱包
 * Created by Jelly on 2016/3/16.
 */
public class MyWalletActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "MyWalletActivity";
    @BindView(R.id.nickname_text)
    TextView nicknameText;
    @BindView(R.id.money_text)
    TextView moneyText;
    @BindView(R.id.card_text)
    TextView cardText;
    @BindView(R.id.bill_text)
    TextView billText;
    BGABadgeImageView userIcon;

    private Balance balance;
    private List<ChargeCardItem> chargeCardItems;
    private List<Bill> bills;

    private HttpCacheUtil httpCacheUtil;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.my_wallet);
        ButterKnife.bind(this);
        userIcon = findViewById(R.id.usericon);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    /**
     * 当界面获取焦点
     */
    @Override
    protected void onResume() {
        super.onResume();
        setActivityView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void loadObjectAttribute() {
        httpCacheUtil = new HttpCacheUtil(context);
    }

    @Override
    public void setActivityView() {
        Login.DataEntity loginData = application.getLogin().getData();
        if (!TextUtils.isEmpty(loginData.getCustName())) {
            nicknameText.setText(loginData.getCustName());
        }
        if (!TextUtils.isEmpty(loginData.getUsericon())) {
            Picasso.with(context).load(loginData.getUsericon()).transform(new CircleTransform()).into(userIcon);
        }
        Bitmap avatarBadgeBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_vip);
        userIcon.showDrawableBadge(avatarBadgeBitmap);

        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetBalance.Token, loginData.getToken());

        if(httpCacheUtil.isExistCache(RequestParam.GetBalance.InterfaceName)){
            balance = JsonUtil.objectFromJson(httpCacheUtil.readHttp(RequestParam.GetBalance.InterfaceName),Balance.class);
            application.getLogin().getData().setBalanceAmt(balance.getCrm_balanceamt_get().getBalanceAmt());
            application.cacheLogin();
            moneyText.setText(balance.getCrm_balanceamt_get().getBalanceAmt());
        }

        //获取余额
        ApiService.getPileService().GetBalance(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.GetBalance.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(balances -> {
                    if (balances.size() == 0) {
                        return null;
                    }
                    return balances.get(0);
                })
                .map(balance -> {
                    if (balance == null) {
                        return null;
                    }
                    if (TextUtils.equals(BeanParam.Failure, balance.getCrm_balanceamt_get().getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(balance.getCrm_balanceamt_get().getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return balance;
                })
                .subscribe(balance -> {
                    if (balance == null) {
                        return;
                    }
                    this.balance = balance;
                    application.getLogin().getData().setBalanceAmt(balance.getCrm_balanceamt_get().getBalanceAmt());
                    application.cacheLogin();
                    moneyText.setText(balance.getCrm_balanceamt_get().getBalanceAmt());
                    //缓存
                    httpCacheUtil.cacheHttp(JsonUtil.Object2Json(balance),RequestParam.GetBalance.InterfaceName);
                }, new ThrowableAction(context));

        if(httpCacheUtil.isExistCache(RequestParam.GetChargeItemCard.InterfaceName)){
            chargeCardItems = JsonUtil.arrayFormJson(httpCacheUtil.readHttp(RequestParam.GetChargeItemCard.InterfaceName),ChargeCardItem[].class);
            cardText.setText(chargeCardItems.size()+"");
        }

        //获取充电卡
        ApiService.getPileService().GetChargeCardItem(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.GetChargeItemCard.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(chargeCardItems1 -> {
                    if (chargeCardItems1.size() == 0) {
                        return chargeCardItems1;
                    }
                    LogUtil.v(TAG,JsonUtil.Object2Json(chargeCardItems1));
                    if (TextUtils.equals(BeanParam.Failure, chargeCardItems1.get(0).getCrm_accounts_get().getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(chargeCardItems1.get(0).getCrm_accounts_get().getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }

                    return chargeCardItems1;
                })
                .subscribe(chargeCardItems1 -> {

                    cardText.setText(chargeCardItems1.size()+"");
                    this.chargeCardItems = chargeCardItems1;

                    //缓存
                    httpCacheUtil.cacheHttp(JsonUtil.Object2Json(chargeCardItems1),RequestParam.GetChargeItemCard.InterfaceName);

                }, throwable -> {
                    MyWalletActivity.this.chargeCardItems = new ArrayList();
                });

        if(httpCacheUtil.isExistCache(RequestParam.GetBill.InterfaceName)){
            bills = JsonUtil.arrayFormJson(httpCacheUtil.readHttp(RequestParam.GetBill.InterfaceName),Bill[].class);
            billText.setText(bills.size() + "");
        }

        //获得账单
        ApiService.getPileService().GetBill(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.GetBill.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(bills1 -> {
                    if (bills1.size() == 0) {
                        return bills1;
                    }
                    if (TextUtils.equals(BeanParam.Failure, bills1.get(0).getCrm_balancelog_get().getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(bills1.get(0).getCrm_balancelog_get().getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return bills1;
                })
                .subscribe(bills1 -> {
                    billText.setText(bills1.size() + "");
                    this.bills = bills1;
                    //缓存
                    httpCacheUtil.cacheHttp(JsonUtil.Object2Json(bills1),RequestParam.GetBill.InterfaceName);
                }, throwable -> {
                    this.bills = new ArrayList();
                });

    }

    @OnClick({R.id.balanceLayout, R.id.chargeCardLayout, R.id.billLayout,R.id.invoiceLayout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.balanceLayout: //余额
                if (balance == null) {
                    break;
                }
                BalanceActivity.startActivity(this, (ArrayList) chargeCardItems);
                break;
            case R.id.chargeCardLayout: //充电卡
                if (chargeCardItems == null) {
                    break;
                }
                PileCardActivity.startActivity(this, (ArrayList) chargeCardItems);
                break;
            case R.id.billLayout: //账单
                if (bills == null) {
                    break;
                }
                BillActivity.startActivity(this, (ArrayList) bills);
                break;
            case R.id.invoiceLayout:
                SelectInvoiceActivity.startActivity(context);
                break;
        }
    }

    /**
     * 打开界面
     * @param context
     */
    public static void staticActivity(Context context) {
        Intent intent = new Intent(context, MyWalletActivity.class);
        context.startActivity(intent);
    }


    @Override
    public String getTAG() {
        return TAG;
    }

}

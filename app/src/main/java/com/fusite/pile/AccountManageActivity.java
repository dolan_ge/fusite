package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.api.ChinaService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.UpdateUserInfo;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.BitmapUtil;
import com.fusite.utils.CircleTransform;
import com.squareup.picasso.Picasso;
import com.weidongjian.meitu.wheelviewdemo.view.LoopView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bingoogolapple.badgeview.BGABadgeImageView;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 账户管理
 * Created by Jelly on 2016/3/7.
 */
public class AccountManageActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "AccountManageActivity";
    @BindView(R.id.nickname_text)
    TextView nicknameText;
    @BindView(R.id.level_text)
    TextView levelText;
    @BindView(R.id.sex_text)
    TextView sexText;
    @BindView(R.id.phone_text)
    TextView phoneText;
    @BindView(R.id.certification_text)
    TextView certificationText;
    BGABadgeImageView avatarIcon;
    @BindView(R.id.car_text)
    TextView carText;

    public static final int NICKNAMEREQUEST = 0x003; //修改昵称
    public static final int ModCarQuest = 0x004;

    private PopupWindow sexPw; //性别选择框
    private String currSex; //当前选择的性别

    public static final String TempFileName = "temp_photo.png"; //临时文件名称
    public static final String TempFileDir = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static final String TempFilePath = TempFileDir + "/" + TempFileName; //临时文件路径

    private String userIconPath;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.account_manage);
        ButterKnife.bind(this);
        avatarIcon = findViewById(R.id.avatar_icon);
        init();
        loadObjectAttribute();
        setActivityView();
        setListener();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void setActivityView() {
        Login loginDao = application.getLogin();
        if (!TextUtils.isEmpty(loginDao.getData().getUsericon())) {
            Picasso.with(context).load(loginDao.getData().getUsericon()).transform(new CircleTransform()).placeholder(R.drawable.slide_portrait).error(R.drawable.slide_portrait).into(avatarIcon);
        }
        Bitmap avatarBadgeBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_vip);
        avatarIcon.showDrawableBadge(avatarBadgeBitmap);
        if (!TextUtils.isEmpty(loginDao.getData().getCustName())) {
            nicknameText.setText(loginDao.getData().getCustName());
        }
        sexText.setText(loginDao.getData().getSex());
        phoneText.setText(loginDao.getData().getMobile());
        if (TextUtils.equals(loginDao.getData().getVerified(), "1")) {
            certificationText.setText("是");
        } else {
            certificationText.setText("否");
        }

        if(!TextUtils.isEmpty(loginDao.getData().getBrand()) && !TextUtils.isEmpty(loginDao.getData().getSeries())){
            carText.setText(loginDao.getData().getBrand() + loginDao.getData().getSeries());
        }

    }


    public void showAvatar(View view) {
        FunctionConfig functionConfig = new FunctionConfig.Builder()
                .setEnableCamera(true)
                .setEnableEdit(true)
                .setEnableCrop(true)
                .setCropSquare(true)
                .build();
        GalleryFinal.openGallerySingle(0x001, functionConfig, new MyGalleryFinalCallBack(this));
    }

    /**
     * 显示性别PopupWindow
     *
     * @param view
     */
    public void showSexPw(View view) {
        if (sexPw != null && !sexPw.isShowing()) {
            sexPw.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
            return;
        }
        View root = LayoutInflater.from(context).inflate(R.layout.sex_pw_layout, null);
        LoopView loopView = (LoopView) root.findViewById(R.id.loopView);
        ImageView bg = (ImageView) root.findViewById(R.id.bg);
        TextView cancel = (TextView) root.findViewById(R.id.cancel);
        TextView ok = (TextView) root.findViewById(R.id.ok);
        final List<String> items = new ArrayList<>();
        items.add("男");
        items.add("女");
        loopView.setItems(items);
        loopView.setNotLoop();
        loopView.setTextSize(20);
        loopView.setViewPadding(100, 0, 100, 0);
        Login.DataEntity entity = application.getLogin().getData();
        if (TextUtils.equals("男", entity.getSex())) {
            loopView.setInitPosition(0);
        } else {
            loopView.setInitPosition(1);
        }
        loopView.setListener(index -> currSex = items.get(index));
        bg.setOnClickListener(v -> {
            sexPw.dismiss();
        });
        cancel.setOnClickListener(v -> {
            sexPw.dismiss();
        });
        ok.setOnClickListener(v -> {
            if (TextUtils.isEmpty(currSex) || TextUtils.equals(currSex, application.getLogin().getData().getSex())) {
                sexPw.dismiss();
                return;
            }
            Map<String, String> map = new HashMap();
            map.put(RequestParam.UpdateUserInfo.Token, entity.getToken());
            map.put(RequestParam.UpdateUserInfo.Sex, NetParam.ChinaToUnicode(currSex));
            Map<String, String> mapParam = new HashMap();
            mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), entity.getCustID(), NetParam.sign_method, NetParam.sign, RequestParam.UpdateUserInfo.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));
            ChinaService.UpdateUserInfo(mapParam)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(updateUserInfo -> {
                        if (updateUserInfo == null) {
                            return;
                        }
                        application.getLogin().getData().setSex(currSex);
                        application.cacheLogin();
                        sexText.setText(currSex);
                        if (sexPw != null && sexPw.isShowing()) {
                            sexPw.dismiss();
                        }
                    }, throwable -> {

                    });
        });
        sexPw = popupWindowUtil.getPopupWindow(context, root, windowUtil.getScreenWidth(this), windowUtil.getScreenHeight(this));
        sexPw.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0);
    }

    /**
     * 更新用户状态
     *
     * @return
     */
    public Observable<UpdateUserInfo> updateUserInfo(Map<String, String> map, String custId) {
        return ApiService.getPileService().UpdateUserInfo(NetParam.trancode, NetParam.mode, NetParam.getTime(), custId, NetParam.sign_method, RequestParam.UpdateUserInfo.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(updateUserInfo -> {
                    if (TextUtils.equals(BeanParam.Failure, updateUserInfo.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(updateUserInfo.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return updateUserInfo;
                });
    }


    @OnClick({R.id.avatar_layout, R.id.nickname_layout, R.id.sex_layout, R.id.loginout_btn, R.id.loginpwd_layout, R.id.pay_layout, R.id.certification_layout, R.id.car_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.avatar_layout:
                showAvatar(view);
                break;
            case R.id.nickname_layout:
                ModCustNameActivity.startActivity(context, NICKNAMEREQUEST);
                break;
            case R.id.sex_layout:
                showSexPw(view);
                break;
            case R.id.loginout_btn:
                dialogUtil.showSureListenerDialog(this, "是否要退出账户！", (dialog, index) -> {
                    dialog.dismiss();
                    Login.DataEntity entity = application.getLogin().getData();
                    Map<String, String> map = new HashMap();
                    map.put(RequestParam.Logout.Token, entity.getToken());
                    ApiService.getPileService().Logout(NetParam.trancode, NetParam.mode, NetParam.getTime(), entity.getCustID(), NetParam.sign_method, RequestParam.Logout.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map(logouts -> {
                                if (logouts == null) {
                                    return null;
                                }
                                return logouts.get(0);
                            })
                            .map(logout -> {
                                if (logout == null) {
                                    return null;
                                }
                                if (TextUtils.equals(BeanParam.Failure, logout.getCrm_logout().getIsSuccess())) {
                                    Error error = errorParamUtil.checkReturnState(logout.getCrm_logout().getReturnStatus());
                                    toastUtil.toastError(context, error, null);
                                    return null;
                                }
                                return logout;
                            })
                            .subscribe(logout -> {
                                application.loginOut();
                                finish();
                            }, throwable -> {
                            });
                });
                break;
            case R.id.loginpwd_layout:
                ModLoginPwdActivity.startActivity(context);
                break;
            case R.id.pay_layout:
                ModPayPasswordActivity.startActivity(context);
                break;
            case R.id.certification_layout:
                VerifiedActivity.startActivity(context);
                break;
            case R.id.car_layout:
                SelectCarTypeActivity.startActivity(this,ModCarQuest);
                break;
        }
    }


    private static class MyGalleryFinalCallBack implements GalleryFinal.OnHanlderResultCallback {
        public WeakReference<AccountManageActivity> mActivity;

        public MyGalleryFinalCallBack(AccountManageActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
            AccountManageActivity activity = mActivity.get();
            if (activity == null) {
                return;
            }
            Observable.just(resultList.get(0).getPhotoPath())
                    .observeOn(Schedulers.io())
                    .map(s -> { //压缩图片
                        BitmapUtil bitmapUtil = BitmapUtil.getInstance();
                        return bitmapUtil.Bitmap2Bytes(bitmapUtil.getimage(s, 50));
                    })
                    .flatMap(bytes -> {
                        return ApiService.getPileService().upload(new MultipartBody.Builder().setType(MultipartBody.FORM)
                                .addFormDataPart("avatar", "avatar.png", RequestBody.create(MediaType.parse("image/png/jpg; charset=utf-8"), bytes)).build());
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(uploadImage -> { //设置头像
                        Picasso.with(activity.context).load(uploadImage.getData().get(0).getUrl()).placeholder(R.drawable.round_viewplace).error(R.drawable.round_viewplace).transform(new CircleTransform()).into(activity.avatarIcon);
                        return uploadImage;
                    })
                    .observeOn(Schedulers.newThread())
                    .flatMap(uploadImage -> {
                        activity.userIconPath = uploadImage.getData().get(0).getUrl();
                        Login.DataEntity dataEntity = activity.application.getLogin().getData();
                        Map<String, String> map = new HashMap();
                        map.put(RequestParam.UpdateUserInfo.Token, dataEntity.getToken());
                        map.put(RequestParam.UpdateUserInfo.Usericon, uploadImage.getData().get(0).getUrl());
                        return activity.updateUserInfo(map, dataEntity.getCustID());
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(updateUserInfo -> { //删除临时文件
                        File file = new File(TempFilePath);
                        if (file.exists()) {
                            file.delete();
                        }
                        return updateUserInfo;
                    })
                    .subscribe(updateUserInfo -> {
                        activity.application.getLogin().getData().setUsericon(activity.userIconPath);
                        activity.application.cacheLogin();
                    }, throwable -> {
                    });
        }

        @Override
        public void onHanlderFailure(int requestCode, String errorMsg) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case NICKNAMEREQUEST:
                if (resultCode == ModCustNameActivity.SUCCESS) {
                    Login.DataEntity entity = application.getLogin().getData();
                    nicknameText.setText(entity.getCustName());
                } else if (resultCode == ModCustNameActivity.FAILURE) {

                }
                break;
            case ModCarQuest:
                if(resultCode == SelectCarTypeActivity.Success){
                    Login.DataEntity loginData = application.getLogin().getData();
                    carText.setText(loginData.getBrand() + loginData.getSeries());
                }
                break;
        }
    }


    /**
     * 启动AccountManageActivity
     *
     * @param context
     */
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, AccountManageActivity.class);
        context.startActivity(intent);
    }

}

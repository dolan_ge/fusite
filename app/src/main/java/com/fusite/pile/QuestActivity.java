package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fusite.activities.UtilActivity;

/**
 * Created by Jelly on 2016/6/22.
 */
public class QuestActivity extends UtilActivity{

    public String TAG = "QuestActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.quest_layout);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void setActivityView() {

    }

    public static void startActivity(Context context){
        Intent intent = new Intent(context,QuestActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

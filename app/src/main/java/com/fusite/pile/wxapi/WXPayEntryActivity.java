package com.fusite.pile.wxapi;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;

import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.constant.Keys;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.impl.BackCarDaoImpl;
import com.fusite.impl.PayDaoImpl;
import com.fusite.impl.ReChargeDaoImpl;
import com.fusite.impl.RentPayDaoImpl;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.MyApplication;
import com.fusite.pile.PayActivity;
import com.fusite.pile.RechargeActivity;
import com.fusite.utils.ActivityControl;
import com.fusite.utils.DoubleUtil;
import com.fusite.utils.ErrorParamUtil;
import com.fusite.utils.LogUtil;
import com.fusite.utils.ToastUtil;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.util.HashMap;
import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

	public String TAG = "WXPayEntryActivity";

	private IWXAPI api;

	private ReChargeDaoImpl reChargeDao;

	private final int msgReCharge = 0x001;

	private static final String RechargeActivityTag = "RechargeActivity";

	private static final String PayActivityTag = "PayActivity";

	private PayDaoImpl payDao;
	private final int msgPay = 0x002;

	private RentPayDaoImpl rentPayDao;
	private final int msgRentPay = 0x003;

	private BackCarDaoImpl backCarDao;
	private final int msgBackCar = 0x004;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		api = WXAPIFactory.createWXAPI(this, Keys.AppId);
		api.handleIntent(getIntent(), this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {
		LogUtil.v(TAG,req.getType()+"");
	}

	@Override
	public void onResp(BaseResp resp) {
		LogUtil.d(TAG, "onResp: errCode=" + resp.errCode + ", errStr=" + resp.errStr);
		if(resp.errCode == ConstantsAPI.COMMAND_UNKNOWN){ //支付成功
			if(RechargeActivity.IsWxPay){ //充值
				if(RechargeActivity.type == RechargeActivity.BalanceAmt){ //余额充值
					//重置缓存
					if(!TextUtils.isEmpty(RechargeActivity.price)){
						MyApplication application = (MyApplication) getApplication();
						application.getLogin().getData().setBalanceAmt(DoubleUtil.add(application.getLogin().getData().getBalanceAmt(),RechargeActivity.price));
						application.cacheLogin();
					}
					//设置返回消息
					finish();
					ActivityControl.finishResult(RechargeActivityTag,RechargeActivity.Success);
				}else{
					Intent intent = new Intent();
					finish();
					ActivityControl.finishResult(RechargeActivityTag,RechargeActivity.Success,intent);
				}
			}else if(PayActivity.IsWxPay){ //订单支付
				MyApplication application = (MyApplication) getApplication();
				Login.DataEntity loginData = application.getLogin().getData();
				payDao = new PayDaoImpl(this,handler,msgPay);
				rentPayDao = new RentPayDaoImpl(this,handler,msgRentPay);
				backCarDao = new BackCarDaoImpl(this,handler,msgBackCar);
				if (PayActivity.payType == PayActivity.AppointPay) {
					finish();
					Intent intent = getIntent();
					ActivityControl.finishResult(PayActivityTag,PayActivity.Success,intent);
//					payDao.getDao(loginData.getToken(), PayActivity.orderNo, PayActivity.OutTradNo, "0", PayDaoImpl.WECHATPAY, PayDaoImpl.APPOINTPAY, loginData.getCustID());
				}else if (PayActivity.payType == PayActivity.ChargePay) {
					finish();
					Intent intent = getIntent();
					intent.putExtra("OrderNo", PayActivity.orderNo);
					ActivityControl.finishResult(PayActivityTag,PayActivity.Success,intent);
//					payDao.getDao(loginData.getToken(), PayActivity.orderNo, PayActivity.OutTradNo, "0", PayDaoImpl.WECHATPAY, PayDaoImpl.CHARGEPAY, loginData.getCustID());
				}else if(PayActivity.payType == PayActivity.RentCarPay){
					rentPayDao.getRentPay(loginData.getToken(),loginData.getCustID(),PayActivity.orderNo,RentPayDaoImpl.WPay,"",PayActivity.OutTradNo);
				}else if(PayActivity.payType == PayActivity.RentBillingPay){
					backCarDao.getBackCar(loginData.getToken(),loginData.getCustID(),PayActivity.orderNo,PayActivity.OutTradNo,BackCarDaoImpl.WPay,"");
				}else if(PayActivity.payType == PayActivity.ServiceBookPay || PayActivity.payType == PayActivity.ServiceAmtPay){
					Map<String, String> map = new HashMap<>();
					map.put(RequestParam.ServicePay.Token, loginData.getToken());
					map.put(RequestParam.ServicePay.OrderNo, PayActivity.orderNo);
					map.put(RequestParam.ServicePay.Type, RequestParam.ServicePay.WxPay);
					map.put(RequestParam.ServicePay.PayAmt, PayActivity.price);
					map.put(RequestParam.ServicePay.PayOrderNo, PayActivity.OutTradNo);
					ApiService.getPileService().ServicePay(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.ServicePay.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
							.subscribeOn(Schedulers.newThread())
							.observeOn(AndroidSchedulers.mainThread())
							.map(servicePay -> {
								if (TextUtils.equals(BeanParam.Failure, servicePay.getIsSuccess())) {
									ErrorParamUtil errorParamUtil = ErrorParamUtil.getInstance();
									ToastUtil toastUtil = ToastUtil.getInstance();
									Error error = errorParamUtil.checkReturnState(servicePay.getReturnStatus());
									toastUtil.toastError(this, error, null);
									return null;
								}
								return servicePay;
							})
							.subscribe(servicePay -> {
								if (servicePay == null) {
									return;
								}
								Intent intent = getIntent();
								intent.putExtra("OrderNo", PayActivity.orderNo);
								ActivityControl.finishResult(PayActivityTag,PayActivity.Success,intent);
							}, new ThrowableAction(this));
				}
			}
		}else{
			Toast.makeText(WXPayEntryActivity.this,"支付失败",Toast.LENGTH_SHORT).show();
			finish();
		}
	}

	Handler handler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			ErrorParamUtil errorParamUtil = ErrorParamUtil.getInstance();
			ToastUtil toastUtil = ToastUtil.getInstance();
			Intent intent = getIntent();
			switch (msg.what){
				case msgReCharge:
					if(reChargeDao.reChargeDao == null || TextUtils.equals(reChargeDao.reChargeDao.getCrm_recharge().getIsSuccess(),"N")){
						Error error = errorParamUtil.checkReturnState(reChargeDao.reChargeDao.getCrm_recharge().getReturnStatus());
						toastUtil.toastError(WXPayEntryActivity.this,error,null);
						finish();
						ActivityControl.finishResult(RechargeActivityTag,RechargeActivity.Failure);
					}

					break;
				case msgPay:
					if (TextUtils.equals(payDao.dao.getIsSuccess(), "N")) {
						Error errorDao = errorParamUtil.checkReturnState(payDao.dao.getReturnStatus());
						toastUtil.toastError(WXPayEntryActivity.this, errorDao,null);
						finish();
						return;
					}
					finish();
					intent.putExtra("OrderNo", PayActivity.orderNo);
					ActivityControl.finishResult(PayActivityTag,PayActivity.Success,intent);
					break;
				case msgRentPay:
					if(TextUtils.equals(rentPayDao.rentPay.getIsSuccess(),"N")){
						Error error = errorParamUtil.checkReturnState(rentPayDao.rentPay.getReturnStatus());
						toastUtil.toastError(WXPayEntryActivity.this,error,null);
						finish();
						return;
					}
					finish();
					intent.putExtra("OrderNo", PayActivity.orderNo);
					ActivityControl.finishResult(PayActivityTag,PayActivity.Success,intent);
					break;
				case msgBackCar:
					if(TextUtils.equals("N",backCarDao.backCar.getIsSuccess())){
						Error error = errorParamUtil.checkReturnState(backCarDao.backCar.getReturnStatus());
						toastUtil.toastError(WXPayEntryActivity.this,error,null);
						return;
					}
					finish();
					intent.putExtra("OrderNo", PayActivity.orderNo);
					ActivityControl.finishResult(PayActivityTag,PayActivity.Success,intent);
					break;
			}
		}
	};

}
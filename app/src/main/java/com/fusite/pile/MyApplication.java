package com.fusite.pile;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.amap.api.maps.AMapOptions;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.fusite.bean.Login;
import com.fusite.impl.LoginDaoImpl;
import com.fusite.map.MyLocation;
import com.fusite.param.DefaultParam;
import com.fusite.utils.LogUtil;
import com.fusite.utils.SPCacheUtil;
import com.fusite.view.PicassoImageLoader;
import com.tencent.bugly.crashreport.CrashReport;

import cn.finalteam.galleryfinal.CoreConfig;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.ThemeConfig;

/**
 * MyApplication
 * Created by Jelly on 2016/3/1.
 */
public class MyApplication extends Application {
    /**
     * TAG
     */
    private static String TAG = "MyApplication";
    /**
     * SharePreference操作工具
     */
    private SPCacheUtil spCacheUtil = SPCacheUtil.getInstance();
    /**
     * 是否登录
     */
    private boolean isLogin;

    private Login login;
    private LoginDaoImpl loginDaoImpl;
    private MyLocation location;
    private CameraPosition cameraPosition;
    private int currTab = MainActivity.pileTab;
    private static MyApplication instance;
    public static synchronized MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initBugly();
        loadCacheLogin();
        loadCacheLocation();
        setMapOption();
        initGallery();
    }

    private void initBugly() {
        CrashReport.initCrashReport(getApplicationContext(), getString(R.string.buglyAppId), BuildConfig.DEBUG);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    /**
     * 初始化图库
     */
    public void initGallery(){
        //界面属性
        ThemeConfig theme = new ThemeConfig.Builder()
                .setTitleBarBgColor(getResources().getColor(R.color.actionbar_bg))
                .setFabNornalColor(getResources().getColor(R.color.yellowBg))
                .setFabPressedColor(getResources().getColor(R.color.yellowBgPressed))
                .setCropControlColor(getResources().getColor(R.color.yellowBg))
                .setCheckSelectedColor(getResources().getColor(R.color.yellowBg))
                .build();
        //配置功能
        FunctionConfig functionConfig = new FunctionConfig.Builder()
                .setEnableCamera(true)
                .build();
        //配置imageloader
        PicassoImageLoader imageloader = new PicassoImageLoader();

        CoreConfig coreConfig = new CoreConfig.Builder(getApplicationContext(),imageloader, theme)
                .setFunctionConfig(functionConfig).build();
        GalleryFinal.init(coreConfig);
    }

    public CameraPosition getCameraPosition() {
        return cameraPosition;
    }

    public void setCameraPosition(CameraPosition cameraPosition) {
        this.cameraPosition = cameraPosition;
    }

    public MyLocation getLocation() {
        return location;
    }

    public void setLocation(MyLocation location) {
        this.location = location;
    }

    public void setCurrTab(int currTab) {
        this.currTab = currTab;
    }

    public int getCurrTab() {
        return currTab;
    }

    /**
     * 返回在APP中用户的登录状态
     * @return
     */
    public boolean isLogin(){
        if(login != null){
            return  true;
        }else {
            return false;
        }
    }

    /**
     * 设置地图的初始显示位置
     */
    public void setMapOption(){
        AMapOptions aMapOptions = new AMapOptions();
        LatLng latLng = new LatLng(26.642979,106.645918);
        if(location != null) {
            latLng = new LatLng(Double.parseDouble(location.getLatitude()), Double.parseDouble(location.getLongitude()));
        }
        aMapOptions.camera(CameraPosition.fromLatLngZoom(latLng, DefaultParam.ZOOM));
        cameraPosition = aMapOptions.getCamera();
    }

    /**
     * 加载缓存登录,如果缓存中存在登录缓存，不需要重新登录
     */
    public void loadCacheLogin(){
        loginDaoImpl = new LoginDaoImpl(this);
        login = loginDaoImpl.getCacheObject();
        if(login != null){ //如果LoginDao不为空，说明在缓存中已经有登录缓存，不需要重新登录
            isLogin = true;
        }else{
            LogUtil.v(TAG,"登录缓存为空");
        }
    }

    /**
     * 加载缓存中的位置信息
     */
    public void loadCacheLocation(){
        location = spCacheUtil.getMyLocation(this);
        if(location != null){
            LogUtil.v(TAG,"成功获取缓存中的位置："+location.getAdCode());
        }else{
            LogUtil.v(TAG,"位置缓存为空");
        }
    }

    /**
     * 缓存对象
     */
    public void cacheLogin(){
        loginDaoImpl = new LoginDaoImpl(this);
        loginDaoImpl.cacheObject(login);
    }

    /**
     * 注销登录
     */
    public void loginOut(){
        loginDaoImpl = new LoginDaoImpl(this);
        loginDaoImpl.clearCache();
        login = null;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }


}

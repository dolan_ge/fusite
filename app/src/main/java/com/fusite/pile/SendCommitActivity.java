package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ChinaService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.view.CustomDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 发送评论界面
 * Created by Jelly on 2016/6/2.
 */
public class SendCommitActivity extends UtilActivity {

    /**
     * TAG
     */
    public String TAG = "SendCommitActivity";
    @BindView(R.id.input)
    EditText input;
    @BindView(R.id.score1)
    ImageView score1;
    @BindView(R.id.score2)
    ImageView score2;
    @BindView(R.id.score3)
    ImageView score3;
    @BindView(R.id.score4)
    ImageView score4;
    @BindView(R.id.score5)
    ImageView score5;

    private ImageView[] scores;
    private int score;
    public static final int MaxLength = 140;

    private String orgNo;
    private String orgClasses;

    public static final String PileCommit = "EVC";
    public static final String RentCommit = "EVR";
    public static final String ServiceCommit = "EVM";

    private CustomDialog loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.send_commit);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {
        scores = new ImageView[]{score1, score2, score3, score4, score5};
        orgNo = intent.getStringExtra("orgNo");
        orgClasses = intent.getStringExtra("orgClasses");
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {

    }

    @OnClick({R.id.send, R.id.score1, R.id.score2, R.id.score3, R.id.score4, R.id.score5})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send:
                String commit = input.getText().toString();
                if (score == 0) {
                    Toast.makeText(context, "请选择评分", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(commit)) {
                    Toast.makeText(context, "请输入评论", Toast.LENGTH_SHORT).show();
                    return;
                } else if (commit.length() > MaxLength) {
                    Toast.makeText(context, "评论过长", Toast.LENGTH_SHORT).show();
                    return;
                }
                loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
                //发送评论
                Login.DataEntity dataEntity = application.getLogin().getData();
                Map<String, String> map = new HashMap();
                map.put(RequestParam.UpdateUserInfo.Token, dataEntity.getToken());
                map.put(RequestParam.SendCommit.OrgClasses, orgClasses);
                map.put(RequestParam.SendCommit.OrderNo, orgNo);
                map.put(RequestParam.SendCommit.Comment, commit);
                map.put(RequestParam.SendCommit.Score, score + "");
                Map<String, String> mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, NetParam.sign, RequestParam.SendCommit.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));
                ChinaService.SendCommit(mapParam)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(sendCommit -> {
                            dialogUtil.dismiss(loading);
                            return sendCommit;
                        })
                        .map(sendCommit -> {
                            if(TextUtils.equals(BeanParam.Failure,sendCommit.getIsSuccess())){
                                Error error = errorParamUtil.checkReturnState(sendCommit.getReturnStatus());
                                toastUtil.toastError(context,error,null);
                                return null;
                            }
                            return sendCommit;
                        })
                        .subscribe(sendCommit -> {
                            if(sendCommit == null){
                                return;
                            }
                            Toast.makeText(context,"评论发送成功",Toast.LENGTH_SHORT).show();
                            finish();
                        });
                break;
            case R.id.score1:
                clickScore(0);
                break;
            case R.id.score2:
                clickScore(1);
                break;
            case R.id.score3:
                clickScore(2);
                break;
            case R.id.score4:
                clickScore(3);
                break;
            case R.id.score5:
                clickScore(4);
                break;
        }
    }

    /**
     * 点亮评分
     *
     * @param index
     */
    public void clickScore(int index) {
        if (index >= scores.length) {
            return;
        }
        for (int i = 0; i <= index; i++) {
            scores[i].setImageResource(R.drawable.fillstar);
        }
        for (int i = index + 1; i < scores.length; i++) {
            scores[i].setImageResource(R.drawable.nostar);
        }

        score = (index+1) * 2;
    }

    public static void startActivity(Context context, String orgClasses, String orgNo) {
        Intent intent = new Intent(context, SendCommitActivity.class);
        intent.putExtra("orgClasses", orgClasses);
        intent.putExtra("orgNo", orgNo);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

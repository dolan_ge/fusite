package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jelly on 2016/6/16.
 */
public class HintTextActivity extends UtilActivity {
    public String TAG = "HintTextActivity";
    @BindView(R.id.hint)
    TextView hint;

    private String hintStr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.hint_text);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }


    @Override
    public void loadObjectAttribute() {
        hintStr = intent.getStringExtra("hint");
    }

    @Override
    public void setActivityView() {
        hint.setText(hintStr);
    }

    public static void startActivity(Context context, String hint) {
        Intent intent = new Intent(context, HintTextActivity.class);
        intent.putExtra("hint", hint);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

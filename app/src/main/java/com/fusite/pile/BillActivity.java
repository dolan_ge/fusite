package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ListView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.BillListAdapter;
import com.fusite.bean.Bill;
import com.fusite.bean.DateSort;
import com.fusite.utils.DateUtil;
import com.fusite.utils.ListUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 消费账单
 * Created by Jelly on 2016/3/23.
 */
public class BillActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "BillActivity";

    @BindView(R.id.list)
    ListView list;

    private BillListAdapter adapter;
    private List<Bill> showList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.bill);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void loadObjectAttribute() {
        application = (MyApplication) this.getApplication();
        Observable.create(
                (Observable.OnSubscribe<List<Bill>>) subscriber -> {
                    showList = getIntent().getParcelableArrayListExtra("daos");
                    subscriber.onStart();
                    subscriber.onNext(showList);
                    subscriber.onCompleted();
                })
                .observeOn(Schedulers.io())
                .map(bills -> {
                    List<DateSort> dateSorts = new ArrayList<>();
                    for (Bill bill : bills) {
                        bill.setDate(DateUtil.getDate(bill.getCrm_balancelog_get().getCreateDate()));
                        dateSorts.add(bill);
                    }
                    ListUtil.sortTime(dateSorts);
                    bills.clear();
                    for (DateSort dateSort : dateSorts) {
                        bills.add((Bill) dateSort);
                    }
                    return bills;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bills -> {
                    adapter = new BillListAdapter(context, showList);
                    list.setAdapter(adapter);
                });
    }

    @Override
    public void setActivityView() {

    }

    /**
     * 开启界面
     */
    public static void startActivity(Context context, ArrayList<Bill> daos) {
        Intent intent = new Intent(context, BillActivity.class);
        intent.putParcelableArrayListExtra("daos", daos);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Dynamic;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.OneDynamic;
import com.fusite.handlerinterface.DialogFreshListener;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.CircleTransform;
import com.fusite.utils.DateUtil;
import com.fusite.utils.ErrorParamUtil;
import com.fusite.utils.LogUtil;
import com.fusite.utils.ToastUtil;
import com.fusite.view.SendDynamicDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bingoogolapple.refreshlayout.BGANormalRefreshViewHolder;
import cn.bingoogolapple.refreshlayout.BGARefreshLayout;
import cn.bingoogolapple.refreshlayout.BGARefreshViewHolder;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/14.
 */
public class DynamicActivity extends UtilActivity implements BGARefreshLayout.BGARefreshLayoutDelegate, DialogFreshListener {
    /**
     * TAG
     */
    public String TAG = "DynamicActivity";
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.content)
    TextView content;
    @BindView(R.id.image1)
    ImageView image1;
    @BindView(R.id.image2)
    ImageView image2;
    @BindView(R.id.image3)
    ImageView image3;
    @BindView(R.id.images1)
    LinearLayout images1;
    @BindView(R.id.image4)
    ImageView image4;
    @BindView(R.id.image5)
    ImageView image5;
    @BindView(R.id.image6)
    ImageView image6;
    @BindView(R.id.images2)
    LinearLayout images2;
    @BindView(R.id.image7)
    ImageView image7;
    @BindView(R.id.image8)
    ImageView image8;
    @BindView(R.id.image9)
    ImageView image9;
    @BindView(R.id.images3)
    LinearLayout images3;
    @BindView(R.id.like)
    ImageView like;
    @BindView(R.id.likeNum)
    TextView likeNum;
    @BindView(R.id.sendCommit)
    ImageView sendCommit;
    @BindView(R.id.commentNum)
    TextView commentNum;
    @BindView(R.id.commit)
    LinearLayout commit;
    @BindView(R.id.icon)
    ImageView icon;
    @BindView(R.id.freshLayout)
    BGARefreshLayout freshLayout;

    private View commit_item1;
    private View commit_item2;
    private ImageView[] images;
    private OneDynamic.DataEntity data;
    private List<OneDynamic.DataEntity.ReplyContentsEntity> commitList;
    private SendDynamicDialog dynamicDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.dynamic);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {
        images = new ImageView[]{image1, image2, image3, image4, image5, image6, image7, image8, image9};
        freshLayout.setDelegate(this);
        BGARefreshViewHolder refreshViewHolder = new BGANormalRefreshViewHolder(this, false);
        freshLayout.setRefreshViewHolder(refreshViewHolder);
    }

    @Override
    public void loadObjectAttribute() {
        data = changeData(intent.getParcelableExtra("data"));
    }

    @Override
    public void setActivityView() {
        if (data != null) {
            name.setText(data.getCustName());
            content.setText(data.getContent());

            String timeStr = DateUtil.getTimeDistance(data.getCreateDate());
            if(TextUtils.equals(DateUtil.now,timeStr)){
                time.setText(DateUtil.now);
            }else{
                time.setText(timeStr+"前");
            }

            Picasso.with(context).load(data.getUsericon()).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).transform(new CircleTransform()).into(icon);

            List<OneDynamic.DataEntity.PicturesEntity> imageUrls = data.getPictures();
            for (int i = 0; i < imageUrls.size(); i++) {
                images[i].setVisibility(View.VISIBLE);
                images[i].setTag(imageUrls.get(i).getUrl());
                Picasso.with(context).load(imageUrls.get(i).getUrl()).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).into(images[i]);
                if (!images[i].isClickable()) {
                    images[i].setOnClickListener(v -> ImageActivity.startActivity(context, v.getTag() + ""));
                }
            }
            for (int i = imageUrls.size(); i < images.length; i++) {
                images[i].setVisibility(View.INVISIBLE);
            }
            images1.setVisibility(View.VISIBLE);
            images2.setVisibility(View.VISIBLE);
            images3.setVisibility(View.VISIBLE);
            if (imageUrls.size() <= 6) {
                images3.setVisibility(View.GONE);
            }
            if (imageUrls.size() <= 3) {
                images2.setVisibility(View.GONE);
            }
            if (imageUrls.size() == 0) {
                images1.setVisibility(View.GONE);
            }
            commentNum.setText(data.getReplies());

            Login.DataEntity dataEntity = application.getLogin().getData();
            commit.removeAllViews();
            commitList = data.getReplyContents() == null ? new ArrayList<>() : data.getReplyContents();
            for (int i = 0; i < commitList.size(); i++) {
                OneDynamic.DataEntity.ReplyContentsEntity reply = commitList.get(i);
                if (TextUtils.equals(BeanParam.Dynamic.commit, reply.getType())) { //评论
                    commit_item1 = LayoutInflater.from(context).inflate(R.layout.commit_item1, commit, false);
                    TextView name = (TextView) commit_item1.findViewById(R.id.name);
                    TextView content = (TextView) commit_item1.findViewById(R.id.content);
                    name.setText(reply.getFrom_Name());
                    content.setText(reply.getContent());

                    if(!TextUtils.equals(dataEntity.getCustID(),reply.getFrom_Id())){
                        commit_item1.setOnClickListener(v -> showSendCommitDialog(reply.getFrom_Id(),reply.getFrom_Name()));
                    }

                    commit.addView(commit_item1);
                } else { //回复
                    commit_item2 = LayoutInflater.from(context).inflate(R.layout.commit_item2, commit, false);
                    TextView name1 = (TextView) commit_item2.findViewById(R.id.name1);
                    TextView name2 = (TextView) commit_item2.findViewById(R.id.name2);
                    TextView content = (TextView) commit_item2.findViewById(R.id.content);
                    name1.setText(reply.getFrom_Name());
                    name2.setText(reply.getTo_Name());
                    content.setText(reply.getContent());

                    if(!TextUtils.equals(dataEntity.getCustID(),reply.getFrom_Id())){
                        commit_item2.setOnClickListener(v -> showSendCommitDialog(reply.getFrom_Id(),reply.getFrom_Name()));
                    }


                    commit.addView(commit_item2);
                }
            }

            setLike();
        }
    }

    @OnClick(R.id.sendCommit)
    public void onClick() {
        showSendCommitDialog("","");
    }

    public void showSendCommitDialog(String toId,String toName) {
        if (dynamicDialog == null) {
            dynamicDialog = new SendDynamicDialog(context, this);
        }
        dynamicDialog.show(data.getForumNo(), toId,toName);
    }

    @Override
    public void refresh() {
        freshLayout.beginRefreshing();
    }

    /**
     * 设置点赞视图
     */
    public void setLike() {
        likeNum.setText(data.getLikes());
        if (TextUtils.equals(BeanParam.Dynamic.onlike, data.getLiked())) {
            like.setImageResource(R.drawable.onlike);
        } else {
            like.setImageResource(R.drawable.like);
        }
        like.setOnClickListener(v -> {
            ImageView iv = (ImageView) v;
            iv.setClickable(false);
            Login.DataEntity dataEntity1 = application.getLogin().getData();
            Map<String, String> map = new HashMap<String, String>();
            map.put(RequestParam.Like.Token, dataEntity1.getToken());
            if (TextUtils.equals(BeanParam.Dynamic.onlike, data.getLiked())) {
                map.put(RequestParam.Like.Type, RequestParam.Like.Cancel);
            } else {
                map.put(RequestParam.Like.Type, RequestParam.Like.Like);
            }
            map.put(RequestParam.Like.ForumNo, data.getForumNo());
            ApiService.getPileService().Like(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity1.getCustID(), NetParam.sign_method, RequestParam.Like.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(like1 -> {
                        if (TextUtils.equals(BeanParam.Failure, like1.getIsSuccess())) {
                            Error error = ErrorParamUtil.getInstance().checkReturnState(like1.getReturnStatus());
                            ToastUtil.getInstance().toastError(context, error, null);
                            return null;
                        }
                        return like1;
                    })
                    .subscribe(like1 -> {
                        iv.setClickable(true);
                        if (like1 == null) {
                            return;
                        }
                        if (TextUtils.equals(BeanParam.Dynamic.onlike, data.getLiked())) {
                            data.setLiked(BeanParam.Dynamic.like);
                            data.setLikes(changeNum(data.getLikes(), false));
                        } else {
                            data.setLiked(BeanParam.Dynamic.onlike);
                            data.setLikes(changeNum(data.getLikes(), true));
                        }
                        setLike();
                    });
        });
    }

    public String changeNum(String numStr, boolean isAdd) {
        LogUtil.v("num", numStr);
        int num = Integer.parseInt(numStr);
        if (isAdd) {
            num++;
        } else {
            num--;
        }
        return num + "";
    }

    public Observable<OneDynamic> loadData() {
        Login.DataEntity dataEntity = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.OneDynamic.Token, dataEntity.getToken());
        map.put(RequestParam.OneDynamic.ForumNo, data.getForumNo());
        return ApiService.getPileService().OneDynamic(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.OneDynamic.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(oneDynamic -> {
                    if (TextUtils.equals(BeanParam.Failure, oneDynamic.getIsSuccess())) {
                        return null;
                    }
                    return oneDynamic;
                });
    }

    /**
     * 改变数据对象
     */
    public OneDynamic.DataEntity changeData(Dynamic.DataEntity dataEntity) {
        OneDynamic.DataEntity temp = new OneDynamic.DataEntity();
        temp.setForumNo(dataEntity.getForumNo());
        temp.setLiked(dataEntity.getLiked());
        temp.setContent(dataEntity.getContent());
        temp.setCreateDate(dataEntity.getCreateDate());
        temp.setCustId(dataEntity.getCustId());
        temp.setCustName(dataEntity.getCustName());
        temp.setLikes(dataEntity.getLikes());
        temp.setReplies(dataEntity.getReplies());
        temp.setUsericon(dataEntity.getUsericon());

        List<Dynamic.DataEntity.PicturesEntity> pictures = dataEntity.getPictures();
        List<OneDynamic.DataEntity.PicturesEntity> tempPictures = new ArrayList<>();
        OneDynamic.DataEntity.PicturesEntity picture = null;
        for (int i = 0; i < pictures.size(); i++) {
            picture = new OneDynamic.DataEntity.PicturesEntity();
            picture.setUrl(pictures.get(i).getUrl());
            tempPictures.add(picture);
        }
        temp.setPictures(tempPictures);

        List<Dynamic.DataEntity.ReplyContentsEntity> replys = dataEntity.getReplyContents();
        List<OneDynamic.DataEntity.ReplyContentsEntity> tempReplys = new ArrayList<>();
        OneDynamic.DataEntity.ReplyContentsEntity reply = null;
        for (int i = 0; i < replys.size(); i++) {
            reply = new OneDynamic.DataEntity.ReplyContentsEntity();
            Dynamic.DataEntity.ReplyContentsEntity tempReply = replys.get(i);
            reply.setCreateDate(tempReply.getCreateDate());
            reply.setContent(tempReply.getContent());
            reply.setFrom_Id(tempReply.getFrom_Id());
            reply.setFrom_Name(tempReply.getFrom_Name());
            reply.setItem(tempReply.getItem());
            reply.setTo_Id(tempReply.getTo_Id());
            reply.setTo_Name(tempReply.getTo_Name());
            reply.setType(tempReply.getType());
            tempReplys.add(reply);
        }
        temp.setReplyContents(tempReplys);

        return temp;
    }

    @Override
    public void onBGARefreshLayoutBeginRefreshing(BGARefreshLayout refreshLayout) {
        loadData().subscribe(oneDynamic -> {
            refreshLayout.endRefreshing();
            if (oneDynamic == null) {
                return;
            }
            data = oneDynamic.getData();
            setActivityView();
        }, throwable -> {
            refreshLayout.endRefreshing();
        });
    }

    @Override
    public boolean onBGARefreshLayoutBeginLoadingMore(BGARefreshLayout refreshLayout) {
        return false;
    }

    public static void startActivity(Context context, Dynamic.DataEntity data) {
        Intent intent = new Intent(context, DynamicActivity.class);
        intent.putExtra("data", data);
        context.startActivity(intent);
    }


    @Override
    public String getTAG() {
        return TAG;
    }


}

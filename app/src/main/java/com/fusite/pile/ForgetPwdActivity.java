package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.DefaultParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.CountDownUtil;
import com.fusite.utils.LogUtil;
import com.fusite.utils.RegExpUtil;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/3/1.
 */
public class ForgetPwdActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "ForgetPwdActivity";

    @BindView(R.id.forget_pwd_code_btn)
    Button forgetPwdCodeBtn;
    @BindView(R.id.forget_pwd_phone_et)
    EditText forgetPwdPhoneEt;
    @BindView(R.id.forget_pwd_code_clear)
    ImageView forgetPwdCodeClear;
    @BindView(R.id.forget_pwd_code_et)
    EditText forgetPwdCodeEt;


    private int msgCountDown = 0x002;

    private String phone;

    private String code;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.forget_pwd);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setListener() {
        setEtClearListener();
    }

    @Override
    public void setActivityView() {

    }

    /**
     * 设置输入框的输入监听事件，当里面有内容是，出现清空按钮
     */
    public void setEtClearListener() {
        forgetPwdCodeEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int length = forgetPwdCodeEt.getText().length();
                if (length > 0) {
                    if(!forgetPwdCodeClear.isShown()){
                        forgetPwdCodeClear.setVisibility(View.VISIBLE);
                    }
                    if(!forgetPwdCodeClear.isClickable()){
                        forgetPwdCodeClear.setOnClickListener(v -> forgetPwdCodeEt.setText(""));
                    }
                } else {
                    forgetPwdCodeClear.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * 获得重置密码的验证码
     *
     * @param view
     * @return
     */
    public View getForgetCode(View view) {
        phone = forgetPwdPhoneEt.getText().toString();
        if (TextUtils.isEmpty(phone) || !RegExpUtil.regPhone(phone)) {
            Log.v(TAG, "手机号码输入有误");
            return view;
        }
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.Code.Mobile, phone);
        map.put(RequestParam.Code.CodeType, RequestParam.Code.FORGETCODE);
        ApiService.getPileService().getCode(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.Code.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(codes -> {
                    forgetPwdCodeBtn.setEnabled(false);
                    CountDownUtil countDownUtil = new CountDownUtil(new MyHandler(context), msgCountDown, DefaultParam.COUNTDOWN);
                    countDownUtil.startCounrtDown();
                    return codes;
                })
                .map(codes -> {
                    if (TextUtils.equals(BeanParam.Failure, codes.get(0).getCrm_validation().getIsSuccess())) {
                        Toast.makeText(context, "验证码获取失败，请60秒重新获取", Toast.LENGTH_SHORT).show();
                        return null;
                    }
                    return codes;
                })
                .subscribe(codes -> {
                    if (codes != null) {
                        Toast.makeText(context, "验证码发送成功", Toast.LENGTH_SHORT).show();
                    }
                }, throwable -> {
                    LogUtil.v(TAG,throwable.getMessage());
                });
        return view;
    }

    /**
     * 进入输入密码界面
     *
     * @param view
     */
    public View nextTip(View view) {
        phone = forgetPwdPhoneEt.getText().toString();
        code = forgetPwdCodeEt.getText().toString();

        if (TextUtils.isEmpty(phone) || !RegExpUtil.regPhone(phone)) {
            Log.v(TAG, "手机号码输入错误");
            return view;
        }
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.CheckCode.Mobile, phone);
        map.put(RequestParam.CheckCode.Code, code);
        map.put(RequestParam.CheckCode.Type, RequestParam.CheckCode.Login);
        ApiService.getPileService().CheckCode(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.CheckCode.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(checkCode -> {
                    if (TextUtils.equals(BeanParam.Failure, checkCode.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(checkCode.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return checkCode;
                })
                .subscribe(checkCode -> {
                    if (checkCode == null) {
                        return;
                    }
                    SetLoginPwdActivity.startActivity(context, phone, code);
                }, new ThrowableAction(this));
        return view;
    }


    private static class MyHandler extends Handler {
        private WeakReference<ForgetPwdActivity> mActivity;

        private MyHandler(Context context) {
            mActivity = new WeakReference(context);
        }

        @Override
        public void handleMessage(Message msg) {
            ForgetPwdActivity activity = mActivity.get();
            if(activity == null){
                return;
            }
            if (msg.what == activity.msgCountDown) {
                int count = (int) msg.obj;
                activity.forgetPwdCodeBtn.setText("等待" + count + "秒");
                if (count == 0) {
                    activity.forgetPwdCodeBtn.setEnabled(true);
                    activity.forgetPwdCodeBtn.setText("获取验证码");
                }
            }
        }
    }

    /**
     * 打开ForgetPwdActivity
     *
     * @param context
     */
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ForgetPwdActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}
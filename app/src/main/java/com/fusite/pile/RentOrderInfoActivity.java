package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.RentOrderInfo;
import com.fusite.impl.RentOrderCancelDaoImpl;
import com.fusite.impl.RentOrderInfoDaoImpl;
import com.fusite.utils.DateUtil;
import com.fusite.utils.DoubleUtil;
import com.fusite.view.CustomSureDialog;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 正在租车
 * Created by Jelly on 2016/4/21.
 */
public class RentOrderInfoActivity extends UtilActivity {

    public String TAG = "RentOrderInfoActivity";
    @BindView(R.id.carIcon)
    ImageView carIcon;
    @BindView(R.id.carName)
    TextView carName;
    @BindView(R.id.license)
    TextView license;
    @BindView(R.id.pickCarStation)
    TextView pickCarStation;
    @BindView(R.id.pickCarTime)
    TextView pickCarTime;
    @BindView(R.id.returnCarStation)
    TextView returnCarStation;
    @BindView(R.id.returnCarTime)
    TextView returnCarTime;
    @BindView(R.id.preRentAmt)
    TextView preRentAmt;
    @BindView(R.id.insurenceAmt)
    TextView insurenceAmt;
    @BindView(R.id.serviceAmt)
    TextView serviceAmt;
    @BindView(R.id.deposit)
    TextView deposit;
    @BindView(R.id.sum)
    TextView sum;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.orderNoText)
    TextView orderNoText;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.ok)
    TextView ok;

    private String orderNo;

    private RentOrderInfoDaoImpl rentOrderDao;

    private static final int msgOrderInfo = 0x001;

    private static final String DateFormat = "MM/dd HH:mm";

    private String sumPrice;

    private RentOrderInfoDaoImpl isFinishDao;

    private static final int msgFinish = 0x003;

    public static final int BillingRequest = 0x004;

    private RentOrderCancelDaoImpl rentOrderCancelDao;

    private static final int msgOrderCancel = 0x004;

    public static final int Success = 0x005;

    public static final int Failure = 0x006;

    public String vehicleId;

    private CustomSureDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.rent_car_order_info);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {
        MyHandler handler = new MyHandler(this);
        orderNo = intent.getStringExtra("orderNo");
        rentOrderDao = new RentOrderInfoDaoImpl(this, handler, msgOrderInfo);
        isFinishDao = new RentOrderInfoDaoImpl(this,handler,msgFinish);
        rentOrderCancelDao = new RentOrderCancelDaoImpl(this,handler,msgOrderCancel);
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {
        Login.DataEntity dataEntity = application.getLogin().getData();
        rentOrderDao.getRentOrderInfo(dataEntity.getToken(), dataEntity.getCustID(), orderNo);
        orderNoText.setText(orderNo);
    }

    @OnClick({R.id.carInfo, R.id.ok,R.id.phoneInfo})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.carInfo:
                if(rentOrderDao == null || TextUtils.equals(rentOrderDao.rentOrderInfo.getData().getOrderStatus(),RentOrderInfoDaoImpl.NotPickCar)){
                    Toast.makeText(context,"请先取车",Toast.LENGTH_SHORT).show();
                    return;
                }
                CarOperActivity.startActivity(context,vehicleId);
                break;
            case R.id.ok:
                if(TextUtils.equals(RentOrderInfoDaoImpl.NotPickCar,rentOrderDao.rentOrderInfo.getData().getOrderStatus())){ //取消订单

                    dialog = new CustomSureDialog(context);
                    dialog.setHintText("取消订单后，您支付的金额将会在3~5和工作日内退还给您，是否取消？");
                    dialog.setCancelClickListener((dialog1, index) -> dialog1.dismiss());
                    dialog.setOkClickListener((dialog1, index) -> {
                        Login.DataEntity dataEntity = application.getLogin().getData();
                        rentOrderCancelDao.getRentOrderCancel(dataEntity.getToken(),dataEntity.getCustID(),orderNo);
                    });
                    dialog.show();
                    return;
                }else if(TextUtils.equals(RentOrderInfoDaoImpl.BackedCar,rentOrderDao.rentOrderInfo.getData().getOrderStatus())){ //结算
                    RentOrderInfo.DataEntity dataEntity = rentOrderDao.rentOrderInfo.getData();
                    BillingActivity.Data data = new BillingActivity.Data();
                    data.setOrderNo(dataEntity.getOrderNo());
                    data.setBrand(dataEntity.getBrand());
                    data.setLicense(dataEntity.getLicensePlate());
                    data.setTotalAmt(dataEntity.getTotalAmt());
                    data.setRealAmt(dataEntity.getRealAmt());
                    data.setClearingAmt(dataEntity.getClearingAmt());
                    data.setRentBeginTime(dataEntity.getRealBeginTime());
                    data.setRentEndTime(dataEntity.getRealEndTime());
                    BillingActivity.startActivity(context,BillingRequest,data);
                    return;
                }
                Login.DataEntity dataEntity = application.getLogin().getData();
                isFinishDao.getRentOrderInfo(dataEntity.getToken(),dataEntity.getCustID(),orderNo);
                break;
            case R.id.phoneInfo:
                Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + phone.getText().toString()));
                startActivity(intent);
                break;
        }
    }

    private static class MyHandler extends Handler{
        private WeakReference<RentOrderInfoActivity> mActivity;

        private MyHandler(RentOrderInfoActivity activity){
            mActivity = new WeakReference<RentOrderInfoActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            RentOrderInfoActivity activity = mActivity.get();
            if(activity == null){
                return;
            }
            switch (msg.what) {
                case msgOrderInfo:
                    if (TextUtils.equals("N", activity.rentOrderDao.rentOrderInfo.getIsSuccess())) {
                        Error error = activity.errorParamUtil.checkReturnState(activity.rentOrderDao.rentOrderInfo.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, error, null);
                        return;
                    }
                    RentOrderInfo.DataEntity dataEntity = activity.rentOrderDao.rentOrderInfo.getData();

                    activity.vehicleId = dataEntity.getVehicleId();
                    activity.carName.setText(dataEntity.getBrand());
                    activity.license.setText(dataEntity.getLicensePlate());
                    activity.pickCarStation.setText(dataEntity.getOrgName());
                    activity.returnCarStation.setText(dataEntity.getReturnOrgName());
                    activity.pickCarTime.setText(DateUtil.getSdfDate(dataEntity.getRentBeginTime(), DateFormat));
                    activity.returnCarTime.setText(DateUtil.getSdfDate(dataEntity.getRentEndTime(), DateFormat));
                    activity.preRentAmt.setText(dataEntity.getPreRentAmt());
                    activity.insurenceAmt.setText(dataEntity.getInsurenceAmt());
                    activity.serviceAmt.setText(dataEntity.getServiceAmt());
                    activity.deposit.setText(dataEntity.getDeposit());
                    activity.sumPrice = DoubleUtil.add(dataEntity.getPreRentAmt(), dataEntity.getInsurenceAmt(), dataEntity.getServiceAmt(), dataEntity.getDeposit());
                    activity.sum.setText(activity.sumPrice);
                    activity.phone.setText(dataEntity.getTel());

                    if(TextUtils.equals(dataEntity.getOrderStatus(),RentOrderInfoDaoImpl.NotPickCar)){
                        activity.title.setText("未取车");
                        activity.ok.setText("取消订单");
                    }else if(TextUtils.equals(dataEntity.getOrderStatus(),RentOrderInfoDaoImpl.Using)){
                        activity.title.setText("使用中");
                    }
                    if(!TextUtils.isEmpty(dataEntity.getPicture())){
                        Picasso.with(activity.context).load(dataEntity.getPicture()).into(activity.carIcon);
                    }
                    break;
                case msgFinish:
                    if(TextUtils.equals("N",activity.isFinishDao.rentOrderInfo.getIsSuccess())){
                        Error error = activity.errorParamUtil.checkReturnState(activity.isFinishDao.rentOrderInfo.getReturnStatus());
                        activity.toastUtil.toastError(activity.context,error,null);
                        return;
                    }

                    RentOrderInfo.DataEntity dataEntity1 = activity.isFinishDao.rentOrderInfo.getData();

                    if(!TextUtils.equals(RentOrderInfoDaoImpl.BackedCar,dataEntity1.getOrderStatus())){
                        Toast.makeText(activity.context,"请前往服务点结束租车服务后点击结算！",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    BillingActivity.Data data = new BillingActivity.Data();
                    data.setOrderNo(dataEntity1.getOrderNo());
                    data.setBrand(dataEntity1.getBrand());
                    data.setLicense(dataEntity1.getLicensePlate());
                    data.setTotalAmt(dataEntity1.getTotalAmt());
                    data.setRealAmt(dataEntity1.getRealAmt());
                    data.setClearingAmt(dataEntity1.getClearingAmt());
                    data.setRentBeginTime(dataEntity1.getRealBeginTime());
                    data.setRentEndTime(dataEntity1.getRealEndTime());
                    BillingActivity.startActivity(activity.context,BillingRequest,data);
                    break;
                case msgOrderCancel:
                    activity.dialog.dismiss();
                    if(TextUtils.equals("N",activity.rentOrderCancelDao.rentOrderCancel.getIsSuccess())){
                        Error error = activity.errorParamUtil.checkReturnState(activity.rentOrderCancelDao.rentOrderCancel.getReturnStatus());
                        activity.toastUtil.toastError(activity.context,error,null);
                        return;
                    }
                    activity.setResult(Success);
                    activity.finish();
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case BillingRequest:
                if(resultCode == BillingActivity.Success){
                    Toast.makeText(context,"结算成功",Toast.LENGTH_SHORT).show();
                    setResult(Success);
                    finish();
                }else{

                }
                break;
        }
    }

    public static void startActivity(Context context, String orderNo,int requestCode) {
        Intent intent = new Intent(context, RentOrderInfoActivity.class);
        intent.putExtra("orderNo", orderNo);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent,requestCode);
    }

    public static void startActivity(Fragment fragment,String orderNo,int requestCode){
        Intent intent = new Intent(fragment.getContext(),RentOrderInfoActivity.class);
        intent.putExtra("orderNo",orderNo);
        fragment.startActivityForResult(intent,requestCode);
    }


    @Override
    public String getTAG() {
        return TAG;
    }

}

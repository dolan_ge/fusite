package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.DeleteChargeCard;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.view.CustomDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/7/21.
 */
public class CardInputPasswordActivity extends UtilActivity {

    public String TAG = "CardInputPasswordActivity";
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.no)
    TextView no;
    @BindView(R.id.password2Layout)
    LinearLayout password2Layout;
    @BindView(R.id.line)
    View line;
    @BindView(R.id.password3Layout)
    LinearLayout password3Layout;
    @BindView(R.id.password2)
    EditText password2;
    @BindView(R.id.password3)
    EditText password3;

    private int type;
    private String accountID;
    private String accountNo;

    private CustomDialog loading;

    public static final int Success = 0x001;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.card_input_password);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void loadObjectAttribute() {
        type = intent.getIntExtra("type", 0);
        accountID = intent.getStringExtra("accountID");
        accountNo = intent.getStringExtra("accountNo");
    }

    @Override
    public void setActivityView() {

        no.setText(intent.getStringExtra("accountNo"));

        switch (type) {
            case PileCardOperActivity.Unbund:
                title.setText("解绑");
                break;
            case PileCardOperActivity.ModPassword:
                title.setText("修改密码");
                password2Layout.setVisibility(View.VISIBLE);
                line.setVisibility(View.VISIBLE);
                password3Layout.setVisibility(View.VISIBLE);
                break;
            case PileCardOperActivity.Loss:
                title.setText("挂失");
                break;
        }
    }

    @OnClick(R.id.ok)
    public void onClick() {
        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        String passwordStr = password.getText().toString();
        switch (type) {
            case PileCardOperActivity.Unbund:

                if (TextUtils.isEmpty(passwordStr)) {
                    Toast.makeText(context, "密码不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }

                map.clear();
                map.put(RequestParam.DeleteChargeCard.Token, loginData.getToken());
                map.put(RequestParam.DeleteChargeCard.AccountId, accountID);
                map.put(RequestParam.DeleteChargeCard.AccountPwd, passwordStr);

                loading = dialogUtil.startLoading(context, loading, "正在处理");

                ApiService.getPileService().DeleteChargeCard(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.DeleteChargeCard.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(deleteChargeCards -> {
                            DeleteChargeCard deleteChargeCard = deleteChargeCards.get(0);
                            if (TextUtils.equals(BeanParam.Failure, deleteChargeCard.getCrm_accounts_delete().getIsSuccess())) {
                                Error error = errorParamUtil.checkReturnState(deleteChargeCard.getCrm_accounts_delete().getReturnStatus());
                                toastUtil.toastError(context, error, null);
                                return null;
                            }
                            return deleteChargeCard;
                        })
                        .subscribe(deleteChargeCard -> {
                            if (deleteChargeCard == null) {
                                return;
                            }
                            dialogUtil.dismiss(loading);
                            setResult(Success);
                            finish();
                        }, new ThrowableAction(context));
                break;
            case PileCardOperActivity.ModPassword:
                String passwrod2Str = password2.getText().toString();
                String password3Str = password3.getText().toString();

                if (NetParam.isEmpty(passwordStr, passwrod2Str, password3Str)) {
                    Toast.makeText(context, "请输入密码", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!TextUtils.equals(password3Str, passwrod2Str)) {
                    Toast.makeText(context, "两次输入的新密码必须相同", Toast.LENGTH_SHORT).show();
                    return;
                }

                loading = dialogUtil.startLoading(context, loading, "正在处理");

                map.clear();
                map.put(RequestParam.CardModPassword.Token, loginData.getToken());
                map.put(RequestParam.CardModPassword.AccountNo, accountNo);
                map.put(RequestParam.CardModPassword.AccountPwd, passwordStr);
                map.put(RequestParam.CardModPassword.NewAccountPwd, password3Str);
                ApiService.getPileService().CardModPassword(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.CardModPassword.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(cardModPassword -> {
                            if (TextUtils.equals(BeanParam.Failure, cardModPassword.getIsSuccess())) {
                                Error error = errorParamUtil.checkReturnState(cardModPassword.getReturnStatus());
                                toastUtil.toastError(context, error, null);
                                return null;
                            }
                            return cardModPassword;
                        })
                        .subscribe(cardModPassword -> {
                            dialogUtil.dismiss(loading);
                            if (cardModPassword == null) {
                                return;
                            }
                            finish();
                        }, new ThrowableAction(context));

                break;
            case PileCardOperActivity.Loss:
                if (NetParam.isEmpty(passwordStr)) {
                    Toast.makeText(context, "请输入密码", Toast.LENGTH_SHORT).show();
                    return;
                }
                map.clear();
                map.put(RequestParam.FreezeCard.Token, loginData.getToken());
                map.put(RequestParam.FreezeCard.AccountNo, accountNo);
                map.put(RequestParam.FreezeCard.AccountPwd, passwordStr);
                ApiService.getPileService().FreezeCard(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.FreezeCard.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(freezeCard -> {
                            if (TextUtils.equals(BeanParam.Failure, freezeCard.getIsSuccess())) {
                                Error error = errorParamUtil.checkReturnState(freezeCard.getIsSuccess());
                                toastUtil.toastError(context, error, null);
                                return null;
                            }
                            return freezeCard;
                        })
                        .subscribe(freezeCard -> {
                            dialogUtil.dismiss(loading);
                            if (freezeCard == null) {
                                return;
                            }
                            finish();
                        }, new ThrowableAction(context));

                break;
        }
    }

    public static void startActivity(Context context, String accountID, String accountNo, int type) {
        Intent intent = new Intent(context, CardInputPasswordActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("accountID", accountID);
        intent.putExtra("accountNo", accountNo);
        context.startActivity(intent);
    }

    public static void startActivity(Context context, String accountID, String accountNo, int type, int requestCode) {
        Intent intent = new Intent(context, CardInputPasswordActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("accountID", accountID);
        intent.putExtra("accountNo", accountNo);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent,requestCode);
    }


}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.view.CustomDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 修改登录密码
 * Created by Jelly on 2016/6/6.
 */
public class ModLoginPwdActivity extends UtilActivity {
    /**
     * TAG
     */
    private String TAG = "ModLoginPwdActivity";
    @BindView(R.id.oldPassword)
    EditText oldPassword;
    @BindView(R.id.newPassword1)
    EditText newPassword1;
    @BindView(R.id.newPassword2)
    EditText newPassword2;

    private CustomDialog loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.mod_login_pwd);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void setActivityView() {

    }

    @OnClick(R.id.okBtn)
    public void onClick() {
        String oldPasswordStr = oldPassword.getText().toString();
        String newPassword1Str = newPassword1.getText().toString();
        String newPassword2Str = newPassword2.getText().toString();

        if(TextUtils.isEmpty(oldPasswordStr) || TextUtils.isEmpty(newPassword1Str) || TextUtils.isEmpty(newPassword2Str)){
            Toast.makeText(this,"输入密码不能为空!",Toast.LENGTH_SHORT).show();
            return;
        }

        if (!TextUtils.equals(newPassword1Str, newPassword2Str)) {
            Toast.makeText(context, "两次输入的新密码不相同!", Toast.LENGTH_SHORT).show();
            newPassword2.setText("");
            return;
        }

        if(TextUtils.equals(oldPasswordStr,newPassword1Str)){
            Toast.makeText(context,"原密码和新密码不能相同!",Toast.LENGTH_SHORT).show();
            return;
        }

        loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
        Login.DataEntity dataEntity = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.UpdatePassword.Token, dataEntity.getToken());
        map.put(RequestParam.UpdatePassword.Type, RequestParam.UpdatePassword.Password);
        map.put(RequestParam.UpdatePassword.OldPassword, oldPasswordStr);
        map.put(RequestParam.UpdatePassword.NewPassword, newPassword1Str);
        ApiService.getPileService().UpdatePassword(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.UpdatePassword.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(updatePassword -> {
                    dialogUtil.dismiss(loading);
                    return updatePassword;
                })
                .map(updatePassword -> {
                    if(TextUtils.equals(BeanParam.Failure,updatePassword.getIsSuccess())){
                        Error error = errorParamUtil.checkReturnState(updatePassword.getReturnStatus());
                        toastUtil.toastError(context,error,null);
                        return null;
                    }
                    return updatePassword;
                })
                .subscribe(updatePassword -> {
                    if (updatePassword == null) {
                        return;
                    }
                    Toast.makeText(context, "修改密码成功", Toast.LENGTH_SHORT).show();
                    finish();
                },new ThrowableAction(this));
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, ModLoginPwdActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

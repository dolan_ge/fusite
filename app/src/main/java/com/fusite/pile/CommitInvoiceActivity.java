package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ChinaService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.map.MyLocation;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.AssetsUtils;
import com.fusite.utils.JsonUtil;
import com.fusite.view.CustomDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.qqtheme.framework.picker.AddressPicker;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 填写开票信息
 * Created by Jelly on 2016/6/22.
 */
public class CommitInvoiceActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "InvoiceActivity";
    @BindView(R.id.addr)
    TextView addr;
    @BindView(R.id.money)
    TextView money;
    @BindView(R.id.billTo)
    EditText billTo;
    @BindView(R.id.recipient)
    EditText recipient;
    @BindView(R.id.tel)
    EditText tel;
    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.payHint)
    TextView payHint;


    private String areaStr; //地区
    private String moneyStr; //价格
    private String orderNoStr;
    private CustomDialog loading;
    private String type;

    private MyLocation location;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.invoice_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void loadObjectAttribute() {
        moneyStr = intent.getStringExtra("money");
        orderNoStr = intent.getStringExtra("orderNo");
        type = intent.getStringExtra("type");
    }

    @Override
    public void setActivityView() {
        money.setText(moneyStr + "元");
        if(Double.parseDouble(moneyStr) < 200){
            payHint.setVisibility(View.VISIBLE);
        }else{
            payHint.setVisibility(View.GONE);
        }
        //初始化地区
        location = application.getLocation();
        if(location != null){
            areaStr = location.getProvince() + location.getCity() + location.getDistrict();
            addr.setText(location.getProvince() + "    " + location.getCity() + "    " + location.getDistrict());
            address.setText(location.getAddress());
        }
    }

    @OnClick({R.id.selectAddr, R.id.ok})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.selectAddr:
                ArrayList<AddressPicker.Province> data = new ArrayList<>();
                String json = AssetsUtils.readText(this, "city.json");
                data.addAll(JsonUtil.arrayFormJson(json, AddressPicker.Province[].class));
                AddressPicker picker = new AddressPicker(this, data);

                if(location != null){
                    picker.setSelectedItem(location.getProvince(), location.getCity(), location.getDistrict());
                }

                //picker.setHideProvince(true);//加上此句举将只显示地级及县级
                //picker.setHideCounty(true);//加上此句举将只显示省级及地级

                picker.setOnAddressPickListener((province, city, county) -> {
                    addr.setText(province + "    " + city + "    " + county);
                    areaStr = province + city + county;
                });

                picker.show();
                break;
            case R.id.ok:
                String billToStr = billTo.getText().toString();
                String recipientStr = recipient.getText().toString();
                String telStr = tel.getText().toString();
                String addrStr = address.getText().toString();

                if (NetParam.isEmpty(orderNoStr, billToStr, recipientStr, telStr, areaStr, addrStr)) {
                    Toast.makeText(context, "请填写完整信息！", Toast.LENGTH_SHORT).show();
                    return;
                }

                loading = dialogUtil.startLoading(context,loading,R.string.loadingText);

                Login.DataEntity loginData = application.getLogin().getData();
                Map<String, String> map = new HashMap<>();
                map.put(RequestParam.InvoiceSet.Token, loginData.getToken());
                map.put(RequestParam.InvoiceSet.OrderNo, orderNoStr);
                map.put(RequestParam.InvoiceSet.BillTo, billToStr);
                map.put(RequestParam.InvoiceSet.Recipient, recipientStr);
                map.put(RequestParam.InvoiceSet.Tel, telStr);
                map.put(RequestParam.InvoiceSet.Area, areaStr);
                map.put(RequestParam.InvoiceSet.Address, addrStr);
                map.put(RequestParam.InvoiceSet.OrderType,type);
                //是否为专用发票
                map.put(RequestParam.InvoiceSet.InvoiceType,RequestParam.InvoiceSet.CommentInvoice);

                Map<String, String> mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, NetParam.sign, RequestParam.InvoiceSet.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));

                ChinaService.InvoiceSet(mapParam)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(invoiceSet -> {
                            dialogUtil.dismiss(loading);
                            return invoiceSet;
                        })
                        .map(invoiceSet -> {
                            if (TextUtils.equals(BeanParam.Failure, invoiceSet.getIsSuccess())) {
                                Error error = errorParamUtil.checkReturnState(invoiceSet.getReturnStatus());
                                toastUtil.toastError(context, error, null);
                                return null;
                            }
                            return invoiceSet;
                        })
                        .subscribe(invoiceSet -> {
                            if (invoiceSet == null) {
                                return;
                            }
                            finish();
                        },new ThrowableAction(context));
                break;
        }
    }

    public static void startActivity(Context context, String money, String orderNo, String type) {
        Intent intent = new Intent(context, CommitInvoiceActivity.class);
        intent.putExtra("money", money);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("type",type);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}
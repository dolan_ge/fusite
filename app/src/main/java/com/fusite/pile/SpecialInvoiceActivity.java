package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.api.ChinaService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.UploadImage;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.map.MyLocation;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.AssetsUtils;
import com.fusite.utils.BitmapUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.view.CustomDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import cn.qqtheme.framework.picker.AddressPicker;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/7/20.
 */
public class SpecialInvoiceActivity extends UtilActivity {

    public String TAG = "SpecialInvoiceActivity";
    @BindView(R.id.area)
    TextView area;
    @BindView(R.id.money)
    TextView money;
    @BindView(R.id.companyName)
    EditText companyName;
    @BindView(R.id.identifaction)
    EditText identifaction;
    @BindView(R.id.bankName)
    EditText bankName;
    @BindView(R.id.accountNo)
    EditText accountNo;
    @BindView(R.id.registerAddr)
    EditText registerAddr;
    @BindView(R.id.companyTel)
    EditText companyTel;
    @BindView(R.id.recipient)
    EditText recipient;
    @BindView(R.id.tel)
    EditText tel;
    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.businessLicense)
    ImageView businessLicense;
    @BindView(R.id.orgCode)
    ImageView orgCode;
    @BindView(R.id.taxRegister)
    ImageView taxRegister;

    private MyLocation location;
    private String areaStr;
    private String moneyStr;
    private String orderNoStr;
    private String type;
    private GalleryHandler handler;

    private static String businessLicensePath;
    private static String orgCodePath;
    private static String taxRegisterPath;

    private String businessLicenseUrl;
    private String orgCodeUrl;
    private String taxRegisterUrl;

    private CustomDialog loading;

    public static final int SUCCESS = 0X001;
    public static final int FAIL = 0X002;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.special_invoice);
        ButterKnife.bind(this);
        loadObjectAttribute();
        setActivityView();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void loadObjectAttribute() {
        moneyStr = intent.getStringExtra("money");
        orderNoStr = intent.getStringExtra("orderNo");
        type = intent.getStringExtra("type");
    }


    @Override
    public void setActivityView() {
        //设置初始地址
        location = application.getLocation();
        if (location != null) {
            areaStr = location.getProvince() + "   " + location.getCity() + "   " + location.getDistrict();
            area.setText(areaStr);
            address.setText(location.getAddress());
        }
        money.setText(moneyStr + "元");
    }

    @OnClick({R.id.selectArea, R.id.ok, R.id.businessLicense, R.id.orgCode, R.id.taxRegister})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.selectArea:
                ArrayList<AddressPicker.Province> data = new ArrayList<>();
                String json = AssetsUtils.readText(this, "city.json");
                data.addAll(JsonUtil.arrayFormJson(json, AddressPicker.Province[].class));
                AddressPicker picker = new AddressPicker(this, data);

                if (location != null) {
                    picker.setSelectedItem(location.getProvince(), location.getCity(), location.getDistrict());
                }

                //picker.setHideProvince(true);//加上此句举将只显示地级及县级
                //picker.setHideCounty(true);//加上此句举将只显示省级及地级

                picker.setOnAddressPickListener((province, city, county) -> {
                    area.setText(province + "    " + city + "    " + county);
                    areaStr = province + city + county;
                });

                picker.show();
                break;
            case R.id.ok:

                String billToStr = companyName.getText().toString();
                String recipientStr = recipient.getText().toString();
                String telStr = tel.getText().toString();
                String addrStr = address.getText().toString();
                String identifactionStr = identifaction.getText().toString();
                String bookNameStr = bankName.getText().toString();
                String registerAddrStr = registerAddr.getText().toString();
                String companyTelStr = companyTel.getText().toString();
                String accountNoStr = accountNo.getText().toString();


                if (NetParam.isEmpty(businessLicensePath, orgCodePath, taxRegisterPath)) {
                    Toast.makeText(context, "请选择图片", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (NetParam.isEmpty(billToStr, recipientStr, telStr, addrStr, identifactionStr, bookNameStr, registerAddrStr, companyTelStr,accountNoStr)) {
                    Toast.makeText(context, "请填写完整信息", Toast.LENGTH_SHORT).show();
                    return;
                }

                //压缩map
                Func1<String,byte[]> compressMap = s -> {
                    BitmapUtil bitmapUtil1 = BitmapUtil.getInstance();
                    return bitmapUtil1.Bitmap2Bytes(bitmapUtil1.getimage(s,400));
                };

                //上传图片
                Func1<byte[],Observable<UploadImage>> uploadImageFlatMap = bytes ->
                        ApiService.getPileService().upload(new MultipartBody.Builder().setType(MultipartBody.FORM)
                                .addFormDataPart("invoice", "invoice.png", RequestBody.create(MediaType.parse("image/png/jpg; charset=utf-8"), bytes)).build());

                loading = dialogUtil.startLoading(context,loading,"正在上传");

                Observable.just(businessLicensePath)
                        .observeOn(Schedulers.newThread())
                        .map(compressMap)
                        .flatMap(uploadImageFlatMap)
                        .map(uploadImage -> {
                            if(TextUtils.equals(BeanParam.Failure,uploadImage.getIsSuccess())){
                                Toast.makeText(context,"上传失败",Toast.LENGTH_SHORT).show();
                                return null;
                            }
                            businessLicenseUrl = uploadImage.getData().get(0).getUrl();
                            return orgCodePath;
                        })
                        .map(compressMap)
                        .flatMap(uploadImageFlatMap)
                        .map(uploadImage -> {
                            if(TextUtils.equals(BeanParam.Failure,uploadImage.getIsSuccess())){
                                Toast.makeText(context,"上传失败",Toast.LENGTH_SHORT).show();
                                return null;
                            }
                            orgCodeUrl = uploadImage.getData().get(0).getUrl();
                            return taxRegisterPath;
                        })
                        .map(compressMap)
                        .flatMap(uploadImageFlatMap)
                        .map(uploadImage -> {
                            if(TextUtils.equals(BeanParam.Failure,uploadImage.getIsSuccess())){
                                Toast.makeText(context,"上传失败",Toast.LENGTH_SHORT).show();
                                return null;
                            }
                            taxRegisterUrl = uploadImage.getData().get(0).getUrl();
                            return taxRegisterUrl;
                        })
                        .flatMap(s -> {
                            Login.DataEntity loginData = application.getLogin().getData();
                            Map<String, String> map = new HashMap<>();
                            map.put(RequestParam.InvoiceSet.Token, loginData.getToken());
                            map.put(RequestParam.InvoiceSet.OrderNo, orderNoStr);
                            map.put(RequestParam.InvoiceSet.Identifaction, identifactionStr);
                            map.put(RequestParam.InvoiceSet.BankName, bookNameStr);
                            map.put(RequestParam.InvoiceSet.RegisterAddr, registerAddrStr);
                            map.put(RequestParam.InvoiceSet.CompanyTel, companyTelStr);
                            map.put(RequestParam.InvoiceSet.BillTo, billToStr);
                            map.put(RequestParam.InvoiceSet.Recipient, recipientStr);
                            map.put(RequestParam.InvoiceSet.Tel, telStr);
                            map.put(RequestParam.InvoiceSet.Area, areaStr);
                            map.put(RequestParam.InvoiceSet.Address, addrStr);
                            map.put(RequestParam.InvoiceSet.OrderType, type);
                            map.put(RequestParam.InvoiceSet.BusinessLicense,businessLicenseUrl);
                            map.put(RequestParam.InvoiceSet.OrgCode,orgCodeUrl);
                            map.put(RequestParam.InvoiceSet.TaxRegister,taxRegisterUrl);
                            map.put(RequestParam.InvoiceSet.AccountNo,accountNoStr);
                            //是否为专用发票
                            map.put(RequestParam.InvoiceSet.InvoiceType, RequestParam.InvoiceSet.SpecialInvoice);
                            Map<String, String> mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, NetParam.sign, RequestParam.InvoiceSet.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));
                            return ChinaService.InvoiceSet(mapParam);
                        })
                        .map(invoiceSet -> {
                            if (TextUtils.equals(BeanParam.Failure, invoiceSet.getIsSuccess())) {
                                Error error = errorParamUtil.checkReturnState(invoiceSet.getReturnStatus());
                                toastUtil.toastError(context, error, null);
                                return null;
                            }
                            return invoiceSet;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(invoiceSet -> {
                            dialogUtil.dismiss(loading);
                            if (invoiceSet == null) {
                                return;
                            }
                            Toast.makeText(context, "上传成功", Toast.LENGTH_SHORT).show();
                            setResult(SUCCESS);
                            finish();
                        },new ThrowableAction(context));
                break;
            case R.id.businessLicense:
                startGallery(0x001);
                break;
            case R.id.orgCode:
                startGallery(0x002);
                break;
            case R.id.taxRegister:
                startGallery(0x003);
                break;
        }
    }

    private void startGallery(int requestCode) {
        FunctionConfig config = new FunctionConfig.Builder()
                .setEnableCamera(true)
                .build();
        GalleryFinal.openGallerySingle(requestCode, config, handler == null ? handler = new GalleryHandler(this) : handler);
    }

    public void setImage(int requestCode) {
        switch (requestCode) {
            case 0x001:
                if (!TextUtils.isEmpty(businessLicensePath)) {
                    Picasso.with(context).load(new File(businessLicensePath)).into(businessLicense);
                }
                if (!orgCode.isShown()) {
                    orgCode.setVisibility(View.VISIBLE);
                }
                break;
            case 0x002:
                if (!TextUtils.isEmpty(orgCodePath)) {
                    Picasso.with(context).load(new File(orgCodePath)).into(orgCode);
                }
                taxRegister.setVisibility(View.VISIBLE);
                break;
            case 0x003:
                if (!TextUtils.isEmpty(taxRegisterPath)) {
                    Picasso.with(context).load(new File(taxRegisterPath)).into(taxRegister);
                }
                break;
        }
    }

    private static class GalleryHandler implements GalleryFinal.OnHanlderResultCallback {
        private WeakReference<SpecialInvoiceActivity> mActivity;

        public GalleryHandler(SpecialInvoiceActivity activity) {
            mActivity = new WeakReference<SpecialInvoiceActivity>(activity);
        }

        @Override
        public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
            SpecialInvoiceActivity activity = mActivity.get();
            switch (reqeustCode) {
                case 0x001:
                    businessLicensePath = resultList.get(0).getPhotoPath();
                    break;
                case 0x002:
                    orgCodePath = resultList.get(0).getPhotoPath();
                    break;
                case 0x003:
                    taxRegisterPath = resultList.get(0).getPhotoPath();
                    break;
            }
            activity.setImage(reqeustCode);
        }

        @Override
        public void onHanlderFailure(int requestCode, String errorMsg) {

        }

    }

    public static void startActivity(Context context, String money, String orderNo, String type,int requestCode) {
        Activity activity = (Activity) context;
        Intent intent = new Intent(context, SpecialInvoiceActivity.class);
        intent.putExtra("money", money);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("type", type);
        activity.startActivityForResult(intent,requestCode);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ChinaService;
import com.fusite.bean.Login;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;

import java.util.HashMap;
import java.util.Map;

import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/3/31.
 */
public class ModCustNameActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "ModCustNameActivity";
    /**
     * 昵称输入框
     */
    private EditText custName_et;
    /**
     * 完成按钮
     */
    private TextView ok;
    /**
     * 成功
     */
    public static final int SUCCESS = 0x001;
    /**
     * 失败
     */
    public static final int FAILURE = 0x002;

    public static final int MaxNickLength = 6;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.mod_custname);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {
        custName_et = (EditText) this.findViewById(R.id.custName_et);
        ok = (TextView) this.findViewById(R.id.ok);
    }

    @Override
    public void setListener() {
        setOkListener();
    }

    @Override
    public void setActivityView() {
        Login.DataEntity entity = application.getLogin().getData();
        custName_et.setText(entity.getCustName());
    }


    @Override
    public View back(View view) {
        String custName = custName_et.getText().toString();
        Login.DataEntity entity = application.getLogin().getData();
        if (!TextUtils.equals(custName, entity.getCustName()) && !TextUtils.isEmpty(custName)) {
            dialogUtil.showSureListenerDialog(context, "是否取消本次修改?", (dialog, index) -> {
                setResult(FAILURE);
                finish();
            });
        }
        finish();
        return view;
    }

    public void setOkListener() {
        ok.setOnClickListener(v -> {
            String custName = custName_et.getText().toString();
            if (TextUtils.isEmpty(custName)) {
                Toast.makeText(ModCustNameActivity.this, "昵称不能为空！", Toast.LENGTH_SHORT).show();
                return;
            }
            Login.DataEntity entity = application.getLogin().getData();
            Map<String, String> map = new HashMap();
            map.put(RequestParam.UpdateUserInfo.Token, entity.getToken());
            map.put(RequestParam.UpdateUserInfo.CustName, custName);
            Map<String, String> mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), entity.getCustID(), NetParam.sign_method,NetParam.sign, RequestParam.UpdateUserInfo.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));
            ChinaService.UpdateUserInfo(mapParam)
                    .subscribeOn(Schedulers.newThread())
                    .map(updateUserInfo -> {
                        if (TextUtils.equals(BeanParam.Failure, updateUserInfo.getIsSuccess())) {
                            com.fusite.bean.Error error = errorParamUtil.checkReturnState(updateUserInfo.getReturnStatus());
                            toastUtil.toastError(context, error, null);
                            setResult(FAILURE);
                            return null;
                        }
                        return updateUserInfo;
                    })
                    .subscribe(updateUserInfo -> {
                        if (updateUserInfo == null) {
                            finish();
                            return;
                        }
                        application.getLogin().getData().setCustName(custName);
                        application.cacheLogin();
                        setResult(SUCCESS);
                        finish();
                    }, throwable -> {
                        Log.v(TAG,throwable.getMessage());
                    });
        });
    }

    /**
     * 开启界面
     */
    public static void startActivity(Context context, int requestCode) {
        Intent intent = new Intent(context, ModCustNameActivity.class);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

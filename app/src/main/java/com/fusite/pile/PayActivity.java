package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.PayResult;
import com.fusite.bean.Unifiedorder;
import com.fusite.fragments.BaseFragment;
import com.fusite.handlerinterface.NetErrorHandlerListener;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.impl.BackCarDaoImpl;
import com.fusite.impl.PayDaoImpl;
import com.fusite.impl.RentPayDaoImpl;
import com.fusite.param.BeanParam;
import com.fusite.param.DefaultParam;
import com.fusite.param.NetErrorEnum;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.constant.Keys;
import com.fusite.utils.DoubleUtil;
import com.fusite.utils.PayUtil;
import com.fusite.utils.WeChatUtil;
import com.fusite.view.CustomDialog;
import com.fruitknife.util.XmlResolve;
import com.jellycai.service.ThreadManage;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 支付页面
 * Created by Jelly on 2016/3/11.
 */
public class PayActivity extends UtilActivity implements View.OnClickListener, NetErrorHandlerListener {

    public String TAG = "PayActivity";

    @BindView(R.id.payType_text)
    TextView payTypeText;
    @BindView(R.id.payPrice_text)
    TextView payPriceText;
    @BindView(R.id.tran_text)
    TextView tranText;
    @BindView(R.id.tran_btn)
    ImageView tranBtn;
    @BindView(R.id.alipay_btn)
    ImageView alipayBtn;
    @BindView(R.id.wchatpay_btn)
    ImageView wchatpayBtn;
    @BindView(R.id.pay_btn)
    Button payBtn;
    /**
     * 输入支付密码
     */
    private PopupWindow payPw;
    /**
     * 支付密码
     */
    private String payPassword = "";
    /**
     * 密码输入框
     */
    private ImageView[] input_ets;
    /**
     * 用户选择的支付方式
     */
    private ImageView selectPay;

    /**
     * 请求码,设置支付密码
     */
    private static final int PayPwd = 0x001;
    /**
     * 需要支付的金额
     */
    public static String price;
    /**
     * 支付测试
     */
    private boolean isTest = false;
    /**
     * 支付订单ID
     */
    private String tradeNo;
    /**
     * 充值请求
     */
    private static final int RECHARGEQEQUEST = 0x003;
    /**
     * 支付类型: 1：电桩预约支付 2：电桩充电支付
     */
    public static int payType = 0;

    public static final int AppointPay = 1;
    public static final String AppointPayStr = "充电预约";

    public static final int ChargePay = 2;
    public static final String ChargePayStr = "电桩充电";

    public static final int RentCarPay = 3;
    public static final String RentCarPayStr = "汽车租赁";

    public static final int RentBillingPay = 4;
    public static final String RentBillingPayStr = "租车结算";

    public static final int ServiceBookPay = 5;
    public static final String ServiceBookPayStr = "服务预约";

    public static final int ServiceAmtPay = 6;
    public static final String ServiceAmtPayStr = "服务支付";

    /**
     * PayDaoImpl
     */
    private PayDaoImpl payDao;
    /**
     * 支付消息
     */
    private static final int msgPay = 0x004;
    /**
     * 是否为余额支付
     */
    private boolean isBalance = false;
    /**
     * 支付成功
     */
    public static final int Success = 0x005;
    /**
     * 支付失败
     */
    public static final int Failure = 0x006;

    private RentPayDaoImpl rentPayDao;

    private static final int msgRentPay = 0x007;

    public static String orderNo;

    private BackCarDaoImpl backCarDao;

    private static final int msgBackCar = 0x008;

    public static String OutTradNo;

    /**
     * 微信
     */
    public static boolean IsWxPay = false;
    private MyHandler handler;
    private CustomDialog loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.pay);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dialogUtil.dismiss(loading);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void loadObjectAttribute() {
        handler = new MyHandler(this);
        payDao = new PayDaoImpl(this, handler, msgPay);
        payType = intent.getIntExtra("payType", -1);
        price = intent.getStringExtra("price");
        orderNo = intent.getStringExtra("OrderNo");
        rentPayDao = new RentPayDaoImpl(this, handler, msgRentPay);
        backCarDao = new BackCarDaoImpl(this, handler, msgBackCar);
    }

    @Override
    public void setListener() {
        setPayRadioBtnListener();
    }

    @Override
    public void setActivityView() {
        Login.DataEntity entity = application.getLogin().getData();
        if (!TextUtils.isEmpty(entity.getBalanceAmt())) {
            tranText.setText(resourceUtil.getResourceString(PayActivity.this, R.string.money_sign) + entity.getBalanceAmt());
        }
        payPriceText.setText(price);
        payBtn.setText("确认支付" + price + "元");
        if (payType == AppointPay) {
            payTypeText.setText(AppointPayStr);
        } else if (payType == ChargePay) {
            payTypeText.setText(ChargePayStr);
        } else if (payType == RentCarPay) {
            payTypeText.setText(RentCarPayStr);
        } else if (payType == RentBillingPay) {
            payTypeText.setText(RentBillingPayStr);
        } else if (payType == ServiceBookPay) {
            payTypeText.setText(ServiceBookPayStr);
        } else if(payType == ServiceAmtPay){
            payTypeText.setText(ServiceAmtPayStr);
        }
    }

    /**
     * 设置支付选择的监听事件
     */
    public void setPayRadioBtnListener() {
        tranBtn.setOnClickListener(v -> setPay(((ImageView) v)));
//        alipayBtn.setOnClickListener(v -> setPay(((ImageView) v)));// TODO 支付宝之后需要启用
        wchatpayBtn.setOnClickListener(v -> setPay(((ImageView) v)));
    }

    @Override
    public View back(View view) {
        dialogUtil.showSureListenerDialog(context, "是否取消支付？", (dialog, index) -> {
            dialog.dismiss();
            //取消预约支付
            intent.putExtra("OrderNo", orderNo);
            setResult(Failure, intent);
            finish();
        });
        return view;
    }

    /**
     * 设置支付
     */
    public void setPay(ImageView pay) {
        if (selectPay != null) { //如果选择的支付方式不为空，改变为未选中
            selectPay.setImageResource(R.drawable.pay_noselect_rb);
        }
        pay.setImageResource(R.drawable.pay_select_rb);
        selectPay = pay;
    }

    /**
     * 支付
     *
     * @param pay
     * @return
     */
    public View pay(View pay) {
        if (selectPay == null) { //请选择支付方式
            Toast.makeText(this, R.string.nopayId_hint, Toast.LENGTH_SHORT).show();
            return pay;
        } else if (selectPay.getId() == R.id.alipay_btn) { //支付宝支付
            String orderInfo = "";
            tradeNo = orderNo;

            String passback_params = "";
            if (payType == AppointPay){
                MyApplication application = (MyApplication) getApplication();
                Login.DataEntity loginData = application.getLogin().getData();
                Map<String,String> mapCondition = new HashMap<String,String>();
                mapCondition.put("Token",loginData.getToken());
                mapCondition.put("CustId",loginData.getCustID());
                mapCondition.put("Device","Android");
                mapCondition.put("PayType","AppointPay");
                passback_params = NetParam.spliceCondition(mapCondition);
            }else if(payType == ChargePay){
                MyApplication application = (MyApplication) getApplication();
                Login.DataEntity loginData = application.getLogin().getData();
                Map<String,String> mapCondition = new HashMap<String,String>();
                mapCondition.put("Token",loginData.getToken());
                mapCondition.put("CustId",loginData.getCustID());
                mapCondition.put("Device","Android");
                mapCondition.put("PayType","ChargePay");
                passback_params = NetParam.spliceCondition(mapCondition);
            }

            if (isTest) {
                orderInfo = PayUtil.getOrderInfo("测试的商品", passback_params, "0.01", tradeNo);
            } else {
                orderInfo = PayUtil.getOrderInfo("支付订单", passback_params, price, tradeNo);
            }
            String sign = PayUtil.sign(orderInfo);
            try {
                /**
                 * 仅需对sign 做URL编码
                 */
                sign = URLEncoder.encode(sign, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            /**
             * 完整的符合支付宝参数规范的订单信息
             */
            final String payInfo = orderInfo + "&sign=\"" + sign + "\"&" + PayUtil.getSignType();
            ThreadManage.start(() -> {
                // 构造PayTask 对象
                PayTask alipay = new PayTask(PayActivity.this);
                // 调用支付接口，获取支付结果
                String result = alipay.pay(payInfo, true);
                Message msg = new Message();
                msg.what = PayUtil.SDK_PAY_FLAG;
                msg.obj = result;
                handler.sendMessage(msg);
            });
        } else if (selectPay.getId() == R.id.wchatpay_btn) { //微信支付

            loading = dialogUtil.startLoading(context,loading,R.string.loadingText);

            String xmlStr = "";
            OutTradNo = orderNo;

            String attach = "";
            if (payType == AppointPay){
                MyApplication application = (MyApplication) getApplication();
                Login.DataEntity loginData = application.getLogin().getData();
                Map<String,String> mapCondition = new HashMap<String,String>();
                mapCondition.put("Token",loginData.getToken());
                mapCondition.put("CustId",loginData.getCustID());
                mapCondition.put("Device","Android");
                mapCondition.put("PayType","AppointPay");
                attach = NetParam.spliceCondition(mapCondition);
            }else if(payType == ChargePay){
                MyApplication application = (MyApplication) getApplication();
                Login.DataEntity loginData = application.getLogin().getData();
                Map<String,String> mapCondition = new HashMap<String,String>();
                mapCondition.put("Token",loginData.getToken());
                mapCondition.put("CustId",loginData.getCustID());
                mapCondition.put("Device","Android");
                mapCondition.put("PayType","ChargePay");
                attach = NetParam.spliceCondition(mapCondition);
            }

            if (isTest) {
                xmlStr = WeChatUtil.getOrderInfo("测试的商品", "该测试商品的详细描述", "1", OutTradNo,attach);
            } else {
                xmlStr = WeChatUtil.getOrderInfo("支付订单", "支付玛帮新能源订单", (int)(Double.parseDouble(price) * 100) + "", OutTradNo,attach);
            }

            RequestBody body = RequestBody.create(MediaType.parse("text/xml"), xmlStr);
            ApiService.getWxApi().getGenprePay(body)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(xml -> {
                        try {
                            String xmlStr1 = xml.string();

                            Unifiedorder unifiedorder = new XmlResolve().xmlToObject(xmlStr1, Unifiedorder.class);

                            if(TextUtils.equals("time_expire时间过短，刷卡至少1分钟，其他5分钟",unifiedorder.return_msg)){
                                Toast.makeText(context,"启动微信失败,请校准当前时间为标准时间！",Toast.LENGTH_SHORT).show();
                                dialogUtil.dismiss(loading);
                                return;
                            }


                            IWXAPI api = WXAPIFactory.createWXAPI(PayActivity.this, Keys.AppId, false);
                            PayReq request = new PayReq();
                            request.appId = unifiedorder.appid;
                            request.partnerId = unifiedorder.mch_id;
                            request.prepayId = unifiedorder.prepay_id;
                            request.packageValue = "Sign=WXPay";
                            request.nonceStr = unifiedorder.nonce_str;
                            request.timeStamp = WeChatUtil.getTimeStamp();
                            request.sign = WeChatUtil.getRequestSign(request);
                            request.extData = "1";
                            api.registerApp(Keys.AppId); //把应用注册到微信
                            api.sendReq(request);
                            IsWxPay = true;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, new ThrowableAction(this));

        } else if (selectPay.getId() == R.id.tran_btn) { //余额支付
            //模拟没有设置支付密码
            Login.DataEntity entity = application.getLogin().getData();
            if (TextUtils.equals(entity.getExistsPayPassword(), "0")) { //0 :未设置 1：已经设置
                dialogUtil.showSureListenerDialog(this, "您还没有设置支付密码，是否前往设置?", (dialog, index) -> {
                    SetPayPwdActivity.startAppActivity(PayActivity.this, PayPwd);

                    if(dialog != null){
                        dialog.dismiss();
                    }

                });
            } else {
                showPayPw(pay);
            }
        }
        return pay;
}


    /**
     * 弹出支付PopupWindow
     */
    public void showPayPw(View parent) {

        if (payPw != null && !payPw.isShowing()) {
            payPw.showAtLocation(parent, Gravity.CENTER, 0, 0);
            payPw.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
            payPw.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            return;
        }

        View root = getLayoutInflater().inflate(R.layout.pay_keyboard, null);
        Button num0 = (Button) root.findViewById(R.id.num0);
        Button num1 = (Button) root.findViewById(R.id.num1);
        Button num2 = (Button) root.findViewById(R.id.num2);
        Button num3 = (Button) root.findViewById(R.id.num3);
        Button num4 = (Button) root.findViewById(R.id.num4);
        Button num5 = (Button) root.findViewById(R.id.num5);
        Button num6 = (Button) root.findViewById(R.id.num6);
        Button num7 = (Button) root.findViewById(R.id.num7);
        Button num8 = (Button) root.findViewById(R.id.num8);
        Button num9 = (Button) root.findViewById(R.id.num9);
        Button delete_btn = (Button) root.findViewById(R.id.delete_btn);
        Button dismiss_btn = (Button) root.findViewById(R.id.dismiss_btn);
        num0.setOnClickListener(this);
        num1.setOnClickListener(this);
        num2.setOnClickListener(this);
        num3.setOnClickListener(this);
        num4.setOnClickListener(this);
        num5.setOnClickListener(this);
        num6.setOnClickListener(this);
        num7.setOnClickListener(this);
        num8.setOnClickListener(this);
        num9.setOnClickListener(this);
        delete_btn.setOnClickListener(this);
        dismiss_btn.setOnClickListener(this);
        input_ets = new ImageView[DefaultParam.PAYPASSWOEDSUM];
        input_ets[0] = (ImageView) root.findViewById(R.id.input_et0);
        input_ets[1] = (ImageView) root.findViewById(R.id.input_et1);
        input_ets[2] = (ImageView) root.findViewById(R.id.input_et2);
        input_ets[3] = (ImageView) root.findViewById(R.id.input_et3);
        input_ets[4] = (ImageView) root.findViewById(R.id.input_et4);
        input_ets[5] = (ImageView) root.findViewById(R.id.input_et5);
        payPw = popupWindowUtil.getPopupWindow(this, root, windowUtil.getScreenWidth(this), windowUtil.getScreenHeight(this));
        payPw.setOnDismissListener(() -> payPassword = "");
        payPw.setAnimationStyle(R.style.paypw_anim);
        payPw.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
    }

    @Override
    public void onClick(View v) {
        Button btn = (Button) v;
        switch (v.getId()) {
            case R.id.dismiss_btn: //取消按钮
                payPw.dismiss();
                break;
            case R.id.delete_btn: //删除一位支付密码的方法
                if (payPassword.length() == 0) { //已经没有支付密码了，不能再减少了
                    return;
                }
                payPassword = payPassword.substring(0, payPassword.length() - 1);
                reInputEt(payPassword);
                break;
            default:
                if (payPassword.length() == 6) { //如果支付密码已经有6位，不能继续输入
                    return;
                }
                payPassword += btn.getText();
                reInputEt(payPassword);
                if (payPassword.length() == 6) { //如果是最后一位输入6位，直接支付
                    isBalance = true;
                    Login.DataEntity entity = application.getLogin().getData();
                    if (payType == AppointPay) {
                        payDao.getDao(entity.getToken(), orderNo, "0", payPassword, PayDaoImpl.BALANCEPAY, PayDaoImpl.APPOINTPAY, entity.getCustID());
                    } else if (payType == ChargePay) {
                        payDao.getDao(entity.getToken(), orderNo, "0", payPassword, PayDaoImpl.BALANCEPAY, PayDaoImpl.CHARGEPAY, entity.getCustID());
                    } else if (payType == RentCarPay) {
                        rentPayDao.getRentPay(entity.getToken(), entity.getCustID(), orderNo, RentPayDaoImpl.BalancePay, payPassword, "");
                    } else if (payType == RentBillingPay) {
                        backCarDao.getBackCar(entity.getToken(), entity.getCustID(), orderNo, "", BackCarDaoImpl.BalancePay, payPassword);
                    }else if(payType == ServiceBookPay || payType == ServiceAmtPay){
                        Map<String, String> map = new HashMap<>();
                        map.put(RequestParam.ServicePay.Token, entity.getToken());
                        map.put(RequestParam.ServicePay.OrderNo, orderNo);
                        map.put(RequestParam.ServicePay.Type, RequestParam.ServicePay.BalancePay);
                        map.put(RequestParam.ServicePay.PayAmt,price);
                        map.put(RequestParam.ServicePay.PayPassword,payPassword);
                        ApiService.getPileService().ServicePay(NetParam.trancode, NetParam.mode, NetParam.getTime(), entity.getCustID(), NetParam.sign_method, RequestParam.ServicePay.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .map(servicePay -> {
                                    if (TextUtils.equals(BeanParam.Failure, servicePay.getIsSuccess())) {
                                        Error error = errorParamUtil.checkReturnState(servicePay.getReturnStatus());
                                        toastUtil.toastError(context, error, null);
                                        return null;
                                    }
                                    return servicePay;
                                })
                                .subscribe(servicePay -> {
                                    if (servicePay == null) {
                                        return;
                                    }
                                    if(payPw != null){
                                        payPw.dismiss();
                                    }
                                    intent.putExtra("OrderNo", orderNo);
                                    setResult(Success, intent);
                                    finish();
                                }, new ThrowableAction(context));
                    }
                }
        }
    }

    /**
     * 更改密码框状态
     */
    public void reInputEt(String payPassword) {
        int index = payPassword.length();
        for (int i = 0; i < index; i++) {
            input_ets[i].setImageResource(R.drawable.paypassword_input);
        }
        for (int i = index; i < DefaultParam.PAYPASSWOEDSUM; i++) {
            input_ets[i].setImageResource(R.drawable.paypassword_noinput);
        }
    }


    private static class MyHandler extends Handler {
        private WeakReference<PayActivity> mActivity;

        public MyHandler(PayActivity payActivity) {
            mActivity = new WeakReference<PayActivity>(payActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            PayActivity activity = mActivity.get();
            switch (msg.what) {
                case PayUtil.SDK_PAY_FLAG:
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Login.DataEntity entity = activity.application.getLogin().getData();
                        if (payType == AppointPay) {
                            activity.setResult(Success, activity.intent);
                            if (activity.isBalance) {
                                activity.payPw.dismiss();
                                //更新缓存中的余额
                                String balance = DoubleUtil.delete(activity.application.getLogin().getData().getBalanceAmt(), activity.price);
                                activity.application.getLogin().getData().setBalanceAmt(balance);
                                activity.application.cacheLogin();
                            }
                            activity.finish();
                        } else if (payType == ChargePay) {
                            activity.intent.putExtra("OrderNo", orderNo);
                            activity.setResult(Success, activity.intent);
                            if (activity.isBalance) {
                                activity.payPw.dismiss();
                                //更新缓存中的余额
                                String balance = DoubleUtil.delete(activity.application.getLogin().getData().getBalanceAmt(), activity.price);
                                activity.application.getLogin().getData().setBalanceAmt(balance);
                                activity.application.cacheLogin();
                            }
                            activity.finish();
                        } else if (payType == RentCarPay) {
                            activity.rentPayDao.getRentPay(entity.getToken(), entity.getCustID(), orderNo, RentPayDaoImpl.AliPay, "", activity.tradeNo);
                        } else if (payType == RentBillingPay) {
                            activity.backCarDao.getBackCar(entity.getToken(), entity.getCustID(), orderNo, activity.tradeNo, BackCarDaoImpl.AliPay, "");
                        } else if (payType == ServiceBookPay || payType == ServiceAmtPay) {
                            Map<String, String> map = new HashMap<>();
                            map.put(RequestParam.ServicePay.Token, entity.getToken());
                            map.put(RequestParam.ServicePay.OrderNo, orderNo);
                            map.put(RequestParam.ServicePay.Type, RequestParam.ServicePay.AliPay);
                            map.put(RequestParam.ServicePay.PayAmt,activity.price);
                            map.put(RequestParam.ServicePay.PayOrderNo, activity.tradeNo);
                            ApiService.getPileService().ServicePay(NetParam.trancode, NetParam.mode, NetParam.getTime(), entity.getCustID(), NetParam.sign_method, RequestParam.ServicePay.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .map(servicePay -> {
                                        if (TextUtils.equals(BeanParam.Failure, servicePay.getIsSuccess())) {
                                            Error error = activity.errorParamUtil.checkReturnState(servicePay.getReturnStatus());
                                            activity.toastUtil.toastError(activity.context, error, null);
                                            return null;
                                        }
                                        return servicePay;
                                    })
                                    .subscribe(servicePay -> {
                                        if (servicePay == null) {
                                            return;
                                        }
                                        activity.intent.putExtra("OrderNo", orderNo);
                                        activity.setResult(Success, activity.intent);
                                    }, new ThrowableAction(activity.context));
                        }
                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(activity, "支付结果确认中", Toast.LENGTH_SHORT).show();
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            Toast.makeText(activity, "支付失败", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case msgPay:
                    if (TextUtils.equals(activity.payDao.dao.getIsSuccess(), "N")) {
                        activity.intent.putExtra("OrderNo", orderNo);
                        activity.setResult(Failure, activity.intent);
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.payDao.dao.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, activity);
                        return;
                    }
                    activity.intent.putExtra("OrderNo", orderNo);
                    activity.setResult(Success, activity.intent);
                    if (activity.isBalance) {
                        activity.payPw.dismiss();
                        //更新缓存中的余额
                        String balance = DoubleUtil.delete(activity.application.getLogin().getData().getBalanceAmt(), activity.price);
                        activity.application.getLogin().getData().setBalanceAmt(balance);
                        activity.application.cacheLogin();
                    }
                    activity.finish();
                    break;
                case msgRentPay:
                    if (TextUtils.equals(activity.rentPayDao.rentPay.getIsSuccess(), "N")) {
                        Error error = activity.errorParamUtil.checkReturnState(activity.rentPayDao.rentPay.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, error, activity);
                        return;
                    }
                    activity.intent.putExtra("OrderNo", orderNo);
                    activity.setResult(Success, activity.intent);
                    if (activity.isBalance) {
                        activity.payPw.dismiss();
                        //更新缓存中的余额
                        String balance = DoubleUtil.delete(activity.application.getLogin().getData().getBalanceAmt(), activity.price);
                        activity.application.getLogin().getData().setBalanceAmt(balance);
                        activity.application.cacheLogin();
                    }
                    activity.finish();
                    break;
                case msgBackCar:
                    if (TextUtils.equals("N", activity.backCarDao.backCar.getIsSuccess())) {
                        Error error = activity.errorParamUtil.checkReturnState(activity.backCarDao.backCar.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, error, activity);
                        return;
                    }
                    activity.intent.putExtra("OrderNo", orderNo);
                    activity.setResult(Success, activity.intent);
                    if (activity.isBalance) {
                        activity.payPw.dismiss();
                        //更新缓存中的余额
                        String balance = DoubleUtil.delete(activity.application.getLogin().getData().getBalanceAmt(), activity.price);
                        activity.application.getLogin().getData().setBalanceAmt(balance);
                        activity.application.cacheLogin();
                    }
                    activity.finish();
                    break;
                default:
                    break;
            }

        }
    }

    @Override
    public void handlerError(String returnState) {
        if (TextUtils.equals(returnState, NetErrorEnum.余额不足.getState())) {
            dialogUtil.showSureListenerDialog(PayActivity.this, "您的余额不足，是否充值！", (dialog, index) -> {
                dialog.dismiss();
                RechargeActivity.startActivity(PayActivity.this, RECHARGEQEQUEST, RechargeActivity.BalanceAmt, "");
            }, (dialog, index) -> {
                dialog.dismiss();
                payPw.dismiss();
            });
        } else if (TextUtils.equals(returnState, NetErrorEnum.支付密码错误.getState())) {
            payPassword = "";
            reInputEt(payPassword);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                back(null);
                break;
        }
        return false;
    }

    /**
     * 返回结果
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PayPwd:
                if (resultCode == SetPayPwdActivity.SUCCESS) {
                    showPayPw(tranBtn);
                } else if (resultCode == SetPayPwdActivity.FAILURE) {
                    Toast.makeText(this, "支付密码设置失败!", Toast.LENGTH_SHORT).show();
                }
                break;
            case RECHARGEQEQUEST:
                if (resultCode == RechargeActivity.Success) {
                    Toast.makeText(this, "充值成功，请输入支付密码支付!", Toast.LENGTH_SHORT).show();
                    tranText.setText(application.getLogin().getData().getBalanceAmt());
                    payPassword = "";
                    reInputEt(payPassword);
                } else if (resultCode == RechargeActivity.Failure) {
                    Toast.makeText(this, "充值失败，请选择其他操作方式!", Toast.LENGTH_SHORT).show();
                    payPw.dismiss();
                }
        }
    }

    /**
     * 开启支付订单界面
     */
    public static void startPayActivity(Context context, String OrderNo, int payType, String price, int requestCode) {
        Intent intent = new Intent(context, PayActivity.class);
        intent.putExtra("OrderNo", OrderNo);
        intent.putExtra("payType", payType);
        intent.putExtra("price", price);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, requestCode);
    }

    /**
     * 开启支付订单界面
     */
    public static void startPayActivity(BaseFragment fragment, String OrderNo, int payType, String price, int requestCode) {
        Intent intent = new Intent(fragment.getContext(), PayActivity.class);
        intent.putExtra("OrderNo", OrderNo);
        intent.putExtra("payType", payType);
        intent.putExtra("price", price);
        fragment.startActivityForResult(intent,requestCode);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.bean.ChargeCardItem;
import com.fusite.bean.Login;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 账户余额
 * Created by Jelly on 2016/3/16.
 */
public class BalanceActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "BalanceActivity";
    @BindView(R.id.money)
    TextView money;
    private List<ChargeCardItem> daos;
    public static final int RECHARGE = 0x001;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.balance);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    /**
     * 当界面获取焦点
     */
    @Override
    protected void onResume() {
        super.onResume();
        setActivityView();
    }

    @Override
    public void loadObjectAttribute() {
        daos = getIntent().getParcelableArrayListExtra("daos");
    }

    @Override
    public void setActivityView() {
        Login.DataEntity entity = application.getLogin().getData();
        if (!TextUtils.isEmpty(entity.getBalanceAmt())) {
            money.setText("¥" + entity.getBalanceAmt());
        }
    }

    public View recharge(View view) {
        RechargeActivity.startActivity(this, RECHARGE,RechargeActivity.BalanceAmt,"");
        return view;
    }

    public View cardRecharge(View view) {
        PileCardActivity.startActivity(this, (ArrayList) daos, 1);
        return view;
    }

    public View withdraw(View view) {
        PileCardActivity.startActivity(this, (ArrayList) daos, 2);
        return view;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECHARGE) {
            switch (resultCode) {
                case RechargeActivity.Success:
                    Toast.makeText(BalanceActivity.this, "充值成功！", Toast.LENGTH_SHORT).show();
                    setActivityView();
                    break;
                case RechargeActivity.Failure:
                    Toast.makeText(BalanceActivity.this, "充值失败，请联系客服退款!", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    /**
     * 启动界面
     */
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, BalanceActivity.class);
        context.startActivity(intent);
    }

    /**
     * 开启界面
     */
    public static void startActivity(Context context, ArrayList<ChargeCardItem> daos) {
        Intent intent = new Intent(context, BalanceActivity.class);
        intent.putParcelableArrayListExtra("daos", daos);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

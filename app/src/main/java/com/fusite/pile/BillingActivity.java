package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.impl.BackCarDaoImpl;
import com.fusite.utils.DateUtil;
import com.fusite.utils.DoubleUtil;
import com.fusite.view.CustomSureDialog;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 租车结算
 * Created by Jelly on 2016/4/25.
 */
public class BillingActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "BillingActivity";
    @BindView(R.id.orderNo)
    TextView orderNo;
    @BindView(R.id.brand)
    TextView brand;
    @BindView(R.id.license)
    TextView license;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.battery)
    TextView battery;
    @BindView(R.id.payPrice)
    TextView payPrice;
    @BindView(R.id.sparePrice)
    TextView sparePrice;
    @BindView(R.id.refundPrice)
    TextView refundPrice;

    private Data data;

    private BackCarDaoImpl backCarDao;

    private static final int msgBackCar = 0x001;

    /**
     * 成功
     */
    public static final int Success = 0x002;
    /**
     * 失败
     */
    public static final int Failure = 0x003;

    private String needPayPrice;

    public static final int PayRequest = 0x004;

    private CustomSureDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.billing);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void loadObjectAttribute() {
        data = intent.getParcelableExtra("data");
        backCarDao = new BackCarDaoImpl(this, new MyHandler(this), msgBackCar);
    }


    @Override
    public void setActivityView() {
        orderNo.setText(data.getOrderNo());
        brand.setText(data.getBrand());
        license.setText(data.getLicense());
        payPrice.setText(data.getTotalAmt());
        sparePrice.setText(data.getRealAmt());
        refundPrice.setText(data.getClearingAmt());
        time.setText(DateUtil.getTwoDateDay(data.getRentEndTime(),data.getRentBeginTime()));
    }

    private static class MyHandler extends Handler{
        private WeakReference<BillingActivity> mActivity;

        public MyHandler(BillingActivity activity){
            mActivity = new WeakReference<BillingActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            BillingActivity activity = mActivity.get();
            if(activity == null){
                return;
            }
            switch (msg.what) {
                case msgBackCar:
                    if(TextUtils.equals("N",activity.backCarDao.backCar.getIsSuccess())){
                        Error error = activity.errorParamUtil.checkReturnState(activity.backCarDao.backCar.getReturnStatus());
                        activity.toastUtil.toastError(activity.context,error,null);
                        return;
                    }
                    activity.dialog.dismiss();
                    activity.setResult(Success);
                    activity.finish();
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case PayRequest:
                if(resultCode == PayActivity.Success){
                    setResult(Success);
                    finish();
                }else{
                    setResult(Failure);
                    finish();
                }
                break;
        }
    }

    public static void startActivity(Context context, int requestCode, Data data) {
        Intent intent = new Intent(context, BillingActivity.class);
        intent.putExtra("data", data);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent,requestCode);
    }

    @OnClick(R.id.ok)
    public void onClick() {

        dialog = new CustomSureDialog(context);
        dialog.setHintText("取消订单后，您支付的金额将会在3~5和工作日内退还给您，是否取消？");
        dialog.setCancelClickListener((dialog1, index) -> dialog1.dismiss());
        dialog.setOkClickListener((dialog1, index) -> {
            if(Double.parseDouble(data.getTotalAmt()) < Double.parseDouble(data.getRealAmt())){
                needPayPrice = DoubleUtil.delete(data.getRealAmt(),data.getTotalAmt());
                PayActivity.startPayActivity(context,data.getOrderNo(),PayActivity.RentBillingPay,needPayPrice,PayRequest);
                return;
            }
            Login.DataEntity dataEntity = application.getLogin().getData();
            backCarDao.getBackCar(dataEntity.getToken(),dataEntity.getCustID(),data.getOrderNo(),"","","");
        });
        dialog.show();


    }

    static class Data implements Parcelable {
        public String orderNo;
        public String brand;
        public String license;
        public String rentTime;
        public String TotalAmt;
        public String RealAmt;
        public String ClearingAmt;
        public String RentBeginTime;
        public String RentEndTime;

        public String getRentBeginTime() {
            return RentBeginTime;
        }

        public void setRentBeginTime(String rentBeginTime) {
            RentBeginTime = rentBeginTime;
        }

        public String getRentEndTime() {
            return RentEndTime;
        }

        public void setRentEndTime(String rentEndTime) {
            RentEndTime = rentEndTime;
        }

        public Data() {

        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getRentTime() {
            return rentTime;
        }

        public void setRentTime(String rentTime) {
            this.rentTime = rentTime;
        }

        public String getTotalAmt() {
            return TotalAmt;
        }

        public void setTotalAmt(String totalAmt) {
            TotalAmt = totalAmt;
        }

        public String getRealAmt() {
            return RealAmt;
        }

        public void setRealAmt(String realAmt) {
            RealAmt = realAmt;
        }

        public String getClearingAmt() {
            return ClearingAmt;
        }

        public void setClearingAmt(String clearingAmt) {
            ClearingAmt = clearingAmt;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.orderNo);
            dest.writeString(this.brand);
            dest.writeString(this.license);
            dest.writeString(this.rentTime);
            dest.writeString(this.TotalAmt);
            dest.writeString(this.RealAmt);
            dest.writeString(this.ClearingAmt);
            dest.writeString(this.RentBeginTime);
            dest.writeString(this.RentEndTime);
        }

        protected Data(Parcel in) {
            this.orderNo = in.readString();
            this.brand = in.readString();
            this.license = in.readString();
            this.rentTime = in.readString();
            this.TotalAmt = in.readString();
            this.RealAmt = in.readString();
            this.ClearingAmt = in.readString();
            this.RentBeginTime = in.readString();
            this.RentEndTime = in.readString();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {

            @Override
            public Data createFromParcel(Parcel source) {
                return new Data(source);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.fusite.activities.UtilActivity;

/**
 * Created by Jelly on 2016/7/19.
 */
public class ExampleActivity extends UtilActivity{

    public String TAG = "ExampleActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.example);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void setActivityView() {

    }

    public static void startActivity(Context context){
        Intent intent = new Intent(context,ExampleActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

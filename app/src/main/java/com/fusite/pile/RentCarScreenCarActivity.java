package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.ViewPageAdapter;
import com.fusite.bean.CarModels;
import com.fusite.fragment.RentBrandsScreenFragment;
import com.fusite.fragment.RentPriceScreenFragment;
import com.fusite.fragment.RentTypeScreenFragment;
import com.fusite.fragments.BaseFragment;
import com.fusite.utils.AnimationUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 租车筛选
 * Created by Jelly on 2016/4/18.
 */
public class RentCarScreenCarActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "RentCarScreenCarActivity";
    /**
     * 成功
     */
    public static final int Success = 0x001;
    /**
     * 失败
     */
    public static final int Failure = 0x002;

    public List<CarModels.DataEntity> list;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.type)
    TextView type;
    @BindView(R.id.brands)
    TextView brands;
    @BindView(R.id.tab_line)
    ImageView tabLine;
    @BindView(R.id.vp)
    ViewPager vp;

    private BaseFragment[] fragments;

    private ViewPageAdapter viewPageAdapter;

    private TextView currTab;

    private int previousPosition;

    private String priceScreen;

    private String typeScreen;

    private String brandScreen;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.screen_car);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {
        list = intent.getParcelableArrayListExtra("CarModels");
        fragments = new BaseFragment[]{new RentPriceScreenFragment(), new RentTypeScreenFragment(), new RentBrandsScreenFragment()};
        viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager(), fragments);
        currTab = price;
        priceScreen = intent.getStringExtra("PriceScreen");
        typeScreen = intent.getStringExtra("TypeScreen");
        brandScreen = intent.getStringExtra("BrandScreen");
    }

    @Override
    public void setListener() {
        setViewPageListener();
    }

    @Override
    public void setActivityView() {
        vp.setAdapter(viewPageAdapter);
        tabLine.getLayoutParams().width = windowUtil.getScreenWidth(this) / fragments.length;
    }

    public String getBrandScreen() {
        return brandScreen;
    }

    public void setBrandScreen(String brandScreen) {
        this.brandScreen = brandScreen;
    }

    public String getPriceScreen() {
        return priceScreen;
    }

    public void setPriceScreen(String priceScreen) {
        this.priceScreen = priceScreen;
    }

    public String getTypeScreen() {
        return typeScreen;
    }

    public void setTypeScreen(String typeScreen) {
        this.typeScreen = typeScreen;
    }

    public List<CarModels.DataEntity> getList() {
        return list;
    }


    public void setViewPageListener() {
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (currTab != null) {
                    currTab.setTextColor(resourceUtil.getResourceColor(context, R.color.defaultTabColor));
                }
                switch (position) {
                    case 0:
                        currTab = price;
                        break;
                    case 1:
                        currTab = type;
                        break;
                    case 2:
                        currTab = brands;
                        break;
                }
                currTab.setTextColor(resourceUtil.getResourceColor(context, R.color.tab_select_color));
                //设置线条移动的动画
                Animation animation = AnimationUtil.startTabLineAnimation(windowUtil, RentCarScreenCarActivity.this, previousPosition, position, fragments.length);
                tabLine.startAnimation(animation);
                previousPosition = position * windowUtil.getScreenWidth(RentCarScreenCarActivity.this) / fragments.length;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @OnClick({R.id.ok, R.id.cancel,R.id.price,R.id.type,R.id.brands})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok:
                intent.putExtra("Price", priceScreen);
                intent.putExtra("Type", typeScreen);
                intent.putExtra("Brand", brandScreen);
                setResult(Success, intent);
                finish();
                break;
            case R.id.cancel:
                setResult(Failure);
                finish();
                break;
            case R.id.price:
                vp.setCurrentItem(0);
                break;
            case R.id.type:
                vp.setCurrentItem(1);
                break;
            case R.id.brands:
                vp.setCurrentItem(2);
                break;
        }
    }

    public static void startActivity(Context context, int requestCode, ArrayList<CarModels.DataEntity> list, String Price, String Type, String Brand) {
        if (list.size() == 0) {
            return;
        }
        Intent intent = new Intent(context, RentCarScreenCarActivity.class);
        intent.putExtra("PriceScreen", Price);
        intent.putExtra("TypeScreen", Type);
        intent.putExtra("BrandScreen", Brand);
        intent.putParcelableArrayListExtra("CarModels", list);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

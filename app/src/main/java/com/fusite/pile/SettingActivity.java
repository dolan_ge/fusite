package com.fusite.pile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.PackageUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;
import com.zhy.http.okhttp.request.RequestCall;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Call;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/22.
 */
public class SettingActivity extends UtilActivity {

    public String TAG = "SettingActivity";
    @BindView(R.id.cacheSize)
    TextView cacheSize;
    @BindView(R.id.version)
    TextView version;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.setting_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setActivityView();
        setListener();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {
        try {
            cacheSize.setText(getFormatSize(getFolderSize(getCacheDir())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.v("version",PackageUtil.getVersionCode(context)+"");
        version.setText(PackageUtil.getVersionCode(context) + "");
    }

    public static long getFolderSize(File file) throws Exception {
        long size = 0;
        try {
            File[] fileList = file.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                // 如果下面还有文件
                if (fileList[i].isDirectory()) {
                    size = size + getFolderSize(fileList[i]);
                } else {
                    size = size + fileList[i].length();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return size;
    }

    /**
     * 格式化单位
     *
     * @param size
     * @return
     */
    public static String getFormatSize(double size) {
        double kiloByte = size / 1024;
        if (kiloByte < 1) {
            return size + "B";
        }

        double megaByte = kiloByte / 1024;
        if (megaByte < 1) {
            BigDecimal result1 = new BigDecimal(Double.toString(kiloByte));
            return result1.setScale(2, BigDecimal.ROUND_HALF_UP)
                    .toPlainString() + "KB";
        }

        double gigaByte = megaByte / 1024;
        if (gigaByte < 1) {
            BigDecimal result2 = new BigDecimal(Double.toString(megaByte));
            return result2.setScale(2, BigDecimal.ROUND_HALF_UP)
                    .toPlainString() + "MB";
        }

        double teraBytes = gigaByte / 1024;
        if (teraBytes < 1) {
            BigDecimal result3 = new BigDecimal(Double.toString(gigaByte));
            return result3.setScale(2, BigDecimal.ROUND_HALF_UP)
                    .toPlainString() + "GB";
        }
        BigDecimal result4 = new BigDecimal(teraBytes);
        return result4.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString() + "TB";
    }

    /**
     * 删除方法 这里只会删除某个文件夹下的文件，如果传入的directory是个文件，将不做处理 * *
     * @param directory
     */
    private void deleteFilesByDirectory(File directory) {
        if (directory != null && directory.exists() && directory.isDirectory()) {
            for (File item : directory.listFiles()) {
                if (item.isDirectory()) {
                    deleteFilesByDirectory(item);
                } else {
                    item.delete();
                }
            }
        }
    }

    @OnClick({R.id.about, R.id.clearCacheLayout, R.id.feedBackLayout, R.id.questLayout, R.id.versionLayout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.about:
                AboutActivity.startActivity(context);
                break;
            case R.id.clearCacheLayout:
                deleteFilesByDirectory(getCacheDir());
                cacheSize.setText("");
                break;
            case R.id.feedBackLayout:
                FeedbackActivity.startActivity(context);
                break;
            case R.id.questLayout:
                QuestActivity.startActivity(context);
                break;
            case R.id.versionLayout: //版本更新
                Map<String, String> map = new HashMap<>();
                ApiService.getPileService().GetVersionUpdateInfo(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.GetVersionUpdateInfo.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(versionUpdateInfo -> {
                            if (TextUtils.equals(BeanParam.Failure, versionUpdateInfo.getIsSuccess())) {
                                Error error = errorParamUtil.checkReturnState(versionUpdateInfo.getReturnStatus());
                                toastUtil.toastError(context, error, null);
                                return null;
                            }
                            return versionUpdateInfo;
                        })
                        .subscribe(versionUpdateInfo -> {
                            if (versionUpdateInfo == null) {
                                return;
                            }
                            float currVersion = PackageUtil.getVersionCode(context);
                            float netVersion = Float.valueOf(versionUpdateInfo.getData().getVersion());
                            if (netVersion > currVersion) { //开始更新
                                startUpdate(versionUpdateInfo.getData().getPath());
                            } else {
                                Toast.makeText(context, "已经是最新版本", Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
        }
    }

    public void startUpdate(String downloadPath) {
        //构造请求
        RequestCall requestCall = OkHttpUtils
                .get()
                .url(downloadPath)
                .build();

        //打开进度栏
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("下载进度");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "取消下载", (dialog, which) -> {
            progressDialog.dismiss();
        });
        progressDialog.setOnDismissListener(dialog -> requestCall.cancel());
        progressDialog.show();
        //开始下载
        requestCall.execute(new FileCallBack(Environment.getExternalStorageDirectory().getAbsolutePath(), "fusite_android.apk") {
            /**
             * 下载出现错误
             * @param call
             * @param e
             * @param id
             */
            @Override
            public void onError(Call call, Exception e, int id) {
                progressDialog.dismiss();
            }

            /**
             * 下载完成调用
             * @param response
             * @param id
             */
            @Override
            public void onResponse(File response, int id) {
                progressDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(response),
                        "application/vnd.android.package-archive");
                context.startActivity(intent);
            }

            /**
             * 加载进度
             * @param progress
             * @param total
             * @param id
             */
            @Override
            public void inProgress(float progress, long total, int id) {
                progressDialog.setProgress((int) (progress * 100));
            }
        });
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SettingActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

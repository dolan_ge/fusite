package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.api.ChinaService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.UpdateVerifiedInfo;
import com.fusite.bean.UploadImage;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.BitmapUtil;
import com.fusite.view.CustomDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/16.
 */
public class UploadVerifiedInfoActivity extends UtilActivity {

    public String TAG = "UploadVerifiedInfoActivity";
    @BindView(R.id.driverLicense)
    ImageView driverLicense;
    @BindView(R.id.idCard1)
    ImageView idCard1;
    @BindView(R.id.idCard2)
    ImageView idCard2;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.cardNo)
    EditText cardNo;

    private static final int DriverLicenseRequestCode = 0x001;
    private static final int IdCard1RequestCode = 0x002;
    private static final int IdCard2RequestCode = 0x003;

    private String driverLicensePath;
    private String idCard1Path;
    private String idCard2Path;

    private String driverLicenseUrl;
    private String idCard1Url;
    private String idCard2Url;

    private CustomDialog loading;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.upload_verified_info);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {

    }

    @OnClick({R.id.driverLicenseLayout, R.id.idCard1Layout, R.id.idCard2Layout, R.id.btnOk, R.id.exampleLayout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.driverLicenseLayout:
                startGallery(DriverLicenseRequestCode);
                break;
            case R.id.idCard1Layout:
                startGallery(IdCard1RequestCode);
                break;
            case R.id.idCard2Layout:
                startGallery(IdCard2RequestCode);
                break;
            case R.id.btnOk:

                String nameStr = name.getText().toString();
                String cardNoStr = cardNo.getText().toString();

                if (TextUtils.isEmpty(driverLicensePath) || TextUtils.isEmpty(idCard1Path) || TextUtils.isEmpty(idCard2Path)) {
                    Toast.makeText(context, "请选择所有图片!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(nameStr) || TextUtils.isEmpty(cardNoStr)) {
                    Toast.makeText(context, "填写姓名和身份证号码!", Toast.LENGTH_SHORT).show();
                    return;
                }

                loading = dialogUtil.startLoading(context, loading, R.string.loadingText);

                Observable.just(driverLicensePath)
                        .observeOn(Schedulers.io())
                        .map(s -> {
                            BitmapUtil bitmapUtil1 = BitmapUtil.getInstance();
                            return bitmapUtil1.Bitmap2Bytes(bitmapUtil1.getimage(s, 400));
                        })
                        .flatMap(bytes ->
                                ApiService.getPileService().upload(new MultipartBody.Builder().setType(MultipartBody.FORM)
                                        .addFormDataPart("verify1", "verify1.png", RequestBody.create(MediaType.parse("image/png/jpg; charset=utf-8"), bytes)).build())
                        )
                        .map(uploadImage -> {
                            if (TextUtils.equals(BeanParam.Failure, uploadImage.getIsSuccess())) {
                                Toast.makeText(context, "上传失败", Toast.LENGTH_SHORT).show();
                                dialogUtil.dismiss(loading);
                                return null;
                            }
                            driverLicenseUrl = uploadImage.getData().get(0).getUrl();
                            return idCard1Path;
                        })
                        .map(s -> {
                            BitmapUtil bitmapUtil1 = BitmapUtil.getInstance();
                            return bitmapUtil1.Bitmap2Bytes(bitmapUtil1.getimage(s, 400));
                        })
                        .flatMap(bytes -> ApiService.getPileService().upload(new MultipartBody.Builder().setType(MultipartBody.FORM)
                                .addFormDataPart("verify2", "verify2.png", RequestBody.create(MediaType.parse("image/png/jpg; charset=utf-8"), bytes)).build()))
                        .map(uploadImage -> {
                            if (TextUtils.equals(BeanParam.Failure, uploadImage.getIsSuccess())) {
                                Toast.makeText(context, "上传失败", Toast.LENGTH_SHORT).show();
                                dialogUtil.dismiss(loading);
                                return null;
                            }
                            idCard1Url = uploadImage.getData().get(0).getUrl();
                            return idCard2Path;
                        })
                        .map(s -> {
                            BitmapUtil bitmapUtil1 = BitmapUtil.getInstance();
                            return bitmapUtil1.Bitmap2Bytes(bitmapUtil1.getimage(s, 400));
                        })
                        .flatMap(bytes -> ApiService.getPileService().upload(new MultipartBody.Builder().setType(MultipartBody.FORM)
                                .addFormDataPart("verify3", "verify3.png", RequestBody.create(MediaType.parse("image/png/jpg; charset=utf-8"), bytes)).build()))
                        .map(uploadImage -> {
                            if (TextUtils.equals(BeanParam.Failure, uploadImage.getIsSuccess())) {
                                Toast.makeText(context, "上传失败", Toast.LENGTH_SHORT).show();
                                dialogUtil.dismiss(loading);
                                return null;
                            }
                            idCard2Url = uploadImage.getData().get(0).getUrl();
                            return idCard2Url;
                        })
                        .flatMap(s -> {
                            Login.DataEntity dataEntity = application.getLogin().getData();
                            Map<String, String> map = new HashMap<String, String>();
                            map.put(RequestParam.UpdateVerifiedInfo.Token, dataEntity.getToken());
                            map.put(RequestParam.UpdateVerifiedInfo.DrivingLicence, driverLicenseUrl);
                            map.put(RequestParam.UpdateVerifiedInfo.IdentityPositive, idCard1Url);
                            map.put(RequestParam.UpdateVerifiedInfo.IdentityNegative, idCard2Url);
                            map.put(RequestParam.UpdateVerifiedInfo.Name, nameStr);
                            map.put(RequestParam.UpdateVerifiedInfo.IdentityNum, cardNoStr);
                            Map<String, String> mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, NetParam.sign, RequestParam.UpdateVerifiedInfo.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));
                            return ChinaService.chinaRequest(mapParam, UpdateVerifiedInfo.class);
                        })
                        .map(updateVerifiedInfo -> {
                            if (TextUtils.equals(BeanParam.Failure, updateVerifiedInfo.getIsSuccess())) {
                                Error error = errorParamUtil.checkReturnState(updateVerifiedInfo.getReturnStatus());
                                toastUtil.toastError(context, error, null);
                                return null;
                            }
                            return updateVerifiedInfo;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(updateVerifiedInfo -> {
                            dialogUtil.dismiss(loading);
                            if (updateVerifiedInfo == null) {
                                return;
                            }
                            Toast.makeText(context, "上传成功", Toast.LENGTH_SHORT).show();
                            finish();
                        }, new ThrowableAction(this));
                break;
            case R.id.exampleLayout:
                ExampleActivity.startActivity(context);
                break;
        }
    }


    private static class GalleryHandler implements GalleryFinal.OnHanlderResultCallback {

        private WeakReference<UploadVerifiedInfoActivity> mActivity;

        public GalleryHandler(UploadVerifiedInfoActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
            UploadVerifiedInfoActivity activity = mActivity.get();

            if (activity == null) {
                return;
            }

            String path = resultList.get(0).getPhotoPath();
            switch (reqeustCode) {
                case DriverLicenseRequestCode:
                    Picasso.with(activity.context).load(new File(path)).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).into(activity.driverLicense);
                    activity.driverLicensePath = path;
                    break;
                case IdCard1RequestCode:
                    Picasso.with(activity.context).load(new File(path)).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).into(activity.idCard1);
                    activity.idCard1Path = path;
                    break;
                case IdCard2RequestCode:
                    Picasso.with(activity.context).load(new File(path)).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).into(activity.idCard2);
                    activity.idCard2Path = path;
                    break;
            }
        }

        @Override
        public void onHanlderFailure(int requestCode, String errorMsg) {

        }

    }

    /**
     * 上传图片
     */
    public Observable<UploadImage> uploadFile(String path) {
        return ApiService.getPileService().upload(new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("verify", "verify.png", RequestBody.create(MediaType.parse("image/png/jpg; charset=utf-8"), new File(path))).build())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(uploadImage -> {
                    if (TextUtils.equals(BeanParam.Failure, uploadImage.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(uploadImage.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return uploadImage;
                });
    }

    public void startGallery(int requestCode) {
        GalleryFinal.openGallerySingle(requestCode, new GalleryHandler(this));
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, UploadVerifiedInfoActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }

}

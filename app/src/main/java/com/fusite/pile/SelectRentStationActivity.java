package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ListView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.DistrictListAdapter;
import com.fusite.adpater.RentStationListAdapter;
import com.fusite.bean.Error;
import com.fusite.bean.NearbyRentStation;
import com.fusite.impl.NearbyRentStationDaoImpl;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jelly on 2016/4/21.
 */
public class SelectRentStationActivity extends UtilActivity{

    public String TAG = "SelectRentStationActivity";

    public static final int Success = 0x001;

    public static final int Failure = 0x002;
    @BindView(R.id.districtList)
    ListView districtList;
    @BindView(R.id.stationList)
    ListView stationList;

    private NearbyRentStationDaoImpl nearbyRentStationDao;

    private static final int msgNearby = 0x003;

    private DistrictListAdapter districtListAdapter;

    private RentStationListAdapter rentStationListAdapter;

    private int currDistrict = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.select_rent_station);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {
        nearbyRentStationDao = new NearbyRentStationDaoImpl(context, new MyHandler(this), msgNearby);
    }

    @Override
    public void setListener() {
        setDistrictListItemListener();
        setStationListItemListener();
    }

    @Override
    public void setActivityView() {
        nearbyRentStationDao.getNearbyRentStation(intent.getStringExtra("CityCode"));
    }

    public void setDistrictListItemListener() {
        districtList.setOnItemClickListener((parent, view, position, id) -> {
            currDistrict = position;
            districtListAdapter.setSelectItem(position);
            districtListAdapter.notifyDataSetChanged();
            List<NearbyRentStation.DataEntity> dataEntity = nearbyRentStationDao.nearbyRentStation.getData();
            rentStationListAdapter.setList(dataEntity.get(position).getStations());
            rentStationListAdapter.notifyDataSetChanged();
        });
    }

    public void setStationListItemListener(){
        stationList.setOnItemClickListener((parent, view, position, id) -> {
            List<NearbyRentStation.DataEntity.StationsEntity> list = nearbyRentStationDao.nearbyRentStation.getData().get(currDistrict).getStations();
            intent.putExtra("returnStation",list.get(position).getOrgName());
            intent.putExtra("returnOrgId",list.get(position).getOrgId());
            setResult(Success,intent);
            finish();
        });
    }

    private static class MyHandler extends Handler{

        private WeakReference<SelectRentStationActivity> mActivity;

        public MyHandler(SelectRentStationActivity activity){
            mActivity = new WeakReference<SelectRentStationActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            SelectRentStationActivity activity = mActivity.get();
            if(activity == null){
                return;
            }
            switch (msg.what){
                case msgNearby:
                    if (TextUtils.equals("N", activity.nearbyRentStationDao.nearbyRentStation.getIsSuccess())) {
                        Error error = activity.errorParamUtil.checkReturnState(activity.nearbyRentStationDao.nearbyRentStation.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, error, null);
                        return;
                    }
                    activity.districtListAdapter = new DistrictListAdapter(activity.nearbyRentStationDao.nearbyRentStation.getData(), activity.context);
                    activity.districtList.setAdapter(activity.districtListAdapter);
                    activity.rentStationListAdapter = new RentStationListAdapter(activity.nearbyRentStationDao.nearbyRentStation.getData().get(0).getStations(), activity.context);
                    activity.stationList.setAdapter(activity.rentStationListAdapter);
                    break;
            }
        }
    }

    /**
     * 打开界面
     */
    public static void startActivity(Context context, int requestCode, String CityCode) {
        Intent intent = new Intent(context, SelectRentStationActivity.class);
        intent.putExtra("CityCode", CityCode);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Login;
import com.fusite.bean.UpdateInfo;
import com.fusite.fragment.TabFragment;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.CircleTransform;
import com.fusite.utils.ToastUtils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import cn.bingoogolapple.badgeview.BGABadgeImageView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * 主框架界面
 * Created by Jelly on 2016/2/25.
 */
public class MainActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "MainActivity";
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.money)
    TextView money;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.station_icon)
    ImageView stationIcon;
    @BindView(R.id.station_title)
    TextView stationTitle;
    @BindView(R.id.fix_icon)
    ImageView fixIcon;
    @BindView(R.id.fix_text)
    TextView fixText;
    BGABadgeImageView userIcon;
    @BindView(R.id.noread)
    ImageView noread;

    private TabFragment tabFragment;
    private FragmentManager fm;

    private ImageView currTabImage;
    private TextView currTabText;
    private int currTab;

    private int[] tabImage = new int[]{R.drawable.dianzhuan, R.drawable.service};
    private int[] onTabImage = new int[]{R.drawable.ondianzhuan, R.drawable.onservice};

    public static final int pileTab = 0;
    public static final int serviceTab = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.main);
        ButterKnife.bind(this);
        userIcon = findViewById(R.id.user_icon);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }


    @Override
    protected void onResume() {
        super.onResume();
        loadDrawLayout(); //在获取焦点之前，加载侧滑栏数据，如果有数据的话
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (fm == null) {
            fm = getSupportFragmentManager();
        }
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(tabFragment);
    }

    @Override
    public void init() {
        application.setCurrTab(pileTab); //初始化为第一页
    }

    @Override
    public void loadObjectAttribute() {
        if (currTabImage == null) {
            currTabImage = stationIcon;
        }
        if (currTabText == null) {
            currTabText = stationTitle;
        }
        if (currTab == 0) {
            currTab = R.id.station;
        }
    }

    @Override
    public void setListener() {
        addDrawListener();
    }

    @Override
    public void setActivityView() {
        if (tabFragment == null) {
            tabFragment = new TabFragment();
        }
        if (fm == null) {
            fm = getSupportFragmentManager();
        }
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.real_content, tabFragment);
        ft.commit();
    }


    @OnClick({R.id.user_layout, R.id.wallet_layout, R.id.appoint_layout, R.id.order_layout, R.id.news_layout, R.id.collect_layout, R.id.set_layout, R.id.station, R.id.service})
    public void onClick(View view) {
        if (fm == null) {
            fm = getSupportFragmentManager();
        }
        FragmentTransaction ft = fm.beginTransaction();
        switch (view.getId()) {
            case R.id.user_layout:
                if (!application.isLogin()) { //没有登录，进入登录界面
                    LoginActivity.startActivity(context);
                    return;
                }
                AccountManageActivity.startActivity(context);
                break;
            case R.id.wallet_layout:
                if (!application.isLogin()) { //没有登录，进入登录界面
                    LoginActivity.startActivity(context);
                    return;
                }
                MyWalletActivity.staticActivity(this);
                break;
            case R.id.appoint_layout:
                if (!application.isLogin()) { //没有登录，进入登录界面
                    LoginActivity.startActivity(context);
                    return;
                }
                MyAppointActivity.startActivity(MainActivity.this);
                break;
            case R.id.order_layout:
                if (!application.isLogin()) { //没有登录，进入登录界面
                    LoginActivity.startActivity(context);
                    return;
                }
                MyOrderActivity.startAppActivity(MainActivity.this);
                break;
            case R.id.news_layout:
                if (!application.isLogin()) {
                    LoginActivity.startActivity(context);
                    return;
                }
                noread.setVisibility(View.GONE);
                MyNewsActivity.startActivity(context);
                break;
            case R.id.collect_layout:
                if (!application.isLogin()) {
                    LoginActivity.startActivity(context);
                    return;
                }
                MyCollectActivity.startActivity(context);
                break;
            case R.id.set_layout:
                SettingActivity.startActivity(context);
                break;
            case R.id.station:
                if (R.id.station == currTab) {
                    return;
                }
                currTab = R.id.station;
                currTabImage.setImageResource(tabImage[Integer.parseInt(currTabImage.getTag().toString())]);
                currTabImage = stationIcon;
                currTabImage.setImageResource(onTabImage[Integer.parseInt(currTabImage.getTag().toString())]);
                currTabText.setTextColor(resourceUtil.getResourceColor(context, R.color.defaultTabColor));
                currTabText = stationTitle;
                currTabText.setTextColor(resourceUtil.getResourceColor(context, R.color.actionbar_bg));
                application.setCurrTab(pileTab);

                if (tabFragment.isAdded()) {
                    ft.show(tabFragment);
                }
                ft.commit();

                tabFragment.getMapFragment().switchTab(pileTab);
                if (tabFragment.isSwitchList()) {
                    tabFragment.switchListFragment();
                }
                break;
            case R.id.service:
                if (R.id.service == currTab) {
                    return;
                }
                currTab = R.id.service;
                currTabImage.setImageResource(tabImage[Integer.parseInt(currTabImage.getTag().toString())]);
                currTabImage = fixIcon;
                currTabImage.setImageResource(onTabImage[Integer.parseInt(currTabImage.getTag().toString())]);
                currTabText.setTextColor(resourceUtil.getResourceColor(context, R.color.defaultTabColor));
                currTabText = fixText;
                currTabText.setTextColor(resourceUtil.getResourceColor(context, R.color.actionbar_bg));
                application.setCurrTab(serviceTab);

                if (tabFragment.isAdded()) {
                    ft.show(tabFragment);
                }

                ft.commit();

                tabFragment.getMapFragment().switchTab(serviceTab);
                if (tabFragment.isSwitchList()) {
                    tabFragment.switchListFragment();
                }

                break;
        }
    }


    /**
     * 加载侧滑栏里面的数据，如果用户没有登录直返回,在这里加载用户头像
     */
    public void loadDrawLayout() {
        application = (MyApplication) getApplication();
        if (!application.isLogin()) { //没有登录
            userName.setText(R.string.slide_login_hint);
            userIcon.setImageResource(R.drawable.slide_portrait);
            money.setText("¥0.00");
            return;
        }
        Login loginDao = application.getLogin();
        if (TextUtils.isEmpty(loginDao.getData().getCustName())) { //昵称
            userName.setText("昵称");
        } else {
            userName.setText(loginDao.getData().getCustName());
        }
        if (!TextUtils.isEmpty(loginDao.getData().getBalanceAmt())) { //余额
            money.setText("¥" + loginDao.getData().getBalanceAmt());
        }
        if (!TextUtils.isEmpty(loginDao.getData().getUsericon())) {
            Uri uri = Uri.parse(loginDao.getData().getUsericon());
            Picasso.with(context).load(application.getLogin().getData().getUsericon()).transform(new CircleTransform()).into(userIcon);
        }
        Bitmap avatarBadgeBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_vip);
        userIcon.showDrawableBadge(avatarBadgeBitmap);
    }


    public void addDrawListener() {
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                if(!application.isLogin()){
                    return;
                }
                Login.DataEntity loginData = application.getLogin().getData();
                Map<String, String> map = new HashMap<String, String>();
                map.put(RequestParam.UpdateInfo.Token, loginData.getToken());
                ApiService.getPileService().UpdateInfo(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.UpdateInfo.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(updateInfo -> {
                            if (TextUtils.equals(BeanParam.Failure, updateInfo.getIsSuccess())) {
                                return null;
                            }
                            return updateInfo;
                        })
                        .subscribe(updateInfo -> {
                            if(updateInfo == null){
                                return;
                            }
                            UpdateInfo.DataEntity infoData = updateInfo.getData();
                            //更新余额
                            application.getLogin().getData().setBalanceAmt(infoData.getBalanceAmt());
                            application.cacheLogin();
                            money.setText("¥"+infoData.getBalanceAmt());
                            //实名认证
                            if(TextUtils.equals(infoData.getIsCertification(),BeanParam.UpdateInfo.isCertification)){
                                application.getLogin().getData().setVerified(BeanParam.UpdateInfo.isCertification);
                                application.cacheLogin();
                            }else{
                                application.getLogin().getData().setVerified(BeanParam.UpdateInfo.noCertification);
                                application.cacheLogin();
                            }
                            //新消息
                            if(TextUtils.equals(infoData.getHasMessage(),BeanParam.UpdateInfo.HasMessage)){
                                noread.setVisibility(View.VISIBLE);
                            }else{
                                noread.setVisibility(View.GONE);
                            }
                        },new ThrowableAction(context));
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    /**
     * 切换侧滑栏
     */
    public void switchDrawLayout(boolean isSwitch) {
        if (isSwitch) {
            drawerLayout.openDrawer(Gravity.LEFT);
        } else {
            drawerLayout.closeDrawer(Gravity.LEFT);
        }
    }


    /**
     * 开启界面
     */
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

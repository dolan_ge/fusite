package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.api.ChinaService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.BitmapUtil;
import com.fusite.view.CustomDialog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.finalteam.galleryfinal.FunctionConfig;
import cn.finalteam.galleryfinal.GalleryFinal;
import cn.finalteam.galleryfinal.model.PhotoInfo;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/13.
 */
public class SendDynamicActivity extends UtilActivity {

    public String TAG = "SendDynamicActivity";
    @BindView(R.id.input)
    EditText input;
    @BindView(R.id.image1)
    ImageView image1;
    @BindView(R.id.image2)
    ImageView image2;
    @BindView(R.id.image3)
    ImageView image3;
    @BindView(R.id.image4)
    ImageView image4;
    @BindView(R.id.image5)
    ImageView image5;
    @BindView(R.id.image6)
    ImageView image6;
    @BindView(R.id.image7)
    ImageView image7;
    @BindView(R.id.image8)
    ImageView image8;
    @BindView(R.id.image9)
    ImageView image9;
    @BindView(R.id.images2)
    LinearLayout images2;

    public static final int PhotoRequestGallery = 0x001;
    public static final int MaxPhotoNum = 9;

    private ImageView[] images;

    private List<String> urls;

    private List<PhotoInfo> resultList;

    private CustomDialog loading;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.send_dynamic);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void init() {
        resultList = new ArrayList<>();
    }

    @Override
    public void loadObjectAttribute() {
        if (images == null) {
            images = new ImageView[]{image1, image2, image3, image4, image5, image6, image7, image8, image9};
        }
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {

    }

    public void startGallery(int index) {
        int max = MaxPhotoNum - index;
        //修改最大的图片选择数量
        FunctionConfig config = new FunctionConfig.Builder()
                .setEnableCamera(true)
                .setMutiSelectMaxSize(max)
                .build();
        GalleryFinal.openGalleryMuti(0x001, config, new MyHandler(this));
    }

    private static class MyHandler implements GalleryFinal.OnHanlderResultCallback {

        private WeakReference<SendDynamicActivity> mActivity;

        private MyHandler(SendDynamicActivity activity) {
            mActivity = new WeakReference<SendDynamicActivity>(activity);
        }

        @Override
        public void onHanlderFailure(int requestCode, String errorMsg) {

        }

        @Override
        public void onHanlderSuccess(int reqeustCode, List<PhotoInfo> resultList) {
            SendDynamicActivity activity = mActivity.get();

            if (activity == null) {
                return;
            }

            if (activity.resultList == null) {
                activity.resultList = new ArrayList<PhotoInfo>();
            }else{
                activity.resultList.clear();
            }

            for (int i = 0; i < resultList.size(); i++) {
                activity.resultList.add(resultList.get(i));
            }
            activity.setImage();
        }
    }


    /**
     * 设置图片
     */
    public void setImage() {
        for (int i = 0; i < resultList.size(); i++) {
            Picasso.with(context).load(new File(resultList.get(i).getPhotoPath())).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).resize(100,100).into(images[i]);
            if (images[i].isClickable()) {
                images[i].setClickable(false);
            }
        }

        if (resultList.size() >= 5) {
            images2.setVisibility(View.VISIBLE);
        }

        if (resultList.size() < 9) {
            images[resultList.size()].setImageResource(R.drawable.add_img);
            images[resultList.size()].setOnClickListener(v -> startGallery(resultList.size()));
        }

    }

    /**
     * 上传动态
     */
    public void uploadDynamic() {
        String content = input.getText().toString();
        if (TextUtils.isEmpty(content)) {
            Toast.makeText(context, "内容不能为空", Toast.LENGTH_SHORT).show();
            dialogUtil.dismiss(loading);
            return;
        } else if (content.length() > 300) {
            Toast.makeText(context, "内容不能超过300字", Toast.LENGTH_SHORT).show();
            dialogUtil.dismiss(loading);
            return;
        }
        Login.DataEntity dataEntity = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.SendDynamic.Token, dataEntity.getToken());
        map.put(RequestParam.SendDynamic.Content, content);
        if (urls != null) {
            for (int i = 0; i < urls.size(); i++) {
                map.put(RequestParam.SendDynamic.Url + (i + 1), urls.get(i));
            }
        }
        Map<String, String> mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, NetParam.sign, RequestParam.SendDynamic.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));
        ChinaService.SendDynamic(mapParam)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(dynamic -> {
                    if (TextUtils.equals(BeanParam.Failure, dynamic.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(dynamic.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return dynamic;
                })
                .subscribe(dynamic -> {
                    dialogUtil.dismiss(loading);
                    if (dynamic == null) {
                        return;
                    }
                    Toast.makeText(context, "发表成功", Toast.LENGTH_SHORT).show();
                    finish();
                }, new ThrowableAction(this));
    }

    public void uploadDynamic(int index) {
        Observable.just(resultList.get(index).getPhotoPath())
                .observeOn(Schedulers.newThread())
                .map(s -> {
                    BitmapUtil bitmapUtil1 = BitmapUtil.getInstance();
                    return bitmapUtil1.Bitmap2Bytes(bitmapUtil1.getimage(s,200));
                })
                .flatMap(bytes -> ApiService.getPileService().upload(new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("dynamic", "dynamic.png", RequestBody.create(MediaType.parse("image/png/jpg; charset=utf-8"), bytes)).build()))
                .map(uploadImage -> {
                    if (TextUtils.equals(BeanParam.Failure, uploadImage.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(uploadImage.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return uploadImage;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(uploadImage -> {

                    if (uploadImage == null) {
                        return;
                    }

                    if (urls == null) {
                        urls = new ArrayList<String>();
                    }

                    urls.add(uploadImage.getData().get(0).getUrl()); //上传成功后保存图片地址

                    if (index + 1 == resultList.size()) { //所有图片都上传完成了
                        uploadDynamic();
                    } else {
                        uploadDynamic(index + 1);
                    }

                }, new ThrowableAction(this));

    }

    @OnClick({R.id.image1, R.id.send})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image1:
                startGallery(0);
                break;
            case R.id.send:
                loading = dialogUtil.startLoading(context,loading,R.string.loadingText);

                if (resultList == null || resultList.size() == 0) { //不上传图片
                    uploadDynamic();
                }else{
                    uploadDynamic(0); //上传图片
                }

                break;
        }
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SendDynamicActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ChinaService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.Suggest;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 反馈
 * Created by Jelly on 2016/6/22.
 */
public class FeedbackActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "FeedbackActivity";
    @BindView(R.id.input)
    EditText input;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.feedback_layout);
        ButterKnife.bind(this);
        setActivityView();
    }

    @Override
    public void setActivityView() {

    }

    @OnClick(R.id.ok)
    public void onClick() {

        String suggestStr = input.getText().toString();

        if(TextUtils.isEmpty(suggestStr)){
            Toast.makeText(context,"建议不能为空", Toast.LENGTH_SHORT).show();
            return;
        }

        if(suggestStr.length() > 300){
            Toast.makeText(context,"建议过长", Toast.LENGTH_SHORT).show();
            return;
        }

        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.SendSuggest.Token, loginData.getToken());
        map.put(RequestParam.SendSuggest.Suggestion,suggestStr);
        Map<String, String> mapParam = NetParam.getParamMap(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, NetParam.sign, RequestParam.SendSuggest.InterfaceName, NetParam.fields, NetParam.spliceCondition(map));
        ChinaService.chinaRequest(mapParam, Suggest.class)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(suggest -> {
                    if (TextUtils.equals(BeanParam.Failure, suggest.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(suggest.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return suggest;
                })
                .subscribe(suggest -> {
                    if (suggest == null) {
                        return;
                    }
                    finish();
                }, throwable -> {

                });
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, FeedbackActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

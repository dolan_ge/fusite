package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.fusite.activities.UtilActivity;
import com.fusite.param.BeanParam;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jelly on 2016/6/23.
 */
public class SelectInvoiceActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "SelectInvoiceActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.select_invoice_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {

    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {

    }

    @OnClick({R.id.chargeInvoiceLayout,R.id.invoiceHistoryLayout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.invoiceHistoryLayout: //开票历史
                InvoiceHistoryActivity.startActivity(context);
                break;
            case R.id.chargeInvoiceLayout: //充电开票
                InvoicingActivity.startActivity(context,BeanParam.Invoice.EVC);
                break;
        }
    }

    public static void startActivity(Context context){
        Intent intent = new Intent(context,SelectInvoiceActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

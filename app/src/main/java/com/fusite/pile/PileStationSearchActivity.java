package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.SearchListAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.PileStation;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.LogUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 搜索电桩
 * Created by Jelly on 2016/3/28.
 */
public class PileStationSearchActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "PileStationSearchActivity";
    @BindView(R.id.search_et)
    EditText searchEt;
    @BindView(R.id.list)
    ListView list;
    /**
     * 搜索集合
     */
    private List<PileStation> pileStations;
    /**
     * 搜索出来的数据
     */
    private List<PileStation> showPileStations;
    /**
     * 适配器
     */
    private SearchListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.search);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void loadObjectAttribute() {
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.PileStations.Position, RequestParam.ServiceStations.PositionValue);
        ApiService.getPileService().getPileStations
                (NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.PileStations.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .map(pileStations -> setCondition(pileStations))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pileStations -> PileStationSearchActivity.this.pileStations = pileStations
                        , throwable -> {
                            LogUtil.v(TAG, throwable.getMessage());
                        });
    }

    @Override
    public void setListener() {
        setInputListener();
        setSearchListItemClickListener();
    }

    @Override
    public void setActivityView() {

    }

    /**
     * 设置搜索列表的点击监听事件
     */
    public void setSearchListItemClickListener() {
        list.setOnItemClickListener((parent, view, position, id) -> {
            if (showPileStations.get(position) != null) {
                PileStationInfoActivity.startActivity(context, showPileStations.get(position).getEvc_stations_getall().getOrgId());
            }
        });
    }

    /**
     * 设置输入监听
     */
    public void setInputListener() {
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String searchText = searchEt.getText().toString();
                LogUtil.v(TAG, "开始搜索：" + searchText);
                if (pileStations == null || pileStations.size() == 0) {
                    return;
                }
                try {
                    showPileStations = searchUtil.searchListOnRegExp(searchText, pileStations);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
                if (showPileStations != null) {
                    adapter = new SearchListAdapter(context, application, showPileStations);
                    list.setAdapter(adapter);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * 设置条件
     */
    public List<PileStation> setCondition(List<PileStation> daos) {
        for (int i = 0; i < daos.size(); i++) {
            PileStation dao = daos.get(i);
            PileStation.EvcStationsGetallEntity entity = dao.getEvc_stations_getall();
            dao.setCondition(entity.getOrgName() + entity.getAddr() + entity.getCity());
        }
        return daos;
    }

    /**
     * 从附近界面跳入到搜索界面
     *
     * @param context
     */
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, PileStationSearchActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ListView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.InvoiceHistoryAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.DateSort;
import com.fusite.bean.Error;
import com.fusite.bean.InvoiceHistory;
import com.fusite.bean.Login;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.DateUtil;
import com.fusite.utils.ListUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/22.
 */
public class
InvoiceHistoryActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "InvoiceListActivity";
    @BindView(R.id.list)
    ListView list;

    private InvoiceHistoryAdapter adpater;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.invoice_list_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void setActivityView() {
        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.InvoiceHistory.Token,loginData.getToken());
        ApiService.getPileService().InvoiceHistory(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.InvoiceHistory.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(invoiceHistory -> {
                    if(TextUtils.equals(BeanParam.Failure,invoiceHistory.getIsSuccess())){
                        Error error = errorParamUtil.checkReturnState(invoiceHistory.getReturnStatus());
                        toastUtil.toastError(context,error,null);
                        return null;
                    }
                    return invoiceHistory;
                })
                .subscribe(invoiceHistory -> {
                    if (invoiceHistory == null) {
                        return;
                    }

                    //排序
                    List<DateSort> dateSortList = new ArrayList<DateSort>();
                    List<InvoiceHistory.DataEntity> tempList = invoiceHistory.getData();
                    for (int i = 0; i < tempList.size(); i++) {
                        tempList.get(i).setDate(DateUtil.getDate(tempList.get(i).getCreateDate().replace("T"," ")));
                        dateSortList.add(tempList.get(i));
                    }

                    ListUtil.sortTime(dateSortList);

                    adpater = new InvoiceHistoryAdapter(context, dateSortList);
                    list.setAdapter(adpater);
                }, throwable -> {

                });
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, InvoiceHistoryActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

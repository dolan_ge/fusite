package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.RentOrderInfo;
import com.fusite.impl.RentOrderInfoDaoImpl;
import com.fusite.impl.RentOrdersDaoImpl;
import com.fusite.param.BeanParam;
import com.fusite.utils.DateUtil;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 已完成租车订单界面显示
 * Created by Jelly on 2016/4/27.
 */
public class RentCarOrderInfoActivity extends UtilActivity {

    public String TAG = "RentCarOrderInfoActivity";
    @BindView(R.id.orgName)
    TextView orgName;
    @BindView(R.id.brand)
    TextView brand;
    @BindView(R.id.license)
    TextView license;
    @BindView(R.id.pickCarStation)
    TextView pickCarStation;
    @BindView(R.id.pickCarTime)
    TextView pickCarTime;
    @BindView(R.id.returnCarStation)
    TextView returnCarStation;
    @BindView(R.id.returnCarTime)
    TextView returnCarTime;
    @BindView(R.id.orderState)
    ImageView orderState;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.payType)
    TextView payType;
    @BindView(R.id.phone)
    TextView phone;

    private String orderNo;

    private RentOrderInfoDaoImpl rentOrderInfoDao;

    private static final int msgOrderInfo = 0x001;

    private static final String DateFormat = "yyyy-MM-dd HH:mm";

    private CustomDialog loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.rent_finish_order_info);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onStop() {
        super.onStop();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void loadObjectAttribute() {
        orderNo = intent.getStringExtra("orderNo");
        rentOrderInfoDao = new RentOrderInfoDaoImpl(context, new MyHandler(this), msgOrderInfo);
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {
        loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
        Login.DataEntity dataEntity = application.getLogin().getData();
        rentOrderInfoDao.getRentOrderInfo(dataEntity.getToken(), dataEntity.getCustID(), orderNo);
    }

    @OnClick({R.id.sendCommitLayout, R.id.phoneLayout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendCommitLayout:
                SendCommitActivity.startActivity(context,SendCommitActivity.RentCommit,orderNo);
                break;
            case R.id.phoneLayout:
                Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + phone.getText().toString()));
                startActivity(intent);
                break;
        }
    }


    private static class MyHandler extends Handler {

        private WeakReference<RentCarOrderInfoActivity> mActivity;

        public MyHandler(RentCarOrderInfoActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            RentCarOrderInfoActivity activity = mActivity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case msgOrderInfo:
                    activity.dialogUtil.dismiss(activity.loading);
                    if (TextUtils.equals("N", activity.rentOrderInfoDao.rentOrderInfo.getIsSuccess())) {
                        Error error = activity.errorParamUtil.checkReturnState(activity.rentOrderInfoDao.rentOrderInfo.getReturnStatus());
                        activity.toastUtil.toastError(activity.context, error, null);
                        return;
                    }
                    RentOrderInfo.DataEntity dataEntity = activity.rentOrderInfoDao.rentOrderInfo.getData();
                    activity.orgName.setText(dataEntity.getOrgName());
                    activity.brand.setText(dataEntity.getBrand());
                    activity.license.setText(dataEntity.getLicensePlate());
                    activity.pickCarTime.setText(DateUtil.getSdfDate(dataEntity.getRentBeginTime(), DateFormat));
                    activity.returnCarTime.setText(DateUtil.getSdfDate(dataEntity.getRentEndTime(), DateFormat));
                    activity.pickCarStation.setText(dataEntity.getOrgName());
                    activity.returnCarStation.setText(dataEntity.getReturnOrgName());
                    activity.price.setText(dataEntity.getRealAmt());
                    activity.phone.setText(dataEntity.getTel());

                    if (TextUtils.equals(RentOrdersDaoImpl.FinishRent, dataEntity.getOrderStatus())) {
                        activity.orderState.setImageResource(R.drawable.orderfinish);
                    } else {
                        activity.orderState.setImageResource(R.drawable.ordercancel);
                    }

                    //支付类型
                    if (TextUtils.equals(BeanParam.RentOrderInfo.BalancePay, dataEntity.getPayType())) {
                        activity.payType.setText("余额支付");
                    } else if (TextUtils.equals(BeanParam.RentOrderInfo.AliPay, dataEntity.getPayType())) {
                        activity.payType.setText("支付宝支付");
                    } else if (TextUtils.equals(BeanParam.RentOrderInfo.WPay, dataEntity.getPayType())) {
                        activity.payType.setText("微信支付");
                    } else {
                        activity.payType.setText("未知");
                    }

                    //时间
                    if (!TextUtils.isEmpty(dataEntity.getRealBeginTime()) && !TextUtils.isEmpty(dataEntity.getRealEndTime())) {
                        activity.time.setText(DateUtil.getTwoDateDay(dataEntity.getRealEndTime(), dataEntity.getRealBeginTime()));
                    }

                    break;
            }
        }
    }

    public static void startActivity(Context context, String orderNo) {
        Intent intent = new Intent(context, RentCarOrderInfoActivity.class);
        intent.putExtra("orderNo", orderNo);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ListView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.NewsListAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.DateSort;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.News;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.DateUtil;
import com.fusite.utils.HttpCacheUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.ListUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/24.
 */
public class MyNewsActivity extends UtilActivity {

    public String TAG = "MyNewsActivity";
    @BindView(R.id.list)
    ListView list;

    private NewsListAdapter adapter;
    private HttpCacheUtil httpCacheUtil;
    private List<DateSort> dateSortList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.mynews_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void loadObjectAttribute() {
        httpCacheUtil = new HttpCacheUtil(context);
    }

    @Override
    public void setActivityView() {
        if(httpCacheUtil.isExistCache(RequestParam.GetNews.InterfaceName)){
            News news = JsonUtil.objectFromJson(httpCacheUtil.readHttp(RequestParam.GetNews.InterfaceName),News.class);
            dateSortList = sort(news);
            adapter = new NewsListAdapter(context, dateSortList);
            list.setAdapter(adapter);
            loadData();
        }else{
            loadData();
        }
    }

    /**
     * 加载网络数据
     */
    public void loadData(){
        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetNews.Token, loginData.getToken());
        ApiService.getPileService().GetNews(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.GetNews.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(news -> {
                    if(TextUtils.equals(BeanParam.Failure,news.getIsSuccess())){
                        Error error = errorParamUtil.checkReturnState(news.getReturnStatus());
                        toastUtil.toastError(context,error,null);
                        return null;
                    }
                    return news;
                })
                .subscribe(news -> {
                    if (news == null) {
                        return;
                    }
                    //时间排序
                    dateSortList = sort(news);


                    adapter = new NewsListAdapter(context, dateSortList);
                    list.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                    //缓存数据
                    httpCacheUtil.cacheHttp(JsonUtil.Object2Json(news),RequestParam.GetNews.InterfaceName);
                }, throwable -> {

                });
    }

    public List<DateSort> sort(News news){
        //时间排序
        List<DateSort> dateSortList = new ArrayList<DateSort>();
        List<News.DataEntity> temp = news.getData();
        for (int i = 0; i < temp.size(); i++) {
            temp.get(i).setDate(DateUtil.getDate(temp.get(i).getCreateDate()));
            dateSortList.add(temp.get(i));
        }
        ListUtil.sortTime(dateSortList);
        return dateSortList;
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MyNewsActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

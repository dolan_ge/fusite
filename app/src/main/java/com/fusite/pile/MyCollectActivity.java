package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ListView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.CollectListAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.CollectList;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.HttpCacheUtil;
import com.fusite.utils.JsonUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/3.
 */
public class MyCollectActivity extends UtilActivity {
    /**
     * TAG
     */
    private String TAG = "MyCollectActivity";

    @BindView(R.id.list)
    ListView list;

    private List<CollectList.DataEntity> dataList;
    private CollectListAdapter adapter;
    private HttpCacheUtil httpCacheUtil;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.my_collect);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setActivityView();
    }
    @Override
    public void loadObjectAttribute() {
        httpCacheUtil = new HttpCacheUtil(context);
    }

    @Override
    public void setListener() {
        setItemOnClickListener();
    }


    public void setItemOnClickListener() {
        list.setOnItemClickListener((parent, view, position, id) -> {
            CollectList.DataEntity dataEntity = dataList.get(position);
            if(TextUtils.equals(dataEntity.getOrgClasses(),BeanParam.CollectList.EVC)){
                PileStationInfoActivity.startActivity(context, dataEntity.getOrgId());
            }else{
                RentStationInfoActivity.startActivity(context,dataEntity.getOrgId());
            }
        });
    }

    @Override
    public void setActivityView() {
        if(httpCacheUtil.isExistCache(RequestParam.GetCollect.InterfaceName)){
            CollectList collectList = JsonUtil.objectFromJson(httpCacheUtil.readHttp(RequestParam.GetCollect.InterfaceName),CollectList.class);
            dataList = collectList.getData();
            adapter = new CollectListAdapter(context, dataList);
            list.setAdapter(adapter);
            loadData();
        }else{
            loadData();
        }
    }

    /**
     * 加载数据
     */
    public void loadData(){
        Login.DataEntity dataEntity = application.getLogin().getData();
        Map<String, String> map = new HashMap<>();
        map.put(RequestParam.GetCollect.Token, dataEntity.getToken());
        ApiService.getPileService().GetCollect(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.GetCollect.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(collectList -> {
                    if (TextUtils.equals(BeanParam.Failure, collectList.getIsSuccess())) {
                        Error error = errorParamUtil.checkReturnState(collectList.getReturnStatus());
                        toastUtil.toastError(context, error, null);
                        return null;
                    }
                    return collectList;
                })
                .subscribe(collectList -> {
                    if (collectList == null) {
                        dataList = new ArrayList<>();
                        adapter = new CollectListAdapter(context, dataList);
                        list.setAdapter(adapter);
                        return;
                    }
                    dataList = collectList.getData();
                    adapter = new CollectListAdapter(context, dataList);
                    list.setAdapter(adapter);
                    //缓存
                    httpCacheUtil.cacheHttp(JsonUtil.Object2Json(collectList),RequestParam.GetCollect.InterfaceName);
                }, throwable -> {

                });
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MyCollectActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

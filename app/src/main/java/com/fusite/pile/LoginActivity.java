package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.param.BeanParam;
import com.fusite.param.DefaultParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.CountDownUtil;
import com.fusite.utils.LogUtil;
import com.fusite.utils.RegExpUtil;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * 登录界面，点击ActionBar上面的注册按钮直接进入注册界面
 * Created by Jelly on 2016/3/1.
 */
public class LoginActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "LoginActivity";

    @BindView(R.id.normal_btn)
    TextView normalBtn;
    @BindView(R.id.quick_login_btn)
    TextView quickLoginBtn;
    @BindView(R.id.normal_phone_clear)
    ImageView normalPhoneClear;
    @BindView(R.id.normal_phone_et)
    EditText normalPhoneEt;
    @BindView(R.id.normal_pwd_see)
    ImageView normalPwdSee;
    @BindView(R.id.normal_pwd_et)
    EditText normalPwdEt;
    @BindView(R.id.normal_layout)
    LinearLayout normalLayout;
    @BindView(R.id.quick_code_btn)
    Button quickCodeBtn;
    @BindView(R.id.quick_phone_et)
    EditText quickPhoneEt;
    @BindView(R.id.quick_code_clear)
    ImageView quickCodeClear;
    @BindView(R.id.quick_code_et)
    EditText quickCodeEt;
    @BindView(R.id.quick_login_layout)
    LinearLayout quickLoginLayout;


    private int msgCountDown = 0x003;
    private CustomDialog loading;
    private boolean isPasswordVisible = false;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.login);
        ButterKnife.bind(this);
        loadObjectAttribute();
        init();
        setListener();
        setActivityView();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dialogUtil.dismiss(loading);
    }

    @Override
    public void setListener() {
        setEtClearListener();
    }

    @Override
    public void setActivityView() {

    }

    /**
     * 设置手机输入是出现清除输入按钮
     */
    public void setEtClearListener() {
        normalPhoneEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (normalPhoneEt.getText().length() > 0) {
                    if (!normalPhoneClear.isShown()) {
                        normalPhoneClear.setVisibility(View.VISIBLE);
                    }
                    if (!normalPhoneClear.isClickable()) {
                        normalPhoneClear.setOnClickListener(v -> normalPhoneEt.setText(""));
                    }
                } else {
                    normalPhoneClear.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        quickCodeEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (quickCodeEt.getText().length() > 0) {
                    if (!quickCodeClear.isShown()) {
                        quickCodeClear.setVisibility(View.VISIBLE);
                    }
                    if (!quickCodeClear.isClickable()) {
                        quickCodeClear.setOnClickListener(v -> quickCodeEt.setText(""));
                    }
                } else {
                    quickCodeClear.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick({R.id.reg_btn, R.id.normal_btn, R.id.quick_login_btn, R.id.forget_pwd_btn, R.id.quick_login_in_btn,
            R.id.quick_code_btn, R.id.normal_login_in_btn, R.id.normal_pwd_see})
    public void onClick(View view) {
        String phone = "";
        Map<String, String> map = new HashMap<>();
        switch (view.getId()) {
            case R.id.reg_btn: //注册
                RegActivity.startActivity(context);
                break;
            case R.id.normal_btn: //正常登录切换按钮
                if (!normalLayout.isShown()) {
                    normalLayout.setVisibility(View.VISIBLE);
                }
                if (quickLoginLayout.isShown()) {
                    quickLoginLayout.setVisibility(View.GONE);
                }
                if (!(normalBtn.getTextColors().getDefaultColor() == resourceUtil.getResourceColor(context, R.color.tab_select_color))) {
                    normalBtn.setTextColor(resourceUtil.getResourceColor(context, R.color.tab_select_color));
                }
                if (!(quickLoginBtn.getTextColors().getDefaultColor() == resourceUtil.getResourceColor(context, R.color.defaultTabColor))) {
                    quickLoginBtn.setTextColor(resourceUtil.getResourceColor(context, R.color.defaultTabColor));
                }
                break;
            case R.id.quick_login_btn: //快速登录切换按钮
                if (!quickLoginLayout.isShown()) {
                    quickLoginLayout.setVisibility(View.VISIBLE);
                }
                if (normalLayout.isShown()) {
                    normalLayout.setVisibility(View.GONE);
                }
                if (!(quickLoginBtn.getTextColors().getDefaultColor() == resourceUtil.getResourceColor(context, R.color.tab_select_color))) {
                    quickLoginBtn.setTextColor(resourceUtil.getResourceColor(context, R.color.tab_select_color));
                }
                if (!(normalBtn.getTextColors().getDefaultColor() == resourceUtil.getResourceColor(context, R.color.defaultTabColor))) {
                    normalBtn.setTextColor(resourceUtil.getResourceColor(context, R.color.defaultTabColor));
                }
                break;
            case R.id.forget_pwd_btn: //忘记密码
                ForgetPwdActivity.startActivity(context);
                break;
            case R.id.quick_login_in_btn: //快速登录
                phone = quickPhoneEt.getText().toString();
                String code = quickCodeEt.getText().toString();
                if (!RegExpUtil.regPhone(phone)) {
                    Toast.makeText(this, R.string.phoneFormatError, Toast.LENGTH_SHORT).show();
                    quickPhoneEt.setText("");
                    break;
                }
                loading = dialogUtil.startLoading(context,loading,R.string.nowLogin);
                map.clear();
                map.put(RequestParam.Login.Mobile, phone);
                map.put(RequestParam.Login.Code, code);
                map.put(RequestParam.Login.Type, RequestParam.Login.CodeLogin);
                startLogin(map);
                break;
            case R.id.quick_code_btn: //获取验证码
                phone = quickPhoneEt.getText().toString();
                if (!RegExpUtil.regPhone(phone)) {
                    Toast.makeText(this, R.string.phoneFormatError, Toast.LENGTH_SHORT).show();
                    quickPhoneEt.setText("");
                    break;
                }
                map.clear();
                map.put(RequestParam.Code.Mobile, phone);
                map.put(RequestParam.Code.CodeType, RequestParam.Code.LOGINCODE);
                ApiService.getPileService().getCode(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.Code.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(codes -> { //开启倒计时
                            quickCodeBtn.setEnabled(false);
                            CountDownUtil countDownUtil = new CountDownUtil(new MyHandler(context), msgCountDown, DefaultParam.COUNTDOWN);
                            countDownUtil.startCounrtDown();
                            return codes;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(codes -> { //错误提示
                            if (TextUtils.equals(BeanParam.Failure, codes.get(0).getCrm_validation().getIsSuccess())) {
                                Toast.makeText(context, "验证码获取失败，请60秒重新获取", Toast.LENGTH_SHORT).show();
                                return null;
                            }
                            return codes;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(codes -> {
                            if (codes != null) {
                                Toast.makeText(context, "验证码发送成功", Toast.LENGTH_SHORT).show();
                            }
                        }, throwable -> {
                            LogUtil.v(TAG,throwable.getMessage());
                        });
                break;
            case R.id.normal_login_in_btn: //正常登录
                phone = normalPhoneEt.getText().toString();
                String password = normalPwdEt.getText().toString();
                if (!RegExpUtil.regPhone(phone)) {
                    Toast.makeText(this, R.string.phoneFormatError, Toast.LENGTH_SHORT).show();
                    normalPhoneEt.setText("");
                    break;
                }
                loading = dialogUtil.startLoading(context,loading,R.string.nowLogin);
                map.clear();
                map.put(RequestParam.Login.Mobile, phone);
                map.put(RequestParam.Login.Password, password);
                map.put(RequestParam.Login.Type, RequestParam.Login.PasswordLogin);
                startLogin(map);
                break;
            case R.id.normal_pwd_see:
                togglePasswordVisibility();
                break;
        }
    }

    private void togglePasswordVisibility() {
        isPasswordVisible = !isPasswordVisible;

        int selectionIndex = -1;
        if (normalPwdEt.isFocused()) {
            selectionIndex = normalPwdEt.getSelectionStart();
        }
        if (isPasswordVisible) {
            normalPwdSee.setImageResource(R.drawable.password_invisible);
            normalPwdEt.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            normalPwdSee.setImageResource(R.drawable.password_visible);
            normalPwdEt.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        }

        if (selectionIndex != -1) {
            normalPwdEt.setSelection(selectionIndex);
        }
    }

    /**
     * 登录
     * @param map
     */
    public void startLogin(Map<String, String> map) {
        ApiService.getPileService().Login(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.Login.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(login -> {
                    dialogUtil.dismiss(loading);
                    if (TextUtils.equals(login.getIsSuccess(), BeanParam.Failure)) {
                        Error errorDao = errorParamUtil.checkReturnState(login.getReturnStatus());
                        toastUtil.toastError(context, errorDao, null);
                        return null;
                    }
                    return login;
                })
                .subscribe(login -> {
                    if (login != null) {
                        application.setLogin(login);
                        application.cacheLogin();
                        finish();
                    }
                }, new ThrowableAction(this));
    }

    private static class MyHandler extends Handler {

        private WeakReference<LoginActivity> mActivity;

        public MyHandler(Context context) {
            mActivity = new WeakReference(context);
        }

        @Override
        public void handleMessage(Message msg) {
            LoginActivity activity = mActivity.get();
            if (activity == null) {
                return;
            }
            if (msg.what == activity.msgCountDown) {
                int count = (int) msg.obj;
                activity.quickCodeBtn.setText("等待" + count + "秒");
                if (count == 0) {
                    activity.quickCodeBtn.setEnabled(true);
                    activity.quickCodeBtn.setText("获取验证码");
                }
            }
        }
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

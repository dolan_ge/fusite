package com.fusite.pile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.ActivityInfo;
import com.fusite.bean.CompleteOrder;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.bean.PayResult;
import com.fusite.bean.Unifiedorder;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.impl.CompleteOrderDaoImpl;
import com.fusite.impl.ReChargeDaoImpl;
import com.fusite.impl.RentOrdersDaoImpl;
import com.fusite.param.NetErrorEnum;
import com.fusite.param.NetParam;
import com.fusite.constant.Keys;
import com.fusite.param.RequestParam;
import com.fusite.utils.DateUtil;
import com.fusite.utils.JsonUtil;
import com.fusite.utils.LogUtil;
import com.fusite.utils.PayUtil;
import com.fusite.utils.WeChatUtil;
import com.fusite.view.CustomDialog;
import com.fruitknife.util.XmlResolve;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * 充值
 * Created by Jelly on 2016/3/16.
 */
public class RechargeActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "RechargeActivity";
    @BindView(R.id.tv_activity)
    TextView tvActivity;
    @BindView(R.id.price1)
    TextView price1;
    @BindView(R.id.price2)
    TextView price2;
    @BindView(R.id.price3)
    TextView price3;
    @BindView(R.id.price4)
    TextView price4;
    @BindView(R.id.price5)
    TextView price5;
    @BindView(R.id.price6)
    TextView price6;
    @BindView(R.id.alipay_btn)
    ImageView alipayBtn;
    @BindView(R.id.wchatpay_btn)
    ImageView wchatpayBtn;
    @BindView(R.id.inputPrice)
    EditText inputPrice;
    @BindView(R.id.checkBox)
    CheckBox checkBox;

    private TextView selectPrice;//选中的价格
    private ImageView selectPay;//选择的支付方式
    private Boolean isTest = false;//是否为支付测试
    private String tradeNo;//支付宝支付订单号
    private ReChargeDaoImpl reChargeDao;
    private ActivityInfo.CrmActivityGetBean crmActivityGetBean; // 充值活动信息
    private static final int msgReCharge = 0x002;//充值消息
    public static String price;//支付价格
    public static final int Success = 0x003;//充值成功
    public static final int Failure = 0X004;//充值失败
    public static boolean IsWxPay = false; //微信
    public static String OutTradNo; //微信支付订单
    private MyHandler handler;
    public static String accountNo;
    public static int type; //充值类型
    public static final int BalanceAmt = 0; //余额充值
    public static final int CardAmt = 1;//卡操作

    private CustomDialog loading;
    private AppCompatDialog activityInfoDialog; // 点击活动标题，弹出窗口显示活动的详情窗口

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.recharge);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dialogUtil.dismiss(loading);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Login.DataEntity loginData = application.getLogin().getData();
        Map<String, String> conditionMap = new HashMap<>();
        conditionMap.put(RequestParam.ActivityInfo.Token, loginData.getToken());
        String condition = NetParam.spliceCondition(conditionMap);
        Map<String, String> param = NetParam.getParamMap(RequestParam.ActivityInfo.InterfaceName,loginData.getCustID(),condition);
        ApiService.getPileService().activityInfo(param)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<ActivityInfo>>() {
                    @Override
                    public void call(List<ActivityInfo> activityInfoList) {
                        if (activityInfoList.isEmpty()) {
                            return;
                        }
                        ActivityInfo activityInfo = activityInfoList.get(0);
                        if (activityInfo.getCrm_activity_get() == null) {
                            return;
                        }
                        crmActivityGetBean = activityInfo.getCrm_activity_get();
                        tvActivity.setText(crmActivityGetBean.getActivityDesc());
                    }
                }, new ThrowableAction(context, loading));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissActivityInfoDialog();
    }

    @Override
    public void loadObjectAttribute() {
        handler = new MyHandler(this);
        reChargeDao = new ReChargeDaoImpl(this, handler, msgReCharge);
        type = intent.getIntExtra("type", 0);
        accountNo = intent.getStringExtra("accountNo");
    }

    @Override
    public void setListener() {

    }

    @Override
    public void setActivityView() {
        setCheckBoxListener();
    }

    public void setCheckBoxListener(){
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if(isChecked){
                    ReadProtocolActivity.startActivity(context);
                }
        });
    }

    /**
     * 开始支付
     *
     * @param view
     * @return
     */
    public View pay(View view) {
        if (selectPay == null) { //没有选择支付方式
            Toast.makeText(this, R.string.nopayId_hint, Toast.LENGTH_SHORT).show();
            return view;
        }

        String inputPriceStr = inputPrice.getText().toString();

        if (selectPrice == null && TextUtils.isEmpty(inputPriceStr)) { //没有选择充值金额
            Toast.makeText(this, R.string.noprice_hint, Toast.LENGTH_SHORT).show();
            return view;
        }

        if(!checkBox.isChecked()){
            Toast.makeText(this, "请先阅读协议", Toast.LENGTH_SHORT).show();
            return view;
        }

        if(!TextUtils.isEmpty(inputPriceStr)){
            price = inputPriceStr;
        }else{
            price = selectPrice.getText().toString().replace("元", "");
        }

        switch (selectPay.getId()) {
            case R.id.alipay_btn:

                String orderInfo = "";
                tradeNo = PayUtil.getOutTradeNo();

                Map<String,String> mapCondition = new HashMap<String,String >();
                Login.DataEntity loginData = application.getLogin().getData();

                if(type == BalanceAmt){
                    mapCondition.put("PayType","RechargeBalance");
                }else{
                    mapCondition.put("PayType","RechargeCard");
                    mapCondition.put("AccountNo",accountNo);
                }

                mapCondition.put("Device","Android");
                mapCondition.put("CustId",loginData.getCustID());
                mapCondition.put("key","genlex2004");

                String passback_params = NetParam.spliceCondition(mapCondition);

                if (isTest) {
                    orderInfo = PayUtil.getOrderInfo("测试支付", passback_params, "0.01", tradeNo);
                } else {
                    orderInfo = PayUtil.getOrderInfo(price + "元充值卡", passback_params, price, tradeNo);
                }

                Log.v("orderInfo ",orderInfo);

                String sign = PayUtil.sign(orderInfo);
                try {
                    /**
                     * 仅需对sign 做URL编码
                     */
                    sign = URLEncoder.encode(sign, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                /**
                 * 完整的符合支付宝参数规范的订单信息
                 */
                final String payInfo = orderInfo + "&sign=\"" + sign + "\"&" + PayUtil.getSignType();

                new Thread(() -> {
                    // 构造PayTask 对象
                    PayTask alipay = new PayTask(RechargeActivity.this);
                    // 调用支付接口，获取支付结果
                    String result = alipay.pay(payInfo, true);
                    Message msg = new Message();
                    msg.what = PayUtil.SDK_PAY_FLAG;
                    msg.obj = result;
                    handler.sendMessage(msg);
                }).start();

                break;
            case R.id.wchatpay_btn: //微信支付

                loading = dialogUtil.startLoading(context,loading,R.string.loadingText);

                String xmlStr = "";
                OutTradNo = WeChatUtil.genOutTradNo();

                Map<String,String> mapCondition1 = new HashMap<String,String >();

                Login.DataEntity loginData1 = application.getLogin().getData();

                if(type == BalanceAmt){
                    mapCondition1.put("PayType","RechargeBalance");
                }else{
                    mapCondition1.put("PayType","RechargeCard");
                    mapCondition1.put("AccountNo",accountNo);
                }

                mapCondition1.put("Device","Android");
                mapCondition1.put("CustId",loginData1.getCustID());
                mapCondition1.put("key","genlex2004");

                String attach = NetParam.spliceCondition(mapCondition1);

                if (isTest) {
                    xmlStr = WeChatUtil.getOrderInfo("测试", "测试", "1", OutTradNo,attach);
                } else {
                    xmlStr = WeChatUtil.getOrderInfo(price + "元充值卡", "玛帮新能源电桩" + price + "元充值卡", (int)(Double.parseDouble(price) * 100)  + "", OutTradNo,attach);
                }

                RequestBody body = RequestBody.create(MediaType.parse("text/xml"), xmlStr);
                ApiService.getWxApi().getGenprePay(body)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(xml -> {
                            try {

                                String xmlStr1 = xml.string();
                                Unifiedorder unifiedorder = new XmlResolve().xmlToObject(xmlStr1, Unifiedorder.class);

                                Log.v("infoXml",xmlStr1);

                                if(TextUtils.equals("time_expire时间过短，刷卡至少1分钟，其他5分钟",unifiedorder.return_msg)){
                                    Toast.makeText(context,"启动微信失败,请校准当前时间为标准时间！",Toast.LENGTH_SHORT).show();
                                    dialogUtil.dismiss(loading);
                                    return;
                                }

                                IWXAPI api = WXAPIFactory.createWXAPI(RechargeActivity.this, Keys.AppId, false);
                                PayReq request = new PayReq();
                                request.appId = unifiedorder.appid;
                                request.partnerId = unifiedorder.mch_id;
                                request.prepayId = unifiedorder.prepay_id;
                                request.packageValue = "Sign=WXPay";
                                request.nonceStr = unifiedorder.nonce_str;
                                request.timeStamp = WeChatUtil.getTimeStamp();
                                request.sign = WeChatUtil.getRequestSign(request);
                                request.extData = "1";
                                api.registerApp(Keys.AppId); //把应用注册到微信
                                api.sendReq(request);
                                IsWxPay = true;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }, new ThrowableAction(this));
                break;
        }
        return view;
    }

    /**
     * 选价格
     *
     * @param price
     */
    public void selectPrice(TextView price) {
        price.setBackgroundResource(R.drawable.select_price_btn_bg);
        price.setTextColor(Color.WHITE);
        if (selectPrice != null && selectPrice != price) { //如果选择的价格不为空，改变背景为未选中
            selectPrice.setBackgroundResource(R.drawable.deleteorder_btn_bg);
            selectPrice.setTextColor(Color.RED);
        }
        selectPrice = price;
    }

    /**
     * 选支付方式
     *
     * @param pay
     */
    public void selectPay(ImageView pay) {
        if (selectPay != null) { //如果选择的支付方式不为空，改变为未选中
            selectPay.setImageResource(R.drawable.pay_noselect_rb);
        }
        pay.setImageResource(R.drawable.pay_select_rb);
        selectPay = pay;
    }

    private void showActivityInfoDialog() {
        if (activityInfoDialog == null) {
            activityInfoDialog = new AppCompatDialog(RechargeActivity.this);
        }
        View view = LayoutInflater.from(RechargeActivity.this).inflate(R.layout.layout_dialog_activity_info, null);
        TextView tvAcitivityName = view.findViewById(R.id.tv_activity_name);
        TextView tvAcitivityDesc = view.findViewById(R.id.tv_activity_desc);
        TextView tvAcitivityTime = view.findViewById(R.id.tv_activity_time);
        tvAcitivityName.setText(crmActivityGetBean.getActivityName());
        tvAcitivityDesc.setText(crmActivityGetBean.getActivityDesc());
        tvAcitivityTime.setText("活动时间：" + crmActivityGetBean.getBeginDateTime() + "至" + crmActivityGetBean.getEndDateTime());
        activityInfoDialog.setContentView(view);
        activityInfoDialog.show();
    }

    private void dismissActivityInfoDialog() {
        if (activityInfoDialog == null || !activityInfoDialog.isShowing()) {
            return;
        }
        activityInfoDialog.dismiss();
    }

    @OnClick({R.id.tv_activity, R.id.price1, R.id.price2, R.id.price3, R.id.price4,R.id.price5,R.id.price6,R.id.alipay_btn, R.id.wchatpay_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_activity:
                if (crmActivityGetBean == null) {
                    return;
                }
                showActivityInfoDialog();
                break;
            case R.id.price1:
                selectPrice(price1);
                break;
            case R.id.price2:
                selectPrice(price2);
                break;
            case R.id.price3:
                selectPrice(price3);
                break;
            case R.id.price4:
                selectPrice(price4);
                break;
            case R.id.price5:
                selectPrice(price5);
                break;
            case R.id.price6:
                selectPrice(price6);
                break;
            case R.id.alipay_btn:
//                selectPay(alipayBtn);
                // TODO 支付宝之后需要启用
                break;
            case R.id.wchatpay_btn:
                selectPay(wchatpayBtn);
                break;
        }
    }

    private static class MyHandler extends Handler {
        private WeakReference<RechargeActivity> mActivity;

        public MyHandler(RechargeActivity activity) {
            mActivity = new WeakReference<RechargeActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            RechargeActivity activity = mActivity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case PayUtil.SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息


                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    LogUtil.d("WxPay", "return status: " + resultStatus);
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Login.DataEntity loginData = activity.application.getLogin().getData();

                        if (activity.type == BalanceAmt) { //余额充值
                            //设置返回消息
                            activity.setResult(Success);
                            activity.finish();
                        } else { //充电卡充值

                            activity.intent.putExtra("price",price);
                            activity.setResult(Success,activity.intent);
                            activity.finish();

                        }

                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(activity, "支付结果确认中", Toast.LENGTH_SHORT).show();
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            Toast.makeText(activity, "支付失败", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                }
                case msgReCharge:
                    if (activity.reChargeDao.reChargeDao == null || TextUtils.equals(activity.reChargeDao.reChargeDao.getCrm_recharge().getIsSuccess(), "N")) {
                        Error errorDao = activity.errorParamUtil.checkReturnState(activity.reChargeDao.reChargeDao.getCrm_recharge().getReturnStatus());
                        activity.toastUtil.toastError(activity.context, errorDao, null);
                        activity.setResult(Failure);
                        activity.finish();
                        return;
                    }

                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 开启界面
     *
     * @param context
     */
    public static void startActivity(Context context, int request, int type, String accountNo) {
        Intent intent = new Intent(context, RechargeActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("accountNo", accountNo);
        Activity activity = (Activity) context;
        activity.startActivityForResult(intent, request);
    }

    @Override
    public String getTAG() {
        return TAG;
    }
}

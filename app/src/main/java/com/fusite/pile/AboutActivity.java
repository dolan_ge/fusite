package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jelly on 2016/6/22.
 */
public class AboutActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "AboutActivity";
    @BindView(R.id.url)
    TextView url;

    private String utlStr = "coming soon...";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.about_layout);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void setActivityView() {

    }

    @OnClick({R.id.website})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.website:
                WebActivity.startActivity(context,utlStr);
                break;
        }
    }

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, AboutActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

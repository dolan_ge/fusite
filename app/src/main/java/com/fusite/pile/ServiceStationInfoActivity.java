package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.ViewPageAdapter;
import com.fusite.bean.FilterServiceStation;
import com.fusite.fragment.CommentFragment;
import com.fusite.fragment.ServiceDetailFragment;
import com.fusite.fragments.BaseFragment;
import com.fusite.handlerinterface.OrgIdFragmentListener;
import com.fusite.utils.AnimationUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 服务点详情
 * Created by Jelly on 2016/5/6.
 */
public class ServiceStationInfoActivity extends UtilActivity implements OrgIdFragmentListener{
    /**
     * TAG
     */
    public String TAG = "ServiceStationInfoActivity";
    @BindView(R.id.tabLine)
    View tabLine;
    @BindView(R.id.vp)
    ViewPager vp;
    @BindView(R.id.detail_icon)
    ImageView detailIcon;
    @BindView(R.id.detail_text)
    TextView detailText;
    @BindView(R.id.comment_icon)
    ImageView commentIcon;
    @BindView(R.id.comment_text)
    TextView commentText;

    private BaseFragment[] fragments;
    private ViewPageAdapter adapter;

    private TextView currText;
    private ImageView currIcon;
    private int previousPosition;

    private int[] onIcons = new int[]{R.drawable.ondetail, R.drawable.oncomment};
    private int[] icons = new int[]{R.drawable.detail, R.drawable.comment};

    public FilterServiceStation.DataEntity dataEntity;
    public String serviceId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.service_station_info);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {

    }

    @Override
    public void loadObjectAttribute() {
        fragments = new BaseFragment[]{new ServiceDetailFragment(), new CommentFragment()};
        adapter = new ViewPageAdapter(getSupportFragmentManager(), fragments);
        currText = detailText;
        currIcon = detailIcon;
        //获得数据
        if(dataEntity == null){
            dataEntity = intent.getParcelableExtra("dataEntity");
        }
        if(TextUtils.isEmpty(serviceId)){
            serviceId = intent.getStringExtra("serviceId");
        }
    }

    @Override
    public void setListener() {
        setViewPageListener();
    }

    @Override
    public void setActivityView() {
        tabLine.getLayoutParams().width = windowUtil.getScreenWidth(this) / fragments.length;
        vp.setAdapter(adapter);
    }

    @OnClick({R.id.detailLayout, R.id.commentLayout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.detailLayout:
                vp.setCurrentItem(0);
                break;
            case R.id.commentLayout:
                vp.setCurrentItem(1);
                break;
        }
    }

    public void setViewPageListener() {
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (currText != null && currIcon != null) {
                    currText.setTextColor(resourceUtil.getResourceColor(context, R.color.defaultTabColor));
                    currIcon.setImageResource(icons[Integer.parseInt(currIcon.getTag().toString())]);
                }
                switch (position) {
                    case 0:
                        currText = detailText;
                        currIcon = detailIcon;
                        break;
                    case 1:
                        currText = commentText;
                        currIcon = commentIcon;
                        break;
                }
                currIcon.setImageResource(onIcons[position]);
                currText.setTextColor(resourceUtil.getResourceColor(context, R.color.tab_select_color));

                Animation animation = AnimationUtil.startTabLineAnimation(windowUtil, ServiceStationInfoActivity.this, previousPosition, position, fragments.length);
                tabLine.startAnimation(animation);
                previousPosition = position * windowUtil.getScreenWidth(ServiceStationInfoActivity.this) / fragments.length;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public String getOrgId() {
        return dataEntity.getOrgId();
    }

    public static void startActivity(Context context, FilterServiceStation.DataEntity dataEntity, String serviceId) {
        Intent intent = new Intent(context, ServiceStationInfoActivity.class);
        intent.putExtra("serviceId",serviceId);
        intent.putExtra("dataEntity",dataEntity);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

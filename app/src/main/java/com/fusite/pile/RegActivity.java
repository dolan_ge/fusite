package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.param.BeanParam;
import com.fusite.param.DefaultParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.CountDownUtil;
import com.fusite.utils.LogUtil;
import com.fusite.utils.RegExpUtil;
import com.fusite.view.CustomDialog;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * 注册界面
 * Created by Jelly on 2016/3/1.
 */
public class RegActivity extends UtilActivity {
    /**
     * TAG
     */
    public String TAG = "RegActivity";
    @BindView(R.id.reg_phone_et)
    EditText regPhoneEt;
    @BindView(R.id.reg_pwd_see)
    ImageView regPwdSee;
    @BindView(R.id.reg_pwd_et)
    EditText regPwdEt;
    @BindView(R.id.reg_code_clear)
    ImageView regCodeClear;
    @BindView(R.id.reg_code_et)
    EditText regCodeEt;
    @BindView(R.id.reg_code_btn)
    Button regCodeBtn;

    private int msgCountDown = 0x002;
    private CustomDialog loading;

    private String phone;
    private String password;
    private boolean isPasswordVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.reg);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    protected void onStop() {
        super.onStop();
        dialogUtil.dismiss(loading);
    }


    @Override
    public void setListener() {
        setEtClearListener();
    }

    @Override
    public void setActivityView() {

    }


    /**
     * 设置输入框清除按钮的监听事件
     */
    public void setEtClearListener() {
        regCodeEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (regCodeEt.getText().length() > 0) {
                    if (!regCodeClear.isShown()) {
                        regCodeClear.setVisibility(View.VISIBLE);
                    }
                    if (!regCodeClear.isClickable()) {
                        regCodeClear.setOnClickListener(v -> regCodeEt.setText(""));
                    }
                } else {
                    regCodeClear.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @OnClick({R.id.login_btn, R.id.reg_code_btn, R.id.regBtn, R.id.reg_pwd_see})
    public void onClick(View view) {
        Map<String, String> map = new HashMap<>();
        switch (view.getId()) {
            case R.id.login_btn:
                LoginActivity.startActivity(RegActivity.this);
                break;
            case R.id.reg_code_btn:
                phone = regPhoneEt.getText().toString();
                if (!RegExpUtil.regPhone(phone)) {
                    Toast.makeText(this, R.string.phoneFormatError, Toast.LENGTH_SHORT).show();
                    break;
                }
                map.clear();
                map.put(RequestParam.Code.Mobile, phone);
                map.put(RequestParam.Code.CodeType, RequestParam.Code.REGCODE);
                ApiService.getPileService().getCode(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.Code.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(codes -> { //开启倒计时
                            regCodeBtn.setEnabled(false);
                            CountDownUtil countDownUtil = new CountDownUtil(new MyHandler(context), msgCountDown, DefaultParam.COUNTDOWN);
                            countDownUtil.startCounrtDown();
                            return codes;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(codes -> { //错误提示
                            if (TextUtils.equals(BeanParam.Failure, codes.get(0).getCrm_validation().getIsSuccess())) {
                                Toast.makeText(context, "验证码获取失败，请60秒重新获取", Toast.LENGTH_SHORT).show();
                                return null;
                            }
                            return codes;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(codes -> {
                            if (codes != null) {
                                Toast.makeText(context, "验证码发送成功", Toast.LENGTH_SHORT).show();
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                LogUtil.v(TAG,throwable.getMessage());
                            }
                        });
                break;
            case R.id.regBtn:
                phone = regPhoneEt.getText().toString();
                password = regPwdEt.getText().toString();
                String code = regCodeEt.getText().toString();
                if (!RegExpUtil.regPhone(phone)) {
                    Toast.makeText(this, R.string.phoneFormatError, Toast.LENGTH_SHORT).show();
                    regPhoneEt.setText("");
                    break;
                }
                if (TextUtils.isEmpty(phone) || TextUtils.isEmpty(password) || TextUtils.isEmpty(code)) {
                    break;
                }
                loading = dialogUtil.startLoading(context,loading,R.string.nowReg);
                map.clear();
                map.put(RequestParam.RegUser.Mobile, phone);
                map.put(RequestParam.RegUser.Password, password);
                map.put(RequestParam.RegUser.Code, code);
                ApiService.getPileService().RegUser(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.RegUser.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(regUsers -> regUsers.get(0))
                        .map(regUser -> { //注册
                            if (TextUtils.equals(regUser.getCrm_register().getIsSuccess(), BeanParam.Failure)) {
                                dialogUtil.dismiss(loading);
                                Error errorDao = errorParamUtil.checkReturnState(regUser.getCrm_register().getReturnStatus());
                                toastUtil.toastError(context, errorDao, null);
                                return null;
                            }
                            return regUser;
                        })
                        .observeOn(Schedulers.newThread())
                        .flatMap(regUser -> { //登录
                            if(regUser == null){
                                return null;
                            }
                            map.clear();
                            map.put(RequestParam.Login.Mobile, phone);
                            map.put(RequestParam.Login.Password, password);
                            map.put(RequestParam.Login.Type, RequestParam.Login.PasswordLogin);
                            Observable<Login> observe = ApiService.getPileService().Login(NetParam.trancode, NetParam.mode, NetParam.getTime(), NetParam.custid, NetParam.sign_method, RequestParam.Login.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map));
                            return observe;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(login -> {
                            if(TextUtils.equals(login.getIsSuccess(),BeanParam.Failure)){
                                Error error = errorParamUtil.checkReturnState(login.getReturnStatus());
                                toastUtil.toastError(context,error,null);
                                return null;
                            }
                            return login;
                        })
                        .subscribe(login -> {
                            if (login == null) {
                                return;
                            }
                            application.setLogin(login);
                            application.cacheLogin();
                            MainActivity.startActivity(context);
                        }, throwable -> {
                            LogUtil.v(TAG,throwable.getMessage());
                        });
                break;
            case R.id.reg_pwd_see:
                togglePasswordVisibility();
                break;
        }
    }

    private void togglePasswordVisibility() {
        isPasswordVisible = !isPasswordVisible;

        int selectionIndex = -1;
        if (regPwdEt.isFocused()) {
            selectionIndex = regPwdEt.getSelectionStart();
        }
        if (isPasswordVisible) {
            regPwdSee.setImageResource(R.drawable.password_invisible);
            regPwdEt.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            regPwdSee.setImageResource(R.drawable.password_visible);
            regPwdEt.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        }

        if (selectionIndex != -1) {
            regPwdEt.setSelection(selectionIndex);
        }
    }

    private static class MyHandler extends Handler{
        private WeakReference<RegActivity> mActivity;

        private MyHandler(Context context){
            mActivity = new WeakReference(context);
        }

        @Override
        public void handleMessage(Message msg) {
            RegActivity activity = mActivity.get();
            if(activity == null){
                return;
            }
            if(msg.what == activity.msgCountDown){
                int count = (int) msg.obj;
                activity.regCodeBtn.setText("等待" + count + "秒");
                if (count == 0) {
                    activity.regCodeBtn.setEnabled(true);
                    activity.regCodeBtn.setText("获取验证码");
                }
            }
        }
    }


    public static void startActivity(Context context) {
        Intent intent = new Intent(context, RegActivity.class);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }

}

package com.fusite.pile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.activities.UtilActivity;
import com.fusite.adpater.ViewPageAdapter;
import com.fusite.api.ApiService;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.fragment.CommentFragment;
import com.fusite.fragment.RentDetailFragment;
import com.fusite.fragment.RentPhotoFragment;
import com.fusite.fragments.BaseFragment;
import com.fusite.handlerinterface.OrgIdFragmentListener;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.utils.AnimationUtil;
import com.fusite.view.CustomDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 租车服务点详情
 * Created by Jelly on 2016/4/18.
 */
public class RentStationInfoActivity extends UtilActivity implements OrgIdFragmentListener {

    /**
     * TAG
     */
    public static String TAG = "CarStationInfoActivity";

    @BindView(R.id.detail_icon)
    ImageView detailIcon;
    @BindView(R.id.detail_text)
    TextView detailText;
    @BindView(R.id.photo_icon)
    ImageView photoIcon;
    @BindView(R.id.photo_text)
    TextView photoText;
    @BindView(R.id.comment_icon)
    ImageView commentIcon;
    @BindView(R.id.comment_text)
    TextView commentText;
    @BindView(R.id.tabLine)
    TextView tabLine;
    @BindView(R.id.vp)
    ViewPager vp;
    @BindView(R.id.collect)
    ImageView collect;

    private ViewPageAdapter pageAdapter;
    private BaseFragment[] fragments;
    private int previousPosition;
    private TextView currText = null;
    private ImageView currIcon = null;
    private int[] onIcons = new int[]{R.drawable.ondetail, R.drawable.onphoto, R.drawable.oncomment};
    private int[] icons = new int[]{R.drawable.detail, R.drawable.photo, R.drawable.comment};

    private String cityCode;
    private String orgId;
    private boolean isCollect;
    private CustomDialog loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.car_station_info);
        ButterKnife.bind(this);
        init();
        loadObjectAttribute();
        setListener();
        setActivityView();
    }

    @Override
    public void init() {

    }


    @Override
    public void loadObjectAttribute() {
        fragments = new BaseFragment[]{new RentDetailFragment(), new RentPhotoFragment(), new CommentFragment()};
        pageAdapter = new ViewPageAdapter(getSupportFragmentManager(), fragments);
        currText = detailText;
        currIcon = detailIcon;
        orgId = intent.getStringExtra("OrgId");
    }

    @Override
    public void setListener() {
        setViewPageListener();
    }

    @Override
    public void setActivityView() {
        tabLine.getLayoutParams().width = windowUtil.getScreenWidth(this) / fragments.length;
        vp.setAdapter(pageAdapter);
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCollect(boolean isCollect) {
        if (isCollect) {
            collect.setImageResource(R.drawable.fill_collect);
        } else {
            collect.setImageResource(R.drawable.collect);
        }
        this.isCollect = isCollect;
    }


    @OnClick({R.id.detailLayout, R.id.imageLayout, R.id.commentLayout,R.id.collect})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.detailLayout:
                vp.setCurrentItem(0);
                break;
            case R.id.imageLayout:
                vp.setCurrentItem(1);
                break;
            case R.id.commentLayout:
                vp.setCurrentItem(2);
                break;
            case R.id.collect:
                Login login = application.getLogin();
                if (login == null) {
                    LoginActivity.startActivity(context);
                    return;
                }
                Login.DataEntity dataEntity = login.getData();
                loading = dialogUtil.startLoading(context,loading,R.string.loadingText);
                Map<String, String> map = new HashMap<>();
                map.put(RequestParam.SetCollect.Token, dataEntity.getToken());
                map.put(RequestParam.SetCollect.OrgId, orgId);
                if (isCollect) {
                    ApiService.getPileService().CancelServiceOrder(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.CancelCollect.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map(cancelServiceOrder -> {
                                dialogUtil.dismiss(loading);
                                return cancelServiceOrder;
                            })
                            .map(cancelServiceOrder -> {
                                if(TextUtils.equals(BeanParam.Failure,cancelServiceOrder.getIsSuccess())){
                                    Error error = errorParamUtil.checkReturnState(cancelServiceOrder.getReturnStatus());
                                    toastUtil.toastError(context,error,null);
                                    return null;
                                }
                                return cancelServiceOrder;
                            })
                            .subscribe(cancelServiceOrder -> {
                                if (cancelServiceOrder == null) {
                                    return;
                                }
                                Toast.makeText(context, "取消收藏成功!", Toast.LENGTH_SHORT).show();
                                setCollect(false);
                            }, throwable -> {

                            });
                } else {
                    ApiService.getPileService().SetCollect(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity.getCustID(), NetParam.sign_method, RequestParam.SetCollect.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map(collect -> {
                                dialogUtil.dismiss(loading);
                                return collect;
                            })
                            .map(collect1 -> {
                                if (TextUtils.equals(BeanParam.Failure, collect1.getIsSuccess())) {
                                    Error error = errorParamUtil.checkReturnState(collect1.getReturnStatus());
                                    toastUtil.toastError(context, error, null);
                                    return null;
                                }
                                return collect1;
                            })
                            .subscribe(collect1 -> {
                                if (collect1 == null) {
                                    return;
                                }
                                Toast.makeText(context, "收藏成功!", Toast.LENGTH_SHORT).show();
                                setCollect(true);
                            }, throwable -> {

                            });
                }
                break;
        }
    }

    public void setViewPageListener() {
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (currText != null && currIcon != null) {
                    currText.setTextColor(resourceUtil.getResourceColor(context, R.color.defaultTabColor));
                    currIcon.setImageResource(icons[Integer.parseInt(currIcon.getTag().toString())]);
                }
                switch (position) {
                    case 0:
                        currText = detailText;
                        currIcon = detailIcon;
                        break;
                    case 1:
                        currText = photoText;
                        currIcon = photoIcon;
                        break;
                    case 2:
                        currText = commentText;
                        currIcon = commentIcon;
                        break;
                }
                currIcon.setImageResource(onIcons[position]);
                currText.setTextColor(resourceUtil.getResourceColor(context, R.color.tab_select_color));

                Animation animation = AnimationUtil.startTabLineAnimation(windowUtil, RentStationInfoActivity.this, previousPosition, position, fragments.length);
                tabLine.startAnimation(animation);
                previousPosition = position * windowUtil.getScreenWidth(RentStationInfoActivity.this) / fragments.length;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public String getOrgId() {
        return orgId;
    }

    /**
     * 开启界面
     */
    public static void startActivity(Context context, String OrgId) {
        Intent intent = new Intent(context, RentStationInfoActivity.class);
        intent.putExtra("OrgId", OrgId);
        context.startActivity(intent);
    }

    @Override
    public String getTAG() {
        return TAG;
    }


}

package com.fusite.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fusite.bean.ChargeCardItem;
import com.fusite.param.BeanParam;
import com.fusite.pile.R;

import java.util.List;

/**
 * Created by Jelly on 2016/3/23.
 */
public class ChargeCardListAdapter extends BaseAdapter{

    private List<ChargeCardItem> list;

    private Context context;

    public ChargeCardListAdapter(Context context,List<ChargeCardItem> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        TextView title;
        TextView no;
        TextView balanceAmt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.pile_card_item,null);
            viewHolder.no = (TextView) convertView.findViewById(R.id.no);
            viewHolder.balanceAmt = (TextView) convertView.findViewById(R.id.balanceAmt);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ChargeCardItem dao = (ChargeCardItem) getItem(position);
        if(TextUtils.isEmpty(dao.getCrm_accounts_get().getAccountStatus())){
            viewHolder.title.setText("异常");
        }else{
            switch (dao.getCrm_accounts_get().getAccountStatus()){
                case BeanParam.PileCard.Normal:
                    viewHolder.title.setText("充电卡(正常)");
                    break;
                case BeanParam.PileCard.Freeze:
                    viewHolder.title.setText("充电卡(冻结)");
                    break;
                case BeanParam.PileCard.Logout:
                    viewHolder.title.setText("充电卡(注销)");
                    break;
            }
        }

        viewHolder.no.setText(dao.getCrm_accounts_get().getAccountNo());
        viewHolder.balanceAmt.setText("余额：" + dao.getCrm_accounts_get().getBalanceAmt());
        return convertView;
    }

}

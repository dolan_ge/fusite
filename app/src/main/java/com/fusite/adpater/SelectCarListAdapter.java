package com.fusite.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.fusite.bean.CarModels;
import com.fusite.pile.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Jelly on 2016/4/18.
 */
public class SelectCarListAdapter extends BaseAdapter{

    private Context context;

    private List<CarModels.DataEntity> list;

    public SelectCarListAdapter(Context context,List<CarModels.DataEntity> list){
        this.context = context;
        this.list = list;
        RequestQueue queue = Volley.newRequestQueue(context);
    }

    public void setList(List<CarModels.DataEntity> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.select_car_list_item,null);
            viewHolder.carIcon = (NetworkImageView) convertView.findViewById(R.id.carIcon);
            viewHolder.useIcon = (ImageView) convertView.findViewById(R.id.useIcon);
            viewHolder.brand = (TextView) convertView.findViewById(R.id.brand);
            viewHolder.carType = (TextView) convertView.findViewById(R.id.carType);
            viewHolder.battery = (TextView) convertView.findViewById(R.id.battery);
            viewHolder.price = (TextView) convertView.findViewById(R.id.price);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CarModels.DataEntity dataEntity = list.get(position);
        viewHolder.carIcon.setDefaultImageResId(R.drawable.caricon);
        viewHolder.carIcon.setErrorImageResId(R.drawable.caricon);
        Picasso.with(context).load(dataEntity.getPicture()).into(viewHolder.carIcon);
        viewHolder.brand.setText(dataEntity.getBrand());
        viewHolder.carType.setText(dataEntity.getStruct() + "|" + dataEntity.getSeries() + "|乘坐" + dataEntity.getCounts() + "人");
        viewHolder.battery.setText("可续航约" + dataEntity.getEnduranceMileage() + "km");
        viewHolder.price.setText("¥" + dataEntity.getPrice());
        if(Integer.parseInt(dataEntity.getAvailableNum()) > 0){
            viewHolder.useIcon.setImageResource(R.drawable.canrent);
        }else{
            viewHolder.useIcon.setImageResource(R.drawable.nocanuse);
        }
        return convertView;
    }

    private class ViewHolder{
        NetworkImageView carIcon;
        ImageView useIcon;
        TextView brand;
        TextView carType;
        TextView battery;
        TextView price;
    }


}

package com.fusite.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fusite.bean.Bill;
import com.fusite.param.BeanParam;
import com.fusite.pile.R;
import com.fusite.utils.DateUtil;

import java.util.List;

/**
 * 账单列表
 * Created by Jelly on 2016/3/24.
 */
public class BillListAdapter extends BaseAdapter{
    /**
     * Context
     */
    private Context context;
    /**
     * 数据集合
     */
    private List<Bill> list;

    private final String DateFormat = "MM-dd HH:mm";

    public BillListAdapter(Context context,List<Bill> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 缓存视图
     */
    private class ViewHolder{
        TextView type;
        TextView price;
        TextView time;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.bill_list_item,null);
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.time = (TextView) convertView.findViewById(R.id.time);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        Bill.CrmBalancelogGetEntity entity = ((Bill)getItem(position)).getCrm_balancelog_get();
        switch (entity.getLogType()){
            case BeanParam.Bill.AliReCharge:
                holder.type.setText("支付宝充值");
                holder.price.setText("+"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.WReCharge:
                holder.type.setText("微信充值");
                holder.price.setText("+"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.BalancePayCharge:
                holder.type.setText("余额充电");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.AliPayCharge:
                holder.type.setText("支付宝充电");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.WPayCharge:
                holder.type.setText("微信充电");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.BalancePayAppoint:
                holder.type.setText("余额充电预约");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.AliPayAppoint:
                holder.type.setText("支付宝充电预约");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.WPayAppoint:
                holder.type.setText("微信充电预约");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.BalancePayRentCar:
                holder.type.setText("余额租车");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.AliPayRentCar:
                holder.type.setText("支付宝租车");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.WPayRentCar:
                holder.type.setText("微信租车");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.BalancePayRentBill:
                holder.type.setText("余额支付超额费用");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.AliPayRentBill:
                holder.type.setText("支付宝支付超额费用");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.WPayRentBill:
                holder.type.setText("微信支付超额费用");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.RentBillBackBalance:
                holder.type.setText("押金退还");
                holder.price.setText("+"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.CancelRentOrder:
                holder.type.setText("退还金额");
                holder.price.setText("+"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.CardAliRecharge:
                holder.type.setText("充电卡支付宝充值");
                holder.price.setText("+"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.CardWxRecharge:
                holder.type.setText("充电卡微信充值");
                holder.price.setText("+"+entity.getAmt()+"元");
                break;
            case BeanParam.Bill.CardPay:
                holder.type.setText("充电卡消费");
                holder.price.setText("-"+entity.getAmt()+"元");
                break;
        }
        holder.time.setText(DateUtil.getSdfDate(entity.getCreateDate(),DateFormat));
        return convertView;
    }
}

package com.fusite.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fusite.bean.DateSort;
import com.fusite.bean.InvoiceHistory;
import com.fusite.param.BeanParam;
import com.fusite.pile.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jelly on 2016/6/25.
 */
public class InvoiceHistoryAdapter extends BaseAdapter {

    private Context context;
    private List<DateSort> dataList = new ArrayList<>();

    public InvoiceHistoryAdapter(Context context, List<DateSort> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList == null ? 0 : dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(viewHolder == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.invoicehistory_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        InvoiceHistory.DataEntity invoiceHistoryData = (InvoiceHistory.DataEntity) getItem(position);
        switch (TextUtils.isEmpty(invoiceHistoryData.getInvoiceStatus()) ? "-1" :  invoiceHistoryData.getInvoiceStatus()){
            case BeanParam.InvoiceHistory.Status0:
                viewHolder.state.setText("已申请");
                break;
            case BeanParam.InvoiceHistory.Status1:
                viewHolder.state.setText("已审核");
                break;
            case BeanParam.InvoiceHistory.Status2:
                viewHolder.state.setText("已邮寄");
                break;
            case BeanParam.InvoiceHistory.Status3:
                viewHolder.state.setText("已取消");
                break;
            default:
                viewHolder.state.setText("状态异常");
                break;
        }

        viewHolder.time.setText(invoiceHistoryData.getCreateDate().replace("T"," "));
        viewHolder.title.setText(invoiceHistoryData.getDocNo());
        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.state)
        TextView state;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

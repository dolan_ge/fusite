package com.fusite.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusite.bean.Commit;
import com.fusite.pile.R;
import com.fusite.utils.DateUtil;

import java.util.List;

/**
 * Created by Jelly on 2016/5/16.
 */
public class CommitListAdapter extends BaseAdapter {

    private List<Commit.DataEntity> list;
    private Context context;
    public static final String DateFormat = "yyyy-MM-dd";


    public CommitListAdapter(List<Commit.DataEntity> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.commit_item, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.time = (TextView) convertView.findViewById(R.id.time);
            viewHolder.content = (TextView) convertView.findViewById(R.id.content);
            viewHolder.score1 = (ImageView) convertView.findViewById(R.id.score1);
            viewHolder.score2 = (ImageView) convertView.findViewById(R.id.score2);
            viewHolder.score3 = (ImageView) convertView.findViewById(R.id.score3);
            viewHolder.score4 = (ImageView) convertView.findViewById(R.id.score4);
            viewHolder.score5 = (ImageView) convertView.findViewById(R.id.score5);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Commit.DataEntity dataEntity = (Commit.DataEntity) getItem(position);
        viewHolder.name.setText(dataEntity.getCustName());
        viewHolder.time.setText(DateUtil.getSdfDate(dataEntity.getCreateDate(),DateFormat));
        viewHolder.content.setText(dataEntity.getComment());

        if(!TextUtils.isEmpty(dataEntity.getScore())){
            int score = (int) Double.parseDouble(dataEntity.getScore());
            ImageView[] scores = new ImageView[]{viewHolder.score5,viewHolder.score4,viewHolder.score3,viewHolder.score2,viewHolder.score1};
            //计算是否有一半的星星
            boolean isHalf = false;
            if(score % 2 != 0){
                isHalf = !isHalf;
            }
            //设置满星
            for(int i = 0;i<score / 2;i++){
                scores[i].setImageResource(R.drawable.fillstar);
            }
            //设置半新
            if(isHalf){
                scores[score/2].setImageResource(R.drawable.halfstar);
            }
            //设置空星
            int startIndex;
            if(isHalf){
                startIndex = score / 2 + 1;
            }else{
                startIndex = score / 2;
            }
            for(int i=startIndex;i<scores.length;i++){
                scores[i].setImageResource(R.drawable.nostar);
            }
        }

        return convertView;
    }

    private class ViewHolder{
        private TextView name;
        private TextView time;
        private TextView content;
        private ImageView score1;
        private ImageView score2;
        private ImageView score3;
        private ImageView score4;
        private ImageView score5;
    }

}

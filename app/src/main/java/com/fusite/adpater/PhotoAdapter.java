package com.fusite.adpater;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.fusite.pile.R;
import com.fusite.utils.LogUtil;
import com.fusite.utils.WindowUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Jelly on 2016/5/19.
 */
public class PhotoAdapter extends BaseAdapter{

    private List<String> list;
    private Context context;

    public PhotoAdapter(Context context,List<String> list){
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView item = new ImageView(context);
        item.setLayoutParams(new GridView.LayoutParams(WindowUtil.getInstance().getScreenWidth((Activity) context)/3, WindowUtil.getInstance().getScreenWidth((Activity) context)/3));
        item.setScaleType(ImageView.ScaleType.CENTER_CROP);
        LogUtil.v("url",getItem(position)+"");
        Picasso.with(context).load(getItem(position)+"").placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).resize(200,200).into(item);
        return item;
    }
}

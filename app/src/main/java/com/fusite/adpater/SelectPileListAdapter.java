package com.fusite.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fusite.bean.PileListItem;
import com.fusite.constant.TypeDefined;
import com.fusite.pile.R;
import com.fusite.utils.JsonUtil;

import java.util.List;

/**
 * 选择电桩列表适配器
 * Created by Jelly on 2016/3/15.
 */
public class SelectPileListAdapter extends BaseAdapter{
    /**
     * 上下文
     */
    private Context context;
    /**
     * 操作数据
     */
    private List<PileListItem> piles;

    public SelectPileListAdapter(Context context,List<PileListItem> piles) {
        this.context = context;
        this.piles = piles;
    }

    @Override
    public int getCount() {
        return piles.size();
    }

    @Override
    public Object getItem(int position) {
        return piles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        TextView facilityName;
        TextView facilityType;
        TextView facilityModel;
        TextView applicableCar;
        TextView appoint_btn;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.select_pile_item,null);
            viewHolder.facilityName = (TextView) convertView.findViewById(R.id.facilityName);
            viewHolder.applicableCar = (TextView) convertView.findViewById(R.id.applicableCar);
            viewHolder.facilityType = (TextView) convertView.findViewById(R.id.facilityType);
            viewHolder.facilityModel = (TextView) convertView.findViewById(R.id.facilityModel);
            viewHolder.appoint_btn = (TextView) convertView.findViewById(R.id.appoint_btn);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        PileListItem.EvcFacilitiesGetEntity entity = ((PileListItem) getItem(position)).getEvc_facilities_get();
        viewHolder.facilityName.setText(entity.getFacilityName().replace("号桩","").replace("号充电桩",""));

        Log.v("carType", JsonUtil.Object2Json(entity));
        if(!TextUtils.isEmpty(entity.getApplicableCar())){
            viewHolder.applicableCar.setText(entity.getApplicableCar().replace("-","、"));
        }

        if(TextUtils.equals(TypeDefined.FACILITY_TYPE_DC, entity.getFacilityType())){
            viewHolder.facilityType.setText("直流桩");
        }else if(TextUtils.equals(TypeDefined.FACILITY_TYPE_AC, entity.getFacilityType())){
            viewHolder.facilityType.setText("交流桩");
        }


        if(TextUtils.equals(entity.getFacilityStatus(),"0")){
            viewHolder.appoint_btn.setText("空闲");
            viewHolder.appoint_btn.setBackgroundResource(R.drawable.pile_state_idle);
        }else if(TextUtils.equals(entity.getFacilityStatus(),"1")){
            viewHolder.appoint_btn.setText("充电中");
            viewHolder.appoint_btn.setBackgroundResource(R.drawable.pile_state_charging);
        }else if(TextUtils.equals(entity.getFacilityStatus(),"2")){
            viewHolder.appoint_btn.setText("预约中");
            viewHolder.appoint_btn.setBackgroundResource(R.drawable.pile_state_charging);
        }else if(TextUtils.equals(entity.getFacilityStatus(),"16")){
           viewHolder.appoint_btn.setText("离线");
            viewHolder.appoint_btn.setBackgroundResource(R.drawable.pile_state_offline);
        }
        return convertView;
    }
}

package com.fusite.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusite.bean.BooleanBean;
import com.fusite.bean.DateSort;
import com.fusite.bean.News;
import com.fusite.pile.R;
import com.fusite.utils.DateUtil;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jelly on 2016/6/26.
 */
public class NewsListAdapter extends BaseAdapter {

    private Context context;
    private List<DateSort> dataList;
    private int maxLines = 4; //最大行
    private List<BooleanBean> isList;

    public NewsListAdapter(Context context, List<DateSort> dataList) {
        this.context = context;
        this.dataList = dataList;
        //初始化isList
        isList = new ArrayList<>();
        for(int i=0;i<dataList.size();i++){
            BooleanBean booleanBean = new BooleanBean(false);
            isList.add(booleanBean);
        }
    }

    @Override
    public int getCount() {
        return dataList == null ? 0 : dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.news_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        News.DataEntity newsData = (News.DataEntity) getItem(position);

        viewHolder.title.setText(newsData.getTitle());

        viewHolder.content.setText(newsData.getContent());
        String timeStr = DateUtil.getTimeDistance(newsData.getCreateDate());
        if(TextUtils.equals(timeStr,DateUtil.now)){
            viewHolder.time.setText(DateUtil.now);
        }else{
            viewHolder.time.setText(timeStr + "前");
        }


        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.icon)
        ImageView icon;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.content)
        ExpandableTextView content;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

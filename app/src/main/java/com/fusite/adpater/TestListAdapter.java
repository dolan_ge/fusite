package com.fusite.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusite.bean.TestBean;
import com.fusite.pile.R;
import com.fusite.pile.WebActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Jelly on 2016/6/14.
 */
public class TestListAdapter extends BaseAdapter{

    private Context context;
    private List<TestBean> dataList;

    public TestListAdapter(Context context, List<TestBean> dataList){
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.test_item,parent,false);
            viewHolder.img = (ImageView) convertView.findViewById(R.id.img);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.content = (TextView) convertView.findViewById(R.id.content);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        TestBean testBean = (TestBean) getItem(position);
        Picasso.with(context).load(testBean.getImgUrl()).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).into(viewHolder.img);
        viewHolder.title.setText(testBean.getTitle());
        viewHolder.content.setText(testBean.getContent());


        convertView.setOnClickListener(v -> WebActivity.startActivity(context,testBean.intoUrl));

        return convertView;
    }

    private class ViewHolder{
        ImageView img;
        TextView title;
        TextView content;
    }
}
;
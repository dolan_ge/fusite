package com.fusite.adpater;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Jelly on 2016/6/21.
 */
public class GuidePageAdapter extends PagerAdapter{

    private List<View> viewList;
    private Context context;

    public GuidePageAdapter(List<View> viewList, Context context) {
        this.viewList = viewList;
        this.context = context;
    }

    /**
     * 销毁Item
     * @param container
     * @param position
     * @param object
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(viewList.get(position));
    }

    /**
     * 加载Item
     * @param container
     * @param position
     * @return
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = viewList.get(position);
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return viewList == null?0:viewList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}

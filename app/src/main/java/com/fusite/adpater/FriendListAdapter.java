package com.fusite.adpater;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fusite.api.ApiService;
import com.fusite.bean.Dynamic;
import com.fusite.bean.Error;
import com.fusite.bean.Login;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.FriendActivity;
import com.fusite.pile.ImageActivity;
import com.fusite.pile.MyApplication;
import com.fusite.pile.R;
import com.fusite.utils.CircleTransform;
import com.fusite.utils.DateUtil;
import com.fusite.utils.ErrorParamUtil;
import com.fusite.utils.LogUtil;
import com.fusite.utils.ToastUtil;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jelly on 2016/6/13.
 */
public class FriendListAdapter extends BaseAdapter {

    private Context context;
    private List<Dynamic.DataEntity> dataList;
    private boolean isScroll;



    public FriendListAdapter(Context context, List<Dynamic.DataEntity> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    public void setScroll(boolean isScroll){
        this.isScroll = isScroll;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.friend_item, parent, false);
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.icon);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.time = (TextView) convertView.findViewById(R.id.time);
            viewHolder.content = (TextView) convertView.findViewById(R.id.content);
            viewHolder.like = (ImageView) convertView.findViewById(R.id.like);
            viewHolder.likeNum = (TextView) convertView.findViewById(R.id.likeNum);
            viewHolder.commitNum = (TextView) convertView.findViewById(R.id.commentNum);
            viewHolder.image1 = (ImageView) convertView.findViewById(R.id.image1);
            viewHolder.image2 = (ImageView) convertView.findViewById(R.id.image2);
            viewHolder.image3 = (ImageView) convertView.findViewById(R.id.image3);
            viewHolder.image4 = (ImageView) convertView.findViewById(R.id.image4);
            viewHolder.image5 = (ImageView) convertView.findViewById(R.id.image5);
            viewHolder.image6 = (ImageView) convertView.findViewById(R.id.image6);
            viewHolder.image7 = (ImageView) convertView.findViewById(R.id.image7);
            viewHolder.image8 = (ImageView) convertView.findViewById(R.id.image8);
            viewHolder.image9 = (ImageView) convertView.findViewById(R.id.image9);
            viewHolder.images1 = (LinearLayout) convertView.findViewById(R.id.images1);
            viewHolder.images2 = (LinearLayout) convertView.findViewById(R.id.images2);
            viewHolder.images3 = (LinearLayout) convertView.findViewById(R.id.images3);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        ImageView[] images = new ImageView[]{viewHolder.image1, viewHolder.image2, viewHolder.image3, viewHolder.image4, viewHolder.image5, viewHolder.image6, viewHolder.image7, viewHolder.image8, viewHolder.image9};
        Dynamic.DataEntity dataEntity = (Dynamic.DataEntity) getItem(position);
        Picasso.with(context).load(dataEntity.getUsericon()).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).resize(100,100).transform(new CircleTransform()).into(viewHolder.icon);
        viewHolder.name.setText(dataEntity.getCustName());


        String time = DateUtil.getTimeDistance(dataEntity.getCreateDate());
        if(TextUtils.equals(DateUtil.now,time)){
            viewHolder.time.setText(DateUtil.now);
        }else{
            viewHolder.time.setText(time+"前");
        }


        viewHolder.content.setText(dataEntity.getContent());
        viewHolder.likeNum.setText(dataEntity.getLikes());
        viewHolder.commitNum.setText(dataEntity.getReplies());

        if (TextUtils.equals(BeanParam.Dynamic.onlike, dataEntity.getLiked())) {
            viewHolder.like.setImageResource(R.drawable.onlike);
        } else {
            viewHolder.like.setImageResource(R.drawable.like);
        }

        setLikeListener(viewHolder,position,dataEntity);

        List<Dynamic.DataEntity.PicturesEntity> imageUrls = dataEntity.getPictures();
        for (int i = 0; i < imageUrls.size(); i++) {
            images[i].setVisibility(View.VISIBLE);
            if(isScroll){
                images[i].setTag(imageUrls.get(i).getUrl());
                images[i].setImageResource(R.drawable.square_viewplace);
            }else{
                images[i].setTag("1");
                Picasso.with(context).load(imageUrls.get(i).getUrl()).placeholder(R.drawable.square_viewplace).error(R.drawable.square_viewplace).resize(200,200).into(images[i]);
            }
            int index = i;
            images[i].setOnClickListener(v -> ImageActivity.startActivity(context, imageUrls.get(index).getUrl()));
        }

        for (int i = imageUrls.size(); i < images.length; i++) {
            images[i].setVisibility(View.INVISIBLE);
        }

        viewHolder.images1.setVisibility(View.VISIBLE);
        viewHolder.images2.setVisibility(View.VISIBLE);
        viewHolder.images3.setVisibility(View.VISIBLE);

        if (imageUrls.size() <= 6) {
            viewHolder.images3.setVisibility(View.GONE);
        }
        if (imageUrls.size() <= 3) {
            viewHolder.images2.setVisibility(View.GONE);
        }
        if (imageUrls.size() == 0) {
            viewHolder.images1.setVisibility(View.GONE);
        }
        return convertView;
    }

    public static class ViewHolder {
        ImageView icon;
        TextView name;
        TextView time;
        TextView content;
        public ImageView like;
        public TextView likeNum;
        TextView commitNum;
        ImageView image1;
        ImageView image2;
        ImageView image3;
        ImageView image4;
        ImageView image5;
        ImageView image6;
        ImageView image7;
        ImageView image8;
        ImageView image9;
        LinearLayout images1;
        LinearLayout images2;
        LinearLayout images3;
    }

    public void setLikeListener(ViewHolder viewHolder,int position,Dynamic.DataEntity dataEntity){
        if (TextUtils.equals(BeanParam.Dynamic.onlike, dataEntity.getLiked())) {
            viewHolder.like.setImageResource(R.drawable.onlike);
            viewHolder.like.setOnClickListener(v -> {
                ImageView iv = (ImageView) v;
                iv.setClickable(false);
                MyApplication application = (MyApplication) ((Activity) context).getApplication();
                Login.DataEntity dataEntity1 = application.getLogin().getData();
                Map<String, String> map = new HashMap<String, String>();
                map.put(RequestParam.Like.Token, dataEntity1.getToken());
                map.put(RequestParam.Like.Type, RequestParam.Like.Cancel);
                map.put(RequestParam.Like.ForumNo, dataEntity.getForumNo());
                ApiService.getPileService().Like(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity1.getCustID(), NetParam.sign_method, RequestParam.Like.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(like -> {
                            if(TextUtils.equals(BeanParam.Failure,like.getIsSuccess())){
                                Error error = ErrorParamUtil.getInstance().checkReturnState(like.getReturnStatus());
                                ToastUtil.getInstance().toastError(context,error,null);
                                return null;
                            }
                            return like;
                        })
                        .subscribe(like -> {
                            iv.setClickable(true);
                            if(like == null){
                                return;
                            }
                            dataEntity.setLiked(BeanParam.Dynamic.like);
                            dataEntity.setLikes(changeNum(dataEntity.getLikes(),false));
                            FriendActivity friendActivity = (FriendActivity) context;
                            friendActivity.updateView(position,dataEntity,false);
                        });
            });
        } else {
            viewHolder.like.setImageResource(R.drawable.like);
            viewHolder.like.setOnClickListener(v -> {
                ImageView iv = (ImageView) v;
                iv.setClickable(false);
                MyApplication application = (MyApplication) ((Activity) context).getApplication();
                Login.DataEntity dataEntity1 = application.getLogin().getData();
                Map<String, String> map = new HashMap<String, String>();
                map.put(RequestParam.Like.Token, dataEntity1.getToken());
                map.put(RequestParam.Like.Type, RequestParam.Like.Like);
                map.put(RequestParam.Like.ForumNo, dataEntity.getForumNo());
                ApiService.getPileService().Like(NetParam.trancode, NetParam.mode, NetParam.getTime(), dataEntity1.getCustID(), NetParam.sign_method, RequestParam.Like.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(like -> {
                            if(TextUtils.equals(BeanParam.Failure,like.getIsSuccess())){
                                Error error = ErrorParamUtil.getInstance().checkReturnState(like.getReturnStatus());
                                ToastUtil.getInstance().toastError(context,error,null);
                                return null;
                            }
                            return like;
                        })
                        .subscribe(like -> {
                            iv.setClickable(true);
                            if(like == null){
                                return;
                            }
                            dataEntity.setLiked(BeanParam.Dynamic.onlike);
                            dataEntity.setLikes(changeNum(dataEntity.getLikes(),true));
                            FriendActivity friendActivity = (FriendActivity) context;
                            friendActivity.updateView(position,dataEntity,true);
                        });
            });
        }
    }

    public String changeNum(String numStr,boolean isAdd){
        LogUtil.v("num",numStr);
        int num = Integer.parseInt(numStr);
        if(isAdd){
            num ++;
        }else{
            num -- ;
        }
        return num + "";
    }

}
;
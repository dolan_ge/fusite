package com.fusite.adpater;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fusite.api.ApiService;
import com.fusite.bean.CompleteOrder;
import com.fusite.bean.DateSort;
import com.fusite.bean.GetServiceOrder;
import com.fusite.bean.Login;
import com.fusite.bean.RentOrders;
import com.fusite.handlerinterface.ThrowableAction;
import com.fusite.impl.CompleteOrderDaoImpl;
import com.fusite.impl.DeleteOrderDaoImpl;
import com.fusite.impl.DeleteRentOrderDaoImpl;
import com.fusite.impl.RentOrdersDaoImpl;
import com.fusite.param.BeanParam;
import com.fusite.param.NetParam;
import com.fusite.param.RequestParam;
import com.fusite.pile.R;
import com.fusite.utils.DateUtil;
import com.fusite.utils.DialogUtil;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 已完成订单
 * Created by Jelly on 2016/3/10.
 */
public class FinishOrderListAdapter extends BaseAdapter {
    /**
     * 数据集合
     */
    private List<DateSort> list;
    /**
     * Context
     */
    private WeakReference<Context> mContext;
    /**
     * DeleteOrderDaoImpl
     */
    private DeleteOrderDaoImpl deleteOrderDao;
    /**
     * 删除消息
     */
    private final int msgDelete = 0x001;

    private DeleteRentOrderDaoImpl deleteRentOrderDao;

    private final int msgDeleteRentOrder = 0x002;

    /**
     * 登录记录
     */
    private Login loginDao;
    /**
     * 当前需要删除的数据
     */
    private int deletePosition;
    /**
     * Dialog操作工具
     */
    private DialogUtil dialogUtil = DialogUtil.getInstance();

    private final String DateFormat = "yyyy-MM-dd HH:mm";

    private final String ChargeTitle = "电桩充电";

    private final String RentCarTitle = "租车";

    private final String ServiceTitle = "服务";

    public FinishOrderListAdapter(Context context, List<DateSort> list, Login loginDao) {
        mContext = new WeakReference<Context>(context);
        this.list = list;
        this.loginDao = loginDao;
        deleteOrderDao = new DeleteOrderDaoImpl(context, handler, msgDelete);
        deleteRentOrderDao = new DeleteRentOrderDaoImpl(context, handler, msgDeleteRentOrder);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView type_text;
        TextView position_text;
        TextView order_time;
        TextView EPId_text;
        TextView delete;
        TextView orderNo;
        ImageView orderState;
        RelativeLayout pickCarStationLayout;
        RelativeLayout returnCarStationLayout;
        RelativeLayout position_layout;
        RelativeLayout EPId_layout;
        TextView pickCarTime;
        TextView pickCarStation;
        TextView returnCarStation;
        RelativeLayout serviceTypeLayout;
        LinearLayout serviceStationLayout;
        TextView serviceTypeName;
        TextView serviceStation;
        TextView serviceTime;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        Context context = mContext.get();
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.finished_order_list_item, null);
            viewHolder.type_text = (TextView) convertView.findViewById(R.id.type_text);
            viewHolder.position_text = (TextView) convertView.findViewById(R.id.position_text);
            viewHolder.order_time = (TextView) convertView.findViewById(R.id.order_time);
            viewHolder.EPId_text = (TextView) convertView.findViewById(R.id.EPId_text);
            viewHolder.delete = (TextView) convertView.findViewById(R.id.delete);
            viewHolder.orderNo = (TextView) convertView.findViewById(R.id.orderNo);
            viewHolder.orderState = (ImageView) convertView.findViewById(R.id.orderState);
            viewHolder.pickCarStationLayout = (RelativeLayout) convertView.findViewById(R.id.pickCarStationLayout);
            viewHolder.returnCarStationLayout = (RelativeLayout) convertView.findViewById(R.id.returnCarStationLayout);
            viewHolder.position_layout = (RelativeLayout) convertView.findViewById(R.id.position_layout);
            viewHolder.EPId_layout = (RelativeLayout) convertView.findViewById(R.id.EPId_layout);
            viewHolder.pickCarStation = (TextView) convertView.findViewById(R.id.pickCarStation);
            viewHolder.returnCarStation = (TextView) convertView.findViewById(R.id.returnCarStation);
            viewHolder.pickCarTime = (TextView) convertView.findViewById(R.id.pickCarTime);
            viewHolder.serviceTypeLayout = (RelativeLayout) convertView.findViewById(R.id.serviceTypeLayout);
            viewHolder.serviceStationLayout = (LinearLayout) convertView.findViewById(R.id.serviceStationLayout);
            viewHolder.serviceTypeName = (TextView) convertView.findViewById(R.id.serviceTypeName);
            viewHolder.serviceStation = (TextView) convertView.findViewById(R.id.serviceStation);
            viewHolder.serviceTime = (TextView) convertView.findViewById(R.id.serviceTime);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (getItem(position) instanceof CompleteOrder) {
            viewHolder.type_text.setText(ChargeTitle);
            final CompleteOrder.EvcOrdersGetEntity data = ((CompleteOrder) getItem(position)).getEvc_orders_get();
            if (TextUtils.equals(data.getOrderStatus(), CompleteOrderDaoImpl.CompleteOrder)) {
                viewHolder.order_time.setText(DateUtil.getSdfDate(data.getRealBeginDateTime(), DateFormat));
                viewHolder.orderState.setImageResource(R.drawable.orderfinish);
            } else {
                viewHolder.order_time.setText(DateUtil.getSdfDate(data.getPlanBeginDateTime() == null ? data.getRealBeginDateTime() : data.getPlanBeginDateTime(), DateFormat));
                viewHolder.orderState.setImageResource(R.drawable.ordercancel);
            }
            viewHolder.position_text.setText(data.getOrgName());
            viewHolder.EPId_text.setText(data.getFacilityID());
            viewHolder.delete.setOnClickListener(v -> {
                dialogUtil.showSureListenerDialog(context, "是否确认删除订单！", (dialog, index) -> {
                    dialog.dismiss();
                    Login.DataEntity entity = loginDao.getData();
                    deletePosition = position;
                    deleteOrderDao.getDeleteOrderDao(entity.getToken(), data.getOrderNo(), entity.getCustID());
                });
            });
            viewHolder.orderNo.setText(data.getOrderNo());
            viewHolder.position_layout.setVisibility(View.VISIBLE);
            viewHolder.EPId_layout.setVisibility(View.VISIBLE);
            viewHolder.pickCarStationLayout.setVisibility(View.GONE);
            viewHolder.returnCarStationLayout.setVisibility(View.GONE);
            viewHolder.serviceTypeLayout.setVisibility(View.GONE);
            viewHolder.serviceStationLayout.setVisibility(View.GONE);
        } else if (getItem(position) instanceof RentOrders.DataEntity) {
            viewHolder.type_text.setText(RentCarTitle);
            RentOrders.DataEntity dataEntity = (RentOrders.DataEntity) getItem(position);
            viewHolder.pickCarStation.setText(dataEntity.getOrgName());
            viewHolder.returnCarStation.setText(dataEntity.getReturnOrgName());
            viewHolder.pickCarTime.setText(DateUtil.getSdfDate(dataEntity.getCreateDate(), DateFormat));
            viewHolder.orderNo.setText(dataEntity.getOrderNo());

            if (TextUtils.equals(RentOrdersDaoImpl.CancelRent, dataEntity.getOrderStatus())) {
                viewHolder.orderState.setImageResource(R.drawable.ordercancel);
            } else {
                viewHolder.orderState.setImageResource(R.drawable.orderfinish);
            }

            viewHolder.delete.setOnClickListener(v -> {
                dialogUtil.showSureListenerDialog(context, "是否确认删除订单！", (dialog, index) -> {
                    dialog.dismiss();
                    Login.DataEntity entity = loginDao.getData();
                    deletePosition = position;
                    deleteRentOrderDao.getDeleteRentOrder(entity.getToken(), entity.getCustID(), dataEntity.getOrderNo());
                });
            });

            viewHolder.pickCarStationLayout.setVisibility(View.VISIBLE);
            viewHolder.returnCarStationLayout.setVisibility(View.VISIBLE);
            viewHolder.position_layout.setVisibility(View.GONE);
            viewHolder.EPId_layout.setVisibility(View.GONE);
            viewHolder.serviceTypeLayout.setVisibility(View.GONE);
            viewHolder.serviceStationLayout.setVisibility(View.GONE);
        } else if (getItem(position) instanceof GetServiceOrder.DataEntity) {
            viewHolder.type_text.setText(ServiceTitle);
            GetServiceOrder.DataEntity serviceOrder = (GetServiceOrder.DataEntity) getItem(position);
            viewHolder.serviceStation.setText(serviceOrder.getOrgName());
            viewHolder.serviceTypeName.setText(serviceOrder.getServiceName());
            viewHolder.serviceTime.setText(serviceOrder.getPlanTime());
            viewHolder.orderNo.setText(serviceOrder.getOrderNo());
            if (TextUtils.equals(BeanParam.ServiceOrder.State8, serviceOrder.getOrderStatus())) {
                viewHolder.orderState.setImageResource(R.drawable.orderfinish);
            } else {
                viewHolder.orderState.setImageResource(R.drawable.ordercancel);
            }


            viewHolder.delete.setOnClickListener(v -> {
                dialogUtil.showSureListenerDialog(context, "是否确认删除订单！", (dialog, index) -> {
                    dialog.dismiss();
                    deletePosition = position;
                    Login.DataEntity loginData = loginDao.getData();
                    Map<String, String> map = new HashMap<String, String>();
                    map.put(RequestParam.ServicePay.Token, loginData.getToken());
                    map.put(RequestParam.ServicePay.OrderNo, serviceOrder.getOrderNo());
                    ApiService.getPileService().ServiceOrderDelete(NetParam.trancode, NetParam.mode, NetParam.getTime(), loginData.getCustID(), NetParam.sign_method, RequestParam.ServiceOrderDelete.InterfaceName, NetParam.sign, NetParam.fields, NetParam.spliceCondition(map))
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map(serviceOrderDelete -> {
                                if(TextUtils.equals(BeanParam.Failure,serviceOrderDelete.getIsSuccess())){
                                    return null;
                                }
                                return serviceOrderDelete;
                            })
                            .subscribe(serviceOrderDelete -> {
                                if(serviceOrderDelete == null){
                                    return;
                                }
                                list.remove(deletePosition);
                                notifyDataSetChanged();
                            }, new ThrowableAction(context));
                });
            });

            viewHolder.pickCarStationLayout.setVisibility(View.GONE);
            viewHolder.returnCarStationLayout.setVisibility(View.GONE);
            viewHolder.position_layout.setVisibility(View.GONE);
            viewHolder.EPId_layout.setVisibility(View.GONE);
            viewHolder.serviceTypeLayout.setVisibility(View.VISIBLE);
            viewHolder.serviceStationLayout.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Context context = mContext.get();
            switch (msg.what) {
                case msgDelete:
                    if (deleteOrderDao.deleteOrderDao == null || TextUtils.equals(deleteOrderDao.deleteOrderDao.getEvc_order_delete().getIsSuccess(), "N")) {
                        Toast.makeText(context, "对不起，删除订单失败！", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    list.remove(deletePosition);
                    notifyDataSetChanged();
                    break;
                case msgDeleteRentOrder:
                    if (TextUtils.equals("N", deleteRentOrderDao.deleteRentOrder.getIsSuccess())) {
                        Toast.makeText(context, "对不起，删除订单失败！", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    list.remove(deletePosition);
                    notifyDataSetChanged();
                    break;
            }
        }
    };

}

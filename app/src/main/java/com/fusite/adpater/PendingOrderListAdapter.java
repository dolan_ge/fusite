package com.fusite.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fusite.bean.PendingOrder;
import com.fusite.bean.RentOrders;
import com.fusite.impl.PendingOrderImpl;
import com.fusite.impl.RentOrdersDaoImpl;
import com.fusite.pile.R;
import com.fusite.utils.DateUtil;

import java.util.List;

/**
 * Created by Jelly on 2016/3/10.
 */
public class PendingOrderListAdapter extends BaseAdapter{

    private List<Object> list;

    private  Context context;

    private final String DateFormat = "yyyy-MM-dd HH:mm";

    private final String ChargingText = "充电中";

    private final String PayingText = "待支付";

    private final String ChargeOrderTitle = "电桩充电";

    private final String RentsOrderTitle = "租车";

    private final String PendPickCarTitle = "待取车";

    private final String CaringTitle = "使用中";

    private final String ReturnedCarTitle = "待结算";

    public PendingOrderListAdapter(Context context,List<Object> list) {
        this.list = list;
        this.context = context;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        TextView type_text;
        TextView state_text;
        TextView position_text;
        TextView order_time;
        TextView EPId_text;
        TextView orderNo;
        TextView pickCarStation;
        TextView returnCarStation;
        TextView pickCarTime;
        RelativeLayout position_layout;
        RelativeLayout EPId_layout;
        RelativeLayout pickCarStationLayout;
        RelativeLayout returnCarStationLayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.pending_order_list_item,null);
            viewHolder.type_text = (TextView) convertView.findViewById(R.id.type_text);
            viewHolder.state_text = (TextView) convertView.findViewById(R.id.state_text);
            viewHolder.position_text = (TextView) convertView.findViewById(R.id.position_text);
            viewHolder.order_time = (TextView) convertView.findViewById(R.id.order_time);
            viewHolder.EPId_text = (TextView) convertView.findViewById(R.id.EPId_text);
            viewHolder.orderNo = (TextView) convertView.findViewById(R.id.orderNo);
            viewHolder.pickCarStation = (TextView) convertView.findViewById(R.id.pickCarStation);
            viewHolder.returnCarStation = (TextView) convertView.findViewById(R.id.returnCarStation);
            viewHolder.pickCarTime = (TextView) convertView.findViewById(R.id.pickCarTime);
            viewHolder.position_layout = (RelativeLayout) convertView.findViewById(R.id.position_layout);
            viewHolder.EPId_layout = (RelativeLayout) convertView.findViewById(R.id.EPId_layout);
            viewHolder.pickCarStationLayout = (RelativeLayout) convertView.findViewById(R.id.pickCarStationLayout);
            viewHolder.returnCarStationLayout = (RelativeLayout) convertView.findViewById(R.id.returnCarStationLayout);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Object object = getItem(position);
        if(object instanceof PendingOrder){
            viewHolder.type_text.setText(ChargeOrderTitle);

            PendingOrder.EvcOrdersGetEntity data = ((PendingOrder) object).getEvc_orders_get();
            if(TextUtils.equals(data.getOrderStatus(), PendingOrderImpl.CHARGING)){
                viewHolder.state_text.setText(ChargingText);
            }else if(TextUtils.equals(data.getOrderStatus(),PendingOrderImpl.PAYING)){
                viewHolder.state_text.setText(PayingText);
            }
            viewHolder.position_text.setText(data.getOrgName());
            if(!TextUtils.isEmpty(data.getRealBeginDateTime())){
                viewHolder.order_time.setText(DateUtil.getSdfDate(data.getRealBeginDateTime(),DateFormat));
            }
            viewHolder.EPId_text.setText(data.getOrgID());
            viewHolder.orderNo.setText(data.getOrderNo());
            viewHolder.pickCarStationLayout.setVisibility(View.GONE);
            viewHolder.returnCarStationLayout.setVisibility(View.GONE);
        }else if(object instanceof RentOrders.DataEntity){
            viewHolder.type_text.setText(RentsOrderTitle);

            RentOrders.DataEntity dataEntity = (RentOrders.DataEntity) object;
            //1:待取车 2：使用中 3：已还车
            switch (dataEntity.getOrderStatus()){
                case RentOrdersDaoImpl.PendPickCar:
                    viewHolder.state_text.setText(PendPickCarTitle);
                    break;
                case RentOrdersDaoImpl.Caring:
                    viewHolder.state_text.setText(CaringTitle);
                    break;
                case RentOrdersDaoImpl.ReturnedCar:
                    viewHolder.state_text.setText(ReturnedCarTitle);
                    break;
            }

            viewHolder.pickCarStation.setText(dataEntity.getOrgName());
            viewHolder.returnCarStation.setText(dataEntity.getReturnOrgName());
            viewHolder.pickCarTime.setText(DateUtil.getSdfDate(dataEntity.getCreateDate(),DateFormat));
            viewHolder.orderNo.setText(dataEntity.getOrderNo());

            viewHolder.position_layout.setVisibility(View.GONE);
            viewHolder.EPId_layout.setVisibility(View.GONE);
        }
        return convertView;
    }

    /**
     * 重新加载数据
     * @param list
     */
    public void reloadList(List<Object> list){
        this.list = list;
        this.notifyDataSetChanged();
    }
}

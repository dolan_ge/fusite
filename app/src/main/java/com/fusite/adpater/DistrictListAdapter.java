package com.fusite.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fusite.bean.NearbyRentStation;
import com.fusite.pile.R;
import com.fusite.utils.ResourceUtil;

import java.util.List;

/**
 * Created by Jelly on 2016/4/18.
 */
public class DistrictListAdapter extends BaseAdapter{

    public List<NearbyRentStation.DataEntity> list;

    private Context context;

    private int selectItem = 0;

    public DistrictListAdapter(List<NearbyRentStation.DataEntity> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void clearList(){
        list.clear();
    }

    public void setSelectItem(int selectItem) {
        this.selectItem = selectItem;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = LayoutInflater.from(context).inflate(R.layout.district_list_item,null);
        if(position == selectItem){
            item.setBackgroundColor(ResourceUtil.getInstance().getResourceColor(context,R.color.white_bg));
        }
        TextView district = (TextView) item.findViewById(R.id.district);
        district.setText(list.get(position).getDistrict());
        return item;
    }
}

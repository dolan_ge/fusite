package com.fusite.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fusite.bean.Brands;
import com.fusite.pile.R;

import java.util.List;

/**
 * Created by Jelly on 2016/8/16.
 */
public class SelectCarTypeAdapter extends BaseAdapter{

    private List<Brands.DataEntity> dataList;
    private Context context;

    public SelectCarTypeAdapter(Context context, List<Brands.DataEntity> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if(convertView ==  null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_select_car_type,parent,false);
            viewHolder.carType = (TextView) convertView.findViewById(R.id.car_type);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.carType.setText(dataList.get(position).getBrand());

        return convertView;
    }

    private class ViewHolder {
        TextView carType;
    }


}

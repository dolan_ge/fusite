package com.fusite.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusite.bean.Screen;
import com.fusite.pile.R;

import java.util.List;

/**
 * Created by Jelly on 2016/4/20.
 */
public class RentScreenListAdapter extends BaseAdapter{

    private List<Screen> list;

    private int selectRadio = -1;

    private Context context;

    public RentScreenListAdapter(Context context, List<Screen> list){
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = LayoutInflater.from(context).inflate(R.layout.car_screen_item,null);
        TextView title = (TextView) item.findViewById(R.id.title);
        ImageView radiobtn = (ImageView) item.findViewById(R.id.radiobtn);
        Screen screen = (Screen) getItem(position);
        title.setText(screen.getTitle());
        if(selectRadio == -1 && screen.isSelect){
            radiobtn.setImageResource(R.drawable.radiobtn1);
        }else{
            radiobtn.setImageResource(R.drawable.radiobtn0);
        }
        if(selectRadio == position){
            radiobtn.setImageResource(R.drawable.radiobtn1);
        }
        return item;
    }
    /**
     * 选择单选按钮
     */
    public void selectRadio(int position){
        selectRadio = position;
    }
}

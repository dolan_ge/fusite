package com.fusite.adpater;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.fusite.bean.NearbyRentStation;
import com.fusite.map.MyLocation;
import com.fusite.pile.MyApplication;
import com.fusite.pile.R;
import com.fusite.utils.FloatUtil;

import java.util.List;

/**
 * Created by Jelly on 2016/4/18.
 */
public class RentStationListAdapter extends BaseAdapter{

    private List<NearbyRentStation.DataEntity.StationsEntity> list;

    private Context context;

    public RentStationListAdapter(List<NearbyRentStation.DataEntity.StationsEntity> list, Context context){
        this.list = list;
        this.context = context;
    }

    public void setList(List<NearbyRentStation.DataEntity.StationsEntity> list) {
        this.list = list;
    }

    public void clearList(){
        list.clear();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = LayoutInflater.from(context).inflate(R.layout.rent_station_list_item,null);
        TextView orgName = (TextView) item.findViewById(R.id.orgName);
        TextView distance_text = (TextView) item.findViewById(R.id.distance_text);
        TextView addr = (TextView) item.findViewById(R.id.addr);
        NearbyRentStation.DataEntity.StationsEntity dataEntity = list.get(position);
        orgName.setText(dataEntity.getOrgName());
        addr.setText(dataEntity.getAddr());
        LatLng latLng1 = new LatLng(Double.parseDouble(dataEntity.getLatitude()),Double.parseDouble(dataEntity.getLongitude()));
        Activity activity = (Activity) context;
        MyApplication application = (MyApplication) activity.getApplication();
        MyLocation location = application.getLocation();
        LatLng latLng2 = new LatLng(Double.parseDouble(location.getLatitude()),Double.parseDouble(location.getLongitude()));
        distance_text.setText(FloatUtil.mToKm(AMapUtils.calculateLineDistance(latLng1,latLng2))+"km");
        return item;
    }
}

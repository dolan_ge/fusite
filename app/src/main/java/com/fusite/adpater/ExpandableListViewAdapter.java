package com.fusite.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fusite.bean.BooleanBean;
import com.fusite.bean.Invoice;
import com.fusite.param.BeanParam;
import com.fusite.pile.R;
import com.fusite.utils.DateUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jelly on 2016/6/23.
 */
public class ExpandableListViewAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> groupList;
    private List<List<Invoice.DataEntity>> childList;
    private List<List<BooleanBean>> isList;
    private static final int type1 = 0; //充电
    private static final int type2 = 1; //租车
    private static final int type3 = 2; //服务

    private String DateFormat = "MM-dd HH:mm";

    public ExpandableListViewAdapter(Context context, List<String> groupList, List<List<Invoice.DataEntity>> childList,List<List<BooleanBean>> isList) {
        this.context = context;
        this.groupList = groupList;
        this.childList = childList;
        this.isList = isList;
    }

    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childList.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childList.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder groupViewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.invoicing_item1, parent, false);
            groupViewHolder = new GroupViewHolder(convertView);
            convertView.setTag(groupViewHolder);
        }else{
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }

        groupViewHolder.month.setText(DateUtil.getChineseMonth(groupPosition >= groupList.size() ? "-1" : groupList.get(groupPosition)));
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder childViewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.invoicing_item2, parent, false);
            childViewHolder = new ChildViewHolder(convertView);
            convertView.setTag(childViewHolder);
        } else {
            childViewHolder = (ChildViewHolder) convertView.getTag();
        }

        Invoice.DataEntity invoice = (Invoice.DataEntity) getChild(groupPosition,childPosition);



        int type = getChildType(groupPosition, childPosition);
        switch (type) {
            case type1:
                childViewHolder.time.setText(DateUtil.getSdfDate(invoice.getCreateDate(),DateFormat) + " - " + DateUtil.getSdfDate(invoice.getEndtime(),DateFormat));
                childViewHolder.title.setText("充电点");
                childViewHolder.content.setText(invoice.getOrgName());
                childViewHolder.money.setText("¥"+invoice.getAmt());
                break;
            case type2:
                childViewHolder.time.setText(DateUtil.getSdfDate(invoice.getCreateDate(),DateFormat) + " - " + DateUtil.getSdfDate(invoice.getEndtime(),DateFormat));
                childViewHolder.title.setText("租车点");
                childViewHolder.content.setText(invoice.getOrgName());
                childViewHolder.money.setText("¥"+invoice.getAmt());
                break;
            case type3:
                childViewHolder.time.setText(DateUtil.getSdfDate(invoice.getCreateDate(),DateFormat) + " - " + DateUtil.getSdfDate(invoice.getEndtime(),DateFormat));
                childViewHolder.title.setText("服务点");
                childViewHolder.content.setText(invoice.getOrgName());
                childViewHolder.money.setText("¥"+invoice.getAmt());
                break;
        }

        if(isList.get(groupPosition).get(childPosition).isFlag()){
            childViewHolder.selectIcon.setImageResource(R.drawable.invoice_select);
        }else{
            childViewHolder.selectIcon.setImageResource(R.drawable.invoice_no_select);
        }

        return convertView;
    }

    @Override
    public int getChildTypeCount(){
        return 3;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        Invoice.DataEntity invoice = childList.get(groupPosition).get(childPosition);
        int type = -1;
        final String evc = BeanParam.Invoice.EVC;
        final String evr = BeanParam.Invoice.EVR;
        final String evm = BeanParam.Invoice.EVM;
        switch (invoice.getOrderType()) {
            case evc:
                type = type1;
                break;
            case evr:
                type = type2;
                break;
            case evm:
                type = type3;
                break;
        }
        return type;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    static class ChildViewHolder {
        @BindView(R.id.selectIcon)
        ImageView selectIcon;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.content)
        TextView content;
        @BindView(R.id.money)
        TextView money;

        ChildViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class GroupViewHolder {
        @BindView(R.id.month)
        TextView month;

        GroupViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

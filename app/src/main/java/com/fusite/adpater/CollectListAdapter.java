package com.fusite.adpater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fusite.bean.CollectList;
import com.fusite.param.BeanParam;
import com.fusite.pile.R;

import java.util.List;

/**
 * Created by Jelly on 2016/6/3.
 */
public class CollectListAdapter extends BaseAdapter{
    private Context context;
    private List<CollectList.DataEntity> dataList;

    public CollectListAdapter(Context context,List<CollectList.DataEntity> dataList){
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.collect_item,parent,false);
            viewHolder.orgName = (TextView) convertView.findViewById(R.id.orgName);
            viewHolder.addr = (TextView) convertView.findViewById(R.id.addr);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CollectList.DataEntity dataEntity = (CollectList.DataEntity) getItem(position);
        viewHolder.orgName.setText(dataEntity.getOrgName() + (TextUtils.equals(BeanParam.CollectList.EVC,dataEntity.getOrgClasses())?"(充电点)":"(租车点)"));
        viewHolder.addr.setText(dataEntity.getAddr());
        return convertView;
    }

    private class ViewHolder{
        TextView orgName;
        TextView addr;
    }
}

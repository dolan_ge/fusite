package com.jelly.bean;

/**
 * Created by Jelly on 2016/5/1.
 */
public class TextXml {
    public String name1;
    public String name2;
    public String name3;

    public TextXml() {
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getName3() {
        return name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }
}

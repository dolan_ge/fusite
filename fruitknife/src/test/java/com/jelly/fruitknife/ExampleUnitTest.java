package com.jelly.fruitknife;

import android.util.Log;

import com.fruitknife.bean.BasicNameValuePair;
import com.fruitknife.util.ArrayUtil;
import com.fruitknife.util.XmlResolve;
import com.jelly.bean.TextXml;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testArrayUtil(){
        boolean flag = false;
        BasicNameValuePair[] pairs = new BasicNameValuePair[3];
        pairs[0] = new BasicNameValuePair("name0","value0");
        pairs[1] = new BasicNameValuePair("name1","value1");
        pairs[2] = new BasicNameValuePair("name2","value2");

        ArrayUtil<BasicNameValuePair> arrayUtil = new ArrayUtil<>();
        arrayUtil.setArray(pairs);
        try {
            flag = arrayUtil.contain(new BasicNameValuePair("name0","value0"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(flag,false);
    }

    @Test
    public void testXmlResolve(){
        Log.v("1","1");
        XmlResolve xmlResolve = XmlResolve.getInstance();
        TextXml xml = xmlResolve.xmlToObject("<xml><name1>1</name1><name2>2</name2><name3>3</name3></xml>", TextXml.class);
        assertEquals("1",xml.getName1());
        assertEquals("2",xml.getName2());
        assertEquals("3",xml.getName3());
    }


}
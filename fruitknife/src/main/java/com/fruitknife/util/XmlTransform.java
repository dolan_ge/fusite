package com.fruitknife.util;

import com.fruitknife.bean.BasicNameValuePair;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Xml转换器
 * Created by Jelly on 2016/5/1.
 */
public class XmlTransform {
    /**
     * 静态内部类，线程安全
     */
    private static class InstanceHolder{
        static final XmlTransform xmlTransform = new XmlTransform();
    }

    /**
     * 获得单例对象
     * @return
     */
    public static XmlTransform getInstance(){
        return InstanceHolder.xmlTransform;
    }

    /**
     * 通过List获得xml
     * @param list
     * @return
     */
    public String mapToXml(List<BasicNameValuePair> list){
        String xml = "<xml>";
        for (BasicNameValuePair pair : list) {
            xml += "<" + pair.getName() + ">" + pair.getValue() + "</" + pair.getName() + ">";
        }
        xml += "</xml>";
        return xml;
    }

    /**
     * 带根目录的通过List生成xml
     * @param root
     * @param list
     * @return
     */
    public String mapToXml(String root,List<BasicNameValuePair> list){
        String xml = "<" + root + ">";
        for (BasicNameValuePair pair : list) {
            xml += "<" + pair.getName() + ">" + pair.getValue() + "</" + pair.getName() + ">";
        }
        xml += "</"+root+">";
        return xml;
    }

    public <T> String objectToXml(T t) throws Exception{
        String xml = "<xml>";
        Field[] fields = t.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            if(!fields[i].isSynthetic()){
                xml += "<" + fields[i].getName() + ">";
                xml += fields[i].get(t);
                xml += "</" + fields[i].getName() + ">";
            }
        }
        xml += "</xml>";
        return xml;
    }

    public <T> String objectToXml(String root,T t) throws Exception{
        String xml = "<" + root + ">";
        Field[] fields = t.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            if(!fields[i].isSynthetic()){
                xml += "<" + fields[i].getName() + ">";
                xml += fields[i].get(t);
                xml += "</" + fields[i].getName() + ">";
            }
        }
        xml += "</" + root + ">";
        return xml;
    }

}


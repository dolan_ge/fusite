package com.fruitknife.util;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;

/**
 * XML解析器
 * Created by Jelly on 2016/5/1.
 */
public class XmlResolve {

    private static class InstanceHolder{
        static final XmlResolve xmlResolve =  new XmlResolve();
    }


    public static XmlResolve getInstance(){
        return InstanceHolder.xmlResolve;
    }

    public <T> T xmlToObject(String xml,Class<T> tClass){
        T t = null;
        try {
            t = tClass.newInstance();
            Field[] fields = tClass.getDeclaredFields();
            ArrayUtil<String> arrayUtil = new ArrayUtil<>();
            String[] fieldNames = new String[fields.length];
            for (int i = 0; i < fields.length; i++) {
                fieldNames[i] = fields[i].getName();
            }
            arrayUtil.setArray(fieldNames);
            XmlPullParser xmlPullParser = XmlPullParserFactory.newInstance().newPullParser();
            xmlPullParser.setInput(new ByteArrayInputStream(xml.getBytes()),"UTF-8");
            int eventType = xmlPullParser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT){
                String tagName = xmlPullParser.getName();
                if(eventType == XmlPullParser.START_TAG){
                    Log.v("tag",tagName);
                    if(arrayUtil.contain(tagName)){
                        Field field = fields[arrayUtil.getIndex(tagName)];
                        field.setAccessible(true);
                        field.set(t,xmlPullParser.nextText());
                    }
                }
                eventType = xmlPullParser.next();
            }
            return t;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }



}

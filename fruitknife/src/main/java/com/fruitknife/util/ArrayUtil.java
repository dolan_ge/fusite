package com.fruitknife.util;

/**
 * Created by Jelly on 2016/5/1.
 */
public  class ArrayUtil<T> {

    private  T[] array;

    public   void setArray(T[] array){
        this.array = array;
    }

    public boolean contain(T t) throws Exception{
        if(array == null){
            throw new Exception("请先设置数组");
        }
        for (T temp:array) {
            if(temp.equals(t)){
                return true;
            }
        }
        return false;
    }


    public int getIndex(T t) throws Exception{
        if(array == null){
            throw new Exception("请先设置数组");
        }
        for (int i = 0; i < array.length; i++) {
            if(array[i].equals(t)){
                return i;
            }
        }
        return -1;
    }

}

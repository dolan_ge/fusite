package com.fruitknife.bean;

/**
 * Created by Jelly on 2016/4/30.
 */
public class BasicNameValuePair {
    public String name;
    public String value;

    public BasicNameValuePair(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
